# Dokumentasi Web Monitoring 62

## Log Perubahan

1. 31 Desember 2022

   Jangan lupa drop database pkl62_monitoring. trus di import lagi sqlsql.sql yang ada di project ini. jangan lupa jalankan perintah composer update ygy.

2. 4 Januari 2023

   Ditambahkan fitur template Sweetalert. Jika ingin menggunakan template Sweetalert, jangan lupa untuk melakukan
   include pada view yang ingin menggunakan template Sweetalert, contoh:
   `<?= $this->include('layout/sweetalert') ?>` . Jika sudah silahkan atur pada Controller yaitu terdapat 2
   pilih key yaitu sukses dan gagal. Kemudian isikan pesan yang ingin ditampilkan.
   Contoh: `session()->setFlashdata('sukses', 'Password berhasil diubah');`

3. 11 Januari 2023

   Ditambahkan datatables basic. jika ingin menggunakannya import script dengan src `/assets/js/scripts/tables/jquery.dataTables.min.js`.
   silahkan lihat file riset1_listing pada view untuk contoh pemakaian.

## Instalasi

1. Clone repository ini ke dalam folder htdocs atau folder lainnya yang sesuai dengan konfigurasi web server anda. Cara
   clone repository `git clone https://git.stis.ac.id/fatihm/monitoring-pkl-62.git`. Pastikan git sudah terinstall di
   komputer anda. Unduh git [disini](https://git-scm.com/downloads).
2. Cara clone spesifik repository `git clone -b mybranch --single-branch git://sub.domain.com/repo.git`. Contoh jika ingin
   clone branch master `git clone -b master --single-branch https://git.stis.ac.id/fatihm/monitoring-pkl-62.git`
3. Jalankan perintah `composer install` pada folder proyek untuk menginstall library yang dibutuhkan. Pastikan composer
   sudah terinstall di komputer anda. Unduh composer [disini](https://getcomposer.org/download/).
4. Jangan lupa membuat file `.env` dan mengisi konfigurasi database anda. Contoh file `.env` dapat dilihat pada
   file `env.example`.
5. Jalankan perintah `php spark serve` untuk menjalankan server lokal. Buka browser dan akses `localhost:8080` untuk
   melihat hasilnya.

## Kolaborasi

1. Jangan lupa melakukan `git pull` sebelum melakukan perubahan pada proyek.
2. Jangan lupa melakukan `git add .` dan `git commit -m "pesan"` sebelum melakukan `git push`.
3. Jangan lupa melakukan `git push` setelah melakukan perubahan pada proyek.
4. Hal diatas akan lebih mudah dilakukan jika menggunakan Visual Studio Code. Unduh Visual Studio
   Code [disini](https://code.visualstudio.com/download).
5. Proyek ini berbasis framework CodeIgniter 4. Dokumentasi CodeIgniter 4 dapat
   dilihat [disini](https://codeigniter4.github.io/userguide/).
6. Karena menggunakan framework CodeIgniter 4 yang berbasis MVC (Model View Controller), maka struktur folder proyek ini
   juga berbasis MVC. Dokumentasi struktur folder dapat
   dilihat [disini](https://codeigniter4.github.io/userguide/concepts/structure.html).

## Dokumentasi Template Vuexy

Pelajari lebih lanjut mengenai template vuexy di [disini](https://pixinvent.com/demo/vuexy-html-bootstrap-admin-template/documentation/index.html).

## Milestone 1 : Customize CSS For Better UI

(new 9-11-2022 v1)
A. Untuk mempermudah pencarian, cari keyword "TODO" menggunakan ctrl+f pada file.
B. Definisi file

- file 1 : `public/assets/css/components.min.css`
- file 2 : `public/assets/css/bootstrap-extended.min.css`

1. Untuk mengubah warna background sidebar, masuk ke file ` file 1, cari TODO 1`.
2. Untuk mengubah warna text sidebar, masuk ke file `file 1, cari TODO 2`.
3. Untuk mengubah warna navbar saat "RISET" terpilih, masuk file `file 1, cari TODO 3`.
4. Untuk mengubah warna sidebar yang sedang aktif, masuk ke file `file 1, cari TODO 4`.
5. Untuk mengubah warna nested sidebar yang sedang aktif, masuk ke file `file 1, cari TODO 5`.
6. Untuk mengubah warna logo pojok kiri atas, masuk ke file `file 1, cari TODO 6`.
7. Untuk mengubah warna topbar, masuk ke file `file 2, cari TODO 7`.

(new 9-11-2022 v2) 8. Untuk ubah warna tompel sidebar, masuk ke file `file 1, cari TODO 8`

(new 9-11-2022 v3)
Kumpulkan dengan cara push ke branch masing-masing. caranya :

1. Clone repository `git clone -b master --single-branch https://git.stis.ac.id/fatihm/monitoring-pkl-62.git`
2. Jalankan perintah `composer install` pada folder proyek
3. Jalankan perintah `git add .` lalu `git commit -m "<pesan kamu>"` untuk memasukkan file ke git
4. Jalankan perintah `git checkout -b <nama>` untuk buat branch baru, misalnya `git checkout -b tatan`
5. Jalankan perintah `git push -u  origin <nama>` untuk push ke branch baru, misalnya `git push -u  origin tatan`
