<?php

namespace Config;

use App\Filters\CekLogin;
use App\Filters\Riset1Filter;
use App\Filters\Riset2Filter;
use App\Filters\Riset3Filter;
use App\Filters\Riset4Filter;
use CodeIgniter\Config\BaseConfig;
use CodeIgniter\Filters\CSRF;
use CodeIgniter\Filters\DebugToolbar;
use CodeIgniter\Filters\Honeypot;
use CodeIgniter\Filters\InvalidChars;
use CodeIgniter\Filters\SecureHeaders;

class Filters extends BaseConfig
{
    /**
     * Configures aliases for Filter classes to
     * make reading things nicer and simpler.
     *
     * @var array
     */
    public $aliases = [
        'csrf'          => CSRF::class,
        'toolbar'       => DebugToolbar::class,
        'honeypot'      => Honeypot::class,
        'invalidchars'  => InvalidChars::class,
        'secureheaders' => SecureHeaders::class,
        'loginRedirect' => CekLogin::class,
        'Riset1' =>   Riset1Filter::class,
        'Riset2' =>   Riset2Filter::class,
        'Riset3' =>   Riset3Filter::class,
        'Riset4' =>   Riset4Filter::class,
    ];

    /**
     * List of filter aliases that are always
     * applied before and after every request.
     *
     * @var array
     */
    public $globals = [
        'before' => [
            // 'honeypot',
            // 'csrf',
            // 'invalidchars',
        ],
        'after' => [
            'toolbar',
            // 'honeypot',
            // 'secureheaders',
        ],
    ];

    /**
     * List of filter aliases that works on a
     * particular HTTP method (GET, POST, etc.).
     *
     * Example:
     * 'post' => ['foo', 'bar']
     *
     * If you use this, you should disable auto-routing because auto-routing
     * permits any HTTP method to access a controller. Accessing the controller
     * with a method you don’t expect could bypass the filter.
     *
     * @var array
     */
    public $methods = [];

    /**
     * List of filter aliases that should run on any
     * before or after URI patterns.
     *
     * Example:
     * 'isLoggedIn' => ['before' => ['account/*', 'profiles/*']]
     *
     * @var array
     */
    public $filters = [
        'loginRedirect' => [    //check, cant access without login. menyesuaian dengan routes yang ada
            'before' => [
                '/',
                'profil',
                'profil/*',
                'riset-1/*',
                'riset-1-2/*',
                'riset-3/*',
                'riset-4/*',
                'monitoring-pcl',
                'monitoring-pcl/*',
                'pusat-bantuan/*'
            ]
        ],

        'Riset1' => [    //check, cant access without login. menyesuaian dengan routes yang ada
            'before' => [
                'riset-1/listing/*',
                'riset-1/sampel/*',

                'riset-1/listing',
                'riset-1/sampel',

            ]
        ],

        'Riset2' => [    //check, cant access without login. menyesuaian dengan routes yang ada
            'before' => [
                'riset-1-2/listing/*',
                'riset-1-2/sampel/*',

                'riset-1-2/listing',
                'riset-1-2/sampel',
            ]
        ],

        'Riset3' => [    //check, cant access without login. menyesuaian dengan routes yang ada
            'before' => [
                'riset-3/listing/*',
                'riset-3/sampel/*',

                'riset-3/listing',
                'riset-3/sampel',
            ]
        ],

        'Riset4' => [    //check, cant access without login. menyesuaian dengan routes yang ada
            'before' => [
                'riset-4/listing/*',
                'riset-4/sampel/*',

                'riset-4/listing',
                'riset-4/sampel',
            ]
        ],

    ];
}
