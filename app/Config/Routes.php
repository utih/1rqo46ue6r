<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (is_file(SYSTEMPATH . 'Config/Routes.php')) {
    require SYSTEMPATH . 'Config/Routes.php';
}

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
// The Auto Routing (Legacy) is very dangerous. It is easy to create vulnerable apps
// where controller filters or CSRF protection are bypassed.
// If you don't want to define all routes, please use the Auto Routing (Improved).
// Set `$autoRoutesImproved` to true in `app/Config/Feature.php` and set the following to true.
// $routes->setAutoRoute(false);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.

// ROUTES DEBUG. DELETE SOON
$routes->get('debug', 'Dashboard::index');

// ROUTES DASHBOARD : DONE
$routes->get('/', 'Dashboard::index');

// ROUTES AUTH : DONE
$routes->get('login', 'Login::index');
$routes->get('logout', 'Login::logout');
$routes->post('nim_login', 'Login::nim_login');

// ROUTES PROFIL
$routes->group('profil', function ($routes) {
    $routes->get('/', 'User::index');
    $routes->get('ubah-password', 'User::changePassword');
    $routes->post('save-password', 'User::savePassword');
});

// ROUTES FOR RISET 1
$routes->group('riset-1', function ($routes) {
    // $routes->get('listing', 'Riset\Riset1\Riset1Listing::index');
    // $routes->get('listing/detail/(:segment)', 'Riset\Riset1\Riset1Listing::progressListingDetail/$1');
    // $routes->get('listing/detail/(:segment)/(:segment)', 'Riset\Riset1\Riset1Listing::progressListingDetail/$1/$2');

    $routes->get('sampel', 'Riset\Riset1\Riset1Sampel::index');
    $routes->get('sampel/detail/(:segment)', 'Riset\Riset1\Riset1Sampel::progressSampelDetail/$1');
    $routes->get('sampel/detail/(:segment)/(:segment)', 'Riset\Riset1\Riset1Sampel::progressSampelDetail/$1/$2');

    $routes->get('monitoring-tim', 'Riset\Riset1\Riset1MonitoringTim::index');
    $routes->get('monitoring-tim-sampel/(:num)', 'Riset\Riset1\Riset1MonitoringTim::ajaxGetIdTim/$1');

    $routes->get('progress-wilayah', 'Riset\Riset1\Riset1ProgressWilayah::index');
    $routes->post('debug-send', 'Riset\Riset1\Riset1ProgressWilayah::debug');
    $routes->post('debug-tabel', 'Riset\Riset1\Riset1ProgressWilayah::tabel');
    $routes->post('debug-progress', 'Riset\Riset1\Riset1ProgressWilayah::progress');

    $routes->get('progress-tim', 'Riset\Riset1\Riset1ProgressTim::index');
    $routes->get('progress-tim-sampel/(:num)', 'Riset\Riset1\Riset1ProgressTim::ajaxGetIdTimSampel/$1');   

});

// ROUTES FOR RISET 2
$routes->group('riset-1-2', function ($routes) {
    $routes->get('listing', 'Riset\Riset12\Riset12Listing::index');
    $routes->get('listing/detail/(:segment)', 'Riset\Riset12\Riset12Listing::progressListingDetail/$1');
    $routes->get('listing/detail/(:segment)/(:segment)', 'Riset\Riset12\Riset12Listing::progressListingDetail/$1/$2');

    $routes->get('sampel', 'Riset\Riset12\Riset12Sampel::index');
    $routes->get('sampel/(:segment)/detail/(:segment)', 'Riset\Riset12\Riset12Sampel::progressSampelDetail/$1/$2');
    $routes->get('sampel/(:segment)/detail/(:segment)/(:segment)', 'Riset\Riset12\Riset12Sampel::progressSampelDetail/$1/$2/$3');
    $routes->get('sampel/detail/(:segment)', 'Riset\Riset12\Riset12Sampel::progressSampelDetailDummy/$1');
    $routes->get('sampel/detail/(:segment)/(:segment)', 'Riset\Riset12\Riset12Sampel::progressSampelDetailDummy/$1/$2');

    $routes->get('monitoring-tim', 'Riset\Riset12\Riset12MonitoringTim::index');
    $routes->get('monitoring-tim-listing/(:num)', 'Riset\Riset12\Riset12MonitoringTim::ajaxGetIdTim/$1');
    $routes->get('monitoring-tim-sampel/(:num)', 'Riset\Riset12\Riset12MonitoringTim::ajaxGetIdTimSampel/$1');

    $routes->get('progress-wilayah', 'Riset\Riset12\Riset12ProgressWilayah::index');
    $routes->post('debug-send', 'Riset\Riset12\Riset12ProgressWilayah::debug');
    $routes->post('debug-tabel', 'Riset\Riset12\Riset12ProgressWilayah::tabel');
    $routes->post('debug-progress', 'Riset\Riset12\Riset12ProgressWilayah::progress');

    $routes->get('progress-tim', 'Riset\Riset12\Riset12ProgressTim::index');
    $routes->get('progress-tim-sampel/(:num)', 'Riset\Riset12\Riset12ProgressTim::ajaxGetIdTimSampel/$1');    

});

// ROUTES FOR RISET 3
$routes->group('riset-3', function ($routes) {
    $routes->get('listing', 'Riset\Riset3\Riset3Listing::index');
    $routes->get('listing/detail/(:segment)', 'Riset\Riset3\Riset3Listing::progressListingDetail/$1');
    $routes->get('listing/detail/(:segment)/(:segment)', 'Riset\Riset3\Riset3Listing::progressListingDetail/$1/$2');

    $routes->get('sampel', 'Riset\Riset3\Riset3Sampel::index');
    $routes->get('sampel/detail/(:segment)', 'Riset\Riset3\Riset3Sampel::progressSampelDetail/$1');
    $routes->get('sampel/detail/(:segment)/(:segment)', 'Riset\Riset3\Riset3Sampel::progressSampelDetail/$1/$2');

    $routes->get('monitoring-tim', 'Riset\Riset3\Riset3MonitoringTim::index');
    $routes->get('monitoring-tim-listing/(:num)', 'Riset\Riset3\Riset3MonitoringTim::ajaxGetIdTim/$1');
    $routes->get('monitoring-tim-sampel/(:num)', 'Riset\Riset3\Riset3MonitoringTim::ajaxGetIdTimSampel/$1');

    $routes->get('progress-wilayah', 'Riset\Riset3\Riset3ProgressWilayah::index');
    $routes->post('debug-send', 'Riset\Riset3\Riset3ProgressWilayah::debug');
    $routes->post('debug-tabel', 'Riset\Riset3\Riset3ProgressWilayah::tabel');
    $routes->post('debug-progress', 'Riset\Riset3\Riset3ProgressWilayah::progress');

    $routes->get('progress-tim', 'Riset\Riset3\Riset3ProgressTim::index');
    $routes->get('progress-tim-sampel/(:num)', 'Riset\Riset3\Riset3ProgressTim::ajaxGetIdTimSampel/$1');   
});

// ROUTES FOR RISET 4
$routes->group('riset-4', function ($routes) {
    $routes->get('listing', 'Riset\Riset4\Riset4Listing::index');
    $routes->get('listing/detail/(:segment)', 'Riset\Riset4\Riset4Listing::progressListingDetail/$1');
    $routes->get('listing/detail/(:segment)/(:segment)', 'Riset\Riset4\Riset4Listing::progressListingDetail/$1/$2');

    $routes->get('sampel', 'Riset\Riset4\Riset4Sampel::index');
    $routes->get('sampel/detail/(:segment)', 'Riset\Riset4\Riset4Sampel::progressSampelDetail/$1');
    $routes->get('sampel/detail/(:segment)/(:segment)', 'Riset\Riset4\Riset4Sampel::progressSampelDetail/$1/$2');

    $routes->get('monitoring-tim', 'Riset\Riset4\Riset4MonitoringTim::index');
    $routes->get('monitoring-tim-listing/(:num)', 'Riset\Riset4\Riset4MonitoringTim::ajaxGetIdTim/$1');
    $routes->get('monitoring-tim-sampel/(:num)', 'Riset\Riset4\Riset4MonitoringTim::ajaxGetIdTimSampel/$1');

    $routes->get('progress-wilayah', 'Riset\Riset4\Riset4ProgressWilayah::index');
    $routes->post('debug-send', 'Riset\Riset4\Riset4ProgressWilayah::debug');
    $routes->post('debug-tabel', 'Riset\Riset4\Riset4ProgressWilayah::tabel');
    $routes->post('debug-progress', 'Riset\Riset4\Riset4ProgressWilayah::progress');

    $routes->get('progress-tim', 'Riset\Riset4\Riset4ProgressTim::index');
    $routes->get('progress-tim-sampel/(:num)', 'Riset\Riset4\Riset4ProgressTim::ajaxGetIdTimSampel/$1');   
});

// ROUTES FOR MONITORING PCL
$routes->group('monitoring-pcl', function ($routes) {
    $routes->get('/', 'MonitoringPcl\MonitoringPcl::index');
    $routes->get('(:any)', 'MonitoringPcl\MonitoringPcl::lokasiPcl/$1');
});

// ROUTES FOR PUSAT BANTUAN
$routes->group('pusat-bantuan', function ($routes) {
    $routes->get('faq', 'PusatBantuan\FAQ::index');
});


/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (is_file(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}