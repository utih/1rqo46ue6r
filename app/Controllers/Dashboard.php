<?php

namespace App\Controllers;

use App\Models\Riset\Riset12\Riset12ListingModel;
use App\Models\Riset\Riset12\Riset12ProgressModel;
use App\Models\Riset\Riset12\Riset12SampleModel;
use App\Models\Riset\Riset1\Riset1ListingModel;
use App\Models\Riset\Riset1\Riset1ProgressModel;
use App\Models\Riset\Riset3\Riset3ListingModel;
use App\Models\Riset\Riset3\Riset3ProgressModel;
use App\Models\Riset\Riset4\Riset4ListingModel;
use App\Models\Riset\Riset4\Riset4ProgressModel;

class Dashboard extends BaseController
{
    protected $model1;
    protected $model2;
    protected $model3;
    protected $model4;

    protected $ya1;
    protected $ya2;
    protected $ya3;
    protected $ya4;

    protected $sampel12;

    public function __construct()
    {
        $this->model1 = new Riset1ProgressModel();
        $this->model2 = new Riset12ProgressModel();
        $this->model3 = new Riset3ProgressModel();
        $this->model4 = new Riset4ProgressModel();

        $this->ya1 = new Riset1ListingModel();
        $this->ya2 = new Riset12ListingModel();
        $this->ya3 = new Riset3ListingModel();
        $this->ya4 = new Riset4ListingModel();

        $this->sampel12 = new Riset12SampleModel();
    }


    public function index()
    {

        // BUAT TIM INTEGRASI 12 INI MANUAL AJALAH

        //         $semua = 0;
        //         $terisi = 0;
        //         for ($i = 4; $i <= 62; $i++) {
        //             $ucok =  $this->model2->progress_pertim($i);
        //             $semua = $semua + $ucok['semua'];
        //             $terisi = $terisi + $ucok['terisi'];
        //         }

        //         $nilai = round($semua / $terisi * 100, 3);
        //         if ($nilai > 100) {
        //             $nilai = 100;
        //         }


        //         $dataTemp = [
        //             'semua' => $semua,
        //             'terisi' => $terisi,
        //             'nilai' => $nilai
        //         ];


        $counter12 = count($this->sampel12->getAllSampel('1', 'all'));
        $counter12 = $counter12 + count($this->sampel12->getAllSampel('2', 'all'));
        $counter12 = $counter12 + count($this->sampel12->getAllSampel('3', 'all'));


        $dataTemp = [
            'semua' => $counter12,
            'terisi' => '1901',
            'nilai' => '33.480'
        ];


        // dd("mau jalan ga mas");
        $data = [
            'title' => 'Dashboard',
            'active' => 'dashboard',

            'progress1' =>  $this->model1->progress_all(),
            // 'progress2' => $this->model2->progress_all(),
            'progress2' => $dataTemp,
            'progress3' => $this->model3->progress_all(),
            'progress4' => $this->model4->progress_all(),

            'listing1' => count($this->ya1->listing('all')),
            'listing2' => count($this->ya2->listing('all')),
            'listing3' => count($this->ya3->listing('all')),
            'listing4' => count($this->ya4->listing('all')),

            'li1' => $this->ya1,
            'li2' => $this->ya2,
            'li3' => $this->ya3,
            'li4' => $this->ya4,

        ];

        // dd($data);
        return view('dashboard', $data);
    }

    public function debug()
    {
        return redirect()->route('pusat-bantuan/faq');
    }
}
