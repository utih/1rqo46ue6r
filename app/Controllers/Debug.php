<?php

namespace App\Controllers;

use App\Models\Riset\Riset12\Riset12HasilModel;
use App\Models\Riset\Riset12\Riset12SampleModel;
use App\Models\Riset\Riset4\Riset4HasilModel;

class Debug extends BaseController
{
    protected $riset2SampelModel;
    protected $risethasil;
    protected $hasilR4;

    public function __construct()
    {
        $this->riset2SampelModel = new Riset12SampleModel();
        $this->risethasil = new Riset12HasilModel();
        $this->hasilR4 = new Riset4HasilModel();
    }

    public function index()
{
        $data = [
            'title' => 'Riset 2 Sampel',
            'active' => 'riset2_sampel',
            'jauh' => $this->hasilR4->hasil()
            // 'sampel' => $this->riset2SampelModel->getAllSampel('0' , 'kelurahan'),
            // 'detail1111111' => $this->riset2SampelModel->getDetailSampel('0', 'kecamatan', '3573020'),
            // 'detail111' => $this->riset2SampelModel->getDetailSampel('0', 'blok_sensus', '3573020001002B'),
            // 'detail111aaa' => $this->riset2SampelModel->getDetailSampel('0', 'blok_sensus', '3573020009004B'),
            // 'hasil' => $this->risethasil->hasil('00bd599e-bfba-4142-b')
        ];
        dd($data);
    }
}