<?php

namespace App\Controllers;

use App\Models\HasilModel;

class HasilController extends BaseController
{
    public function __construct()
    {
        $this->hasilModel = new HasilModel();
    }

    public function index()
    {
        $data = [
            'title' => 'hasilpencacahan',
            'active' => 'dashboard',
            'belum_dicacah' => $this->hasilModel->getBelumDicacah(),
            'selesai_dicacah' => $this->hasilModel->getSelesaiDicacah(),
            'total_sampel' => $this->hasilModel->getCountSampel()
        ];

        return view('hasilpencacahan', $data);
    }

    public function detail()
    {
        $data = [
            'title' => 'detailpencacahan',
            'active' => 'dashboard'
        ];

        return view('detailpencacahan', $data);
    }
}