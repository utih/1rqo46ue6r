<?php

namespace App\Controllers;

use App\Models\AuthModel;

class Login extends BaseController
{
    protected $monitoring;

    public function __construct()
    {
        $this->monitoring = new AuthModel();
    }

    public function index()
    {
        // CEK JIKA PERNAH LOGIN
        if (session()->has('login')) {
            return redirect()->route('/');
        }

        // INISIASI LOGIN GOOGLE 
        $clientID = '822676909236-n5nnlhp98r2s58d9rhv8chl59cbqj9m8.apps.googleusercontent.com';
        $clientSecret = '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';
        $redirectUri = 'http://localhost:8080/login'; //Harus sama dengan yang kita daftarkan

        $client = new \Google\Client();
        $client->setClientId($clientID);
        $client->setClientSecret($clientSecret);
        $client->setRedirectUri($redirectUri);
        $client->addScope('email');
        $client->addScope('profile');

        if (isset($_GET['code'])) {
            $temp_code = ($_GET['code']);
            $token = $client->fetchAccessTokenWithAuthCode($temp_code);
            // $token = $client->fetchAccessTokenWithAuthCode($_GET['code']);
            if (isset($token['access_token'])) {
                // $client->setAccessToken($token['access_token']);
                $Oauth = new \Google\Service\Oauth2($client);
                $userInfo = $Oauth->userinfo->get();
                $emailLog = $userInfo->email;
                $userLogin = implode('@', explode('@', $emailLog, -1));
                $domain = substr($emailLog, strpos($emailLog, '@') + 1);

                // RING 1 : CEK DOMAIN
                if ($domain != 'stis.ac.id') {
                    return redirect()->route('login')->with('foo', 'Login gagal! , Harap Login Menggunakan Email STIS (jika tidak bisa mengganti alamat email, silahkan ganti browser yang anda gunakan)');
                }

                // RING 2 : CEK USER
                $user = $this->monitoring->getUser($userLogin);
                if (!$user) {
                    return redirect()->route('login')->with('foo', 'Login gagal! , Anda tidak memiliki akses (jika tidak bisa mengganti alamat email, silahkan ganti browser yang anda gunakan)');
                }

                $session = [
                    'login' => true,
                    'nama' => $user[0]['nama'],
                    'email' => $user[0]['email'],
                    'jenis' => $user[0]['jenis'],
                    'jabatan' => $user[0]['jabatan']
                ];
                session()->set($session);
                return redirect()->back();
            }
        }

        $data = [
            'title' => 'Login',
            'client' => $client->createAuthUrl()
        ];
        return view('login', $data);
    }

    public function logout()
    {
        session()->destroy();
        return redirect()->route('/');
    }

    public function nim_login()
    {
        if (isset($_POST['submit'])) {
            $emailLogin = $_POST['login-email'];
            $passLogin = $_POST['login-password'];

            $user = $this->monitoring->nimLogin($emailLogin, $passLogin);
            if ($user) {
                // LOGIN NOW
                $user = $this->monitoring->getUser($emailLogin);
                $session = [
                    'login' => true,
                    'nama' => $user[0]['nama'],
                    'email' => $user[0]['email'],
                    'jenis' => $user[0]['jenis'], 'jabatan' => $user[0]['jabatan']
                ];
                session()->set($session);
                return redirect()->back();
            } else {
                return redirect()->back()->with('foo', 'Login gagal! , Masukkan username dan password yang benar')->withInput();
            }
        }
    }
}