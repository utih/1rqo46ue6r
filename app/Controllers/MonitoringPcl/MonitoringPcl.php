<?php

namespace App\Controllers\MonitoringPcl;

use App\Controllers\BaseController;
use App\Models\MonitoringPcl\MonitoringPclModel;

class MonitoringPCL extends BaseController
{
    protected $monitoringPclModel;

    public function __construct()
    {
        $this->monitoringPclModel = new MonitoringPclModel();
    }

    public function index()
    {
        $data = [
            'title' => 'Monitoring Lokasi PCL',
            'active' => 'monitoring_lokasi_pcl',
        ];

        return view('monitoring_pcl/index', $data);
    }

    public function lokasiPcl($riset)
    {
        $pcl = null;
        $lokus = null;

        if ($riset == 'lokasi-pcl') {
            $pcl = $this->monitoringPclModel->getAllPositionPCL();
            $riset = 'lokasi-pcl';
        	$titelatas = 'Semua Riset';
        } else if ($riset == 'riset-1') {
            $pcl = $this->monitoringPclModel->getPositionPCLByLokus('Kota Surabaya');
            $riset = 'Riset 1';
        	$titelatas = 'Riset 1 MPD';
            $lokus = 'Kota Surabaya';
        } else if ($riset == 'riset-2') {
            $pcl = $this->monitoringPclModel->getPositionPCLByLokus('Kota Malang');
            $riset = 'Riset 2';
        	$titelatas = 'Riset 1 & 2 Integrasi';
            $lokus = 'Kota Malang';
        } else if ($riset == 'riset-3') {
            $pcl = $this->monitoringPclModel->getPositionPCLByLokus('Kabupaten Malang');
            $riset = 'Riset 3';
        	$titelatas = 'Riset 3';
            $lokus = 'Kabupaten Malang';
        } else if ($riset == 'riset-4') {
            $pcl = $this->monitoringPclModel->getPositionPCLByLokus('Kota Batu');
            $riset = 'Riset 4';
        	$titelatas = 'Riset 4';
            $lokus = 'Kota Batu';
        } else {
            session()->setFlashdata('gagal', 'Halaman yang Anda maksud tidak ada!');
            return redirect()->to('/monitoring-pcl');
        }

        $data = [
            'title' => 'Monitoring Lokasi PCL',
            'active' => 'monitoring_lokasi_pcl',
            'riset' => $riset,
            'pcl' => $pcl,
            'lokus' => $lokus,
        	'titelatas' => $titelatas
        ];

        return view('monitoring_pcl/lokasi_pcl', $data);
    }
}