<?php

namespace App\Controllers\PusatBantuan;

use App\Controllers\BaseController;

class FAQ extends BaseController
{
    public function index()
    {
        $data = [
            'title' => 'FAQ',
            'active' => 'faq',
        ];

        return view('pusat_bantuan/faq', $data);
    }
}