<?php

namespace App\Controllers\Riset\Riset1;

use App\Controllers\BaseController;
use App\Models\Riset\Riset1\Riset1ListingModel;

class Riset1Listing extends BaseController
{

    protected $riset1ListingModel;

    public function __construct()
    {
        $this->riset1ListingModel = new Riset1ListingModel();
    }

    public function index()
    {
        $data = [
            'title' => 'Riset 1 Listing',
            'active' => 'riset1_listing',
            'listing_all' => $this->riset1ListingModel->listing('all'),
            'listing_by_bs' => $this->riset1ListingModel->listing('bloksensus'),
            'listing_by_kecamatan' => $this->riset1ListingModel->listing('kecamatan'),
            'listing_by_kelurahandesa' => $this->riset1ListingModel->listing('kelurahan'),
        ];
        // dd($data);
        return view('riset/riset1/riset1_listing', $data);
    }

    public function progressListingDetail($kode, $kode2 = null)
    {
        $detail = null;
        $detailListing = null;
        $pencacahBs = null;
        $detailLength = 0;

        if (strlen($kode) == 7 && $kode2 != null && strlen($kode2) == 3) {
            $detailLength = strlen($kode2);
            $detailListing = $this->riset1ListingModel->getDetailListing('kelurahandesa', $kode, $kode2);
            $detail = $detailListing[0]['nama_desa'];
        } else if (strlen($kode) == 7) {
            $detailLength = strlen($kode);
            $detailListing = $this->riset1ListingModel->getDetailListing('kecamatan', $kode);
            $detail = $detailListing[0]['nama_kecamatan'];
        } else {
            $detail = $kode;
            $detailLength = strlen($kode);
            $detailListing = $this->riset1ListingModel->getDetailListing('blok_sensus', $kode);
            // $pencacahBs = $this->riset1ListingModel->getPclByNim($detailListing[0]['nim']);
        }

        $data = [
            'title' => 'Riset 1 Detail Progress Listing',
            'active' => 'riset1_listing',
            'detail' => $detail,
            'detail_length' => $detailLength,
            'detail_listing' => $detailListing,
            // 'pencacah_bs' => $pencacahBs,
        ];

        // dd($data);
        return view('riset/riset1/riset1_detail_listing', $data);
    }
}