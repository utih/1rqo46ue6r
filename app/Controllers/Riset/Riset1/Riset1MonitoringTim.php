<?php

namespace App\Controllers\Riset\Riset1;

use App\Controllers\BaseController;
use App\Models\Riset\Riset1\Riset1MonitoringTimModel;

class Riset1MonitoringTim extends BaseController
{

    protected $riset1MonitoringTimModel;

    public function __construct()
    {
        $this->riset1MonitoringTimModel = new Riset1MonitoringTimModel();
    }

    public function index()
    {
        $data = [
            'title' => 'Riset 1 Monitoring Tim',
            'active' => 'riset1_monitoring',
            'list_tim' => $this->riset1MonitoringTimModel->listTim(),
            'sampel_by_tim_default' => $this->riset1MonitoringTimModel->sampelByTim(1),
            'pml_by_tim_default' => $this->riset1MonitoringTimModel->pmlByTim(1),
            'ppl_by_tim_default' => $this->riset1MonitoringTimModel->pplByTim(1),
            'url_foto' => 'https://capi62.stis.ac.id/web-service-62/assets/img/',
        ];

        // dd($data);
        return view('riset/riset1/riset1_monitoring_tim', $data);
    }

    public function ajaxGetIdTim($idTim)
    {
        $data = [
            'title' => 'Riset 1 Monitoring Tim',
            'active' => 'riset1_monitoring',
            'list_tim' => $this->riset1MonitoringTimModel->listTim(),
            'sampel_by_tim' => $this->riset1MonitoringTimModel->sampelByTim($idTim),
            'pml_by_tim' => $this->riset1MonitoringTimModel->pmlByTim($idTim),
            'ppl_by_tim' => $this->riset1MonitoringTimModel->pplByTim($idTim),
            'url_foto' => 'https://capi62.stis.ac.id/web-service-62/assets/img/',
            'tim' => $idTim,
        ];

        // dd($data);
        return view('riset/riset1/riset1_monitoring_sampel', $data);
    }
}