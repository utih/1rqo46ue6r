<?php

namespace App\Controllers\Riset\Riset1;

use App\Controllers\BaseController;

use App\Models\Riset\Riset1\Riset1MonitoringTimModel;
use App\Models\Riset\Riset1\Riset1ProgressModel;

class Riset1ProgressTim extends BaseController
{
    protected $model;
    protected $modelProgress;

    public function __construct()
    {
        $this->model = new Riset1MonitoringTimModel();
        $this->modelProgress = new Riset1ProgressModel();
    }

    public function index()
    {

        $data = [
            'title' => 'Riset 1 (MPD) Monitoring Tim',
            'active' => 'riset1_progresstim',
            'list_tim' => $this->model->listTim(),
            'sampel_by_tim_default' => $this->model->sampelByTim(1),
            'pml_by_tim_default' => $this->model->pmlByTim(1),
            'ppl_by_tim_default' => $this->model->pplByTim(1),
            'url_foto' => 'https://capi62.stis.ac.id/web-service-62/assets/img/',
            'wilayah' => $this->modelProgress,
            'idtim' => '1',
        ];

        // dd($data);
        return view('riset/riset1/riset1_progress_tim', $data);
    }

    public function ajaxGetIdTimSampel($idTim)
    {
        $data = [
            'title' => 'Riset 1 (MPD) Monitoring Tim',
            'active' => 'riset1_progresstim',
            'list_tim' => $this->model->listTim(),
            'sampel_by_tim_default' => $this->model->sampelByTim($idTim),
            'pml_by_tim_default' => $this->model->pmlByTim($idTim),
            'ppl_by_tim_default' => $this->model->pplByTim($idTim),
            'url_foto' => 'https://capi62.stis.ac.id/web-service-62/assets/img/',
            'wilayah' => $this->modelProgress,
            'prog' => $this->modelProgress->progress_pertim($idTim),
            'idtim' => $idTim
        ];

        // dd($data);
        return view('riset/riset1/riset1_progress_timupdate', $data);
    }
}
