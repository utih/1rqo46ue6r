<?php

namespace App\Controllers\Riset\Riset1;

use App\Controllers\BaseController;
use App\Models\Riset\Riset1\Riset1SampleModel;

class Riset1Sampel extends BaseController
{
    protected $riset1SampleModel;

    public function __construct()
    {

        $this->riset1SampleModel = new Riset1SampleModel();
    }

    public function index()
    {
        $data = [
            'title' => 'Riset 1 Sampel',
            'active' => 'riset1_sampel',
            // 'sampel_all_2' => $this->riset1SampleModel->sampel2('all'),
            // 'sampel_by_bs2' => $this->riset1SampleModel->sampel2('bloksensus'),
            // 'sampel_by_kabkota2' => $this->riset1SampleModel->sampel2('kabupatenkota'),
            // 'sampel_by_kecamatan2' => $this->riset1SampleModel->sampel2('kecamatan'),
            // 'sampel_by_kelurahandesa2' => $this->riset1SampleModel->sampel2('kelurahandesa'),
            'sampel_all' => $this->riset1SampleModel->sampel('all'),
            'sampel_by_bs' => $this->riset1SampleModel->sampel('bloksensus'),
            'sampel_by_kecamatan' => $this->riset1SampleModel->sampel('kecamatan'),
            'sampel_by_kelurahandesa' => $this->riset1SampleModel->sampel('kelurahan'),
            // 'survei' => 'mpd',
        ];
        // dd($data);
        // return view('riset/riset1/riset1_sampel_2', $data);
        return view('riset/riset1/riset1_sampel', $data);
    }

    public function progressSampelDetail($kode, $kode2 = null)
    {
        $detail = null;
        $detailSampel = null;
        $detailLength = 0;

        if (strlen($kode) == 7 && $kode2 != null && strlen($kode2) == 3) {
            $detailLength = strlen($kode2);
            $detailSampel = $this->riset1SampleModel->detailSampel('kelurahandesa', $kode, $kode2);
            $detail = $detailSampel[0]['nama_desa'];
        } else if (strlen($kode) == 7) {
            $detailLength = strlen($kode);
            $detailSampel = $this->riset1SampleModel->detailSampel('kecamatan', $kode);
            $detail = $detailSampel[0]['nama_kecamatan'];
        } else {
            $detail = $kode;
            $detailLength = strlen($kode);
            $detailSampel = $this->riset1SampleModel->detailSampel('bloksensus', $kode);
        }

        // // BLOK SENSUS
        // if (strlen($kode) == 14) {
        //     $detailLength = strlen($kode);
        //     $detailSampel = $this->riset1SampleModel->detailSampel2('bloksensus', $kode);
        //     $detail = $kode;
        // }
        // // KAB/KOTA
        // if (strlen($kode) == 4) {
        //     $detailLength = strlen($kode);
        //     $detailSampel = $this->riset1SampleModel->detailSampel2('kabupatenkota', $kode);
        //     // $detail = $detailSampel[0]['kabkot'];
        // }
        // // KECAMATAN
        // if (strlen($kode) == 7) {
        //     $detailLength = strlen($kode);
        //     $detailSampel = $this->riset1SampleModel->detailSampel2('kecamatan', $kode);
        //     // $detail = $detailSampel[0]['kecamatan'];
        // }
        // // KELURAHAN
        // if (strlen($kode) == 10) {
        //     $detailLength = strlen($kode);
        //     $detailSampel = $this->riset1SampleModel->detailSampel2('kelurahandesa', $kode);
        //     // $detail = $detailSampel[0]['kelurahan'];
        // }

        $data = [
            'title' => 'Riset 1 Detail Progress Sampel',
            'active' => 'riset1_sampel',
            'survei' => 'mpd',
            'detail' => $detail,
            'detail_length' => $detailLength,
            'detail_sampel' => $detailSampel,
        ];

        // dd($data);
        return view('riset/riset1/riset1_detail_sampel', $data);
    }
}