<?php

namespace App\Controllers\Riset\Riset12;

use App\Controllers\BaseController;
use App\Models\Riset\Riset12\Riset12ListingModel;

class Riset12Listing extends BaseController
{
    protected $model;

    public function __construct()
    {
        $this->model = new Riset12ListingModel();
    }

    public function index()
    {
        $data = [
            'title' => 'Riset 1 & 2 Listing',
            'active' => 'riset_1_2_listing',
            'listing_all' => $this->model->listing('all'),
            'listing_by_bs' => $this->model->listing('bloksensus'),
            'listing_by_kecamatan' => $this->model->listing("kecamatan"),
            'listing_by_kelurahandesa' => $this->model->listing("kelurahan"),
        ];

        // dd($data);
        return view('riset/riset12/riset12_listing', $data);
    }

    public function progressListingDetail($kode, $kode2 = null)
    {
        $detail = null;
        $detailListing = null;
        $pencacahBs = null;
        $detailLength = 0;

        if (strlen($kode) == 7 && $kode2 != null && strlen($kode2) == 3) {
            $detailLength = strlen($kode2);
            $detailListing = $this->model->getDetailListing('kelurahandesa', $kode, $kode2);
            $detail = $detailListing[0]['nama_desa'];
        } else if (strlen($kode) == 7) {
            $detailLength = strlen($kode);
            $detailListing = $this->model->getDetailListing('kecamatan', $kode);
            $detail = $detailListing[0]['nama_kecamatan'];
        } else {
            $detail = $kode;
            $detailLength = strlen($kode);
            $detailListing = $this->model->getDetailListing('blok_sensus', $kode);
            // $pencacahBs = $this->model->getPclByNim($detailListing[0]['nim']);
        }

        $data = [
            'title' => 'Riset 1 & 2 Detail Progress Listing',
            'active' => 'riset_1_2_listing',
            'detail' => $detail,
            'detail_length' => $detailLength,
            'detail_listing' => $detailListing,
            // 'pencacah_bs' => $pencacahBs,
        ];

        // dd($data);
        return view('riset/riset12/riset12_detail_listing', $data);
    }
}