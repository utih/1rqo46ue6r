<?php

namespace App\Controllers\Riset\Riset12;

use App\Controllers\BaseController;
use App\Models\Riset\Riset12\Riset12MonitoringTimModel;

class Riset12MonitoringTim extends BaseController
{
    protected $riset12MonitoringTimModel;

    public function __construct()
    {
        $this->riset12MonitoringTimModel = new Riset12MonitoringTimModel();
    }

    public function index()
    {
        $data = [
            'title' => 'Riset 1 & 2 (Integrasi) Monitoring Tim',
            'active' => 'riset_1_2_monitoring',
            'list_tim' => $this->riset12MonitoringTimModel->listTim(),
            'listing_by_tim_default' => $this->riset12MonitoringTimModel->listingByTim(4),
            'sampel_by_tim_default' => $this->riset12MonitoringTimModel->sampelByTim(4),
            'pml_by_tim_default' => $this->riset12MonitoringTimModel->pmlByTim(4),
            'ppl_by_tim_default' => $this->riset12MonitoringTimModel->pplByTim(4),
            'url_foto' => 'https://capi62.stis.ac.id/web-service-62/assets/img/',
        ];

        // dd($data);
        return view('riset/riset12/riset12_monitoring_tim', $data);
    }

    public function ajaxGetIdTim($idTim)
    {
        $data = [
            'title' => 'Riset 1 & 2 (Integrasi) Monitoring Tim',
            'active' => 'riset12_monitoring',
            'list_tim' => $this->riset12MonitoringTimModel->listTim(),
            'listing_by_tim' => $this->riset12MonitoringTimModel->listingByTim($idTim),
            'pml_by_tim' => $this->riset12MonitoringTimModel->pmlByTim($idTim),
            'ppl_by_tim' => $this->riset12MonitoringTimModel->pplByTim($idTim),
            'url_foto' => 'https://capi62.stis.ac.id/web-service-62/assets/img/',
            'tim' => $idTim,
        ];

        // dd($data);
        return view('riset/riset12/riset12_monitoring_listing', $data);
    }

    public function ajaxGetIdTimSampel($idTim)
    {
        $data = [
            'title' => 'Riset 1 & 2 (Integrasi) Monitoring Tim',
            'active' => 'riset12_monitoring',
            'list_tim' => $this->riset12MonitoringTimModel->listTim(),
            'sampel_by_tim' => $this->riset12MonitoringTimModel->sampelByTim($idTim),
            'pml_by_tim' => $this->riset12MonitoringTimModel->pmlByTim($idTim),
            'ppl_by_tim' => $this->riset12MonitoringTimModel->pplByTim($idTim),
            'url_foto' => 'https://capi62.stis.ac.id/web-service-62/assets/img/',
            'tim' => $idTim,
        ];

        // dd($data);
        return view('riset/riset12/riset12_monitoring_sampel', $data);
    }
}