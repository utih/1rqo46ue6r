<?php

namespace App\Controllers\Riset\Riset12;

use App\Controllers\BaseController;
use App\Models\Riset\Riset12\Riset12MonitoringTimModel;
use App\Models\Riset\Riset12\Riset12ProgressModel;

class Riset12ProgressTim extends BaseController
{
    protected $model;
    protected $modelProgress;

    public function __construct()
    {
        $this->model = new Riset12MonitoringTimModel();
        $this->modelProgress = new Riset12ProgressModel();
    }

    public function index()
    {
        $data = [
            'title' => 'Riset 1 & 2 (Integrasi) Monitoring Tim',
            'active' => 'riset_1_2_progresstim',
            'list_tim' => $this->model->listTim(),
            'sampel_by_tim_default' => $this->model->sampelByTim(4),
            'pml_by_tim_default' => $this->model->pmlByTim(4),
            'ppl_by_tim_default' => $this->model->pplByTim(4),
            'url_foto' => 'https://capi62.stis.ac.id/web-service-62/assets/img/',
            'wilayah' => $this->modelProgress,
            'idtim' => '4',
        ];

        // dd($data);
        return view('riset/riset12/riset12_progress_tim', $data);
    }

    public function ajaxGetIdTimSampel($idTim)
    {
        $data = [
            'title' => 'Riset 1 & 2 (Integrasi) Monitoring Tim',
            'active' => 'riset_1_2_progresstim',
            'list_tim' => $this->model->listTim(),
            'sampel_by_tim_default' => $this->model->sampelByTim($idTim),
            'pml_by_tim_default' => $this->model->pmlByTim($idTim),
            'ppl_by_tim_default' => $this->model->pplByTim($idTim),
            'url_foto' => 'https://capi62.stis.ac.id/web-service-62/assets/img/',
            'wilayah' => $this->modelProgress,
            'prog' => $this->modelProgress->progress_pertim($idTim), 
            'idtim' => $idTim
        ];

        // dd($data);
        return view('riset/riset12/riset12_progress_timupdate', $data);
    }
}
