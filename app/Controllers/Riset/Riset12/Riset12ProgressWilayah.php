<?php

namespace App\Controllers\Riset\Riset12;

use App\Controllers\BaseController;
use App\Models\Riset\Riset12\Riset12ProgressModel;

class Riset12ProgressWilayah extends BaseController
{
    protected $model;

    public function __construct()
    {
        $this->model = new Riset12ProgressModel();
    }

    public function index()
    {
        session()->setFlashdata('gagal', 'Maaf Halaman Progress per Wilayah Riset Integrasi 12 Tidak Dapat Diload, Diarahkan ke Progress Per Tim');
        return redirect()->route('riset-1-2/progress-tim');


        $data = [
            'title' => 'Riset 1 & 2 (Integrasi) Sampel Progress berdasarkan Wilayah',
            'active' => 'riset_1_2_progress',
            'url_detail' => '/riset-1-2/sampel/detail/',
            'url_ajax' => '/riset-1-2',
            'wilayah' => $this->model,
        ];

        return view('riset/riset12/riset12_wilayah', $data);
    }

    // MUNCULIN OPTION SELECT NYA DISINI AJA
    public function debug()
    {
        $data = [
            'kode1' => $_POST["data1"],
            'kode2' => $_POST["data2"],
            'wilayah' => $this->model,
            'url_ajax' =>  $_POST["url_ajax"]
        ];
        return view('layout/selectoption_desa', $data);
    }

    // MUNCULIN TABLE NYA 
    public function tabel()
    {
        $data = [
            'kode1' => $_POST["data1"],
            'kode2' => $_POST["data2"],
            'kode3' => $_POST["data3"],
            'wilayah' => $this->model,
            'url_detail' => '/riset-1-2/sampel/detail/',
            'judul' => "Riset 1 & 2 (Integrasi) Sampel Progress berdasarkan Wilayah",
        ];

        if ($_POST["myselect"] == 'kecamatan') {
            if ($_POST["data1"] == 'all_kecamatan') {
                return view('layout/tabelkecamatan', $data);
            } else {
                return view('layout/tabelkeldes', $data);
            }
        }

        if ($_POST["myselect"] == 'keldes') {
            if ($_POST["data2"] == 'all_keldes') {
                return view('layout/tabelkeldes', $data);
            } else {
                return view('layout/tabelbs', $data);
            }
        }
    }

    // MUNCULIN PROGRESS NYA 
    public function progress()
    {
        // HITUNG PROGRESSNYA
        $prog = "0";
        $judul = "semua";

        if ($_POST["myselect"] == 'kecamatan') {
            if ($_POST["kec"] == 'all_kecamatan') {
                // SEMUA
                $prog = $this->model->progress_all();
                $judul = "Keseluruhan";
            } else {
                //KECAMATAN
                $prog = $this->model->progress_kecamatan($_POST["kec"]);
                $judul = "Kecamatan " . $this->model->getNamaKecamatan($_POST["kec"]);
            }
        }

        if ($_POST["myselect"] == 'keldes') {

            if ($_POST["des"] == 'all_keldes') {
                //KECAMATAN
                $prog = $this->model->progress_kecamatan($_POST["kec"]);
                $judul = "Kecamatan " . $this->model->getNamaKecamatan($_POST["kec"]);
            } else {
                //KELDES    
                $prog = $this->model->progress_keldes($_POST["kec"], $_POST["des"]);
                $judul = "Kelurahan/Desa " . $this->model->getNamaKeldes($_POST["kec"], $_POST["des"]);
            }
        }

        $data = [
            'prog' => $prog,
            'judul' => $judul . " (Riset 1 & 2 Integrasi)",
        ];

        return view('layout/progress', $data);
    }
}
