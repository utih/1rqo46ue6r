<?php

namespace App\Controllers\Riset\Riset12;

use App\Controllers\BaseController;
use App\Models\Riset\Riset12\Riset12SampleModel;

class Riset12Sampel extends BaseController
{
    protected $riset12SampelModel;

    public function __construct()
    {

        $this->riset12SampelModel = new Riset12SampleModel();
    }

    public function index()
    {
        $data = [
            'title' => 'Riset 1 & 2 Sampel',
            'active' => 'riset_1_2_sampel',
            'r1_sampel_by_bs' => $this->riset12SampelModel->getAllSampel('1', 'bloksensus'),
            'r1_sampel_by_kecamatan' => $this->riset12SampelModel->getAllSampel('1', 'kecamatan'),
            'r1_sampel_by_kelurahandesa' => $this->riset12SampelModel->getAllSampel('1', 'kelurahan'),
            'r1_sampel_all' => $this->riset12SampelModel->getAllSampel('1', 'all'),

            'r2_sampel_by_bs' => $this->riset12SampelModel->getAllSampel('2', 'bloksensus'),
            'r2_sampel_by_kecamatan' => $this->riset12SampelModel->getAllSampel('2', 'kecamatan'),
            'r2_sampel_by_kelurahandesa' => $this->riset12SampelModel->getAllSampel('2', 'kelurahan'),
            'r2_sampel_all' => $this->riset12SampelModel->getAllSampel('2', 'all'),

            'r3_sampel_by_bs' => $this->riset12SampelModel->getAllSampel('3', 'bloksensus'),
            'r3_sampel_by_kecamatan' => $this->riset12SampelModel->getAllSampel('3', 'kecamatan'),
            'r3_sampel_by_kelurahandesa' => $this->riset12SampelModel->getAllSampel('3', 'kelurahan'),
            'r3_sampel_all' => $this->riset12SampelModel->getAllSampel('3', 'all'),
        ];

        // dd($data);
        return view('riset/riset12/riset12_sampel', $data);
    }

    public function progressSampelDetail($eligible, $kode, $kode2 = null)
    {
        $detail = null;
        $detailSampel = null;
        $detailLength = 0;

        if (strlen($kode) == 7 && $kode2 != null && strlen($kode2) == 3) {
            $detailLength = strlen($kode2);
            if ($eligible == '1') {
                $detailSampel = $this->riset12SampelModel->getDetailSampel('1', 'kelurahandesa', $kode, $kode2);
            } else if ($eligible == '2') {
                $detailSampel = $this->riset12SampelModel->getDetailSampel('2', 'kelurahandesa', $kode, $kode2);
            } else if ($eligible == '3') {
                $detailSampel = $this->riset12SampelModel->getDetailSampel('3', 'kelurahandesa', $kode, $kode2);
            }
            $detail = $detailSampel[0]['nama_desa'];
        } else if (strlen($kode) == 7) {
            $detailLength = strlen($kode);
            if ($eligible == '1') {
                $detailSampel = $this->riset12SampelModel->getDetailSampel('1', 'kecamatan', $kode);
            } else if ($eligible == '2') {
                $detailSampel = $this->riset12SampelModel->getDetailSampel('2', 'kecamatan', $kode);
            } else if ($eligible == '3') {
                $detailSampel = $this->riset12SampelModel->getDetailSampel('3', 'kecamatan', $kode);
            }
            $detail = $detailSampel[0]['nama_kecamatan'];
        } else {
            $detail = $kode;
            $detailLength = strlen($kode);
            if ($eligible == '1') {
                $detailSampel = $this->riset12SampelModel->getDetailSampel('1', 'bloksensus', $kode);
            } else if ($eligible == '2') {
                $detailSampel = $this->riset12SampelModel->getDetailSampel('2', 'bloksensus', $kode);
            } else if ($eligible == '3') {
                $detailSampel = $this->riset12SampelModel->getDetailSampel('3', 'bloksensus', $kode);
            }
        }

        $data = [
            'title' => 'Riset 1 & 2 Detail Sampel',
            'active' => 'riset_1_2_sampel',
            'detail' => $detail,
            'detail_length' => $detailLength,
            'detail_sampel' => $detailSampel,
        ];

        // dd($data);
        return view('riset/riset12/riset12_detail_sampel', $data);
    }

    public function progressSampelDetailDummy($kode, $kode2 = null)
    {
        $detail = null;
        $detailSampel = null;
        $detailLength = 0;

        if (strlen($kode) == 7 && $kode2 != null && strlen($kode2) == 3) {
            $detailLength = strlen($kode2);
            $detailSampel = $this->riset12SampelModel->getDetailSampelDummy('kelurahandesa', $kode, $kode2);
            $detail = $detailSampel[0]['nama_desa'];
        } else if (strlen($kode) == 7) {
            $detailLength = strlen($kode);
            $detailSampel = $this->riset12SampelModel->getDetailSampelDummy('kecamatan', $kode);
            $detail = $detailSampel[0]['nama_kecamatan'];
        } else {
            $detail = $kode;
            $detailLength = strlen($kode);
            $detailSampel = $this->riset12SampelModel->getDetailSampelDummy('bloksensus', $kode);
        }

        $data = [
            'title' => 'Riset 1 & 2 Detail Sampel',
            'active' => 'riset_1_2_sampel',
            'detail' => $detail,
            'detail_length' => $detailLength,
            'detail_sampel' => $detailSampel,
        ];

        // dd($data);
        return view('riset/riset12/riset12_detail_sampel', $data);
    }
}