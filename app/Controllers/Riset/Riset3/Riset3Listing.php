<?php

namespace App\Controllers\Riset\Riset3;

use App\Controllers\BaseController;
use App\Models\Riset\Riset3\Riset3ListingModel;

class Riset3Listing extends BaseController
{
    protected $riset4ListingModel;

    public function __construct()
    {
        $this->riset4ListingModel = new Riset3ListingModel();
    }

    public function index()
    {
        $data = [
            'title' => 'Riset 3 Listing',
            'active' => 'riset3_listing',
            'listing_all' => $this->riset4ListingModel->listing('all'),
            'listing_by_bs' => $this->riset4ListingModel->listing('bloksensus'),
            'listing_by_kecamatan' => $this->riset4ListingModel->listing('kecamatan'),
            'listing_by_kelurahandesa' => $this->riset4ListingModel->listing('kelurahan'),
        ];

        // dd($data);
        return view('riset/riset3/riset3_listing', $data);
    }

    public function progressListingDetail($kode, $kode2 = null)
    {
        $detail = null;
        $detailListing = null;
        $pencacahBs = null;
        $detailLength = 0;

        if (strlen($kode) == 7 && $kode2 != null && strlen($kode2) == 3) {
            $detailLength = strlen($kode2);
            $detailListing = $this->riset4ListingModel->getDetailListing('kelurahandesa', $kode, $kode2);
            $detail = $detailListing[0]['nama_desa'];
        } else if (strlen($kode) == 7) {
            $detailLength = strlen($kode);
            $detailListing = $this->riset4ListingModel->getDetailListing('kecamatan', $kode);
            $detail = $detailListing[0]['nama_kecamatan'];
        } else {
            $detail = $kode;
            $detailLength = strlen($kode);
            $detailListing = $this->riset4ListingModel->getDetailListing('blok_sensus', $kode);
            // $pencacahBs = $this->riset4ListingModel->getPclByNim($detailListing[0]['nim']);
        }

        $data = [
            'title' => 'Riset 3 Detail Progress Listing',
            'active' => 'riset3_listing',
            'detail' => $detail,
            'detail_length' => $detailLength,
            'detail_listing' => $detailListing,
            // 'pencacah_bs' => $pencacahBs,
        ];

        // dd($data);
        return view('riset/riset3/riset3_detail_listing', $data);
    }
}