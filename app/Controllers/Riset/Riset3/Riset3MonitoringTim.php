<?php

namespace App\Controllers\Riset\Riset3;

use App\Controllers\BaseController;
use App\Models\Riset\Riset3\Riset3MonitoringTimModel;

class Riset3MonitoringTim extends BaseController
{
    protected $riset3MonitoringTimModel;

    public function __construct()
    {
        $this->riset3MonitoringTimModel = new Riset3MonitoringTimModel();
    }

    public function index()
    {
        $data = [
            'title' => 'Riset 3 Monitoring Tim',
            'active' => 'riset3_monitoring',
            'list_tim' => $this->riset3MonitoringTimModel->listTim(),
            'listingModel' => $this->riset3MonitoringTimModel,
            'listing_by_tim_default' => $this->riset3MonitoringTimModel->listingByTim(63),
            'sampel_by_tim_default' => $this->riset3MonitoringTimModel->sampelByTim(63),
            'pml_by_tim_default' => $this->riset3MonitoringTimModel->pmlByTim(63),
            'ppl_by_tim_default' => $this->riset3MonitoringTimModel->pplByTim(63),
            'url_foto' => 'https://capi62.stis.ac.id/web-service-62/assets/img/',
        ];

        // dd($data);
        return view('riset/riset3/riset3_monitoring_tim', $data);
    }

    public function ajaxGetIdTim($idTim)
    {
        $data = [
            'title' => 'Riset 3 Monitoring Tim',
            'active' => 'riset3_monitoring',
            'list_tim' => $this->riset3MonitoringTimModel->listTim(),
            'listing_by_tim' => $this->riset3MonitoringTimModel->listingByTim($idTim),
            'pml_by_tim' => $this->riset3MonitoringTimModel->pmlByTim($idTim),
            'ppl_by_tim' => $this->riset3MonitoringTimModel->pplByTim($idTim),
            'url_foto' => 'https://capi62.stis.ac.id/web-service-62/assets/img/',
            'tim' => $idTim,
        ];

        // dd($data);
        return view('riset/riset3/riset3_monitoring_listing', $data);
    }

    public function ajaxGetIdTimSampel($idTim)
    {
        $data = [
            'title' => 'Riset 3 Monitoring Tim',
            'active' => 'riset3_monitoring',
            'list_tim' => $this->riset3MonitoringTimModel->listTim(),
            'sampel_by_tim' => $this->riset3MonitoringTimModel->sampelByTim($idTim),
            'pml_by_tim' => $this->riset3MonitoringTimModel->pmlByTim($idTim),
            'ppl_by_tim' => $this->riset3MonitoringTimModel->pplByTim($idTim),
            'url_foto' => 'https://capi62.stis.ac.id/web-service-62/assets/img/',
            'tim' => $idTim,
        ];

        // dd($data);
        return view('riset/riset3/riset3_monitoring_sampel', $data);
    }
}