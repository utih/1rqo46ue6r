<?php

namespace App\Controllers\Riset\Riset3;

use App\Controllers\BaseController;


use App\Models\Riset\Riset3\Riset3MonitoringTimModel;
use App\Models\Riset\Riset3\Riset3ProgressModel;

class Riset3ProgressTim extends BaseController
{
    protected $model;
    protected $modelProgress;

    public function __construct()
    {
        $this->model = new Riset3MonitoringTimModel();
        $this->modelProgress = new Riset3ProgressModel();
    }

    public function index()
    {

        $data = [
            'title' => 'Riset 3 Monitoring Tim',
            'active' => 'riset3_progresstim',
            'list_tim' => $this->model->listTim(),
            'sampel_by_tim_default' => $this->model->sampelByTim(63),
            'pml_by_tim_default' => $this->model->pmlByTim(63),
            'ppl_by_tim_default' => $this->model->pplByTim(63),
            'url_foto' => 'https://capi62.stis.ac.id/web-service-62/assets/img/',
            'wilayah' => $this->modelProgress,
            'idtim' => '63',
        ];

        // dd($data);
        return view('riset/riset3/riset3_progress_tim', $data);
    }

    public function ajaxGetIdTimSampel($idTim)
    {
        $data = [
            'title' => 'Riset 3 Monitoring Tim',
            'active' => 'riset3_progresstim',
            'list_tim' => $this->model->listTim(),
            'sampel_by_tim_default' => $this->model->sampelByTim($idTim),
            'pml_by_tim_default' => $this->model->pmlByTim($idTim),
            'ppl_by_tim_default' => $this->model->pplByTim($idTim),
            'url_foto' => 'https://capi62.stis.ac.id/web-service-62/assets/img/',
            'wilayah' => $this->modelProgress,
            'prog' => $this->modelProgress->progress_pertim($idTim),
            'idtim' => $idTim
        ];

        // dd($data);
        return view('riset/riset3/riset3_progress_timupdate', $data);
    }
}
