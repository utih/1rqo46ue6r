<?php

namespace App\Controllers\Riset\Riset3;

use App\Controllers\BaseController;
use App\Models\Riset\Riset3\Riset3SampleModel;

class Riset3Sampel extends BaseController
{
    protected $riset4SampleModel;

    public function __construct()
    {

        $this->riset4SampleModel = new Riset3SampleModel();
    }

    public function index()
    {
        $data = [
            'title' => 'Riset 3 Sampel',
            'active' => 'riset3_sampel',
            'sampel_all' => $this->riset4SampleModel->sampel('all'),
            'sampel_by_bs' => $this->riset4SampleModel->sampel('bloksensus'),
            'sampel_by_kecamatan' => $this->riset4SampleModel->sampel('kecamatan'),
            'sampel_by_kelurahandesa' => $this->riset4SampleModel->sampel('kelurahan'),
        ];

        // dd($data);
        return view('riset/riset3/riset3_sampel', $data);
    }

    public function progressSampelDetail($kode, $kode2 = null)
    {

        $detail = null;
        $detailSampel = null;
        $detailLength = 0;

        if (strlen($kode) == 7 && $kode2 != null && strlen($kode2) == 3) {
            $detailLength = strlen($kode2);
            $detailSampel = $this->riset4SampleModel->detailSampel('kelurahandesa', $kode, $kode2);
            $detail = $detailSampel[0]['nama_desa'];
        } else if (strlen($kode) == 7) {
            $detailLength = strlen($kode);
            $detailSampel = $this->riset4SampleModel->detailSampel('kecamatan', $kode);
            $detail = $detailSampel[0]['nama_kecamatan'];
        } else {
            $detail = $kode;
            $detailLength = strlen($kode);
            $detailSampel = $this->riset4SampleModel->detailSampel('bloksensus', $kode);
        }

        $data = [
            'title' => 'Riset 3 Detail Progress Sampel',
            'active' => 'riset3_sampel',
            'detail' => $detail,
            'detail_length' => $detailLength,
            'detail_sampel' => $detailSampel,
        ];

        // dd($data);
        return view('riset/riset3/riset3_detail_sampel', $data);
    }
}
