<?php

namespace App\Controllers\Riset\Riset4;

use App\Controllers\BaseController;
use App\Models\Riset\Riset4\Riset4MonitoringTimModel;

class Riset4MonitoringTim extends BaseController
{

    protected $riset4MonitoringTimModel;

    public function __construct()
    {
        $this->riset4MonitoringTimModel = new Riset4MonitoringTimModel();
    }

    public function index()
    {
        $data = [
            'title' => 'Riset 4 Monitoring Tim',
            'active' => 'riset4_monitoring',
            'list_tim' => $this->riset4MonitoringTimModel->listTim(),
            'listingModel' => $this->riset4MonitoringTimModel,
            'listing_by_tim_default' => $this->riset4MonitoringTimModel->listingByTim(95),
            'sampel_by_tim_default' => $this->riset4MonitoringTimModel->sampelByTim(95),
            'pml_by_tim_default' => $this->riset4MonitoringTimModel->pmlByTim(95),
            'ppl_by_tim_default' => $this->riset4MonitoringTimModel->pplByTim(95),
            'url_foto' => 'https://capi62.stis.ac.id/web-service-62/assets/img/',
        ];

        // dd($data);
        return view('riset/riset4/riset4_monitoring_tim', $data);
    }

    public function ajaxGetIdTim($idTim)
    {
        $data = [
            'title' => 'Riset 4 Monitoring Tim',
            'active' => 'riset4_monitoring',
            'list_tim' => $this->riset4MonitoringTimModel->listTim(),
            'listing_by_tim' => $this->riset4MonitoringTimModel->listingByTim($idTim),
            'pml_by_tim' => $this->riset4MonitoringTimModel->pmlByTim($idTim),
            'ppl_by_tim' => $this->riset4MonitoringTimModel->pplByTim($idTim),
            'url_foto' => 'https://capi62.stis.ac.id/web-service-62/assets/img/',
            'tim' => $idTim,
        ];

        // dd($data);
        return view('riset/riset4/riset4_monitoring_listing', $data);
    }

    public function ajaxGetIdTimSampel($idTim)
    {
        $data = [
            'title' => 'Riset 4 Monitoring Tim',
            'active' => 'riset4_monitoring',
            'list_tim' => $this->riset4MonitoringTimModel->listTim(),
            'sampel_by_tim' => $this->riset4MonitoringTimModel->sampelByTim($idTim),
            'pml_by_tim' => $this->riset4MonitoringTimModel->pmlByTim($idTim),
            'ppl_by_tim' => $this->riset4MonitoringTimModel->pplByTim($idTim),
            'url_foto' => 'https://capi62.stis.ac.id/web-service-62/assets/img/',
            'tim' => $idTim,
        ];

        // dd($data);
        return view('riset/riset4/riset4_monitoring_sampel', $data);
    }
}