<?php

namespace App\Controllers\Riset\Riset4;

use App\Controllers\BaseController;

use App\Models\Riset\Riset4\Riset4MonitoringTimModel;
use App\Models\Riset\Riset4\Riset4ProgressModel;

class Riset4ProgressTim extends BaseController
{
    protected $model;
    protected $modelProgress;

    public function __construct()
    {
        $this->model = new Riset4MonitoringTimModel();
        $this->modelProgress = new Riset4ProgressModel();
    }

    public function index()
    {

        $data = [
            'title' => 'Riset 4',
            'active' => 'riset4_progresstim',
            'list_tim' => $this->model->listTim(),
            'sampel_by_tim_default' => $this->model->sampelByTim(95),
            'pml_by_tim_default' => $this->model->pmlByTim(95),
            'ppl_by_tim_default' => $this->model->pplByTim(95),
            'url_foto' => 'https://capi62.stis.ac.id/web-service-62/assets/img/',
            'wilayah' => $this->modelProgress,
            'idtim' => '95',
        ];

        // dd($data);
        return view('riset/riset4/riset4_progress_tim', $data);
    }

    public function ajaxGetIdTimSampel($idTim)
    {
        $data = [
            'title' => 'Riset 4',
            'active' => 'riset4_progresstim',
            'list_tim' => $this->model->listTim(),
            'sampel_by_tim_default' => $this->model->sampelByTim($idTim),
            'pml_by_tim_default' => $this->model->pmlByTim($idTim),
            'ppl_by_tim_default' => $this->model->pplByTim($idTim),
            'url_foto' => 'https://capi62.stis.ac.id/web-service-62/assets/img/',
            'wilayah' => $this->modelProgress,
            'prog' => $this->modelProgress->progress_pertim($idTim),
            'idtim' => $idTim
        ];

        // dd($data);
        return view('riset/riset4/riset4_progress_timupdate', $data);
    }
}
