<?php

namespace App\Controllers\Riset\Riset4;

use App\Controllers\BaseController;
use App\Models\Riset\Riset4\Riset4ProgressModel;

class Riset4ProgressWilayah extends BaseController
{
    protected $model;

    public function __construct()
    {
        $this->model = new Riset4ProgressModel();
    }

    public function index()
    {
        $data = [
            'title' => 'Riset 4 Sampel Progress berdasarkan Wilayah',
            'active' => 'riset4_progress',
            'url_detail' => '/riset-4/sampel/detail/',
            'url_ajax' => '/riset-4',
            'wilayah' => $this->model,
        ];

        return view('riset/riset4/riset4_wilayah', $data);
    }

    // MUNCULIN OPTION SELECT NYA DISINI AJA
    public function debug()
    {
        $data = [
            'kode1' => $_POST["data1"],
            'kode2' => $_POST["data2"],
            'wilayah' => $this->model,
            'url_ajax' =>  $_POST["url_ajax"]
        ];
        return view('layout/selectoption_desa', $data);
    }

    // MUNCULIN TABLE NYA 
    public function tabel()
    {
        $data = [
            'kode1' => $_POST["data1"],
            'kode2' => $_POST["data2"],
            'kode3' => $_POST["data3"],
            'wilayah' => $this->model,
            'url_detail' => '/riset-4/sampel/detail/',
            'judul' => "Riset 4 Sampel Progress berdasarkan Wilayah",
        ];

        if ($_POST["myselect"] == 'kecamatan') {
            if ($_POST["data1"] == 'all_kecamatan') {
                return view('layout/tabelkecamatan', $data);
            } else {
                return view('layout/tabelkeldes', $data);
            }
        }

        if ($_POST["myselect"] == 'keldes') {
            if ($_POST["data2"] == 'all_keldes') {
                return view('layout/tabelkeldes', $data);
            } else {
                return view('layout/tabelbs', $data);
            }
        }
    }

    // MUNCULIN PROGRESS NYA 
    public function progress()
    {
        // HITUNG PROGRESSNYA
        $prog = "0";
        $judul = "semua";

        if ($_POST["myselect"] == 'kecamatan') {
            if ($_POST["kec"] == 'all_kecamatan') {
                // SEMUA
                $prog = $this->model->progress_all();
                $judul = "Keseluruhan";
            } else {
                //KECAMATAN
                $prog = $this->model->progress_kecamatan($_POST["kec"]);
                $judul = "Kecamatan " . $this->model->getNamaKecamatan($_POST["kec"]);
            }
        }

        if ($_POST["myselect"] == 'keldes') {

            if ($_POST["des"] == 'all_keldes') {
                //KECAMATAN
                $prog = $this->model->progress_kecamatan($_POST["kec"]);
                $judul = "Kecamatan " . $this->model->getNamaKecamatan($_POST["kec"]);
            } else {
                //KELDES    
                $prog = $this->model->progress_keldes($_POST["kec"], $_POST["des"]);
                $judul = "Kelurahan/Desa " . $this->model->getNamaKeldes($_POST["kec"], $_POST["des"]);
            }
        }

        $data = [
            'prog' => $prog,
            'judul' => $judul . " (Riset 4)",
        ];

        return view('layout/progress', $data);
    }
}