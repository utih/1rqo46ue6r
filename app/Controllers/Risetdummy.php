<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\Riset\Riset4\Riset4cobamodelbutRUTA;
use App\Models\Riset\Riset4\Riset4ProgressModel;


class Risetdummy extends BaseController
{
    protected $dummy;
    protected $dummyruta;

    public function __construct()
    {
        $this->dummy = new Riset4ProgressModel();
        $this->dummyruta = new Riset4cobamodelbutRUTA();
    }

    public function index()
    {
        $data = [
            'title' => 'Riset 4 Sampel Progress berdasarkan Wilayah',
            'active' => 'riset3_sampelaa',
            'url_detail' => '/riset-4/sampel/detail/',
            'wilayah' => $this->dummy,
        ];

        // dd($data);
        return view('risetdummy', $data);
    }

    // MUNCULIN OPTION SELECT NYA DISINI AJA
    public function debug()
    {
        $data = [
            'kode1' => $_POST["data1"],
            'kode2' => $_POST["data2"],
            'wilayah' => $this->dummy
        ];

        if ($_POST["myselect"] == 'keldes') {
            return view('layout/selectoption_desa', $data);
        } else if ($_POST["myselect"] == 'bs') {
            return view('layout/selectoption_bs', $data);
        }
    }

    // MUNCULIN TABLE NYA 
    public function tabel()
    {
        $data = [
            'kode1' => $_POST["data1"],
            'kode2' => $_POST["data2"],
            'kode3' => $_POST["data3"],
            'wilayah' => $this->dummy,
            'url_detail' => '/riset-4/sampel/detail/',
            'judul' => "Riset 4 Sampel Progress berdasarkan Wilayah",
        ];

        if ($_POST["myselect"] == 'kecamatan') {
            if ($_POST["data1"] == 'all_kecamatan') {
                return view('layout/tabelkecamatan', $data);
            } else {
                return view('layout/tabelkeldes', $data);
            }
        }

        if ($_POST["myselect"] == 'keldes') {
            if ($_POST["data2"] == 'all_keldes') {
                return view('layout/tabelkeldes', $data);
            } else {
                return view('layout/tabelbs', $data);
            }
        }
    }

    // MUNCULIN PROGRESS NYA 
    public function progress()
    {

        // HITUNG PROGRESSNYA
        $prog = "0";
        $judul = "semua";

        if ($_POST["myselect"] == 'kecamatan') {
            if ($_POST["kec"] == 'all_kecamatan') {
                // SEMUA
                $prog = $this->dummy->progress_all();
                $judul = "Keseluruhan";
            } else {
                //KECAMATAN
                $prog = $this->dummy->progress_kecamatan($_POST["kec"]);
                $judul = "Kecamatan " . $this->dummy->getNamaKecamatan($_POST["kec"]);
            }
        }

        if ($_POST["myselect"] == 'keldes') {

            if ($_POST["des"] == 'all_keldes') {
                //KECAMATAN
                $prog = $this->dummy->progress_kecamatan($_POST["kec"]);
                $judul = "Kecamatan " . $this->dummy->getNamaKecamatan($_POST["kec"]);
            } else {
                //KELDES    
                $prog = $this->dummy->progress_keldes($_POST["kec"], $_POST["des"]);
                $judul = "Kelurahan/Desa " . $this->dummy->getNamaKeldes($_POST["kec"], $_POST["des"]);
            }
        }

        $data = [
            'prog' => $prog,
            'judul' => $judul . " (RISET 4)",
        ];


        return view('layout/progress', $data);
    }
}
