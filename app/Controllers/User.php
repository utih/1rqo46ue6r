<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\AuthModel;

class User extends BaseController
{
    protected $user;
    protected $helpers = ['form'];

    public function __construct()
    {
        $this->user = new AuthModel();
    }

    public function index()
    {
        $data = [
            'title' => 'Profil',
            'active' => 'profil',
        ];

        return view('profil/profil', $data);
    }

    public function changePassword()
    {
        $data = [
            'title' => 'Profil',
            'active' => 'profil',
            'validation' => \Config\Services::validation(),
        ];

        return view('/profil/ubah_password', $data);
    }

    public function savePassword()
    {
        $oldPasswordFromForm = $this->request->getVar('old-password');
        $newPasswordFromForm = $this->request->getVar('new-password');
        $user = $this->user->getUserWithPassword(session()->get('email'));
        $oldPasswordFromDatabase = $user[0]['password'];
        $newPasswordFromForm = md5($newPasswordFromForm);

        if (!$this->validate([
            'old-password' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Password lama harus diisi',
                ]
            ],
            'new-password' => [
                'rules' => 'required|min_length[6]',
                'errors' => [
                    'required' => 'Password baru harus diisi',
                    'min_length' => 'Password harus terdiri dari 6 karakter'
                ]
            ],
            'conf-new-password' => [
                'rules' => 'required|matches[new-password]',
                'errors' => [
                    'required' => 'Konfirmasi password baru harus diisi',
                    'matches' => 'Konfirmasi password baru harus sama dengan password baru',
                ]
            ],
        ])) {
            return redirect()->to('/profil/ubah-password')->withInput();
        }

        if ($oldPasswordFromDatabase != md5($oldPasswordFromForm)) {
            session()->setFlashdata('gagal', 'Password lama yang Anda masukkan salah');
            return redirect()->to('/profil/ubah-password')->withInput();
        }

        if ($newPasswordFromForm == $oldPasswordFromDatabase) {
            session()->setFlashdata('gagal', 'Password baru tidak boleh sama dengan password lama');
            return redirect()->to('/profil/ubah-password')->withInput();
        }

        // AGAK ANEH EMANG (jgn dihujad huhu)
        if ($this->user->savePassword(session()->get('email'), $newPasswordFromForm) != null) {
            session()->setFlashdata('gagal', 'Terjadi kesalahan');
            return redirect()->to('/profil/ubah-password');
        }

        session()->setFlashdata('sukses', 'Password berhasil diubah');
        return redirect()->to('/profil/ubah-password');
    }
}