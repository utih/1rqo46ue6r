<?php

namespace App\Filters;

use CodeIgniter\Filters\FilterInterface;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;

class Riset3Filter implements FilterInterface
{
    /**
     * Do whatever processing this filter needs to do.
     * By default it should not return anything during
     * normal execution. However, when an abnormal state
     * is found, it should return an instance of
     * CodeIgniter\HTTP\Response. If it does, script
     * execution will end and that Response will be
     * sent back to the client, allowing for error pages,
     * redirects, etc.
     *
     * @param RequestInterface $request
     * @param array|null       $arguments
     *
     * @return mixed
     */
    public function before(RequestInterface $request, $arguments = null)
    {
        $jenis = session()->get('jenis');
        $jabatan =  session()->get('jabatan');

        if ($jenis == "mahasiswa") {
            if (
                $jabatan != "BPH" &&
                $jabatan != "Instruktur Utama Riset 3" &&
                $jabatan != "Koordinator Wilayah Kabupaten Malang" &&
                $jabatan != "Bidang TI" &&
                $jabatan != "BPH Riset 3"
            ) {
                session()->setFlashdata('gagal', 'Anda Tidak Memiliki Akses Ke Halaman Ini');
                // return redirect()->route('/');
                return redirect()->back();
            }
        }

        if ($jenis == "bps") {
            if (
                $jabatan != "BPS Kabupaten Malang"
            ) {
                session()->setFlashdata('gagal', 'Anda Tidak Memiliki Akses Ke Halaman Ini');
                // return redirect()->route('/');
                return redirect()->back();
            }
        }

    }

    /**
     * Allows After filters to inspect and modify the response
     * object as needed. This method does not allow any way
     * to stop execution of other after filters, short of
     * throwing an Exception or Error.
     *
     * @param RequestInterface  $request
     * @param ResponseInterface $response
     * @param array|null        $arguments
     *
     * @return mixed
     */
    public function after(RequestInterface $request, ResponseInterface $response, $arguments = null)
    {
        //
    }
}
