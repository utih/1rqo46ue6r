<?php

namespace App\Models;

use CodeIgniter\Model;


class AuthModel extends Model
{
    protected $user;
    protected $userFoto;

    public function __construct()
    {
        parent::__construct();
        $monitoring = \Config\Database::connect('pkl62monitoring');
        $this->user = $monitoring->table('user');

        $pkl62sikoko = \Config\Database::connect('pkl62sikoko');
        $this->userFoto = $pkl62sikoko->table('mahasiswa');
    }

    public function getUser($email)
    {
        $this->user->select('nama, email , jenis ,jabatan');
        return $this->user->where('email', $email)->get()->getResultArray();
    }

    public function getFoto($email){
        $this->userFoto->select('foto');
        return $this->userFoto->where('email', $email . "@stis.ac.id")->get()->getResultArray();
    }

    public function nimLogin($email, $password)
    {
        $passEnc = md5($password);
        return $this->user->where('email', $email)->where('password', $passEnc)->get()->getResultArray();
    }

    public function getUserWithPassword($email)
    {
        return $this->user->where('email', $email)->get()->getResultArray();
    }

    public function savePassword($email, $newPassword)
    {
        $data = [
            'password' => $newPassword,
        ];
        $builder = $this->user->where('email', $email);
        $builder->update($data);
    }
}