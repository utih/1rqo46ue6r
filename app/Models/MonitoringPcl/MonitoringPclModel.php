<?php

namespace App\Models\MonitoringPcl;

use CodeIgniter\Model;

class MonitoringPclModel extends Model
{
    public function __construct()
    {
        $this->db = \Config\Database::connect('pkl62monitoring');
    }

    public function getAllPositionPCL()
    {
        $builder = $this->db->table('posisi_pcl');
        $builder->like('nim', '2', 'after');
        $builder->where('latitude IS NOT NULL');
        $builder->orderBy('nama', 'ASC');
        $query = $builder->get();
        return $query->getResultArray();
    }

    public function getPositionPCLByLokus($lokus, $lokus2 = null)
    {
        if ($lokus2 == 'Kota Malang') {
            $builder = $this->db->table('posisi_pcl');
            $where = "lokus='" . $lokus . "' OR lokus='" . $lokus2 . "'";
            $builder->where($where);
            $builder->like('nim', '2', 'after');
            $builder->where('latitude IS NOT NULL');
            $builder->orderBy('nama', 'ASC');
            $query = $builder->get();
            return $query->getResultArray();
        }
        $builder = $this->db->table('posisi_pcl');
        $builder->where('lokus', $lokus);
        $builder->like('nim', '2', 'after');
        $builder->where('latitude IS NOT NULL');
        $builder->orderBy('nama', 'ASC');
        $query = $builder->get();
        return $query->getResultArray();
    }
}