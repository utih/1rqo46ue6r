<?php

namespace App\Models\Riset\Riset12;

use CodeIgniter\Model;

class Riset12SampleModel extends Model
{
    protected $db;
    protected $dbMonitoring;

    public function __construct()
    {
        $this->db = \Config\Database::connect('riset12');
        $this->dbMonitoring = \Config\Database::connect('pkl62monitoring');
    }

    public function getAllSampel($riset, $kategori)
    {
        $builder = $this->db->table('datast');
        $builder->join('rumahtangga', 'rumahtangga.kodeRuta = datast.kodeRuta', 'left');

        // PROBING RISET, 
        // 1 = HANYA 1
        // 2 = HANYA 2
        // 3 = 1 DAN 2 ;
        // KALAU 0 GAPERLU DI PROBING LAGI

        if ($riset == '1') {
            $builder->where('riset', '1');
        } else if ($riset == '2') {
            $builder->where('riset', '2');
        } else if ($riset == '3') {
            $builder->where('riset', '3');
        }

        //PROBING KATEGORI, 1 = BLOK SENSUS; 2 = KECAMATAN; 3 = KELURAHAN
        if ($kategori == "all") {
            $db2 = $this->dbMonitoring->getDatabase();

            $builder->join('bloksensus', 'bloksensus.id = rumahtangga.kodeBs', 'left');
            $builder->join('kecamatan', 'kecamatan.kecno = bloksensus.kecamatan', 'left');
            $builder->join('desa', 'desa.kecno = bloksensus.kecamatan AND desa.desano = bloksensus.kelurahandesa', 'left');
            $builder->join($db2 . '.posisi_pcl', $db2 . '.posisi_pcl.nim = bloksensus.nim', 'left');
            $builder->select('rumahtangga.*, kecamatan.kecno as kode_kecamatan, kecamatan.nama as nama_kecamatan, desa.desano as kode_desa, desa.nama as nama_desa, posisi_pcl.nim as nim_pcl, posisi_pcl.nama as nama_pcl');
            $query = $builder->get();
            return $query->getResultArray();
        }

        if ($kategori == 'bloksensus') {
            // GET SAMPLE COUNT
            $builder->join('bloksensus', 'bloksensus.id = rumahtangga.kodeBs', 'left');
            $builder->join($this->dbMonitoring->getDatabase() . '.posisi_pcl', $this->dbMonitoring->getDatabase() . '.posisi_pcl.nim = bloksensus.nim', 'left');
            $builder->select('datast.kodeBs as kode_bs, MIN(datast.kodeRuta) as kode_ruta, MIN(posisi_pcl.nim) as nim, MIN(posisi_pcl.nama) as nama_pcl');
            $builder->select('COUNT(*) as jumlah_sampel');
            $builder->groupBy('kode_bs');
            $builder->orderBy('kode_bs', 'DESC');
            $query = $builder->get();
            $query = $query->getResultArray();
            // return $query;

            // GET LISTING COUNT
            $builder2 = $this->db->table('rumahtangga');
            $builder2->join('bloksensus', 'bloksensus.id = rumahtangga.kodeBs', 'left');
            $builder2->join($this->dbMonitoring->getDatabase() . '.posisi_pcl', $this->dbMonitoring->getDatabase() . '.posisi_pcl.nim = bloksensus.nim', 'left');
            $builder2->select('kodeBs as kode_bs, MIN(posisi_pcl.nim) as nim, MIN(posisi_pcl.nama) as nama_pcl');
            $builder2->select('COUNT(*) as jumlah_listing');
            $builder2->groupBy('kode_bs');
            $builder2->orderBy('kode_bs', 'DESC');
            $query2 = $builder2->get();
            $query2 = $query2->getResultArray();
            // return $query2;

            // COMBINE
            for ($i = 0; $i < count($query); $i++) {
                for ($j = 0; $j < count($query2); $j++) {
                    if ($query[$i]['kode_bs'] == $query2[$j]['kode_bs']) {
                        $query[$i]['jumlah_listing'] = $query2[$j]['jumlah_listing'];
                    }
                }
            }
            return $query;
        }

        if ($kategori == 'kecamatan') {
            // GET SAMPLE COUNT
            $builder->join('bloksensus', 'bloksensus.id = rumahtangga.kodeBs', 'left');
            $builder->join('kecamatan', 'kecamatan.kecno = bloksensus.kecamatan', 'left');
            $builder->select('MIN(datast.kodeBs) as kode_bs, kecamatan.kecno as kode_kecamatan, MIN(kecamatan.nama) as nama_kecamatan');
            $builder->select('COUNT(*) as jumlah_sampel');
            $builder->groupBy('kode_kecamatan');
            $builder->orderBy('kode_kecamatan', 'DESC');
            $query = $builder->get();
            $query = $query->getResultArray();
            // return $query;

            // GET LISTING COUNT
            $builder2 = $this->db->table('rumahtangga');
            $builder2->join('bloksensus', 'bloksensus.id = rumahtangga.kodeBs', 'left');
            $builder2->join('kecamatan', 'kecamatan.kecno = bloksensus.kecamatan', 'left');
            $builder2->select('MIN(kodeBs) as kode_bs , bloksensus.kecamatan as kode_kecamatan , MIN(kecamatan.nama) as nama_kecamatan');
            $builder2->select('COUNT(*) as jumlah_listing');
            $builder2->groupBy('kode_kecamatan');
            $builder2->orderBy('kode_kecamatan', 'DESC');
            $query2 = $builder2->get();
            $query2 = $query2->getResultArray();
            // return $query2;

            // COMBINE
            for ($i = 0; $i < count($query); $i++) {
                for ($j = 0; $j < count($query2); $j++) {
                    if ($query[$i]['kode_kecamatan'] == $query2[$j]['kode_kecamatan']) {
                        $query[$i]['jumlah_listing'] = $query2[$j]['jumlah_listing'];
                    }
                }
            }
            return $query;
        }

        if ($kategori == 'kelurahan') {
            // GET SAMPLE COUNT
            $builder->join('bloksensus', 'bloksensus.id = rumahtangga.kodeBs', 'left');
            $builder->join('desa', 'desa.kecno = bloksensus.kecamatan AND desa.desano = bloksensus.kelurahandesa', 'left');
            $builder->select('MIN(datast.kodeBs) as kode_bs, desa.kecno as kode_kecamatan, desa.desano as kode_desa, MIN(desa.nama) as nama_desa');
            $builder->select('COUNT(*) as jumlah_sampel');
            $builder->groupBy(['kode_kecamatan', 'kode_desa']);
            $builder->orderBy('kode_kecamatan ASC, kode_desa ASC');
            $query = $builder->get();
            $query = $query->getResultArray();
            // return $query;

            $builder2 = $this->db->table('rumahtangga');
            $builder2->join('bloksensus', 'bloksensus.id = rumahtangga.kodeBs', 'left');
            $builder2->join('desa', 'desa.kecno = bloksensus.kecamatan AND desa.desano  = bloksensus.kelurahandesa', 'left');
            $builder2->select('MIN(kodeBs) as kode_bs , bloksensus.kecamatan as kode_kecamatan, bloksensus.kelurahandesa as kode_desa , MIN(desa.nama) as nama_desa');
            $builder2->select('COUNT(*) as jumlah_listing');
            $builder2->groupBy(['kode_kecamatan', 'kode_desa']);
            $builder2->orderBy('kode_kecamatan ASC, kode_desa ASC');
            $query2 = $builder2->get();
            $query2 = $query2->getResultArray();
            // return $query2;

            // COMBINE
            for ($i = 0; $i < count($query); $i++) {
                for ($j = 0; $j < count($query2); $j++) {
                    if ($query[$i]['kode_kecamatan'] == $query2[$j]['kode_kecamatan'] && $query[$i]['kode_desa'] == $query2[$j]['kode_desa']) {
                        $query[$i]['jumlah_listing'] = $query2[$j]['jumlah_listing'];
                    }
                }
            }
            return $query;
        }
    }

    public function getDetailSampel($riset, $kategori, $kode, $kode2 = null)
    {
        $builder = $this->db->table('datast');
        $builder->where('datast.riset', $riset);
        $builder->join('rumahtangga', 'rumahtangga.kodeRuta = datast.kodeRuta', 'left');
        $builder->join('bloksensus', 'bloksensus.id = rumahtangga.kodeBs', 'left');
        $builder->join('kabupaten', 'kabupaten.kabno = bloksensus.kabupaten', 'left');
        $builder->join('kecamatan', 'kecamatan.kecno = bloksensus.kecamatan', 'left');
        $builder->join('desa', 'desa.kecno = bloksensus.kecamatan AND desano = bloksensus.kelurahandesa', 'left');
        $builder->join($this->dbMonitoring->getDatabase() . '.posisi_pcl', $this->dbMonitoring->getDatabase() . '.posisi_pcl.nim = bloksensus.nim', 'left');
        if ($kategori == 'bloksensus') {
            $builder->select('datast.riset, rumahtangga.*, bloksensus.*, kecamatan.nama as nama_kecamatan, kabupaten.nama as nama_kabupaten, desa.desano as nomor_desa, desa.nama as nama_desa, posisi_pcl.nim as nim_pcl, posisi_pcl.nama as nama_pcl');
            $builder->where('datast.kodeBs', $kode);
        }
        if ($kategori == 'kecamatan') {
            $builder->select('datast.riset, rumahtangga.*, bloksensus.*, kecamatan.kecno as nomor_kecamatan, kecamatan.nama as nama_kecamatan, kabupaten.nama as nama_kabupaten, posisi_pcl.nim as nim_pcl, posisi_pcl.nama as nama_pcl');
            $builder->where('kecamatan.kecno', $kode);
        }
        if ($kategori == 'kelurahandesa' && $kode2 != null) {
            $builder->select('datast.riset, rumahtangga.*, bloksensus.*, desa.kecno as nomor_kecamatan, kabupaten.nama as nama_kabupaten, kecamatan.nama as nama_kecamatan, desa.desano as nomor_desa, desa.nama as nama_desa, posisi_pcl.nim as nim_pcl, posisi_pcl.nama as nama_pcl');
            $builder->where('desa.desano', $kode2);
            $builder->where('desa.kecno', $kode);
        }
        $query = $builder->get()->getResultArray();
        return $query;

        // PROBING RISET, 
        // 1 = HANYA 1
        // 2 = HANYA 2
        // 3 = 1 DAN 2 ;
        // KALAU 0 GAPERLU DI PROBING LAGI


        // SAMPAI SINI AMAN

        $builder->join('bloksensus', 'id = rumahtangga.kodeBs', 'left');
        $builder->join('kabupaten', 'kabno = bloksensus.kabupaten', 'left');
        $builder->join('kecamatan', 'kecamatan.kecno = bloksensus.kecamatan', 'left');
        $builder->join('desa', 'desa.kecno = bloksensus.kecamatan AND desano = bloksensus.kelurahandesa', 'left');
        $builder->join($this->dbMonitoring->getDatabase() . '.posisi_pcl', $this->dbMonitoring->getDatabase() . '.posisi_pcl.nim = bloksensus.nim', 'left');

        if ($kategori == 'blok_sensus') {
            $builder->select('rumahtangga.*, bloksensus.*, kabupaten.nama as nama_kabupaten, kecamatan.nama as nama_kecamatan, desa.desano as nomor_desa, desa.nama as nama_desa, posisi_pcl.nim as nim_pcl, posisi_pcl.nama as nama_pcl');
            $builder->where('datast.kodeBs', $kode);
        }
        if ($kategori == 'kecamatan') {
            $builder->select('rumahtangga.*, bloksensus.id, bloksensus.kabupaten, bloksensus.kecamatan, bloksensus.nama, bloksensus.nim, bloksensus.status, kabupaten.kabno as nomor_kabupaten, kabupaten.nama as nama_kabupaten, kecamatan.kecno as nomor_kecamatan, kecamatan.nama as nama_kecamatan, posisi_pcl.nim as nim_pcl, posisi_pcl.nama as nama_pcl');
            $builder->where('kecamatan.kecno', $kode);
        }
        if ($kategori == 'kelurahandesa' && $kode2 != null) {
            $builder->select('rumahtangga.*, bloksensus.id, bloksensus.kabupaten, bloksensus.kecamatan, bloksensus.nama, bloksensus.kelurahandesa, bloksensus.nim, bloksensus.status, desa.kecno as nomor_kecamatan, kecamatan.nama as nama_kecamatan, desa.desano as nomor_desa, desa.nama as nama_desa, posisi_pcl.nim as nim_pcl, posisi_pcl.nama as nama_pcl');
            $builder->where('desa.desano', $kode2);
            $builder->where('desa.kecno', $kode);
        }
        $query = $builder->get()->getResultArray();
        return $query;
    }

    public function getDetailSampelDummy($kategori, $kode, $kode2 = null)
    {
        $builder = $this->db->table('datast');
        $builder->join('rumahtangga', 'rumahtangga.kodeRuta = datast.kodeRuta', 'left');
        $builder->join('bloksensus', 'bloksensus.id = rumahtangga.kodeBs', 'left');
        $builder->join('kabupaten', 'kabupaten.kabno = bloksensus.kabupaten', 'left');
        $builder->join('kecamatan', 'kecamatan.kecno = bloksensus.kecamatan', 'left');
        $builder->join('desa', 'desa.kecno = bloksensus.kecamatan AND desano = bloksensus.kelurahandesa', 'left');
        $builder->join($this->dbMonitoring->getDatabase() . '.posisi_pcl', $this->dbMonitoring->getDatabase() . '.posisi_pcl.nim = bloksensus.nim', 'left');
        if ($kategori == 'bloksensus') {
            $builder->select('datast.riset, rumahtangga.*, bloksensus.*, kecamatan.nama as nama_kecamatan, kabupaten.nama as nama_kabupaten, desa.desano as nomor_desa, desa.nama as nama_desa, posisi_pcl.nim as nim_pcl, posisi_pcl.nama as nama_pcl');
            $builder->where('datast.kodeBs', $kode);
        }
        if ($kategori == 'kecamatan') {
            $builder->select('datast.riset, rumahtangga.*, bloksensus.*, kecamatan.kecno as nomor_kecamatan, kecamatan.nama as nama_kecamatan, kabupaten.nama as nama_kabupaten, posisi_pcl.nim as nim_pcl, posisi_pcl.nama as nama_pcl');
            $builder->where('kecamatan.kecno', $kode);
        }
        if ($kategori == 'kelurahandesa' && $kode2 != null) {
            $builder->select('datast.riset, rumahtangga.*, bloksensus.*, desa.kecno as nomor_kecamatan, kabupaten.nama as nama_kabupaten, kecamatan.nama as nama_kecamatan, desa.desano as nomor_desa, desa.nama as nama_desa, posisi_pcl.nim as nim_pcl, posisi_pcl.nama as nama_pcl');
            $builder->where('desa.desano', $kode2);
            $builder->where('desa.kecno', $kode);
        }
        $query = $builder->get()->getResultArray();
        return $query;

        // PROBING RISET, 
        // 1 = HANYA 1
        // 2 = HANYA 2
        // 3 = 1 DAN 2 ;
        // KALAU 0 GAPERLU DI PROBING LAGI

    }
}