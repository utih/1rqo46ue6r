<?php

namespace App\Models\Riset\Riset3;

use CodeIgniter\Model;

class Riset3ListingModel extends Model
{
    protected $db;
    protected $dbMonitoring;

    public function __construct()
    {
        $this->db = \Config\Database::connect('riset3');
        $this->dbMonitoring = \Config\Database::connect('pkl62monitoring');
    }

    public function listing($kategori)
    {

        if ($kategori == 'all') {
            $db2 = $this->dbMonitoring->getDatabase();
            $builder = $this->db->table('rumahtangga');
            $builder->join('bloksensus', 'bloksensus.id = rumahtangga.kodeBs', 'left');
            $builder->join('kecamatan', 'kecamatan.kecno = bloksensus.kecamatan', 'left');
            $builder->join('desa', 'desa.kecno = bloksensus.kecamatan AND desa.desano = bloksensus.kelurahandesa', 'left');
            $builder->join($db2 . '.posisi_pcl', $db2 . '.posisi_pcl.nim = bloksensus.nim', 'left');
            $builder->select('rumahtangga.*, kecamatan.kecno as kode_kecamatan, kecamatan.nama as nama_kecamatan, desa.desano as kode_desa, desa.nama as nama_desa, posisi_pcl.nim as nim_pcl, posisi_pcl.nama as nama_pcl');
            $query = $builder->get();
            return $query->getResultArray();
        }

        if ($kategori == 'bloksensus') {
            $builder = $this->db->table('rumahtangga');
            $builder->select('kodeBs as kode_bs');
            $builder->join('bloksensus', 'bloksensus.id = rumahtangga.kodeBs', 'left');
            $builder->join($this->dbMonitoring->getDatabase() . '.posisi_pcl', $this->dbMonitoring->getDatabase() . '.posisi_pcl.nim = bloksensus.nim', 'left');
            $builder->select('kodeBs as kode_bs, MIN(posisi_pcl.nim) as nim, MIN(posisi_pcl.nama) as nama_pcl');
            $builder->select('COUNT(*) as jumlah_listing');
            $builder->groupBy('kodeBs');
            $builder->orderBy('jumlah_listing', 'DESC');
            $query = $builder->get();
            return $query->getResultArray();
        }

        if ($kategori == 'kecamatan') {
            $builder = $this->db->table('rumahtangga');
            $builder->join('bloksensus', 'bloksensus.id = rumahtangga.kodeBs', 'left');
            $builder->join('kecamatan', 'kecamatan.kecno = bloksensus.kecamatan', 'left');
            $builder->select('MIN(kodeBs) as kode_bs , bloksensus.kecamatan as kode_kecamatan , MIN(kecamatan.nama) as nama_kecamatan');
            $builder->select('COUNT(*) as jumlah_listing');
            $builder->groupBy('kode_kecamatan');
            $builder->orderBy('jumlah_listing', 'DESC');
            $query = $builder->get();
            return $query->getResultArray();
        }

        if ($kategori == 'kelurahan') {
            $builder = $this->db->table('rumahtangga');
            $builder->join('bloksensus', 'bloksensus.id = rumahtangga.kodeBs', 'left');
            $builder->join('desa', 'desa.kecno = bloksensus.kecamatan AND desa.desano  = bloksensus.kelurahandesa', 'left');
            $builder->select('MIN(kodeBs) as kode_bs  , MIN(bloksensus.kecamatan) as kode_kecamatan, bloksensus.kelurahandesa as kode_desa , MIN(desa.nama) as nama_desa');
            $builder->groupBy('kode_desa');
            $builder->select('COUNT(*) as jumlah_listing');
            $builder->orderBy('jumlah_listing', 'DESC');
            $query = $builder->get();
            return $query->getResultArray();
        }
    }

    public function getDetailListing($kategori, $kode, $kode2 = null)
    {
        $builder = $this->db->table('rumahtangga');
        $builder->join('bloksensus', 'id = rumahtangga.kodeBs', 'left');
        $builder->join('kabupaten', 'kabno = bloksensus.kabupaten', 'left');
        $builder->join('kecamatan', 'kecamatan.kecno = bloksensus.kecamatan', 'left');
        $builder->join('desa', 'desa.kecno = bloksensus.kecamatan AND desano = bloksensus.kelurahandesa', 'left');
        $builder->join($this->dbMonitoring->getDatabase() . '.posisi_pcl', $this->dbMonitoring->getDatabase() . '.posisi_pcl.nim = bloksensus.nim', 'left');
        if ($kategori == 'blok_sensus') {
            $builder->select('rumahtangga.*, bloksensus.*, kabupaten.nama as nama_kabupaten, kecamatan.nama as nama_kecamatan, desa.desano as nomor_desa, desa.nama as nama_desa, posisi_pcl.nim as nim_pcl, posisi_pcl.nama as nama_pcl');
            $builder->where('kodeBs', $kode);
        }
        if ($kategori == 'kecamatan') {
            $builder->select('rumahtangga.*, bloksensus.id, bloksensus.kabupaten, bloksensus.kecamatan, bloksensus.nama, bloksensus.nim, bloksensus.status, kabupaten.kabno as nomor_kabupaten, kabupaten.nama as nama_kabupaten, kecamatan.kecno as nomor_kecamatan, kecamatan.nama as nama_kecamatan, posisi_pcl.nim as nim_pcl, posisi_pcl.nama as nama_pcl');
            $builder->where('kecamatan.kecno', $kode);
        }
        if ($kategori == 'kelurahandesa' && $kode2 != null) {
            $builder->select('rumahtangga.*, bloksensus.id, bloksensus.kabupaten, bloksensus.kecamatan, bloksensus.nama, bloksensus.kelurahandesa, bloksensus.nim, bloksensus.status, desa.kecno as nomor_kecamatan, kecamatan.nama as nama_kecamatan, desa.desano as nomor_desa, desa.nama as nama_desa, posisi_pcl.nim as nim_pcl, posisi_pcl.nama as nama_pcl');
            $builder->where('desa.desano', $kode2);
            $builder->where('desa.kecno', $kode);
        }
        $query = $builder->get()->getResultArray();
        return $query;
    }

    public function getPclByNim($nim)
    {
        $builder = $this->dbMonitoring->table('posisi_pcl');
        $builder->select('nama');
        $builder->where('nim', $nim);
        $query = $builder->get();
        return $query->getResultArray();
    }

}
