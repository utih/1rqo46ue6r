<?php

namespace App\Models\Riset\Riset4;

use CodeIgniter\Model;

class Riset4MonitoringTimModel extends Model
{
    protected $db;
    protected $dbMonitoring;
    protected $dbSikoko;

    public function __construct()
    {
        $this->db = \Config\Database::connect('riset4');
        $this->dbMonitoring = \Config\Database::connect('pkl62monitoring');
        $this->dbSikoko = \Config\Database::connect('pkl62sikoko');
    }

    public function listTim()
    {
        $builder = $this->dbMonitoring->table('posisi_pcl');
        $builder->select('id_tim, lokus');
        $builder->like('nim', '2', 'after');
        $builder->where('id_tim >= 95 AND id_tim <= 125');
        $builder->groupBy('id_tim');
        $builder->orderBy('id_tim', 'ASC');
        $query = $builder->get();
        $query = $query->getResultArray();
        // return $query->getResultArray();

        // BUILDER 2 GET ALL MAHASISWA
        $builder2 = $this->dbMonitoring->table('posisi_pcl');
        $builder2->select('nim, nama, id_tim, lokus');
        $builder2->like('nim', '2', 'after');
        $builder2->where('id_tim >= 95 AND id_tim <= 125');
        $builder2->orderBy('id_tim ASC, nama ASC');
        $query = $builder2->get();
        $query = $query->getResultArray();

        $builder->unionAll($builder2)->get();
        return $query;
    }

    public function listingByTim($tim)
    {
        $db2 = $this->dbMonitoring->getDatabase();
        $builder = $this->db->table('usahapariwisata');
        $builder->join('bloksensus', 'bloksensus.id = usahapariwisata.kodeBs', 'left');
        $builder->join($db2 . '.posisi_pcl', $db2 . '.posisi_pcl.nim = bloksensus.nim', 'left');
        $builder->where('posisi_pcl.id_tim', $tim);
        $builder->select('usahapariwisata.kodeBs, posisi_pcl.nama, posisi_pcl.nim, COUNT(*) as jumlah_listing');
        $builder->groupBy('kodeBs');
        $builder->orderBy('jumlah_listing', 'DESC');
        $query = $builder->get();
        return $query->getResultArray();
    }

    public function sampelByTim($tim)
    {
        // GET JUMLAH SAMPEL
        $db2 = $this->dbMonitoring->getDatabase();
        $builder = $this->db->table('datast');
        $builder->join('usahapariwisata', 'usahapariwisata.kodeUUP = datast.kodeUUP', 'left');
        $builder->join('bloksensus', 'bloksensus.id = usahapariwisata.kodeBs', 'left');
        $builder->join($db2 . '.posisi_pcl', $db2 . '.posisi_pcl.nim = bloksensus.nim', 'left');
        $builder->where('posisi_pcl.id_tim', $tim);
        $builder->select('usahapariwisata.kodeBs, posisi_pcl.nama, posisi_pcl.nim, COUNT(*) as jumlah_sampel');
        $builder->groupBy('kodeBs');
        $builder->orderBy('jumlah_sampel', 'DESC');
        $query = $builder->get();
        $query = $query->getResultArray();
        // return $query;

        // GET JUMLAH LISTING
        $builder2 = $this->db->table('usahapariwisata');
        $builder2->join('bloksensus', 'bloksensus.id = usahapariwisata.kodeBs', 'left');
        $builder2->join($db2 . '.posisi_pcl', $db2 . '.posisi_pcl.nim = bloksensus.nim', 'left');
        $builder2->where('posisi_pcl.id_tim', $tim);
        $builder2->select('usahapariwisata.kodeBs, posisi_pcl.nama, posisi_pcl.nim, COUNT(*) as jumlah_listing');
        $builder2->groupBy('kodeBs');
        $builder2->orderBy('jumlah_listing', 'DESC');
        $query2 = $builder2->get();
        $query2 = $query2->getResultArray();
        // return $query2;

        // COMBINE
        for ($i = 0; $i < count($query); $i++) {
            for ($j = 0; $j < count($query2); $j++) {
                if ($query[$i]['kodeBs'] == $query2[$j]['kodeBs']) {
                    $query[$i]['jumlah_listing'] = $query2[$j]['jumlah_listing'];
                }
            }
        }
        return $query;
    }

    public function pmlByTim($tim)
    {
        $db2 = $this->dbSikoko->getDatabase();
        $builder = $this->dbMonitoring->table('timpencacah');
        $builder->where('timpencacah.id_tim', $tim);
        $builder->join('posisi_pcl', 'posisi_pcl.nim = timpencacah.nim_koor', 'left');
        $builder->join($db2 . '.mahasiswa', $db2 . '.mahasiswa.nim = timpencacah.nim_koor', 'left');
        $builder->select('timpencacah.*, posisi_pcl.*, mahasiswa.foto');
        $query = $builder->get();
        return $query->getResultArray();
    }

    public function pplByTim($tim)
    {
        $db2 = $this->dbSikoko->getDatabase();
        $builder = $this->dbMonitoring->table('posisi_pcl');
        $builder->where('posisi_pcl.id_tim', $tim);
        $builder->join('timpencacah', 'timpencacah.nim_koor = posisi_pcl.nim', 'left');
        $builder->where('nama_tim IS NULL');
        $builder->join($db2 . '.mahasiswa', $db2 . '.mahasiswa.nim = posisi_pcl.nim', 'left');
        $builder->select('timpencacah.*, posisi_pcl.*, mahasiswa.foto');
        $builder->like('posisi_pcl.nim', '2', 'after');
        $query = $builder->get();
        return $query->getResultArray();
    }
}