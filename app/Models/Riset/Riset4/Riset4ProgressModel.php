<?php

namespace App\Models\Riset\Riset4;

use CodeIgniter\Model;

class Riset4ProgressModel extends Model
{
    protected $db;
    protected $dbMonitoring;
    protected $agg;

    public function __construct()
    {
        $this->db = \Config\Database::connect('riset4');
        $this->dbMonitoring = \Config\Database::connect('pkl62monitoring');
        $this->agg = \Config\Database::connect('aggregate');
    }

    // CATATAN : getAllKecamatan dan getAllKeldes ini hanya menampilkan yang telah di datast
    public function getAllKecamatan()
    {
        // BENTUK TABEL SAMPEL TERLEBIH DAHULU
        $builder = $this->db->table('datast');
        $builder->join('usahapariwisata', 'usahapariwisata.kodeUUP = datast.kodeUUP', 'left');
        $builder->join('bloksensus', 'bloksensus.id = usahapariwisata.kodeBs', 'left');
        $builder->join('kecamatan', 'kecamatan.kecno = bloksensus.kecamatan', 'left');

        // FILTER DISTINCT KECAMATAN
        $builder->select('kecamatan.kecno as kode_kecamatan , kecamatan.nama as nama_kecamatan');
        $builder->distinct('kecamatan.kecno');
        $builder->orderBy('nama_kecamatan', 'ASC');
        $query = $builder->get();
        return $query->getResultArray();
    }

    public function getAllKeldes($kecamatan)
    {
        // BENTUK TABEL SAMPEL TERLEBIH DAHULU
        $builder = $this->db->table('datast');
        $builder->join('usahapariwisata', 'usahapariwisata.kodeUUP = datast.kodeUUP', 'left');
        $builder->join('bloksensus', 'bloksensus.id = usahapariwisata.kodeBs', 'left');
        $builder->join('kecamatan', 'kecamatan.kecno = bloksensus.kecamatan', 'left');
        $builder->join('desa', 'desa.kecno = bloksensus.kecamatan AND desa.desano = bloksensus.kelurahandesa', 'left');

        // FILTER DISTINCT KELURAHAN BY KECAMATAN
        $builder->select('kecamatan.kecno as kode_kecamatan , kecamatan.nama as nama_kecamatan');
        $builder->select('desa.desano as kode_desa, desa.nama as nama_desa');
        $builder->distinct('desa.desano');
        $builder->where('desa.kecno', $kecamatan);
        $builder->orderBy('nama_desa ASC');
        $query = $builder->get();
        return $query->getResultArray();
    }

    public function getAllBS($kecamatan, $keldes)
    {
        // BENTUK TABEL SAMPEL TERLEBIH DAHULU
        $builder = $this->db->table('datast');
        $builder->join('usahapariwisata', 'usahapariwisata.kodeUUP = datast.kodeUUP', 'left');
        $builder->join('bloksensus', 'bloksensus.id = usahapariwisata.kodeBs', 'left');
        $builder->join('kecamatan', 'kecamatan.kecno = bloksensus.kecamatan', 'left');
        $builder->join('desa', 'desa.kecno = bloksensus.kecamatan AND desa.desano = bloksensus.kelurahandesa', 'left');

        // FILTER DISTINCT BS BY KELURAHAN , KECAMATAN
        $builder->select('kecamatan.kecno as kode_kecamatan , kecamatan.nama as nama_kecamatan');
        $builder->select('desa.desano as kode_desa, desa.nama as nama_desa');
        $builder->select('bloksensus.id as kode_bs');
        $builder->distinct('bloksensus.id');
        $builder->where('desa.kecno', $kecamatan);
        $builder->where('desa.desano', $keldes);
        $builder->orderBy('kode_bs', 'DESC');

        $query = $builder->get();
        return $query->getResultArray();
    }

    public function getNamaKecamatan($kecamatan)
    {
        $builder = $this->db->table('kecamatan');
        $builder->where('kecno', $kecamatan);
        $query = $builder->get();
        $result = $query->getResultArray();
        return $result[0]['nama'];
    }

    public function getNamaKeldes($kecamatan, $keldes)
    {
        $builder = $this->db->table('desa');
        $builder->where('desa.kecno', $kecamatan);
        $builder->where('desa.desano', $keldes);
        $query = $builder->get();
        $result = $query->getResultArray();
        return $result[0]['nama'];
    }

    public function sampel($kategori)
    {

        if ($kategori == 'bloksensus') {
            // GET SAMPLE COUNT
            $builder = $this->db->table('datast');
            $builder->join('usahapariwisata', 'usahapariwisata.kodeUUP = datast.kodeUUP', 'left');
            $builder->join('bloksensus', 'bloksensus.id = usahapariwisata.kodeBs', 'left');
            $builder->join($this->dbMonitoring->getDatabase() . '.posisi_pcl', $this->dbMonitoring->getDatabase() . '.posisi_pcl.nim = bloksensus.nim', 'left');
            $builder->select('datast.kodeBs as kode_bs, MIN(datast.kodeUUP) as kode_ruta, MIN(posisi_pcl.nim) as nim, MIN(posisi_pcl.nama) as nama_pcl');
            $builder->select('COUNT(*) as jumlah_sampel');
            $builder->groupBy('kode_bs');
            $builder->orderBy('kode_bs', 'DESC');
            $query = $builder->get();
            $query = $query->getResultArray();
            return $query;
        }

        if ($kategori == 'kecamatan') {
            // GET SAMPLE COUNT
            $builder = $this->db->table('datast');
            $builder->join('usahapariwisata', 'usahapariwisata.kodeUUP = datast.kodeUUP', 'left');
            $builder->join('bloksensus', 'bloksensus.id = usahapariwisata.kodeBs', 'left');
            $builder->join('kecamatan', 'kecamatan.kecno = bloksensus.kecamatan', 'left');
            $builder->select('MIN(datast.kodeBs) as kode_bs, kecamatan.kecno as kode_kecamatan, MIN(kecamatan.nama) as nama_kecamatan');
            $builder->select('COUNT(*) as jumlah_sampel');
            $builder->groupBy('kode_kecamatan');
            $builder->orderBy('kode_kecamatan', 'DESC');
            $query = $builder->get();
            $query = $query->getResultArray();
            return $query;
        }

        if ($kategori == 'kelurahan') {
            $builder = $this->db->table('datast');
            $builder->join('usahapariwisata', 'usahapariwisata.kodeUUP = datast.kodeUUP', 'left');
            $builder->join('bloksensus', 'bloksensus.id = usahapariwisata.kodeBs', 'left');
            $builder->join('desa', 'desa.kecno = bloksensus.kecamatan AND desa.desano = bloksensus.kelurahandesa', 'left');
            $builder->select('MIN(datast.kodeBs) as kode_bs, desa.kecno as kode_kecamatan, desa.desano as kode_desa, MIN(desa.nama) as nama_desa');
            $builder->select('COUNT(*) as jumlah_sampel');
            $builder->groupBy(['kode_kecamatan', 'kode_desa']);
            $builder->orderBy('kode_kecamatan ASC, kode_desa ASC');
            $query = $builder->get();
            $query = $query->getResultArray();
            return $query;
        }
    }

    public function terisi($kodeBS, $noUrut, $namaPemilikUUP)
    {
        // $builder = $this->agg->table('riset4v5_8_core2');

        $kprov = strtoupper(substr($kodeBS, 0, 2));
        $kkot  = strtoupper(substr($kodeBS, 2, 2));
        $kkec  = strtoupper(substr($kodeBS, 4, 3));
        $kkel  = strtoupper(substr($kodeBS, 7, 3));
        $kodbs = strtoupper(substr($kodeBS, 10, 4));

        // DB 1
        $builder = $this->agg->table('RISET4V5_8_CORE2');
        $builder->where('BLOK1_B101_KODEPROV', $kprov); // NYOCOKIN PAKE kodeprov
        $builder->where('BLOK1_B102_KODEKAB', $kkot); // NYOCOKIN PAKE kodekab
        $builder->where('BLOK1_B103_KODEKEC', $kkec); // NYOCOKIN PAKE kodekec
        $builder->where('BLOK1_B104_KODEDSKEL', $kkel); // NYOCOKIN PAKE kodekel
        $builder->where('BLOK1_B105', $kodbs); // NYOCOKIN PAKE kodeblok
        $builder->where('BLOK1_B106', $noUrut); // NYOCOKIN PAKE kodeblok
        $query = $builder->get();
        $isian = $query->getResultArray();
        if (empty($isian)) {
            return false;
        }
        return true;
    }

    public function progress_all()  // tingkat lokus
    {
        // 1ST : BUILDER COUNT SAMPEL : SEMUA
        $builder = $this->db->table('datast');
        $builder->join('usahapariwisata', 'usahapariwisata.kodeUUP = datast.kodeUUP', 'left');
        $builder->join('bloksensus', 'bloksensus.id = usahapariwisata.kodeBs', 'left');

        $query = $builder->get();
        $sampel = $query->getResultArray();
        $sampel_count = count($sampel);
        // return $sampel;

        if ($sampel_count == 0) {
            return null;
        }

        // 2ND : KITA CEK SCR MANUAL. APAKAH BELIAU INI TERISI APA TIDAK
        $sampel_terisi = 0;
        foreach ($sampel as $s) {
            $d1 = $s['kodeBs'];
            $d2 = $s['noUrutUUP'];
            $d3 = $s['namaPemilikUUP'];
            if ($this->terisi($d1, $d2, $d3)) {
                $sampel_terisi++;
            }
        }

        //3RD : RETURN HASIL PEMBAGIANNYA
        $nilai = round($sampel_terisi / $sampel_count * 100, 3);
        if ($nilai > 100) {
            $nilai = 100;
        }

        $data = [
            'semua' => $sampel_count,
            'terisi' => $sampel_terisi,
            'nilai' => $nilai
        ];
        return $data;
    }

    public function progress_kecamatan($kecamatan)  // tingkat kecamatan
    {
        // 1ST : BUILDER COUNT SAMPEL : KECAMATAN
        $builder = $this->db->table('datast');
        $builder->join('usahapariwisata', 'usahapariwisata.kodeUUP = datast.kodeUUP', 'left');
        $builder->join('bloksensus', 'bloksensus.id = usahapariwisata.kodeBs', 'left');
        $builder->join('kecamatan', 'kecamatan.kecno = bloksensus.kecamatan', 'left');
        $builder->where('kecamatan.kecno', $kecamatan);

        $query = $builder->get();
        $sampel = $query->getResultArray();
        $sampel_count = count($sampel);
        // return $sampel;

        if ($sampel_count == 0) {
            return null;
        }

        // 2ND : KITA CEK SCR MANUAL. APAKAH BELIAU INI TERISI APA TIDAK
        $sampel_terisi = 0;
        foreach ($sampel as $s) {
            $d1 = $s['kodeBs'];
            $d2 = $s['noUrutUUP'];
            $d3 = $s['namaPemilikUUP'];
            if ($this->terisi($d1, $d2, $d3)) {
                $sampel_terisi++;
            }
        }

        //3RD : RETURN HASIL PEMBAGIANNYA
        $nilai = round($sampel_terisi / $sampel_count * 100, 3);
        if ($nilai > 100) {
            $nilai = 100;
        }

        $data = [
            'semua' => $sampel_count,
            'terisi' => $sampel_terisi,
            'nilai' => $nilai
        ];
        return $data;
    }

    public function progress_keldes($kecamatan, $keldes)  // tingkat desa/kelurahan
    {
        // 1ST : BUILDER COUNT SAMPEL : DESKEL
        $builder = $this->db->table('datast');
        $builder->join('usahapariwisata', 'usahapariwisata.kodeUUP = datast.kodeUUP', 'left');
        $builder->join('bloksensus', 'bloksensus.id = usahapariwisata.kodeBs', 'left');
        $builder->join('kecamatan', 'kecamatan.kecno = bloksensus.kecamatan', 'left');
        $builder->join('desa', 'desa.kecno = bloksensus.kecamatan AND desa.desano = bloksensus.kelurahandesa', 'left');

        $builder->where('kecamatan.kecno', $kecamatan);
        $builder->where('desa.desano', $keldes);

        $query = $builder->get();
        $sampel = $query->getResultArray();
        $sampel_count = count($sampel);
        // return $sampel;

        if ($sampel_count == 0) {
            return null;
        }

        // 2ND : KITA CEK SCR MANUAL. APAKAH BELIAU INI TERISI APA TIDAK
        $sampel_terisi = 0;
        foreach ($sampel as $s) {
            $d1 = $s['kodeBs'];
            $d2 = $s['noUrutUUP'];
            $d3 = $s['namaPemilikUUP'];
            if ($this->terisi($d1, $d2, $d3)) {
                $sampel_terisi++;
            }
        }

        //3RD : RETURN HASIL PEMBAGIANNYA
        $nilai = round($sampel_terisi / $sampel_count * 100, 3);
        if ($nilai > 100) {
            $nilai = 100;
        }

        $data = [
            'semua' => $sampel_count,
            'terisi' => $sampel_terisi,
            'nilai' => $nilai
        ];
        return $data;
    }

    public function progress_bs($bloksensus)  // tingkat blok sensus
    {
        // 1ST : BUILDER COUNT SAMPEL : DESKEL
        $builder = $this->db->table('datast');
        $builder->join('usahapariwisata', 'usahapariwisata.kodeUUP = datast.kodeUUP', 'left');
        $builder->join('bloksensus', 'bloksensus.id = usahapariwisata.kodeBs', 'left');

        $builder->where('bloksensus.id', $bloksensus);

        $query = $builder->get();
        $sampel = $query->getResultArray();
        $sampel_count = count($sampel);
        // return $sampel;

        if ($sampel_count == 0) {
            return null;
        }

        // 2ND : KITA CEK SCR MANUAL. APAKAH BELIAU INI TERISI APA TIDAK
        $sampel_terisi = 0;
        foreach ($sampel as $s) {
            $d1 = $s['kodeBs'];
            $d2 = $s['noUrutUUP'];
            $d3 = $s['namaPemilikUUP'];
            if ($this->terisi($d1, $d2, $d3)) {
                $sampel_terisi++;
            }
        }

        //3RD : RETURN HASIL PEMBAGIANNYA
        $nilai = round($sampel_terisi / $sampel_count * 100, 3);
        if ($nilai > 100) {
            $nilai = 100;
        }

        $data = [
            'semua' => $sampel_count,
            'terisi' => $sampel_terisi,
            'nilai' => $nilai
        ];
        return $data;
    }

    public function progress_pertim($tim)  // tingkat tim
    {
        // 1ST : BUILDER COUNT SAMPEL : PERTIM

        // GET JUMLAH SAMPEL
        $db2 = $this->dbMonitoring->getDatabase();
        $builder = $this->db->table('datast');
        $builder->join('usahapariwisata', 'usahapariwisata.kodeUUP = datast.kodeUUP', 'left');
        $builder->join('bloksensus', 'bloksensus.id = usahapariwisata.kodeBs', 'left');
        $builder->join($db2 . '.posisi_pcl', $db2 . '.posisi_pcl.nim = bloksensus.nim', 'left');
        $builder->where('posisi_pcl.id_tim', $tim);

        $query = $builder->get();
        $sampel = $query->getResultArray();
        $sampel_count = count($sampel);
        // return $sampel;

        //check empty :
        if ($sampel_count == 0) {
            return null;
        }

        // 2ND : KITA CEK SCR MANUAL. APAKAH BELIAU INI TERISI APA TIDAK
        $sampel_terisi = 0;
        foreach ($sampel as $s) {
            $d1 = $s['kodeBs'];
            $d2 = $s['noUrutUUP'];
            $d3 = $s['namaPemilikUUP'];
            if ($this->terisi($d1, $d2, $d3)) {
                $sampel_terisi++;
            }
        }

        //3RD : RETURN HASIL PEMBAGIANNYA
        $nilai = round($sampel_terisi / $sampel_count * 100, 3);
        if ($nilai > 100) {
            $nilai = 100;
        }

        $data = [
            'semua' => $sampel_count,
            'terisi' => $sampel_terisi,
            'nilai' => $nilai
        ];
        return $data;
    }
}
