<?php

use App\Models\Riset\Riset12\Riset12ProgressModel;
use App\Models\Riset\Riset1\Riset1ProgressModel;

$this->extend('layout/template'); ?>
<?= $this->section('content'); ?>


<!-- WRITE CONTENT HERE -->
<div class="row">
    <div class="col-12 col-lg-12">
        <div class="card text-center" style="background-color: #f0dee3;">
            <div class="card-body">
                <h3>Selamat Datang, <span class="fw-bolder"><?= (session()->has('nama')) ? session()->get('nama') : 'User' ?></span>
                </h3>
                <h5 class="card-text">
                    di Web Monitoring PKL D-IV 62
                </h5>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12 col-lg-7">
        <div class="card text-center" style="background-color: #4b395f;">


            <div class="card-body">

                <div id="carousel-interval" class="carousel slide" data-bs-ride="carousel" data-bs-interval="4000">
                    <ol class="carousel-indicators">
                        <li data-bs-target="#carousel-interval" data-bs-slide-to="0" class="active"></li>
                        <li data-bs-target="#carousel-interval" data-bs-slide-to="1"></li>

                    </ol>

                    <div class="carousel-inner" role="listbox">

                        <div class="carousel-item active">

                            <!-- eligibel -->
                            <div class="card-body">

                                <h3 class="text-white mb-2">Summary Tiap Riset </h3>

                                <!-- RISET 1 MPD -->
                                <div class="row">

                                    <div class="col-3 d-flex flex-column align-items-center">
                                        <h4 class="text-white">Riset 1</h4>
                                        <!-- <div class="profile-image-wrapper "> -->
                                        <div class="profile-image">
                                            <div class="avatar border border-5">
                                                <img src="assets/images/logo/riset1.png" alt="Profile Picture" height="70" width="70" />
                                            </div>
                                            <h6 class="text-white"> Kota Surabaya </h6>
                                        </div>
                                        <!-- </div> -->
                                    </div>

                                    <div class="col-9 text-white text-start d-flex flex-column justify-content-center">
                                        <small>Kajian Pemanfaatan Mobile Positioning Data (MPD) dalam Menunjang Statistik Pariwisata</small>
                                        <div class="progress-wrapper text-start mt-1">

                                            <?php
                                            $pro =  $listing1 / 10000 * 100;
                                            ?>

                                            <!-- CEK SUDAH TERISI ATAU NGGA -->
                                            <?php

                                            // AMBIL SEMUA SAMPEL DULU
                                            $semua = $li1->listing('all');
                                            $model = new Riset1ProgressModel;
                                            $totalElig = 0;
                                            foreach ($semua as $s) {
                                                $d1 = $s['alamatDomisili'];
                                                $d2 = $s['namaPemberiInformasi'];

                                                if ($model->eligibel_ruta($d1, $d2)) {
                                                    $totalElig++;
                                                }
                                            }

                                            ?>

                                            <div class="row">
                                                <div class="col-6">
                                                    <h3 class="mt-1 text-white text-center"> <?= $listing1 - 2 ?></h3>
                                                    <h6 class="text-white text-center">Sampel Terpilih</h6>
                                                </div>
                                                <div class="col-6">
                                                    <h3 class="mt-1 text-white text-center"> <?= $totalElig; ?> </h3>
                                                    <h6 class="text-white text-center">Total Eligible</h6>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <!-- RISET 1 2 : Integrasi -->
                                <div class="row mt-1">
                                    <div class="col-3 d-flex flex-column align-items-center">
                                        <h4 class="text-white">Riset 1 & 2</h4>
                                        <!-- <div class="profile-image-wrapper"> -->
                                        <div class="profile-image">
                                            <div class="avatar border border-5">
                                                <img src="assets/images/logo/riset2.png" alt="Profile Picture" height="70" width="70" />
                                            </div>
                                            <h6 class="text-white"> Kota Malang </h6>
                                        </div>
                                        <!-- </div> -->
                                    </div>
                                    <div class="col-9 text-white text-start d-flex flex-column justify-content-center">
                                        <small>Pilot Survei Wisatawan Nusantara</small>
                                        <div class="progress-wrapper text-start mt-1">


                                            <?php
                                            $pro =  $listing2 / 10000 * 100;
                                            ?>

                                            <!-- CEK SUDAH TERISI ATAU NGGA -->
                                            <?php

                                            // AMBIL SEMUA SAMPEL DULU
                                            $semua = $li2->listing('all');
                                            $totalElig = 0;

                                            $elig1 = 0;
                                            $elig2 = 0;
                                            $elig3 = 0;

                                            $katakata = "";

                                            foreach ($semua as $s) {
                                                $d1 = $s['TTPWDalam'];
                                                $d2 = $s['TTPWLuar'];

                                                if ($d1 == '1' or $d2 == '1') {
                                                    $totalElig++;
                                                }

                                                // buat 1 
                                                if (($d1 == '1') and ($d2 == '2')) {
                                                    $elig1++;
                                                }

                                                //buat 2
                                                if (($d1 == '2') and ($d2 == '1')) {
                                                    $elig2++;
                                                }

                                                //buat 3
                                                if (($d1 == '1') and ($d2 == '1')) {
                                                    $elig3++;
                                                }

                                                $katakata = "Riset 1 (TTPWLuar) : " . $elig2 . " Ruta; Riset 2 (TTPWDalam) : " . $elig1 . " Ruta; Riset 1&2 : " . $elig3 . " Ruta;";
                                            }

                                            ?>

                                            <div class="row">
                                                <div class="col-4">
                                                    <h3 class="mt-1 text-white text-center">
                                                        <?php
                                                        $listing2 = number_format($listing2, 0, ',', '.');
                                                        ?>

                                                        <?= $listing2; ?></h3>
                                                    <h6 class="text-white text-center">Total Listing</h6>
                                                </div>
                                                <div class="col-4">

                                                    <?php
                                                    $totalElig = number_format($totalElig, 0, ',', '.');
                                                    ?>

                                                    <h3 class="mt-1 text-white text-center" data-bs-toggle="tooltip" data-bs-placement="bottom" title="<?= $katakata; ?>"> <?= $totalElig; ?> </h3>

                                                    <h6 class="text-white text-center">Total Eligible</h6>
                                                </div>
                                                <div class="col-4">
                                                    <h3 class="mt-1 text-white text-center"> 
                                                    <?php 
if($progress2 == null){
    echo 'XXXX';
}
else{
    echo $progress2['semua']; 
}
?>
                                                    </h3>
                                                    <h6 class="text-white text-center">Sampel Terpilih</h6>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <!-- RISET 3 -->
                                <div class="row mt-1">
                                    <div class="col-3 d-flex flex-column align-items-center">
                                        <h4 class="text-white">Riset 3</h4>
                                        <!-- <div class="profile-image-wrapper"> -->
                                        <div class="profile-image">
                                            <div class="avatar border border-5">
                                                <img src="assets/images/logo/riset3.png" alt="Profile Picture" height="70" width="70" />
                                            </div>
                                            <h6 class="text-white"> Kabupaten Malang </h6>

                                        </div>
                                        <!-- </div> -->
                                    </div>
                                    <div class="col-9 text-white text-start d-flex flex-column justify-content-center">
                                        <small>Tenaga Kerja di Industri Pariwisata</small>
                                        <div class="progress-wrapper text-start mt-1">

                                            <?php
                                            $pro =  $listing3 / 10000 * 100;
                                            ?>

                                            <!-- CEK SUDAH TERISI ATAU NGGA -->
                                            <?php

                                            // AMBIL SEMUA SAMPEL DULU
                                            $semua = $li3->listing('all');
                                            $totalElig = 0;

                                            $elig1 = 0;
                                            $elig2 = 0;
                                            $elig3 = 0;

                                            $katakata = "";

                                            foreach ($semua as $s) {
                                                $d1 = $s['kodeEligible'];

                                                if ($d1 != '0') {
                                                    $totalElig++;
                                                }

                                                // buat 1 Pernah Bekerja
                                                if (($d1 == '1')) {
                                                    $elig1++;
                                                }

                                                //buat 2 Sedang Bekerja
                                                if (($d1 == '2')) {
                                                    $elig2++;
                                                }

                                                //buat 3 Sedang dan Pernah Bekerja
                                                if (($d1 == '3')) {
                                                    $elig3++;
                                                }

                                                $katakata = "Pernah Bekerja : " . $elig1 . " Ruta; Sedang Bekerja: " . $elig2 . " Ruta; Sedang&Pernah Bekerja: " . $elig3 . " Ruta;";
                                            }

                                            ?>


                                            <div class="row">
                                                <div class="col-4">
                                                    <?php
                                                    $listing3 = number_format($listing3, 0, ',', '.');
                                                    ?>

                                                    <h3 class="mt-1 text-white text-center"> <?= $listing3; ?></h3>
                                                    <h6 class="text-white text-center">Total Listing</h6>
                                                </div>
                                                <div class="col-4">


                                                    <?php
                                                    $totalElig = number_format($totalElig, 0, ',', '.');
                                                    ?>

                                                    <h3 class="mt-1 text-white text-center" data-bs-toggle="tooltip" data-bs-placement="bottom" title="<?= $katakata; ?>"> <?= $totalElig; ?> </h3>


                                                    <h6 class="text-white text-center">Total Eligible</h6>
                                                </div>
                                                <div class="col-4">
                                                    <h3 class="mt-1 text-white text-center"> 
                                                    <?php 
if($progress3 == null){
    echo 'XXXX';
}
else{
    echo $progress3['semua']; 
}
?>
                                                    </h3>
                                                    <h6 class="text-white text-center">Sampel Terpilih</h6>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                </div>

                                <!-- RISET 4 -->
                                <div class="row mt-1">
                                    <div class="col-3 d-flex flex-column align-items-center">
                                        <h4 class="text-white">Riset 4</h4>
                                        <!-- <div class="profile-image-wrapper"> -->
                                        <div class="profile-image">
                                            <div class="avatar border border-5">
                                                <img src="assets/images/logo/riset4.png" alt="Profile Picture" height="70" width="70" />
                                            </div>
                                            <h6 class="text-white"> Kota Batu </h6>

                                        </div>
                                        <!-- </div> -->
                                    </div>
                                    <div class="col-9 text-white text-start d-flex flex-column justify-content-center ">
                                        <small>Pemanfaatan TIK pada Sektor Pariwisata dan Ekonomi Kreatif</small>
                                        <div class="progress-wrapper text-start mt-1">

                                            <?php
                                            $pro =  $listing4 / 10000 * 100;
                                            ?>

                                            <!-- CEK SUDAH TERISI ATAU NGGA -->
                                            <?php

                                            // AMBIL SEMUA SAMPEL DULU
                                            $semua = $li4->listing('all');
                                            $totalElig = 0;

                                            $elig1 = 0;
                                            $elig2 = 0;

                                            $elig1a = 0;
                                            $elig1b = 0;
                                            $elig1c = 0;

                                            $elig2a = 0;
                                            $elig2b = 0;
                                            $elig2c = 0;

                                            $katakata = "";
                                            foreach ($semua as $s) {
                                                $d1 = $s['skalaUsaha'];
                                                $d2 = $s['jenisUUP'];


                                                if ($d1 == '1' || $d1 == '2') {
                                                    $totalElig++;
                                                }


                                                // buat 1 Mikro
                                                if (($d1 == '1')) {
                                                    $elig1++;

                                                    if ($d2 == '1') {
                                                        $elig1a++;
                                                    } else if ($d2 == '2') {
                                                        $elig1b++;
                                                    } else if ($d2 == '3') {
                                                        $elig1c++;
                                                    }
                                                }

                                                //buat 2 Kecil
                                                if (($d1 == '2')) {
                                                    $elig2++;

                                                    if ($d2 == '1') {
                                                        $elig2a++;
                                                    } else if ($d2 == '2') {
                                                        $elig2b++;
                                                    } else if ($d2 == '3') {
                                                        $elig2c++;
                                                    }
                                                }


                                                $katakata = "Usaha Mikro : " . $elig1 . " UUP (JTW=" . $elig1a . ", JPMM=". $elig1b .", PA=". $elig1c ."); Usaha Kecil : " . $elig2 . " UUP (JTW=" . $elig2a . ", JPMM=". $elig2b .", PA=". $elig2c .");";
                                            }

                                            ?>



                                            <div class="row">
                                                <div class="col-4">
                                                    <?php
                                                    $listing4 = number_format($listing4, 0, ',', '.');
                                                    ?>

                                                    <h3 class="mt-1 text-white text-center"> <?= $listing4; ?></h3>
                                                    <h6 class="text-white text-center">Total Listing</h6>
                                                </div>
                                                <div class="col-4">

                                                    <?php
                                                    $totalElig = number_format($totalElig, 0, ',', '.');
                                                    ?>

                                                    <h3 class="mt-1 text-white text-center" data-bs-toggle="tooltip" data-bs-placement="bottom" title="<?= $katakata; ?>"> <?= $totalElig; ?> </h3>

                                                    <h6 class="text-white text-center">Total Eligible</h6>
                                                </div>
                                                <div class="col-4">
                                                    <h3 class="mt-1 text-white text-center"> 
                                                    <?php 
if($progress4 == null){
    echo 'XXXX';
}
else{
    echo $progress4['semua']; 
}
?>
                                                    </h3>
                                                    <h6 class="text-white text-center">Sampel Terpilih</h6>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- eligibel -->

                        </div>

                        <div class="carousel-item">

                            <!-- eligibel -->
                            <div class="card-body">

                                <h3 class="text-white mb-2">Progres Pengisian Kuesioner</h3>

                                <!-- RISET 1 MPD -->
                                <div class="row">

                                    <div class="col-9 text-white text-start d-flex flex-column justify-content-center">
                                        <small>Kajian Pemanfaatan Mobile Positioning Data (MPD) dalam Menunjang Statistik Pariwisata</small>
                                        <div class="progress-wrapper text-start mt-1">

                                            <!-- ADA -->
                                            <?php if ($progress1 != null) : ?>
                                                <h5 class="text-white">Progres Pengisian&hellip; <?= $progress1['nilai']; ?>% (<?= $progress1['terisi'];; ?> selesai dari <?= $progress1['semua']; ?>
                                                    sampel)</h5>
                                                <div class="progress progress-bar-primary" style="background-color: white;">
                                                    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="80" aria-valuemin="80" aria-valuemax="100" style="width: <?= $progress1['nilai']; ?>%"></div>
                                                </div>
                                                <a class="btn btn-sm btn-primary mt-1 float-start" href="/riset-1/progress-wilayah">Lihat Detail</a>
                                            <?php endif ?>

                                            <!-- TIDAK ADA -->
                                            <?php if ($progress1 == null) :   ?>
                                                <h5 class="text-white"> LOKUS BELUM MEMILIKI PROGRESS PENCACAHAN </h5>
                                                <div class="progress progress-bar-primary" style="background-color: white;">
                                                    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="80" aria-valuemin="80" aria-valuemax="100" style="width: 0%"></div>
                                                </div>
                                            <?php endif ?>

                                        </div>
                                    </div>

                                    <div class="col-3 d-flex flex-column align-items-center">
                                        <h4 class="text-white">Riset 1</h4>
                                        <!-- <div class="profile-image-wrapper "> -->
                                        <div class="profile-image">
                                            <div class="avatar border border-5">
                                                <img src="assets/images/logo/riset1.png" alt="Profile Picture" height="70" width="70" />
                                            </div>
                                            <h6 class="text-white"> Kota Surabaya </h6>
                                        </div>
                                        <!-- </div> -->
                                    </div>

                                </div>

                                <!-- RISET 1 2 : Integrasi -->
                                <div class="row mt-1">



                                    <div class="col-9 text-white text-start d-flex flex-column justify-content-center">
                                        <small>Pilot Survei Wisatawan Nusantara</small>
                                        <div class="progress-wrapper text-start mt-1">



                                            <!-- ADA -->
                                            <?php if ($progress2 != null) : ?>
                                                <h5 class="text-white">Progres Pengisian&hellip;(<?= $progress2['terisi'];; ?> selesai dari <?= $progress2['semua']; ?>
                                                    sampel)</h5>
                                                <div class="progress progress-bar-primary" style="background-color: white;">
                                                    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="80" aria-valuemin="80" aria-valuemax="100" style="width: <?= $progress2['nilai']; ?>%"></div>
                                                </div>
                                                <a class="btn btn-sm btn-primary mt-1 float-start" href="/riset-1-2/progress-wilayah">Lihat Detail</a>
                                            <?php endif ?>

                                            <!-- TIDAK ADA -->
                                            <?php if ($progress2 == null) :   ?>
                                                <h5 class="text-white"> LOKUS BELUM MEMILIKI PROGRESS PENCACAHAN </h5>
                                                <div class="progress progress-bar-primary" style="background-color: white;">
                                                    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="80" aria-valuemin="80" aria-valuemax="100" style="width: 0%"></div>
                                                </div>
                                            <?php endif ?>


                                        </div>
                                    </div>

                                    <div class="col-3 d-flex flex-column align-items-center">
                                        <h4 class="text-white">Riset 1 & 2</h4>
                                        <!-- <div class="profile-image-wrapper"> -->
                                        <div class="profile-image">
                                            <div class="avatar border border-5">
                                                <img src="assets/images/logo/riset2.png" alt="Profile Picture" height="70" width="70" />
                                            </div>
                                            <h6 class="text-white"> Kota Malang </h6>

                                        </div>
                                        <!-- </div> -->
                                    </div>

                                </div>

                                <!-- RISET 3 -->
                                <div class="row mt-1">



                                    <div class="col-9 text-white text-start d-flex flex-column justify-content-center">
                                        <small>Tenaga Kerja di Industri Pariwisata</small>
                                        <div class="progress-wrapper text-start mt-1">

                                            <!-- ADA -->
                                            <?php if ($progress3 != null) : ?>
                                                <h5 class="text-white">Progres Pengisian&hellip; <?= $progress3['nilai']; ?>% (<?= $progress3['terisi'];; ?> selesai dari <?= $progress3['semua']; ?>
                                                    sampel)</h5>
                                                <div class="progress progress-bar-primary" style="background-color: white;">
                                                    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="80" aria-valuemin="80" aria-valuemax="100" style="width: <?= $progress3['nilai']; ?>%"></div>
                                                </div>
                                                <a class="btn btn-sm btn-primary mt-1 float-start" href="/riset-3/progress-wilayah">Lihat Detail</a>
                                            <?php endif ?>

                                            <!-- TIDAK ADA -->
                                            <?php if ($progress3 == null) :   ?>
                                                <h5 class="text-white"> LOKUS BELUM MEMILIKI PROGRESS PENCACAHAN </h5>
                                                <div class="progress progress-bar-primary" style="background-color: white;">
                                                    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="80" aria-valuemin="80" aria-valuemax="100" style="width: 0%"></div>
                                                </div>
                                            <?php endif ?>


                                        </div>
                                    </div>

                                    <div class="col-3 d-flex flex-column align-items-center">
                                        <h4 class="text-white">Riset 3</h4>
                                        <!-- <div class="profile-image-wrapper"> -->
                                        <div class="profile-image">
                                            <div class="avatar border border-5">
                                                <img src="assets/images/logo/riset3.png" alt="Profile Picture" height="70" width="70" />
                                            </div>
                                            <h6 class="text-white"> Kabupaten Malang </h6>

                                        </div>
                                        <!-- </div> -->
                                    </div>
                                </div>

                                <!-- RISET 4 -->
                                <div class="row mt-1">


                                    <div class="col-9 text-white text-start d-flex flex-column justify-content-center ">
                                        <small>Pemanfaatan TIK pada Sektor Pariwisata dan Ekonomi Kreatif</small>
                                        <div class="progress-wrapper text-start mt-1">

                                            <!-- ADA -->
                                            <?php if ($progress4 != null) : ?>
                                                <h5 class="text-white">Progres Pengisian&hellip; <?= $progress4['nilai']; ?>% (<?= $progress4['terisi'];; ?> selesai dari <?= $progress4['semua']; ?>
                                                    sampel)</h5>
                                                <div class="progress progress-bar-primary" style="background-color: white;">
                                                    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="80" aria-valuemin="80" aria-valuemax="100" style="width: <?= $progress4['nilai']; ?>%"></div>
                                                </div>
                                                <a class="btn btn-sm btn-primary mt-1 float-start" href="/riset-4/progress-wilayah">Lihat Detail</a>
                                            <?php endif ?>

                                            <!-- TIDAK ADA -->
                                            <?php if ($progress4 == null) :   ?>
                                                <h5 class="text-white"> LOKUS BELUM MEMILIKI PROGRESS PENCACAHAN </h5>
                                                <div class="progress progress-bar-primary" style="background-color: white;">
                                                    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="80" aria-valuemin="80" aria-valuemax="100" style="width: 0%"></div>
                                                </div>
                                            <?php endif ?>

                                        </div>
                                    </div>

                                    <div class="col-3 d-flex flex-column align-items-center">
                                        <h4 class="text-white">Riset 4</h4>
                                        <!-- <div class="profile-image-wrapper"> -->
                                        <div class="profile-image">
                                            <div class="avatar border border-5">
                                                <img src="assets/images/logo/riset4.png" alt="Profile Picture" height="70" width="70" />
                                            </div>
                                            <h6 class="text-white"> Kota Batu </h6>

                                        </div>
                                        <!-- </div> -->
                                    </div>

                                </div>

                            </div>
                            <!-- eligibel -->

                        </div>

                    </div>

                </div>
            </div>

        </div>
    </div>
    <div class="col-12 col-lg-5">
        <div class="card card-developer-meetup">
            <div class="meetup-img-wrapper rounded-top text-center" style="background-color: #f0dee3;">
                <img class="img-fluid" src="assets/images/logo/aspirasi.png" alt="Meeting Pic" style="width: 20rem;" />
            </div>
            <div class="card-body">
                <div class="meetup-header d-flex justify-content-center">
                    <div class="meetup-day">
                        <!-- Tanggal hari ini -->
                        <h6>Hari ini</h6>
                        <?php setlocale(LC_ALL, 'IND'); ?>
                        <h6 class="mb-1">
                            <?php
                            $hari = date("D");

                            switch ($hari) {
                                case 'Sun':
                                    $hari_ini = "Minggu";
                                    break;

                                case 'Mon':
                                    $hari_ini = "Senin";
                                    break;

                                case 'Tue':
                                    $hari_ini = "Selasa";
                                    break;

                                case 'Wed':
                                    $hari_ini = "Rabu";
                                    break;

                                case 'Thu':
                                    $hari_ini = "Kamis";
                                    break;

                                case 'Fri':
                                    $hari_ini = "Jumat";
                                    break;

                                case 'Sat':
                                    $hari_ini = "Sabtu";
                                    break;

                                default:
                                    $hari_ini = "Tidak di ketahui";
                                    break;
                            }

                            echo ($hari_ini);
                            ?>

                        </h6>
                        <h5 class="mb-0"><strong><?= date("d/m/Y"); ?></strong></h5>

                    </div>
                    <div class="my-auto">

                        <div class="d-flex flex-row row">
                            <div class="col-2">
                                <i data-feather="calendar" class="avatar-icon font-medium-3 text-primary"></i>
                            </div>
                            <div class="col-10">
                                <h5>Mulai</h5>
                                <small class="mb-0">Selasa, 24 Januari 2023</small>
                            </div>
                        </div>
                        <div class="d-flex flex-row row mt-1">
                            <div class="col-2">
                                <i data-feather="calendar" class="avatar-icon font-medium-3 text-secondary"></i>
                            </div>
                            <div class="col-10">
                                <h5>Selesai</h5>
                                <small class="mb-0">Kamis, 02 Februari 2023</small>
                            </div>
                        </div>

                    </div>
                    <!-- <h4 class="card-title mb-25">Kegiatan Pencacahan Lapangan</h4> -->
                    <!-- <h5 class="card-text mb-0">Tersisa ... Hari Lagi</h5> -->
                </div>


                <div class="text-center ms-5 me-5">
                    <h4>Countdown Pencacahan</h4>
                    <div class="d-flex justify-content-evenly">
                        <div style=" margin:2px; border-radius:5px; ; color:white; background-color:#4b395f; display: inline-block;font-size: 0.8em;list-style-type: none;padding: 1em;text-transform: uppercase;font-weight:bold">
                            <span style="  display: block;  font-size: 2rem" id="days"></span>Hari
                        </div>
                        <div style=" margin:2px; border-radius:5px; ; color:white; background-color:#4b395f; display: inline-block;font-size: 0.8em;list-style-type: none;padding: 1em;text-transform: uppercase;font-weight:bold">
                            <span style="  display: block;  font-size: 2rem" id="hours"></span>Jam
                        </div>
                        <div style=" margin:2px; border-radius:5px; ; color:white; background-color:#4b395f; display: inline-block;font-size: 0.8em;list-style-type: none;padding: 1em;text-transform: uppercase;font-weight:bold">
                            <span style="  display: block;  font-size: 2rem" id="minutes"></span>Menit
                        </div>
                        <div style=" margin:2px; border-radius:5px; ; color:white; background-color:#4b395f; display: inline-block;font-size: 0.8em;list-style-type: none;padding: 1em;text-transform: uppercase;font-weight:bold">
                            <span style="  display: block;  font-size: 2rem" id="seconds"></span>Detik
                        </div>
                    </div>

                    <!-- <ul class="ml-0 pl-0">
                        <li style=" margin:2px; border-radius:5px; ; color:white; background-color:#4b395f; display: inline-block;font-size: 0.8em;list-style-type: none;padding: 1em;text-transform: uppercase;font-weight:bold"><span style="  display: block;  font-size: 2rem" id="days"></span>Hari</li>
                        <li style=" margin:2px; border-radius:5px; ; color:white; background-color:#4b395f; display: inline-block;font-size: 0.8em;list-style-type: none;padding: 1em;text-transform: uppercase;font-weight:bold"><span style="  display: block;  font-size: 2rem" id="hours"></span>Jam</li>
                        <li style=" margin:2px; border-radius:5px; ; color:white; background-color:#4b395f; display: inline-block;font-size: 0.8em;list-style-type: none;padding: 1em;text-transform: uppercase;font-weight:bold"><span style="  display: block;  font-size: 2rem" id="minutes"></span>Menit</li>
                        <li style=" margin:2px; border-radius:5px; ; color:white; background-color:#4b395f; display: inline-block;font-size: 0.8em;list-style-type: none;padding: 1em;text-transform: uppercase;font-weight:bold"><span style="  display: block;  font-size: 2rem" id="seconds"></span>Detik</li>
                    </ul> -->
                </div>
            </div>

        </div>
    </div>
</div>



<script>
    // Set the date we're counting down to
    var countDownDate = new Date("Feb 2, 2023 18:00:00").getTime();

    // Update the count down every 1 second
    var x = setInterval(function() {

        // Get today's date and time
        var now = new Date().getTime();

        // Find the distance between now and the count down date
        var distance = countDownDate - now;

        // Time calculations for days, hours, minutes and seconds
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        // Output
        document.getElementById("days").innerHTML = days;
        document.getElementById("hours").innerHTML = hours;
        document.getElementById("minutes").innerHTML = minutes;
        document.getElementById("seconds").innerHTML = seconds;

        // If the count down is over, write some text 
        if (distance < 0) {
            clearInterval(x);
            document.getElementById("days").innerHTML = 00;
            document.getElementById("hours").innerHTML = 00;
            document.getElementById("minutes").innerHTML = 00;
            document.getElementById("seconds").innerHTML = 00;
        }
    }, 1000);
</script>

<?php $this->endSection(); ?>