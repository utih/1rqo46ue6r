<?php $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>

<!-- WRITE CONTENT HERE -->

<div class="row match-height">
    <div class="col-12 col-lg-6">
        <div class="card card-profile mb-5">
            <div class="card-img-top d-flex align-items-center p-1" style="background-color: #4b395f;">
                <h4 class="text-white">Progres Keseluruhan</h4>
            </div>
            <div class="p-2">
                <!-- ... -->
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">Status</th>
                            <th scope="col">Jumlah</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th class="text-start">Belum Dicacah</th>
                            <td><?= $belum_dicacah; ?></td>
                        </tr>
                        <tr>
                            <th class="text-start">Selesai Dicacah</th>
                            <td><?= $selesai_dicacah; ?></td>
                        </tr>
                        <tr class="text-white" style="background-color: #4b395f;">
                            <td class="text-start">Total</td>
                            <td><?= $total_sampel; ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="col-12 col-lg-6">
        <div class="card card-profile mb-5">
            <div class="card-img-top d-flex align-items-center p-1" style="background-color: #4b395f;">
                <h4 class="text-white">Progres Menurut Blok Sensus</h4>
            </div>
            <div class="card-body">
                <!-- ... -->

            </div>
        </div>
    </div>

</div>

<div class="row match-height">

    <div class="col-12 col-lg-12">
        <div class="card card-profile mb-5">
            <div class="card-img-top d-flex align-items-center p-1" style="background-color: #4b395f;">
                <h4 class="text-white">Hasil Pencacahan</h4>
            </div>
            <div class="card-body">
                <!-- ... -->
            </div>
        </div>
    </div>

</div>



<?php $this->endSection(); ?>