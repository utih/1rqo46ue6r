<footer class="footer footer-static footer-light">
    <p class="clearfix mb-0 d-flex justify-content-center fw-bolder">
        <span class="float-md-start d-block d-md-inline-block mt-25" style="color: #ffffff">Copyright &copy; 2022 -
            <?= date('Y'); ?><a class="ms-25" style="color: #fff" href="#" target="_blank">, Web Monitoring PKL 62 D-IV
                Politeknik Statistika STIS</a>
            <span class="d-none d-sm-inline-block">, All Rights Reserved.</span>
        </span>
    </p>
</footer>