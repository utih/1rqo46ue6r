<?php

use App\Models\AuthModel;

$fotoModel = new AuthModel();
$fotoArr = $fotoModel->getFoto(session()->get('email'));
$urlFoto = "https://capi62.stis.ac.id/web-service-62/assets/img/";
$foto = "";

if (!empty($fotoArr)) {
    $foto = $fotoArr[0]['foto'];
}

?>

<nav class="header-navbar navbar navbar-expand-lg align-items-center floating-nav navbar-light navbar-shadow container-xxl" style="margin: 0;">
    <div class="navbar-container d-flex content">
        <div class="bookmark-wrapper d-flex align-items-center">
            <ul class="nav navbar-nav d-xl-none">
                <li class="nav-item"><a class="nav-link menu-toggle" href="#"><i class="ficon" data-feather="menu"></i></a></li>
            </ul>
            <ul class="nav navbar-nav">
                <div class="bookmark-input search-input">
                    <div class="bookmark-input-icon"><i data-feather="search"></i></div>
                    <input class="form-control input" type="text" placeholder="Bookmark" tabindex="0" data-search="search">
                    <ul class="search-list search-list-bookmark"></ul>
                </div>
                </li>
            </ul>
        </div>
        <ul class="nav navbar-nav align-items-center ms-auto">
            <!-- <li class="nav-item">
                <a class="nav-link" id="dropdown-flag" href="#" data-bs-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    <i class="flag-icon flag-icon-id"></i>
                    <span class="selected-language">Indonesia</span>
                </a>
            </li> -->
            <li class="nav-item d-none d-lg-block"><a class="nav-link nav-link-style"><i class="ficon" data-feather="moon"></i></a></li>
            <li class="nav-item nav-search"><a class="nav-link nav-link-search"><i class="ficon" data-feather="search"></i></a>
                <div class="search-input">
                    <div class="search-input-icon"><i data-feather="search"></i></div>
                    <input class="form-control input" type="text" placeholder="Cari sesuatu disini..." tabindex="-1" data-search="search">
                    <div class="search-input-close"><i data-feather="x"></i></div>
                    <ul class="search-list search-list-main"></ul>
                </div>
            </li>
            <li class="nav-item dropdown dropdown-user"><a class="nav-link dropdown-toggle dropdown-user-link" id="dropdown-user" href="#" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <div class="user-nav d-sm-flex d-none"><span class="user-name fw-bolder"><?= (session()->has('nama')) ? session()->get('nama') : '' ?></span><span class="user-status">
                            <?= (session()->has('jenis')) ? ucfirst(session()->get('jenis'))  : '' ?> |
                            <?= (session()->has('jabatan')) ? session()->get('jabatan') : '' ?> </span>
                    </div>
                    <span class="">
                        <img class="round" src="<?php
                                                if ( (session()->get('jenis') == 'dosen') || (session()->get('jenis') == 'bps') ) {
                                                    echo '/assets/images/fotoprofil/dosen.png';
                                                } else {
                                                    // echo ($urlFoto . $foto);
                                                    echo '/assets/images/fotoprofil/mahasiswa.png';
                                                }

                                                ?>" alt="foto profil" height="40" width="40">
                        <span class="avatar-status-online"></span>
                    </span>
                </a>
                <div class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdown-user">
                    <a class="dropdown-item" href="/profil"><i class="me-50" data-feather="user"></i>
                        Profil Saya</a>
                    <a class="dropdown-item" href="/logout" onclick="logout()"><i class="me-50" data-feather="log-out"></i>
                        Keluar</a>
                </div>
            </li>
        </ul>
    </div>
</nav>

<script>
    function logout() {
        event.preventDefault();
        Swal.fire({
            title: 'Apakah Anda ingin keluar?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#d33',
            cancelButtonColor: '#3085d6',
            confirmButtonText: 'Keluar',
            cancelButtonText: 'Batal'
        }).then((result) => {
            if (result.isConfirmed) {
                window.location.href = "/logout";
            }
        })
    }
</script>