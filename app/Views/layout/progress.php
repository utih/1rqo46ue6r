<?php if ($prog != null) : ?>
    <h5 class="mt-2 w-100"> Progress Pencacahan : <?= $judul; ?></h5>
    <h5> <?= $prog['nilai']; ?>% (<?= $prog['terisi'];; ?> selesai dari <?= $prog['semua']; ?> sampel)</h5>
    <div class="progress progress-bar-primary">
        <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="80" aria-valuemin="80" aria-valuemax="100" style="width: <?= $prog['nilai']; ?>%"></div>
    </div>
<?php endif ?>

<?php if ($prog == null) : ?>
    <h5> WILAYAH BELUM MEMILIKI PROGRESS PENCACAHAN </h5>
    <div class="progress progress-bar-primary">
        <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="80" aria-valuemin="80" aria-valuemax="100" style="width: 0%"></div>
    </div>
<?php endif ?>