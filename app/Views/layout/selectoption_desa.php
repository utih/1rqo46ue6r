<h6> Pilih Kelurahan/Desa </h6>

<select class="form-select" id="select-progress-keldes">
    <option value="all_keldes">Semua Kelurahan/Desa</option>

    <?php
    $daerah = $wilayah->getAllKeldes($kode1);
    foreach ($daerah as $d) :
    ?>
        <option value="<?= $d['kode_desa']; ?>"> <?= $d['nama_desa']; ?> </option>
    <?php endforeach ?>
</select>

<!-- SCRIPT UNTUK PERUBAHAN KELURAHAN/DESA -->
<script>
    $('#select-progress-keldes').change(function() {
        var val = $('#select-progress-kecamatan').val();
        var val2 = $('#select-progress-keldes').val();

        // IF DEFAULT, rubah juga yang lain jadi default
        if (val2 == 'all_keldes') {
            $("#my_bs").html("");
        } else {}

        // DONE CHANGE TABLE
        $.ajax({
            url: "<?php echo base_url() . $url_ajax ?>/debug-tabel",
            type: "POST",
            data: {
                data1: val,
                data2: val2,
                data3: '',
                myselect: 'keldes'
            },
            success: function(data) {
                $("#tabel_tabel").html(data);
            }
        });

        // DONE CHANGE PROGRESS
        $.ajax({
            url: "<?php echo base_url()  . $url_ajax ?>/debug-progress",
            type: "POST",
            data: {
                kec: val,
                des: val2,
                bs: '',
                myselect: 'keldes'
            },
            success: function(data) {
                $("#progress_progress").html(data);
            }
        });

    });
</script>