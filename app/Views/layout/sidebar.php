<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="navbar-header py-0">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item me-auto"><a class="navbar-brand" href="#">
                    <!-- ADD IMAGE HERE -->
                    <span class="brand-logo">
                        <img src="/assets/images/logo/logo_pkl_div.png" alt="Web Monitoring" style="height: 30px; width: auto;">
                    </span>
                    <div class="fw-bolder" style="color: #4b395f;">
                        &nbsp;Web Monitoring
                    </div>
                </a>
            </li>
            <li class="nav-item nav-toggle" style="color: #4b395f;">
                <a class="nav-link modern-nav-toggle pe-0" data-bs-toggle="collapse">
                    <i class="d-block d-xl-none text-primary toggle-icon font-medium-4" data-feather="x"></i>
                    <i class="d-none d-xl-block collapse-toggle-icon font-medium-4  text-primary" style="color: #4b395f;" data-feather="disc" data-ticon="disc">
                    </i>
                </a>
            </li>
            <!-- <li class="nav-item d-none d-lg-block"><a class="nav-link nav-link-style"><i class="ficon"
                        data-feather="moon"></i></a></li> -->
        </ul>
    </div>
    <div class="shadow-bottom"></div>
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <li class="<?= ($active == 'dashboard') ? 'active' : '' ?> nav-item"><a class="d-flex align-items-center" href="/"><i data-feather="home"></i><span class="menu-title text-truncate">Dashboard</span></a>
            </li>
            <li class="navigation-header"><span>Riset</span><i data-feather="more-horizontal"></i>
            </li>

            <!-- RISET 1 -->
            <li class="nav-item <?= ($active == 'riset1_sampel' || $active == 'riset1_monitoring' || $active == 'riset1_progress' || $active == 'riset1_progresstim') ? 'active has-sub-open open' : '' ?>">
                <a class="d-flex align-items-center" href="#"><i data-feather="file-text"></i><span class="menu-title text-truncate">Riset 1 (MPD)</span></a>
                <ul class="menu-content">

                    <li class="<?= ($active == 'riset1_sampel' || $active == 'riset1_monitoring') ? 'has-sub open' : '' ?>">
                        <a class="d-flex align-items-center" href="#"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Second Level">Daftar</span></a>
                        <ul class="menu-content">
                            <li><a class="<?= ($active == 'riset1_sampel') ? 'active' : '' ?> d-flex align-items-center" href="/riset-1/sampel"><i data-feather="circle"></i><span class="menu-item text-truncate">Daftar Sampel</span></a>
                            </li>
                            <li><a class="<?= ($active == 'riset1_monitoring') ? 'active' : '' ?> d-flex align-items-center" href="/riset-1/monitoring-tim"><i data-feather="circle"></i><span class="menu-item text-truncate">Daftar Pertim</span></a>
                            </li>
                        </ul>
                    </li>

                    <li class="<?= ($active == 'riset1_progress' || $active == 'riset1_progresstim') ? 'has-sub open' : '' ?>">
                        <a class="d-flex align-items-center" href="#"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Second Level">Progress</span></a>
                        <ul class="menu-content">
                            <li><a class="<?= ($active == 'riset1_progress') ? 'active' : '' ?> d-flex align-items-center" href="/riset-1/progress-wilayah"><i data-feather="circle"></i><span class="menu-item text-truncate">Progress Wilayah</span></a>
                            </li>

                            <li><a class="<?= ($active == 'riset1_progresstim') ? 'active' : '' ?> d-flex align-items-center" href="/riset-1/progress-tim"><i data-feather="circle"></i><span class="menu-item text-truncate">Progress Pertim</span></a>
                            </li>
                        </ul>
                    </li>

                </ul>
            </li>

            <!-- RISET 12 -->
            <li class="nav-item <?= ($active == 'riset_1_2_listing' || $active == 'riset_1_2_sampel' || $active == 'riset_1_2_monitoring' || $active == 'riset_1_2_progress' || $active == 'riset_1_2_progresstim') ? 'active has-sub-open open' : '' ?>">
                <a class="d-flex align-items-center" href="#"><i data-feather="file-text"></i><span class="menu-title text-truncate">Riset 1 & 2 (Integrasi) </span></a>
                <ul class="menu-content">
                    <li class="<?= ($active == 'riset_1_2_listing' || $active == 'riset_1_2_sampel' || $active == 'riset_1_2_monitoring') ? 'has-sub open' : '' ?>">
                        <a class="d-flex align-items-center" href="#"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Second Level">Daftar</span></a>
                        <ul class="menu-content">
                            <li><a class="<?= ($active == 'riset_1_2_listing') ? 'active' : '' ?> d-flex align-items-center" href="/riset-1-2/listing"><i data-feather="circle"></i><span class="menu-item text-truncate">Daftar Listing</span></a>
                            </li>
                            <li><a class="<?= ($active == 'riset_1_2_sampel') ? 'active' : '' ?> d-flex align-items-center" href="/riset-1-2/sampel"><i data-feather="circle"></i><span class="menu-item text-truncate">Daftar Sampel</span></a>
                            </li>
                            <li><a class="<?= ($active == 'riset_1_2_monitoring') ? 'active' : '' ?> d-flex align-items-center" href="/riset-1-2/monitoring-tim"><i data-feather="circle"></i><span class="menu-item text-truncate">Daftar Pertim</span></a>
                            </li>
                        </ul>
                    </li>

                    <li class="<?= ($active == 'riset_1_2_progress' || $active == 'riset_1_2_progresstim') ? 'has-sub open' : '' ?>">
                        <a class="d-flex align-items-center" href="#"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Second Level">Progress</span></a>
                        <ul class="menu-content">
                            <li><a class="<?= ($active == 'riset_1_2_progress') ? 'active' : '' ?> d-flex align-items-center" href="/riset-1-2/progress-wilayah"><i data-feather="circle"></i><span class="menu-item text-truncate">Progress Wilayah</span></a>
                            </li>

                            <li><a class="<?= ($active == 'riset_1_2_progresstim') ? 'active' : '' ?> d-flex align-items-center" href="/riset-1-2/progress-tim"><i data-feather="circle"></i><span class="menu-item text-truncate">Progress Pertim</span></a>
                            </li>
                        </ul>
                    </li>

                </ul>
            </li>

            <!-- RISET 3 -->

            <li class=" nav-item <?= ($active == 'riset3_listing' || $active == 'riset3_sampel' || $active == 'riset3_monitoring' || $active == 'riset3_progress' || $active == 'riset3_progresstim') ? 'active has-sub-open open' : '' ?>">
                <a class="d-flex align-items-center" href="#"><i data-feather="file-text"></i><span class="menu-title text-truncate">Riset 3</span></a>
                <ul class="menu-content">
                    <li class="<?= ($active == 'riset3_listing' || $active == 'riset3_sampel' || $active == 'riset3_monitoring') ? 'has-sub open' : '' ?>">
                        <a class="d-flex align-items-center" href="#"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Second Level">Daftar</span></a>
                        <ul class="menu-content">
                            <li><a class="<?= ($active == 'riset3_listing') ? 'active' : '' ?> d-flex align-items-center" href="/riset-3/listing"><i data-feather="circle"></i><span class="menu-item text-truncate">Daftar Listing</span></a>
                            </li>

                            <li><a class="<?= ($active == 'riset3_sampel') ? 'active' : '' ?> d-flex align-items-center" href="/riset-3/sampel"><i data-feather="circle"></i><span class="menu-item text-truncate">Daftar Sampel</span></a>
                            </li>

                            <li><a class="<?= ($active == 'riset3_monitoring') ? 'active' : '' ?> d-flex align-items-center" href="/riset-3/monitoring-tim"><i data-feather="circle"></i><span class="menu-item text-truncate">Daftar Pertim</span></a>
                            </li>
                        </ul>
                    </li>

                    <li class="<?= ($active == 'riset3_progress' || $active == 'riset3_progresstim') ? 'has-sub open' : '' ?>">
                        <a class="d-flex align-items-center" href="#"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Second Level">Progress</span></a>
                        <ul class="menu-content">
                            <li><a class="<?= ($active == 'riset3_progress') ? 'active' : '' ?> d-flex align-items-center" href="/riset-3/progress-wilayah"><i data-feather="circle"></i><span class="menu-item text-truncate">Progress Wilayah</span></a>
                            </li>

                            <li><a class="<?= ($active == 'riset3_progresstim') ? 'active' : '' ?> d-flex align-items-center" href="/riset-3/progress-tim"><i data-feather="circle"></i><span class="menu-item text-truncate">Progress Pertim</span></a>
                            </li>
                        </ul>
                    </li>

                </ul>
            </li>

            <!-- RISET 4 -->
            <li class="nav-item <?= ($active == 'riset4_listing' || $active == 'riset4_sampel' || $active == 'riset4_monitoring' || $active == 'riset4_progress' || $active == 'riset4_progresstim') ? 'active has-sub-open open' : '' ?>">
                <a class="d-flex align-items-center" href="#"><i data-feather="file-text"></i><span class="menu-title text-truncate">Riset 4</span></a>
                <ul class="menu-content">
                    <li class="<?= ($active == 'riset4_listing' || $active == 'riset4_sampel' || $active == 'riset4_monitoring') ? 'has-sub open' : '' ?>">
                        <a class="d-flex align-items-center" href="#"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Second Level">Daftar</span></a>
                        <ul class="menu-content">
                            <li><a class="<?= ($active == 'riset4_listing') ? 'active' : '' ?> d-flex align-items-center" href="/riset-4/listing"><i data-feather="circle"></i><span class="menu-item text-truncate">Daftar Listing</span></a>
                            </li>

                            <li><a class="<?= ($active == 'riset4_sampel') ? 'active' : '' ?> d-flex align-items-center" href="/riset-4/sampel"><i data-feather="circle"></i><span class="menu-item text-truncate">Daftar Sampel</span></a>
                            </li>

                            <li><a class="<?= ($active == 'riset4_monitoring') ? 'active' : '' ?> d-flex align-items-center" href="/riset-4/monitoring-tim"><i data-feather="circle"></i><span class="menu-item text-truncate">Daftar Pertim</span></a>
                            </li>
                        </ul>
                    </li>

                    <li class="<?= ($active == 'riset4_progress' || $active == 'riset4_progresstim') ? 'has-sub open' : '' ?>">
                        <a class="d-flex align-items-center" href="#"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Second Level">Progress</span></a>
                        <ul class="menu-content">
                            <li><a class="<?= ($active == 'riset4_progress') ? 'active' : '' ?> d-flex align-items-center" href="/riset-4/progress-wilayah"><i data-feather="circle"></i><span class="menu-item text-truncate">Progress Wilayah</span></a>
                            </li>

                            <li><a class="<?= ($active == 'riset4_progresstim') ? 'active' : '' ?> d-flex align-items-center" href="/riset-4/progress-tim"><i data-feather="circle"></i><span class="menu-item text-truncate">Progress Pertim</span></a>
                            </li>
                        </ul>
                    </li>

                </ul>
            </li>

            <!-- MONITORING PCL -->
            <li class=" navigation-header"><span>Monitoring PPL</span><i data-feather="more-horizontal"></i>
            </li>
            <li class="<?= ($active == 'monitoring_lokasi_pcl') ? 'active' : '' ?> nav-item"><a class="d-flex align-items-center" href="/monitoring-pcl"><i data-feather="map-pin"></i><span class="menu-title text-truncate">Lokasi PPL</span></a>
            </li>

            <!-- PUSAT BANTUAN -->
            <li class=" navigation-header"><span>Pusat Bantuan</span><i data-feather="more-horizontal"></i>
            </li>
            <li class="<?= ($active == 'faq') ? 'active' : '' ?> nav-item"><a class="d-flex align-items-center" href="/pusat-bantuan/faq"><i data-feather="help-circle"></i><span class="menu-title text-truncate">FAQ</span></a>
            </li>

            <!-- PROFIL -->
            <li class=" navigation-header"><span>Profil</span><i data-feather="more-horizontal"></i>
            </li>
            <li class="<?= ($active == 'profil') ? 'active' : '' ?> nav-item"><a class="d-flex align-items-center" href="/profil"><i data-feather="user"></i><span class="menu-title text-truncate">Profil
                        Saya</span></a>
            </li>
            <li class=" nav-item"><a class="d-flex align-items-center" href="/logout" onclick="logoutSidebar()"><i data-feather="log-out"></i><span class="menu-title text-truncate">Keluar</span></a>
            </li>
        </ul>
    </div>
</div>

<script>
    function logoutSidebar() {
        event.preventDefault();
        Swal.fire({
            title: 'Apakah Anda ingin keluar?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#d33',
            cancelButtonColor: '#3085d6',
            confirmButtonText: 'Keluar',
            cancelButtonText: 'Batal'
        }).then((result) => {
            if (result.isConfirmed) {
                window.location.href = "/logout";
            }
        })
    }
</script>