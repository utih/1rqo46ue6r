<script>
$(document).ready(function() {
    <?php if (session()->getFlashdata('sukses')) { ?>
    Swal.fire({
        title: 'Sukses',
        text: '<?= session()->getFlashdata('sukses'); ?>',
        icon: 'success',
        showConfirmButton: true,
        // timer: 1500
    })
    <?php } else if (session()->getFlashdata('gagal')) { ?>
    Swal.fire({
        title: 'Gagal',
        text: '<?= session()->getFlashdata('gagal'); ?>',
        icon: 'error',
        showConfirmButton: true,
        // timer: 1500
    })
    <?php } ?>
});
</script>