<div id="table-w3">
    <div class="table-responsive">
        <table class="table my-2" id="table-3">
            <thead>
                <tr class="text-center">
                    <th>Kode BS</th>
                    <th>Pencacah</th>
                    <th>Jumlah Sampel</th>
                    <th>Sampel Selesai</th>
                    <th>Progress</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>

                <?php $popo = $wilayah->sampel('bloksensus');
                $awalan = $kode2;

      
                ?>

                <?php foreach ($popo as $s) : ?>

                    <?php
                    $progress = $wilayah->progress_bs($s['kode_bs']);
                    ?>

                    <?php $pos = strpos($s['kode_bs'], $awalan); ?>
                    <?php if ($pos !== false) : ?>
                        <tr class="text-center">
                            <td><?= $s['kode_bs']; ?></td>
                            <td><?= $s['nama_pcl']; ?> (<?= $s['nim']; ?>)</td>
                            <td><?= $s['jumlah_sampel']; ?></td>
                            <td> <?= $progress['terisi'] ?> </td>
                            <td> <?= $progress['nilai'] ?>%
                                <div class="progress progress-bar-primary">
                                    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="80" aria-valuemin="80" aria-valuemax="100" style="width: <?= $progress['nilai'] ?>%"></div>
                                </div>
                            </td>
                            <td>
                                <a href="<?= $url_detail . $s['kode_bs']; ?>" class="btn btn-success">Detail
                                </a>
                            </td>
                        </tr>
                    <?php endif ?>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>


<?php
$data = [
    'rep' => 3,
    'judul' =>  "",
];
echo view('layout/datatable_new', $data); ?>