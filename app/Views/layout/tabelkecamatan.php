<div id="table-w1">
    <div class="table-responsive">
        <table class="table my-2" id="table-1">
            <thead>
                <tr class="text-center">
                    <th>Nama Kecamatan</th>
                    <th>Jumlah Sampel</th>
                    <th>Sampel Selesai</th>
                    <th>Progress</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php $popo = $wilayah->sampel('kecamatan') ?>
                <?php foreach ($popo as $s) : ?>

                    <?php
                    $progress = $wilayah->progress_kecamatan($s['kode_kecamatan']);
                    ?>

                    <tr class="text-center">
                        <td><?= $s['nama_kecamatan']; ?></td>
                        <td><?= $s['jumlah_sampel']; ?></td>
                        <td> <?= $progress['terisi'] ?> </td>
                        <td> <?= $progress['nilai'] ?>%
                            <div class="progress progress-bar-primary">
                                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="80" aria-valuemin="80" aria-valuemax="100" style="width: <?= $progress['nilai'] ?>%"></div>
                            </div>
                        </td>
                        <td>
                            <a href="<?= $url_detail . $s['kode_kecamatan']; ?>" class="btn btn-success">Detail
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

<?php
$data = [
    'rep' => 1,
    'judul' =>  "",
];
echo view('layout/datatable_new', $data); ?>