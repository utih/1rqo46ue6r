<div id="table-w2">
    <div class="table-responsive">
        <table class="table my-2" id="table-2">

            <thead>
                <tr class="text-center">
                    <th>Nama Desa / Kelurahan</th>
                    <th>Jumlah Sampel</th>
                    <th>Sampel Selesai</th>
                    <th>Progress</th>
                    <th>Aksi</th>
                </tr>
            </thead>

            <tbody>
                <?php $popo = $wilayah->sampel('kelurahan') ?>
                <?php foreach ($popo as $s) : ?>
                    <?php
                    $progress = $wilayah->progress_keldes($s['kode_kecamatan'], $s['kode_desa']);
                    ?>

                    <?php if ($s['kode_kecamatan'] == $kode1) : ?>
                        <tr class="text-center">
                            <td><?= $s['nama_desa']; ?></td>
                            <td><?= $s['jumlah_sampel']; ?></td>
                            <td> <?= $progress['terisi'] ?> </td>
                            <td> <?= $progress['nilai'] ?>%
                                <div class="progress progress-bar-primary">
                                    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="80" aria-valuemin="80" aria-valuemax="100" style="width: <?= $progress['nilai'] ?>%"></div>
                                </div>
                            </td>

                            <td>
                                <a href="<?= $url_detail . $s['kode_kecamatan']; ?>/<?= $s['kode_desa']; ?>" class="btn btn-success">Detail
                                </a>
                            </td>
                        </tr>
                    <?php endif ?>
                <?php endforeach; ?>

            </tbody>
        </table>
    </div>
</div>

<?php
$data = [
    'rep' => 2,
    'judul' =>  "",
];
echo view('layout/datatable_new', $data); ?>