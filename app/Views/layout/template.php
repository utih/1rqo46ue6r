<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/x-icon" href="/assets/images/logo/logo_stis.png">
    <title><?= $title; ?> - Web Monitoring</title>

    <!-- GOOGLE FONTS -->
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600" rel="stylesheet">

    <!-- VENDOR CSS -->
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/assets/vendors/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/assets/vendors/css/forms/select/select2.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/assets/vendors/css/extensions/toastr.min.css">

    <!-- DATATABLE -->
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/assets/vendors/css/tables/datatable/dataTables.bootstrap5.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/assets/vendors/css/tables/datatable/responsive.bootstrap5.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/assets/vendors/css/tables/datatable/buttons.bootstrap5.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/assets/vendors/css/tables/datatable/rowGroup.bootstrap5.min.css">

    <!-- THEME CSS -->
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/assets/css/bootstrap-extended.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/assets/css/colors.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/assets/css/components.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/assets/css/themes/dark-layout.min.css">

    <!-- PAGE CSS -->
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/assets/css/core/menu/menu-types/vertical-menu.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/assets/css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/assets/css/pages/dashboard-ecommerce.min.css">

    <!-- CUSTOM CSS -->
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/assets/css/style.css">

    <!-- LEAFLET CSS -->
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.9.3/dist/leaflet.css" integrity="sha256-kLaT2GOSpHechhsozzB+flnD+zUyjE2LlfWPgU04xyI=" crossorigin="" />
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/assets/css/plugins/maps/map-leaflet.css">

    <!-- JQUERY -->
    <script src="https://code.jquery.com/jquery-3.6.1.min.js" integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous"></script>


    <!-- LEAFLET -->
    <script src="https://unpkg.com/leaflet@1.9.3/dist/leaflet.js" integrity="sha256-WBkoXOwTeyKclOHuWtc+i2uENFpDZ9YPdf5Hf+D7ewM=" crossorigin=""></script>

    <!-- SWEETALERT -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <!-- FONT AWESOME -->
    <script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>

    <!-- VENDOR JS -->
    <script src="<?= base_url(); ?>/assets/vendors/js/vendors.min.js"></script>
</head>

<body class="vertical-layout vertical-menu-modern  navbar-floating footer-static " data-open="click" data-menu="vertical-menu-modern" data-col="">

    <!-- HEADER -->
    <?= $this->include('layout/header'); ?>

    <!-- SIDEBAR -->
    <?= $this->include('layout/sidebar'); ?>

    <!-- DATATABLE -->
    <!-- SCRIPT -->
    <script src="<?= base_url(); ?>/assets/vendors/js/tables/datatable/jquery.dataTables.min.js"></script>
    <script src="<?= base_url(); ?>/assets/vendors/js/tables/datatable/dataTables.bootstrap5.min.js"></script>
    <script src="<?= base_url(); ?>/assets/vendors/js/tables/datatable/dataTables.responsive.min.js"></script>
    <script src="<?= base_url(); ?>/assets/vendors/js/tables/datatable/responsive.bootstrap5.min.js"></script>
    <script src="<?= base_url(); ?>/assets/vendors/js/tables/datatable/datatables.checkboxes.min.js"></script>
    <script src="<?= base_url(); ?>/assets/vendors/js/tables/datatable/datatables.buttons.min.js"></script>
    <script src="<?= base_url(); ?>/assets/vendors/js/tables/datatable/jszip.min.js"></script>
    <script src="<?= base_url(); ?>/assets/vendors/js/tables/datatable/pdfmake.min.js"></script>
    <script src="<?= base_url(); ?>/assets/vendors/js/tables/datatable/vfs_fonts.js"></script>
    <script src="<?= base_url(); ?>/assets/vendors/js/tables/datatable/buttons.html5.min.js"></script>
    <script src="<?= base_url(); ?>/assets/vendors/js/tables/datatable/buttons.print.min.js"></script>

    <!-- SWEET ALERTA -->
    <?= $this->include('layout/sweetalert') ?>

    <!-- MAIN CONTENT -->
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
                <div class="content-body">
                    <!-- INSERT CONTENT HERE -->
                    <?php $this->renderSection('content'); ?>
                </div>
            </div>
        </div>
    </div>

    <!-- FOOTER -->
    <?= $this->include('layout/footer'); ?>

    <button class="btn btn-primary btn-icon scroll-top" type="button"><i data-feather="arrow-up"></i></button>

    <!-- THEME JS -->
    <script src="<?= base_url(); ?>/assets/js/core/app-menu.min.js"></script>
    <script src="<?= base_url(); ?>/assets/js/core/app.min.js"></script>
    <script src="<?= base_url(); ?>/assets/js/scripts/customizer.min.js"></script>


    <!-- JQUERY -->
    <script>
        $(window).on('load', function() {
            if (feather) {
                feather.replace({
                    width: 14,
                    height: 14
                });
            }
        })
    </script>
</body>

</html>