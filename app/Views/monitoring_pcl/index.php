<?php $this->extend('layout/template'); ?>
<?= $this->section('content'); ?>
<?= $this->include('layout/sweetalert') ?>

<!-- BREADCRUMBS -->
<div class="card">
    <div class="card-body">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-start mb-0">Lokasi PPL</h2>
                <div class="breadcrumb-wrapper">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">Monitoring PPL</li>
                        <li class="breadcrumb-item active">Lokasi PPL</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- TITLE -->
<div id="faq-search-filter">
    <div class="card faq-search" style="background-image: url('/assets/images/banner/banner_pencarian.png');">
        <div class="card-body text-center mt-2">
            <!-- MAIN TITLE -->
            <h1 class="fw-bolder" style="color: #4b395f; text-shadow: 2px 2px #fff;">Monitoring Petugas Pendataan
                Lapangan</h1>
            <!-- DESC -->
            <div class="card pt-1 pb-1" style="background-color: rgba(255, 255, 255, 0.7)">
                <p class="fw-bolder h4" style="color: black;">
                    <b>Pilih riset untuk navigasi ke halaman tracking PPL berdasarkan riset.
                        <br>
                        Jika ingin tracking seluruh petugas PPL, tekan tombol di bawah ini.
                    </b>
                </p>
            </div>
            <a href="/monitoring-pcl/lokasi-pcl" class="btn btn-gradient-primary">Seluruh PPL</a>
        </div>
    </div>
</div>

<div id="knowledge-base-content">
    <div class="row kb-search-content-info match-height">
        <!-- RISET 1 -->
        <div class="col-md-6 col-sm-6 col-12 kb-search-content">
            <div class="card riset-hover">
                <a href="/monitoring-pcl/riset-1">
                    <img src="/assets/images/logo/riset1.png"
                        class="card-img-top card-img-monitoring card-img-monitoring-size mt-1" alt="logo riset" />
                    <div class="card-body text-center">
                        <h4>Riset 1 (MPD)</h4>
                        <p class="text-body mt-1 mb-0">
                            Riset 1 (MPD) Surabaya berfokus pada BPS di wilayah setempat.
                        </p>
                    </div>
                </a>
            </div>
        </div>
        <!-- RISET 2 -->
        <div class="col-md-6 col-sm-6 col-12 kb-search-content">
            <div class="card riset-hover">
                <a href="/monitoring-pcl/riset-2">
                    <img src="/assets/images/logo/riset2.png"
                        class="card-img-top card-img-monitoring card-img-monitoring-size mt-1" alt="logo riset" />
                    <div class="card-body text-center">
                        <h4>Riset 1 & 2 (Integrasi)</h4>
                        <p class="text-body mt-1 mb-0">
                            Kota Malang menjadi lokus penelitian dari integrasi Riset 1 & 2.
                        </p>
                    </div>
                </a>
            </div>
        </div>
        <!-- RISET 3 -->
        <div class="col-md-6 col-sm-6 col-12 kb-search-content">
            <div class="card riset-hover">
                <a href="/monitoring-pcl/riset-3">
                    <img src="/assets/images/logo/riset3.png"
                        class="card-img-top card-img-monitoring card-img-monitoring-size mt-1" alt="logo riset" />
                    <div class="card-body text-center">
                        <h4>Riset 3</h4>
                        <p class="text-body mt-1 mb-0">
                            Riset 3 mengambil lokus penelitian di Kabupaten Malang.
                        </p>
                    </div>
                </a>
            </div>
        </div>
        <!-- RISET 4 -->
        <div class="col-md-6 col-sm-6 col-12 kb-search-content">
            <div class="card riset-hover">
                <a href="/monitoring-pcl/riset-4">
                    <img src="/assets/images/logo/riset4.png"
                        class="card-img-top card-img-monitoring card-img-monitoring-size mt-1" alt="logo riset" />
                    <div class="card-body text-center">
                        <h4>Riset 4</h4>
                        <p class="text-body mt-1 mb-0">
                            Lokus peneitian Riset 4 adalah di Kota Batu. 
                        </p>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>

<?= $this->endSection(); ?>