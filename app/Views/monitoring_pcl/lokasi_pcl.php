<?php $this->extend('layout/template'); ?>
<?= $this->section('content'); ?>
<div class="card">
    <div class="card-body">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <?php if ($riset == 'lokasi-pcl') { ?>
                <h2 class="content-header-title float-start mb-0">Lokasi Seluruh PPL</h2>
                <?php } else { ?>
                <h2 class="content-header-title float-start mb-0">Lokasi PPL <?= $riset; ?></h2>
                <?php } ?>
                <div class="breadcrumb-wrapper">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">Monitoring PPL</li>
                        <li class="breadcrumb-item"><a href="/monitoring-pcl" style="color: #6e6b7b">Lokasi PPL</a>
                        </li>
                        <?php if ($riset == 'lokasi-pcl') { ?>
                        <li class="breadcrumb-item active">Lokasi Seluruh PPL</li>
                        <?php } else { ?>
                        <li class="breadcrumb-item active">Lokasi PPL <?= $riset; ?></li>
                        <?php } ?>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-body">
        <?php if ($riset == 'lokasi-pcl') { ?>
        <h1 class="my-2 text-center fw-bolder">Monitoring Seluruh Petugas</h1>
        <?php } else { ?>
        <h1 class="my-2 text-center fw-bolder">Monitoring Petugas <?= $titelatas; ?> (<?= $lokus; ?>)</h1>
        <?php } ?>

        <!-- SELURUH PETUGAS -->
        <?php if ($riset == 'lokasi-pcl') : ?>
        <div class="row">
            <div class="mb-2">
                Posisi PPL diperbarui 5 menit sekali atau ketika PPL berpindah lokasi lebih dari 200 meter.
                <br />
                Oleh karena itu, untuk mendapatkan hasil yang akurat, silahkan tekan tombol
                <a href="/monitoring-pcl/lokasi-pcl" class="badge badge-light-primary fw-bolder">refresh</a>
                sebelum melakukan monitoring PPL.
            </div>
            <div class="col-md-6 mb-1">
                <label class="form-label" for="filter_by_lokus"><b>Posisi PPL Berdasarkan Lokus</b></label>
                <select class="form-select" id="filter_by_lokus" name="filter_by_lokus">
                    <option value="" disabled selected hidden>Pilih berdasarkan lokus</option>
                    <option value="all" data-lat="-7.5360639" data-long="112.2384017" data-zoom="9">
                        Seluruh Lokus
                    </option>
                    <option value="Kota Surabaya" data-lat="-7.289166" data-long="112.734398" data-zoom="12">
                        Kota
                        Surabaya
                    </option>
                    <option value="Kota Malang" data-lat="-7.981894" data-long="112.626503" data-zoom="12">Kota Malang
                    </option>
                    <option value="Kabupaten Malang" data-lat="-8.0495643" data-long="112.6884549" data-zoom="12">
                        Kab. Malang</option>
                    <option value="Kota Batu" data-lat="-7.8671" data-long="112.5239" data-zoom="12">Kota Batu
                    </option>
                </select>
            </div>
            <div class="col-md-6 mb-1">
                <label class="form-label" for="filter_by_petugas"><b>Posisi PPL Berdasarkan Petugas Pencacah
                    </b></label>
                <select class="form-select" name="filter_by_petugas" id="filter_by_petugas">
                    <option value="" disabled selected hidden>Pilih berdasarkan petugas</option>
                    <?php foreach ($pcl as $p) : ?>
                    <option value="<?= $p['nim']; ?>" data-lat="<?= $p['latitude']; ?>"
                        data-long="<?= $p['longitude']; ?>" data-zoom="17">
                        <?= $p['nama']; ?>(<?= $p['nim']; ?>).
                    </option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
        <div class="leaflet-map" id="map"></div>
        <?php endif; ?>
        <!-- JIKA RISET 1 -->
        <?php if ($riset == 'Riset 1') : ?>
        <div class="row">
            <div class="mb-2">
                Posisi PPL diperbarui 5 menit sekali atau ketika PPL berpindah lokasi lebih dari 200 meter.
                <br />
                Oleh karena itu, untuk mendapatkan hasil yang akurat, silahkan tekan tombol
                <a href="/monitoring-pcl/riset-1" class="badge badge-light-primary fw-bolder">refresh</a>
                sebelum melakukan monitoring PPL.
            </div>
         
            <div class="col-md-6 mb-1">
                <label class="form-label" for="filter_by_petugas"><b>Posisi PPL Berdasarkan Petugas Pencacah di
                        <?= $riset; ?>
                    </b></label>
                <select class="form-select" name="filter_by_petugas" id="filter_by_petugas">
                    <option value="" disabled selected hidden>Pilih berdasarkan petugas</option>
                    <?php foreach ($pcl as $p) : ?>
                    <option value="<?= $p['nim']; ?>" data-lat="<?= $p['latitude']; ?>"
                        data-long="<?= $p['longitude']; ?>" data-zoom="17">
                        <?= $p['nama']; ?>(<?= $p['nim']; ?>).
                    </option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
        <div class="leaflet-map" id="map"></div>
        <?php endif; ?>
        <!-- JIKA RISET 2, 3, dan 4 -->
        <?php if ($riset == 'Riset 2' || $riset == 'Riset 3' || $riset == 'Riset 4') : ?>
        <div class="row">
            <div class="mb-2">
                Posisi PPL diperbarui 5 menit sekali atau ketika PPL berpindah lokasi lebih dari 200 meter.
                <br />
                Oleh karena itu, untuk mendapatkan hasil yang akurat, silahkan tekan tombol
                <?php if ($riset == 'Riset 2') { ?>
                <a href="/monitoring-pcl/riset-2" class="badge badge-light-primary fw-bolder">refresh</a>
                sebelum melakukan monitoring PPL.
                <?php } else if ($riset == 'Riset 3') { ?>
                <a href="/monitoring-pcl/riset-3" class="badge badge-light-primary fw-bolder">refresh</a>
                sebelum melakukan monitoring PPL.
                <?php } else if ($riset == 'Riset 4') { ?>
                <a href="/monitoring-pcl/riset-4" class="badge badge-light-primary fw-bolder">refresh</a>
                sebelum melakukan monitoring PPL.
                <?php } ?>
            </div>
            <div class="col-md-6 mb-1">
                <label class="form-label" for="filter_by_petugas"><b>Posisi PPL Berdasarkan Petugas Pencacah di
                        <?= $riset; ?>
                    </b></label>
                <select class="form-select" name="filter_by_petugas" id="filter_by_petugas">
                    <option value="" disabled selected hidden>Pilih berdasarkan petugas</option>
                    <?php foreach ($pcl as $p) : ?>
                    <option value="<?= $p['nim']; ?>" data-lat="<?= $p['latitude']; ?>"
                        data-long="<?= $p['longitude']; ?>" data-zoom="17">
                        <?= $p['nama']; ?>(<?= $p['nim']; ?>).
                    </option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
        <div class="leaflet-map" id="map"></div>
        <?php endif; ?>
    </div>
</div>

<!-- SCRIPT -->
<script src="<?= base_url(); ?>/assets/vendors/js/forms/select/select2.full.min.js"></script>
<script src="<?= base_url(); ?>/assets/js/scripts/forms/form-select2.js"></script>

<script>
$(document).ready(function() {

    // SELECT2
    $('#filter_by_petugas').select2();
    $('#filter_by_lokus').select2();

    // JAWA TIMUR
    <?php if ($riset == 'lokasi-pcl') : ?>
    var lat = -7.5360639;
    var long = 112.2384017;
    var zoom = 9;
    <?php endif; ?>

    // KOTA SURABAYA DAN KOTA MALANG
    <?php if ($riset == 'Riset 1') : ?>
    var lat = -7.7628604;
    var long = 112.4191724;
    var zoom = 10;
    <?php endif; ?>

    // KOTA MALANG
    <?php if ($riset == 'Riset 2') : ?>
    var lat = -7.981894;
    var long = 112.626503;
    var zoom = 12;
    <?php endif; ?>

    // KABUPATEN MALANG
    <?php if ($riset == 'Riset 3') : ?>
    var lat = -8.0495643;
    var long = 112.6884549;
    var zoom = 11;
    <?php endif; ?>

    // KOTA BATU
    <?php if ($riset == 'Riset 4') : ?>
    var lat = -7.8671;
    var long = 112.5239;
    var zoom = 12;
    <?php endif; ?>

    var map = L.map('map').setView([lat, long], zoom);

    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 19,
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);

    $('#filter_by_lokus, #filter_by_petugas').change(function() {
        const lat = $(this).find(':selected').data('lat');
        const long = $(this).find(':selected').data('long');
        const zoom = $(this).find(':selected').data('zoom');
        map.flyTo([lat, long], zoom);
    });

    $('#filter_by_petugas').change(function() {
        const lat = $(this).find(':selected').data('lat');
        const long = $(this).find(':selected').data('long');
        const zoom = 17;
        const circle = L.circle([lat, long], {
            color: 'red',
            fillColor: '#f03',
            fillOpacity: 0.5,
            radius: 25
        }).addTo(map);
    });

    var marker = [];
    <?php if ($pcl) { ?>
    <?php $i = 0; ?>
    <?php foreach ($pcl as $p) : ?>
    marker[<?= $i; ?>] = L.marker([<?= $p['latitude']; ?>, <?= $p['longitude']; ?>]).addTo(map).bindPopup(
        "<b><?= $p['nama']; ?> (<?= $p['nim']; ?>)</b><br />No Hp: <a href='https://wa.me/<?= $p['no_hp']; ?>' target='_blank'> <?= $p['no_hp']; ?></a><br />Lokus: <?= $p['lokus']; ?><br />Id Tim: <?= $p['id_tim']; ?><br />Latitude, Longitude[<?= $p['latitude']; ?>, <?= $p['longitude']; ?>]<br />Akurasi: <?= $p['akurasi']; ?> Meter<br />Diupdate pada: <?= $p['time_created']; ?>"
    ).bindTooltip(
        "<b><?= $p['nama']; ?> (<?= $p['nim']; ?>)</b><br />Lokus: <?= $p['lokus']; ?>"
    ).openTooltip();
    <?php $i++; ?>
    <?php endforeach; ?>
    <?php } ?>

});
</script>


<?= $this->endSection(); ?>