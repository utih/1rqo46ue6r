<?php $this->extend('layout/template'); ?>
<?= $this->section('content'); ?>

<?php

use App\Models\AuthModel;

$fotoModel = new AuthModel();
$fotoArr = $fotoModel->getFoto(session()->get('email'));
$urlFoto = "https://capi62.stis.ac.id/web-service-62/assets/img/";
$foto = "";

if (!empty($fotoArr)) {
    $foto = $fotoArr[0]['foto'];
}

?>

<div class="card">
    <div class="card-body">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-start mb-0">Profil Saya</h2>
                <div class="breadcrumb-wrapper">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">Profil</a>
                        </li>
                        <li class="breadcrumb-item active">Profil Saya
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="app-user-view-account">
    <div class="row">
        <div class="col-xl-6 col-lg-6 col-md-6 order-1 order-md-0">
            <div class="card">
                <div class="card-body">
                    <div class="user-avatar-section">
                        <div class="d-flex align-items-center flex-column">
                            <img class="img-fluid rounded mt-3 mb-2" src="<?php
                                                                            if ((session()->get('jenis') == 'dosen') || (session()->get('jenis') == 'bps')  ) {
                                                                                echo '/assets/images/fotoprofil/dosen.png';
                                                                            } else {
                                                                                echo '/assets/images/fotoprofil/mahasiswa.png';
                                                                            }

                                                                            ?>" height="110" width="110" alt="User avatar" />
                            <div class="user-info text-center">
                                <h4><?= (session()->has('nama')) ? session()->get('nama') : '' ?></h4>
                                <span class="badge bg-secondary badge-glow"><?= (session()->has('jenis')) ? ucfirst(session()->get('jenis')) : '' ?>
                                    | <?= (session()->has('jabatan')) ? session()->get('jabatan') : '' ?></span>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex justify-content-around mb-1 pt-75">
                    </div>
                    <h4 class="fw-bolder border-bottom pb-50 mb-1">Detail</h4>
                    <div class="form form-horizontal">
                        <div class="row">
                            <div class="col-12">
                                <div class="mb-1 row">
                                    <div class="col-sm-3">
                                        <label class="col-form-label" for="name">Nama</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="text" id="name" class="form-control" name="nama" value="<?= (session()->has('nama')) ? session()->get('nama') : '' ?>" disabled />
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="mb-1 row">
                                    <div class="col-sm-3">
                                        <label class="col-form-label" for="status">Status</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="text" id="status" class="form-control" name="status" value="<?= (session()->has('jenis')) ? ucfirst(session()->get('jenis')) : '' ?>" disabled />
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="mb-1 row">
                                    <div class="col-sm-3">
                                        <label class="col-form-label" for="email">Email</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="text" id="email" class="form-control" name="email" value="<?= (session()->has('email')) ? session()->get('email') . '@stis.ac.id' : '' ?>" disabled />
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="mb-1 row">
                                    <div class="col-sm-3">
                                        <label class="col-form-label" for="jabatan">Jabatan</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="text" id="jabatan" class="form-control" name="jabatan" value="<?= (session()->has('jabatan')) ? session()->get('jabatan') : '' ?>" disabled />
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-9 my-1">
                                <a href="/profil/ubah-password" class="btn btn-primary me-1">Ubah Password</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?= $this->endSection(); ?>