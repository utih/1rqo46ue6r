<?php $this->extend('layout/template'); ?>
<?= $this->section('content'); ?>
<?= $this->include('layout/sweetalert') ?>

<div class="card">
    <div class="card-body">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-start mb-0">Ubah Password</h2>
                <div class="breadcrumb-wrapper">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">Profil</a>
                        </li>
                        <li class="breadcrumb-item"><a href="/profil" style="color: #6e6b7b">Profil Saya</a>
                        </li>
                        <li class="breadcrumb-item active">Ubah Password
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6 col-12">
        <div class="card">
            <div class="card-body">
                <div class="card-header">
                    <h3 class="card-title">Ubah Password</h3>
                </div>
                <div class="card-body">
                    <form class="form form-horizontal" id="changePasswordForm" action="/profil/save-password"
                        method="POST">
                        <?= csrf_field(); ?>
                        <div class="row">
                            <div class="col-12">
                                <div class="mb-1 row">
                                    <div class="col-sm-12">
                                        <label class="col-form-label" for="old-password">Password Lama</label>
                                    </div>
                                    <div class="col-sm-9 input-group input-group-merge form-password-toggle">
                                        <input type="password"
                                            class="form-control form-control-merge <?= ($validation->hasError('old-password')) ? 'is-invalid' : ''; ?>"
                                            id="old-password" name="old-password" tabindex="2"
                                            placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;"
                                            aria-describedby="old-password" value="<?= old('old-password'); ?>" />
                                        <span class="input-group-text cursor-pointer"><i data-feather="eye"></i></span>
                                        <div class="invalid-feedback">
                                            <?= $validation->getError('old-password'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="mb-1 row">
                                    <div class="col-sm-12">
                                        <label class="col-form-label" for="new-password">Password Baru</label>
                                    </div>
                                    <div class="col-sm-9 input-group input-group-merge form-password-toggle">
                                        <input type="password"
                                            class="form-control form-control-merge <?= ($validation->hasError('new-password')) ? 'is-invalid' : ''; ?>"
                                            id="new-password" name="new-password" tabindex="2"
                                            placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;"
                                            aria-describedby="new-password" value="<?= old('new-password'); ?>" />
                                        <span class="input-group-text cursor-pointer"><i data-feather="eye"></i></span>
                                        <div class="invalid-feedback">
                                            <?= $validation->getError('new-password'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="mb-1 row">
                                    <div class="col-sm-12">
                                        <label class="col-form-label" for="conf-new-password">Konfirmasi Password
                                            Baru</label>
                                    </div>
                                    <div class="col-sm-9 input-group input-group-merge form-password-toggle">
                                        <input type="password"
                                            class="form-control form-control-merge <?= ($validation->hasError('conf-new-password')) ? 'is-invalid' : ''; ?>"
                                            id="conf-new-password" name="conf-new-password" tabindex="2"
                                            placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;"
                                            aria-describedby="conf-new-password"
                                            value="<?= old('conf-new-password'); ?>" />
                                        <span class="input-group-text cursor-pointer"><i data-feather="eye"></i></span>
                                        <div class="invalid-feedback">
                                            <?= $validation->getError('conf-new-password'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-9 mt-1">
                                <button type="submit" class="btn btn-primary me-1">Ubah Password</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?= $this->endSection(); ?>