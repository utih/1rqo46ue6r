<?php $this->extend('layout/template'); ?>
<?= $this->section('content'); ?>

<div class="card">
    <div class="card-body">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-start mb-0">FAQ</h2>
                <div class="breadcrumb-wrapper">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">Pusat Bantuan</a>
                        </li>
                        <li class="breadcrumb-item active">FAQ
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>

<section id="faq-search-filter">
    <div class="card faq-search" style="background-image: url('/assets/images/banner/banner_aspirasi.png');">
        <div class="card-body text-center">
            <!-- MAIN TITLE -->
            <h1 class="fw-bolder" style="color: #4b395f; text-shadow: 2px 2px #fff;">Frequently Asked Questions</h1>
            <!-- DESC -->
            <div class="card pt-1" style="background-color: rgba(255, 255, 255, 0.7)">
                <p class="fw-bolder h3" style="color: black; font-size: 1.5rem;">
                    <b> DASHBOARD SEDANG MAINTENANCE, JIKA INGIN MELIHAT PROGRESS, BISA LIHAT DI PROGRESS WILAYAH/TIM PER RISET. 
UNTUK RISET 12 INTEGRASI BELUM BISA DIPAKAI YA PROGRESS WILAYAHNYA. TERIMA KASIH
                    </b>
                </p>
            </div>
        </div>
    </div>
</section>

<div class="card">
    <div class="card-body">
        <h3 class="text-center">Frequently Asked Questions</h3>
        <p class="text-center my-2 h5 fst-italic fw-bolder">Pertanyaan seputar Web Monitoring</p>
        <div class="accordion accordion-margin my-2" id="faq-content">
            <!-- FAQ 1 -->
            <div class="card accordion-item background-faq-color">
                <h2 class="accordion-header" id="faq-one-title">
                    <button class="accordion-button collapsed background-faq-color" data-bs-toggle="collapse" role="button" data-bs-target="#faq-one-content" aria-expanded="false" aria-controls="faq-one">
                        Apa Saja Konten Yang Terdapat Pada Web Monitoring PKL D-IV 62? </button>
                </h2>

                <div id="faq-one-content" class="collapse accordion-collapse " aria-labelledby="faq-one" data-bs-parent="#faq-one-content">
                    <div class="accordion-body">
                        Untuk Setiap Lokus, Terdapat Daftar Listing, Daftar Sampel, Daftar Pertim, Progress Per Wilayah, Progress Per Tim, Serta Monitoring PPL. </div>
                </div>
            </div>

            <!-- FAQ 2 -->
            <div class="card accordion-item background-faq-color">
                <h2 class="accordion-header" id="faq-two-title">
                    <button class="accordion-button background-faq-color" data-bs-toggle="collapse" role="button" data-bs-target="#faq-two-content" aria-expanded="true" aria-controls="faq-two">
                        Siapa Saja Yang Memiliki Akses Web Monitoring?
                    </button>
                </h2>
                <div id="faq-two-content" class="collapse accordion-collapse " aria-labelledby="faq-two" data-bs-parent="#faq-two-content">
                    <div class="accordion-body">
                        Yang Memiliki Akses Web Monitoring Adalah Seluruh Dosen Yang Tergabung Dalam Struktur PKL D-IV 62, Intama dan Korwil Masing-Masing Lokus, serta Tim Monitoring Server
                    </div>
                </div>
            </div>

            <!-- FAQ 3 -->
            <div class="card accordion-item background-faq-color">
                <h2 class="accordion-header" id="faq-three-title">
                    <button class="accordion-button collapsed background-faq-color" data-bs-toggle="collapse" role="button" data-bs-target="#faq-three-content" aria-expanded="false" aria-controls="faq-three">
                        Apakah Data Responden Yang Sensitif Ikut Ditampilkan?
                    </button>
                </h2>
                <div id="faq-three-content" class="collapse " aria-labelledby="faq-three" data-bs-parent="#faq-three-content">
                    <div class="accordion-body">
                        Data Responden Telah Difilter, Sehingga Hanya Intama Dan Korwil Lokus Bersangkutan, Serta Korlap Dan Dosen Yang Dapat Melihat Data Tersebut Untuk Keperluan Lapangan
                    </div>
                </div>
            </div>

            <!-- FAQ 4 -->
            <div class="card accordion-item background-faq-color">
                <h2 class="accordion-header" id="faq-four-title">
                    <button class="accordion-button collapsed background-faq-color" data-bs-toggle="collapse" role="button" data-bs-target="#faq-four-content" aria-expanded="false" aria-controls="faq-three">
                    Bagaimana Cara Melihat Lokasi PPL?
                    </button>
                </h2>
                <div id="faq-four-content" class="collapse " aria-labelledby="faq-four" data-bs-parent="#faq-four-content">
                    <div class="accordion-body">
                    Silakan Akses Menu Lokasi PPL Pada Bagian Monitoring PPL. Lokasi PPL Dapat Ditampilkan Secara Keseluruhan Maupun Berdasarkan lokus/riset.                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<?= $this->endSection(); ?>