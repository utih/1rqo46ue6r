<?php $this->extend('layout/template'); ?>
<?= $this->section('content'); ?>
<div class="card">
    <div class="card-body">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-start mb-0">Detail Listing</h2>
                <div class="breadcrumb-wrapper">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">Riset 1
                        </li>
                        <li class="breadcrumb-item">Survei MPD
                        </li>
                        <li class="breadcrumb-item"><a href="/riset-1/listing" style="color: #6e6b7b">Listing</a>
                        </li>
                        <li class="breadcrumb-item active">Detail Listing
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>

<section>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <?php
                    $detailTitle = '';
                    if ($detail_length == 3) {
                        $detailTitle = 'Kelurahan/Desa ' . $detail;
                    } else if ($detail_length == 7) {
                        $detailTitle = 'Kecamatan ' . $detail;
                    } else {
                        $detailTitle = 'Blok Sensus ' . $detail;
                    }
                    ?>
                    <h1 class="mb-2">Riset 1 - Detail Listing <?= $detailTitle; ?></h1>
                    <?php if ($detail_length > 7) : ?>
                        <span>Informasi lebih lanjut mengenai <?= $detailTitle; ?> dapat dilihat
                            <a href="#" class="badge badge-light-primary fw-bolder" data-bs-toggle="modal" data-bs-target="#modalDetail">
                                di sini
                            </a>
                        </span>
                    <?php endif; ?>
                    <!-- MODAL DETAIL UNTUK INFORMASI LEBIH LANJUT -->
                    <div class="modal fade" id="modalDetail" tabindex="-1" aria-labelledby="modalDetailTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h3 class="modal-title fw-bolder" id="modalDetailTitle">
                                        Detail <?= $detailTitle; ?>
                                    </h3>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <div class="text-center">
                                        <h4 class="fw-bold">Rincian</h4>
                                        <ul class="list-group list-group-flush text-start">
                                            <?php if ($detail_length > 7) : ?>
                                                <li class="list-group-item">Kode Blok Sensus :
                                                    <b><?= $detail_listing[0]['kodeBs']; ?></b>
                                                </li>
                                                <li class="list-group-item">Nama Blok Sensus :
                                                    <b><?= $detail_listing[0]['nama']; ?></b>
                                                </li>
                                                <li class="list-group-item">Kabupaten/Kota :
                                                    <b><?= $detail_listing[0]['nama_kabupaten']; ?></b>
                                                </li>
                                                <li class="list-group-item">Kecamatan :
                                                    <b><?= $detail_listing[0]['nama_kecamatan']; ?></b>
                                                </li>
                                                <li class="list-group-item">Satuan Lingkungan Setempat :
                                                    <b><?= $detail_listing[0]['sls']; ?></b>
                                                </li>
                                                <li class="list-group-item">Dicacah Oleh :
                                                    <b><?= $detail_listing[0]['nama']; ?>
                                                        (<?= $detail_listing[0]['nim']; ?>)</b>
                                                </li>
                                            <?php endif; ?>
                                        </ul>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">
                                            Ok
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h2 class="text-center my-2">Daftar Rumah Tangga Hasil Listing</h2>
                    <div id="table-w1">
                        <div class="table-responsive">
                            <table class="table my-2" id="table-1">
                                <thead>
                                    <tr class="text-center">
                                        <th>Kode Ruta</th>
                                        <th>Kode Blok Sensus</th>
                                        <th>Pencacah</th>
                                        <th>Nama Pemberi Informasi</th>
                                        <th>No Hp (Provider)</th>
                                        <th>Alamat Domisili</th>
                                        <th>Kabupaten/Kota</th>
                                        <th>Waktu</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($detail_listing as $dl) : ?>
                                        <tr class="text-center">
                                            <td><?= $dl['kodeRuta']; ?></td>
                                            <td><?= $dl['kodeBs']; ?></td>
                                            <td><?= $dl['nama_pcl']; ?> (<?= $dl['nim_pcl']; ?>)</td>
                                            <td><?= $dl['namaPemberiInformasi']; ?></td>
                                            <td><?= $dl['noHp']; ?> (<?= $dl['provider']; ?>)</td>
                                            <td><?= $dl['alamatDomisili']; ?></td>
                                            <td><?= $dl['kabkot']; ?></td>
                                            <td><?= $dl['time']; ?></td>
                                            <td>
                                                <!-- MODAL DETAIL -->
                                                <div class="vertical-modal-ex">
                                                    <button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#modalListingDetail<?= $dl['kodeRuta']; ?>">
                                                        Detail
                                                    </button>

                                                </div>
                                            </td>
                                        </tr>

                                        <!-- Modal -->
                                        <div class="modal fade" id="modalListingDetail<?= $dl['kodeRuta']; ?>" tabindex="-1" aria-labelledby="modalListingDetailTitle<?= $dl['kodeRuta']; ?>" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h3 class="modal-title fw-bolder" id="modalListingDetailTitle<?= $dl['kodeRuta']; ?>">
                                                            Detail Hasil Listing
                                                        </h3>
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="text-center">
                                                            <h4 class="fw-bold">Rincian</h4>
                                                            <span class="mb-1 fw-bolder">Dicacah Oleh
                                                                <?= $dl['nama_pcl']; ?>
                                                                (<?= $dl['nim_pcl']; ?>)
                                                            </span>
                                                        </div>
                                                        <ul class="list-group list-group-flush text-start">
                                                            <li class="list-group-item">Kode Rumah Tangga:
                                                                <b><?= $dl['kodeRuta']; ?></b>
                                                            </li>
                                                            <li class="list-group-item">Kode Blok Sensus:
                                                                <b><?= $dl['kodeBs']; ?></b>
                                                            </li>
                                                            <li class="list-group-item">Nomor Urut/Nomor Segmen:
                                                                <b><?= $dl['nomorUrut']; ?>/<?= $dl['noSegmen']; ?></b>
                                                            </li>
                                                            <li class="list-group-item">Bangunan Fisik:
                                                                <b><?= $dl['bf']; ?></b>
                                                            </li>
                                                            <li class="list-group-item">Bangunan Sensus:
                                                                <b><?= $dl['bs']; ?></b>
                                                            </li>
                                                            <li class="list-group-item">Nama Pemberi Informasi:
                                                                <b><?= $dl['namaPemberiInformasi']; ?></b>
                                                            </li>
                                                            <li class="list-group-item">Nomor Handphone (Provider):
                                                                <b><?= $dl['noHp']; ?>
                                                                    (<?= $dl['provider']; ?>)</b>
                                                            </li>
                                                            <li class="list-group-item">Alamat Domisili:
                                                                <b><?= $dl['alamatDomisili']; ?></b>
                                                            </li>
                                                            <li class="list-group-item">Provinsi (Kode):
                                                                <b><?= $dl['provinsi']; ?> (<?= $dl['kodeProvinsi']; ?>)</b>
                                                            </li>
                                                            <li class="list-group-item">Kabupaten/Kota (Kode):
                                                                <b> <?= $dl['kabkot']; ?> (<?= $dl['kodeKabkot']; ?>)</b>
                                                            </li>
                                                            <li class="list-group-item">Kecamatan (Kode):
                                                                <b><?= $dl['kecamatan']; ?>
                                                                    (<?= $dl['kodeKecamatan']; ?>)</b>
                                                            </li>
                                                            <li class="list-group-item">Kelurahan (Kode):
                                                                <b><?= $dl['kelurahan']; ?>
                                                                    (<?= $dl['kodeKelurahan']; ?>)</b>
                                                            </li>
                                                            <li class="list-group-item">Waktu Listing :
                                                                <b><?= $dl['time']; ?></b>
                                                            </li>
                                                            <li class="list-group-item">Lokasi : <b> Akurasi
                                                                    <?= $dl['akurasi']; ?> Meter</b>
                                                                <br>
                                                                <iframe class="mt-1" width="100%" height="300" src="https://www.google.com/maps?q=<?= $dl['latitude']; ?>,<?= $dl['longitude']; ?>&hl=es;z=14&output=embed" frameborder="0">
                                                                </iframe>
                                                            </li>
                                                        </ul>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Ok</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="mt-2">
                        <a href="/riset-1/listing" class="btn btn-primary">Kembali</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php
$data = [
    'rep' => 1,
    'judul' => "Detail Progress Listing Riset 2 di " . $detailTitle
];
echo view('layout/datatable', $data); ?>

<?= $this->endSection(); ?>