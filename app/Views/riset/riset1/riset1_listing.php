<?php $this->extend('layout/template'); ?>
<?= $this->section('content'); ?>

<div class="card">
    <div class="card-body">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-start mb-0">Listing</h2>
                <div class="breadcrumb-wrapper">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">Riset 1 (MPD)</li>
                        <li class="breadcrumb-item active">Listing</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>

<div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <!-- //TODO 1 : UBAH JUDULNYA SESUAI JUDUL LISTING/SAMPEL NYA -->
                    <h1 class="text-center">Progress Listing Survei Riset 1 (<i>Mobile Positioning Data</i>)</h1>
                    <h2 class="mt-1" id="title-progress-listing">Progress Listing Berdasarkan Blok Sensus di Riset 1
                        (Kota Surabaya)
                    </h2>

                    <!-- //TODO 2 : (OPSIONAL) PAKAI INI JIKA TABLE YANG INGIN DITAMPILKAN > 1. PERHATIKAN PENAMAANNYA -->
                    <!-- PENAMAANNYA DISINI PAKE table-w1 , table-w2 untuk option value -->
                    <!-- SELECT BERDASARKAN KATEGORI -->
                    <div class="col-lg-2 col-12">
                        <select class="form-select" id="select-progress-w">
                            <option value="table-w1">Blok Sensus</option>
                            <option value="table-w2">Kecamatan</option>
                            <option value="table-w3">Desa / Kelurahan</option>
                            <option value="table-w4">Semua Listing</option>
                        </select>
                    </div>

                    <!-- //TODO 3 : BIKIN TABLE NYA DISINI, DIBUNGKUS DENGAN NAMA NILAI OPTION VALUE TADI-->
                    <!-- TABLE-1 : JUMLAH BLOK SENSUS -->
                    <div id="table-w1">
                        <div class="table-responsive">
                            <table class="table my-2" id="table-1">
                                <thead>
                                    <tr class="text-center">
                                        <th>Kode BS</th>
                                        <th>Pencacah</th>
                                        <th>Jumlah Terlisting</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($listing_by_bs as $l) : ?>
                                    <tr class="text-center">
                                        <td><?= $l['kode_bs']; ?></td>
                                        <td><?= $l['nama_pcl']; ?> (<?= $l['nim']; ?>)</td>
                                        <td><?= $l['jumlah_listing']; ?></td>
                                        <td>
                                            <a href="/riset-1/listing/detail/<?= $l['kode_bs']; ?>"
                                                class="btn btn-success">Detail</a>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <!-- TABLE-2 : JUMLAH KECAMATAN -->
                    <div id="table-w2" style="display: none;">
                        <div class="table-responsive">
                            <table class="table my-2" id="table-2">
                                <thead>
                                    <tr class="text-center">
                                        <th>Nama Kecamatan</th>
                                        <th>Jumlah Terlisting</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($listing_by_kecamatan as $l) : ?>
                                    <tr class="text-center">
                                        <td><?= $l['nama_kecamatan']; ?></td>
                                        <td><?= $l['jumlah_listing']; ?></td>
                                        <td>
                                            <a href="/riset-1/listing/detail/<?= $l['kode_kecamatan']; ?>"
                                                class="btn btn-success">Detail</a>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <!-- TABLE-3 : JUMLAH DESA KELURAHAN -->
                    <div id="table-w3" style="display: none;">
                        <div class="table-responsive">
                            <table class="table my-2" id="table-3">
                                <thead>
                                    <tr class="text-center">
                                        <th>Nama Desa / Kelurahan</th>
                                        <th>Jumlah Terlisting</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($listing_by_kelurahandesa as $l) : ?>
                                    <tr class="text-center">
                                        <td><?= $l['nama_desa']; ?></td>
                                        <td><?= $l['jumlah_listing']; ?></td>
                                        <td>
                                            <a href="/riset-1/listing/detail/<?= $l['kode_kecamatan']; ?>/<?= $l['kode_desa']; ?>"
                                                class="btn btn-success">Detail</a>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <!-- TABLE-4 : SELURUH LISTING -->
                    <div id="table-w4" style="display: none;">
                        <div class="table-responsive">
                            <table class="table my-2" id="table-4">
                                <thead>
                                    <tr class="text-center">
                                        <th>Kode Rumah Tangga</th>
                                        <th>Nim PPL</th>
                                        <th>Nama PPL</th>
                                        <th>Kode Blok Sensus</th>
                                        <th>BF</th>
                                        <th>BS</th>
                                        <th>No Segmen</th>
                                        <th>No Urut Ruta</th>
                                        <th>Kode Kab/Kota</th>
                                        <th>Kab/Kota</th>
                                        <th>Kode Kecamatan</th>
                                        <th>Kecamatan</th>
                                        <th>Kode Kelurahan/Desa</th>
                                        <th>Kelurahan/Desa</th>
                                        <th>Nama Pemberi Informasi</th>
                                        <th>Domisili</th>
                                        <th>Nomor Handphone</th>
                                        <th>Provider</th>
                                        <th>Latitude</th>
                                        <th>Longitude</th>
                                        <th>Akurasi</th>
                                        <th>Waktu Pencacahan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($listing_all as $l) : ?>
                                    <tr class="text-center">
                                        <td><?= $l['kodeRuta']; ?></td>
                                        <td><?= $l['nim_pcl']; ?></td>
                                        <td><?= $l['nama_pcl']; ?></td>
                                        <td><?= $l['kodeBs']; ?></td>
                                        <td><?= $l['bf']; ?></td>
                                        <td><?= $l['bs']; ?></td>
                                        <td><?= $l['noSegmen']; ?></td>
                                        <td><?= $l['nomorUrut']; ?></td>
                                        <td>3578</td>
                                        <td>Kota Surabaya</td>
                                        <td><?= $l['kode_kecamatan']; ?></td>
                                        <td><?= $l['nama_kecamatan']; ?></td>
                                        <td><?= $l['kode_desa']; ?></td>
                                        <td><?= $l['nama_desa']; ?></td>
                                        <td><?= $l['namaPemberiInformasi']; ?></td>
                                        <td><?= $l['alamatDomisili']; ?></td>
                                        <td><?= $l['noHp']; ?></td>
                                        <td><?= $l['provider']; ?></td>
                                        <td><?= $l['latitude']; ?></td>
                                        <td><?= $l['longitude']; ?></td>
                                        <td><?= $l['akurasi']; ?> Meter</td>
                                        <td><?= $l['time']; ?></td>
                                    </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- //TODO 4 : TAMBAHIN JQUERY BIAR DATATABLE NYA BISA JALAN -->
<!-- INI DITAMBAHIN SESUAI DENGAN JUMLAH TABLE -->
<?php
$data = [
    'rep' => 1,
    'judul' => "Progress Listing Berdasarkan Blok Sensus di Riset 1 (Kota Surabaya)"
];
echo view('layout/datatable', $data); ?>

<?php
$data = [
    'rep' => 2,
    'judul' => "Progress Listing Berdasarkan Kecamatan di Riset 1 (Kota Surabaya)"
];
echo view('layout/datatable', $data); ?>

<?php
$data = [
    'rep' => 3,
    'judul' => "Progress Listing Berdasarkan Kelurahan Desa di Riset 1 (Kota Surabaya)"
];
echo view('layout/datatable', $data); ?>

<?php
$data = [
    'rep' => 4,
    'judul' => "Progress Listing Keseluruhan di Riset 1 (Kota Surabaya)"
];
echo view('layout/datatable', $data); ?>

<script>
$('#select-progress-w').change(function() {
    var val = $('#select-progress-w').val();
    if (val == 'table-w1') {
        document.getElementById('title-progress-listing').innerHTML =
            'Progress Listing Berdasarkan Blok Sensus di Riset 1 (Kota Surabaya)';
        document.getElementById('table-w1').style.display = '';
        document.getElementById('table-w2').style.display = 'none';
        document.getElementById('table-w3').style.display = 'none';
        document.getElementById('table-w4').style.display = 'none';

    } else if (val == 'table-w2') {
        document.getElementById('title-progress-listing').innerHTML =
            'Progress Listing Berdasarkan Kecamatan di Riset 1 (Kota Surabaya)';
        document.getElementById('table-w1').style.display = 'none';
        document.getElementById('table-w2').style.display = '';
        document.getElementById('table-w3').style.display = 'none';
        document.getElementById('table-w4').style.display = 'none';
    } else if (val == 'table-w3') {
        document.getElementById('title-progress-listing').innerHTML =
            'Progress Listing Berdasarkan Kelurahan/Desa di Riset 1 (Kota Surabaya)';
        document.getElementById('table-w1').style.display = 'none';
        document.getElementById('table-w2').style.display = 'none';
        document.getElementById('table-w3').style.display = '';
        document.getElementById('table-w4').style.display = 'none';

    } else if (val == 'table-w4') {
        document.getElementById('title-progress-listing').innerHTML =
            'Progress Listing Keseluruhan di Riset 1 (Kota Surabaya)';
        document.getElementById('table-w1').style.display = 'none';
        document.getElementById('table-w2').style.display = 'none';
        document.getElementById('table-w3').style.display = 'none';
        document.getElementById('table-w4').style.display = '';
    }
});
</script>

<?= $this->endSection(); ?>