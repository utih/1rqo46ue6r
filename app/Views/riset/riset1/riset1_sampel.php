<?php

use App\Models\Riset\Riset1\Riset1ProgressModel;

$this->extend('layout/template'); ?>
<?= $this->section('content'); ?>

<div class="card">
    <div class="card-body">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-start mb-0">Sampel</h2>
                <div class="breadcrumb-wrapper">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">Riset 1</li>
                        <li class="breadcrumb-item active">Sampel</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>

<div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <!-- //TODO 1 : UBAH JUDULNYA SESUAI JUDUL LISTING/SAMPEL NYA -->
                    <h1 class="text-center">
                        Progress Sampel Survei Riset 1 (<i>Mobile Positioning Data</i>)
                    </h1>
                    <h2 class="mt-1" id="title-progress-listing">Progress Sampel Berdasarkan Blok Sensus di Riset 1(Kota
                        Surabaya)
                    </h2>

                    <!-- //TODO 2 : (OPSIONAL) PAKAI INI JIKA TABLE YANG INGIN DITAMPILKAN > 1. PERHATIKAN PENAMAANNYA -->
                    <!-- PENAMAANNYA DISINI PAKE table-w1 , table-w2 untuk option value -->
                    <!-- SELECT BERDASARKAN KATEGORI -->
                    <div class="col-lg-2 col-12">
                        <select class="form-select" id="select-progress-w">
                            <option value="table-w1">Blok Sensus</option>
                            <option value="table-w2">Kecamatan</option>
                            <option value="table-w3">Kelurahan / Desa</option>
                            <option value="table-w4">Semua Sampel</option>
                        </select>
                    </div>

                    <!-- //TODO 3 : BIKIN TABLE NYA DISINI, DIBUNGKUS DENGAN NAMA NILAI OPTION VALUE TADI-->
                    <!-- TABLE-1 : JUMLAH BLOK SENSUS -->
                    <div id="table-w1">
                        <div class="table-responsive">
                            <table class="table my-2" id="table-1">
                                <thead>
                                    <tr class="text-center">
                                        <th>Kode BS</th>
                                        <th>Pencacah</th>
                                        <th>Jumlah Sampel</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($sampel_by_bs as $s) : ?>
                                        <tr class="text-center">
                                            <td><?= $s['kode_bs']; ?></td>
                                            <td><?= $s['nama_pcl']; ?> (<?= $s['nim_pcl']; ?>)</td>
                                            <td><?= $s['jumlah_sampel']; ?></td>
                                            <td>
                                                <a href="/riset-1/sampel/detail/<?= $s['kode_bs']; ?>" class="btn btn-success">Detail
                                                </a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <!-- TABLE-2 : JUMLAH KECAMATAN -->
                    <div id="table-w2" style="display: none;">
                        <div class="table-responsive">
                            <table class="table my-2" id="table-2">
                                <thead>
                                    <tr class="text-center">
                                        <th>Nama Kecamatan</th>
                                        <th>Jumlah Sampel</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($sampel_by_kecamatan as $s) : ?>
                                        <tr class="text-center">
                                            <td><?= $s['nama_kecamatan']; ?></td>
                                            <td><?= $s['jumlah_sampel']; ?></td>
                                            <td>
                                                <a href="/riset-1/sampel/detail/<?= $s['kode_kecamatan']; ?>" class="btn btn-success">Detail
                                                </a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <!-- TABLE-3 : JUMLAH DESA KELURAHAN -->
                    <div id="table-w3" style="display: none;">
                        <div class="table-responsive">
                            <table class="table my-2" id="table-3">
                                <thead>
                                    <tr class="text-center">
                                        <th>Nama Desa / Kelurahan</th>
                                        <th>Jumlah Sampel</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($sampel_by_kelurahandesa as $s) : ?>
                                        <tr class="text-center">
                                            <td><?= $s['nama_desa']; ?></td>
                                            <td><?= $s['jumlah_sampel']; ?></td>
                                            <td>
                                                <a href="/riset-1/sampel/detail/<?= $s['kode_kecamatan']; ?>/<?= $s['kode_desa']; ?>" class="btn btn-success">Detail
                                                </a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <!-- TABLE-4 : SELURUH SAMPEL -->
                    <div id="table-w4" style="display: none;">
                        <table class="table my-2 table-responsive" id="table-4">
                            <thead>
                                <tr class="text-center">
                                    <th>Kode Rumah Tangga</th>
                                    <th>Nim PPL</th>
                                    <th>Nama PPL</th>
                                    <th>Kode Blok Sensus</th>
                                    <th>No Segmen</th>
                                    <th>BF</th>
                                    <th>BS</th>
                                    <th>No Urut Ruta</th>
                                    <th>Kode Kab/Kota</th>
                                    <th>Kab/Kota</th>
                                    <th>Kode Kecamatan</th>
                                    <th>Kecamatan</th>
                                    <th>Kode Kelurahan/Desa</th>
                                    <th>Kelurahan/Desa</th>
                                    <th>Nama Pemberi Informasi</th>
                                    <th>Domisili</th>
                                    <th>Nomor Handphone</th>
                                    <th>Provider</th>
                                    <th>Latitude</th>
                                    <th>Longitude</th>
                                    <th>Akurasi</th>
                                    <th>Terisi</th>
                                    <th>Eligible</th>
                                    <th>Waktu Pencacahan</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($sampel_all as $s) : ?>


                                    <!-- CEK SUDAH TERISI ATAU NGGA -->
                                    <?php
                                    $model = new Riset1ProgressModel;
                                    $d1 = $s['alamatDomisili'];
                                    $d2 = $s['namaPemberiInformasi'];
                                    $sudah = false;
                                    $eilgible_ruta = false;
                                    if ($model->terisi($d1, $d2)) {
                                        $sudah = true;
                                    }
                                    if ($model->eligibel_ruta($d1, $d2)) {
                                        $eilgible_ruta = true;
                                    }
                                    ?>

                                    <tr class="text-center">
                                        <td><?= $s['kodeRuta']; ?></td>
                                        <td><?= $s['nim_pcl']; ?></td>
                                        <td><?= $s['nama_pcl']; ?></td>
                                        <td><?= $s['kodeBs']; ?></td>
                                        <td><?= $s['noSegmen']; ?></td>
                                        <td><?= $s['bf']; ?></td>
                                        <td><?= $s['bs']; ?></td>
                                        <td><?= $s['nomorUrut']; ?></td>
                                        <td><?= $s['kodeKabkot']; ?></td>
                                        <td><?= $s['kabkot']; ?></td>
                                        <td><?= $s['kodeKecamatan']; ?></td>
                                        <td><?= $s['kecamatan']; ?></td>
                                        <td><?= $s['kodeKelurahan']; ?></td>
                                        <td><?= $s['kelurahan']; ?></td>
                                        <td><?= $s['namaPemberiInformasi']; ?></td>
                                        <td><?= $s['alamatDomisili']; ?></td>
                                        <td><?= $s['noHp']; ?></td>
                                        <td><?= $s['provider']; ?></td>
                                        <td><?= $s['latitude']; ?></td>
                                        <td><?= $s['longitude']; ?></td>
                                        <td><?= $s['akurasi']; ?> Meter</td>

                                        <td>
                                            <?php if ($sudah) : ?>
                                                <img src="/assets/images/fotoprofil/accept.png" alt="ok" height="30">
                                                <div class="hidden"> SUDAH TERISI </div>
                                            <?php endif ?>

                                            <?php if (!$sudah) : ?>
                                                <div class="hidden"> BELUM TERISI </div>
                                            <?php endif ?>
                                        </td>

                                        <td>


                                            <?php if ($eilgible_ruta && $sudah) : ?>
                                                <img src="/assets/images/fotoprofil/accept.png" alt="ok" height="30">
                                                <div class="hidden"> ELIGIBLE </div>
                                            <?php endif ?>

                                            <?php if (!$eilgible_ruta && $sudah) : ?>
                                                <img src="/assets/images/fotoprofil/deny.png" alt="ok" height="30">
                                                <div class="hidden"> TIDAK ELIGIBLE </div>
                                            <?php endif ?>
                                        </td>

                                        <td><?= $s['time']; ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- //TODO 4 : TAMBAHIN JQUERY BIAR DATATABLE NYA BISA JALAN -->
<!-- INI DITAMBAHIN SESUAI DENGAN JUMLAH TABLE -->
<?php
$data = [
    'rep' => 1,
    'judul' => 'Progress Sampel Berdasarkan Blok Sensus Survei MPD Riset 1 (Kota Surabaya)'
];
echo view('layout/datatable', $data); ?>

<?php
$data = [
    'rep' => 2,
    'judul' => 'Progress Sampel Berdasarkan Kecamatan Survei MPD Riset 1 (Kota Surabaya)'
];
echo view('layout/datatable', $data); ?>

<?php
$data = [
    'rep' => 3,
    'judul' => 'Progress Sampel Berdasarkan Kelurahan Desa Survei MPD Riset 1 (Kota Surabaya)'
];
echo view('layout/datatable', $data); ?>

<?php
$data = [
    'rep' => 4,
    'judul' => "Progress Sampel Keseluruhan di Riset 1 (Kota Surabaya)"
];
echo view('layout/datatable', $data); ?>

<script>
    $('#select-progress-w').change(function() {
        var val = $('#select-progress-w').val();
        if (val == 'table-w1') {
            document.getElementById('title-progress-listing').innerHTML =
                'Progress Sampel Berdasarkan Blok Sensus di Riset 1 (Kota Surabaya)';
            document.getElementById('table-w1').style.display = '';
            document.getElementById('table-w2').style.display = 'none';
            document.getElementById('table-w3').style.display = 'none';
            document.getElementById('table-w4').style.display = 'none';
        } else if (val == 'table-w2') {
            document.getElementById('title-progress-listing').innerHTML =
                'Progress Sampel Berdasarkan Kecamatan di Riset 1 (Kota Surabaya)';
            document.getElementById('table-w1').style.display = 'none';
            document.getElementById('table-w2').style.display = '';
            document.getElementById('table-w3').style.display = 'none';
            document.getElementById('table-w4').style.display = 'none';
        } else if (val == 'table-w3') {
            document.getElementById('title-progress-listing').innerHTML =
                'Progress Sampel Berdasarkan Kelurahan/Desa di Riset 1 (Kota Surabaya)';
            document.getElementById('table-w1').style.display = 'none';
            document.getElementById('table-w2').style.display = 'none';
            document.getElementById('table-w3').style.display = '';
            document.getElementById('table-w4').style.display = 'none';
        } else if (val == 'table-w4') {
            document.getElementById('title-progress-listing').innerHTML =
                'Progress Sampel Keseluruhan di Riset 1 (Kota Surabaya)';
            document.getElementById('table-w1').style.display = 'none';
            document.getElementById('table-w2').style.display = 'none';
            document.getElementById('table-w3').style.display = 'none';
            document.getElementById('table-w4').style.display = '';
        }
    });
</script>

<?= $this->endSection(); ?>