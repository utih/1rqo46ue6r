<?php

use App\Models\Riset\Riset12\Riset12ProgressModel;

$this->extend('layout/template'); ?>
<?= $this->section('content'); ?>
<div class="card">
    <div class="card-body">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-start mb-0">Detail Sampel</h2>
                <div class="breadcrumb-wrapper">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">Riset 1 & 2 (Integrasi)
                        </li>
                        <li class="breadcrumb-item"><a href="/riset-1-2/listing" style="color: #6e6b7b">Sampel</a>
                        </li>
                        <li class="breadcrumb-item active">Detail Sampel
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>

<section>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <?php
                    $detailTitle = '';
                    if ($detail_length == 3) {
                        $detailTitle = 'Kelurahan/Desa ' . $detail;
                    } else if ($detail_length == 7) {
                        $detailTitle = 'Kecamatan ' . $detail;
                    } else {
                        $detailTitle = 'Blok Sensus ' . $detail;
                    }
                    ?>
                    <h1 class="mb-2">
                        Detail Sampel Riset 1 & 2 (Integrasi) <?= $detailTitle; ?></h1>
                    <?php if ($detail_length > 7) : ?>
                        <span>Informasi lebih lanjut mengenai <?= $detailTitle; ?> dapat dilihat
                            <a href="#" class="badge badge-light-primary fw-bolder" data-bs-toggle="modal" data-bs-target="#modalDetail">
                                di sini
                            </a>
                        </span>
                    <?php endif; ?>
                    <!-- MODAL DETAIL UNTUK INFORMASI LEBIH LANJUT -->
                    <div class="modal fade" id="modalDetail" tabindex="-1" aria-labelledby="modalDetailTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h3 class="modal-title fw-bolder" id="modalDetailTitle">
                                        Detail <?= $detailTitle; ?>
                                    </h3>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <div class="text-center">
                                        <h4 class="fw-bold">Rincian</h4>
                                        <ul class="list-group list-group-flush text-start">
                                            <?php if ($detail_length > 7) : ?>
                                                <li class="list-group-item">Kode Blok Sensus :
                                                    <b><?= $detail_sampel[0]['kodeBs']; ?></b>
                                                </li>
                                                <li class="list-group-item">Nama Blok Sensus :
                                                    <b><?= $detail_sampel[0]['nama']; ?></b>
                                                </li>
                                                <li class="list-group-item">Kabupaten/Kota :
                                                    <b><?= $detail_sampel[0]['nama_kabupaten']; ?></b>
                                                </li>
                                                <li class="list-group-item">Kecamatan :
                                                    <b><?= $detail_sampel[0]['nama_kecamatan']; ?></b>
                                                </li>
                                                <li class="list-group-item">Kelurahan/Desa :
                                                    <b><?= $detail_sampel[0]['nama_desa']; ?></b>
                                                </li>
                                                <li class="list-group-item">Satuan Lingkungan Setempat :
                                                    <b><?= $detail_sampel[0]['sls']; ?></b>
                                                </li>
                                                <li class="list-group-item">Dicacah Oleh :
                                                    <b><?= $detail_sampel[0]['nama_pcl']; ?>
                                                        (<?= $detail_sampel[0]['nim_pcl']; ?>)</b>
                                                </li>
                                            <?php endif; ?>
                                        </ul>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">
                                            Ok
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h2 class="text-center my-2">Daftar Rumah Tangga Hasil Sampel</h2>
                    <div id="table-w1">
                        <div class="table-responsive">
                            <table class="table my-2" id="table-1">
                                <thead>
                                    <tr class="text-center">
                                        <th>Kode Ruta</th>
                                        <th>Kode Blok Sensus</th>
                                        <th>Pencacah</th>
                                        <th>Nomor Segmen/bf/bs/Urutan Ruta</th>
                                        <th>Nama Kepala Rumah Tangga</th>
                                        <th>Kode Eligible</th>
                                        <th>Waktu</th>
                                        <th>Terisi</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($detail_sampel as $ds) : ?>

                                        <!-- CEK SUDAH TERISI ATAU NGGA -->
                                        <?php
                                        $model = new Riset12ProgressModel;
                                        $d1 = $ds['kodeBs'];
                                        $d2 = $ds['noUrutRuta'];
                                        $d3 = $ds['namaKrt'];



                                        $sudah = false;
                                        if ($model->terisi($d1, $d2, $d3)) {
                                            $sudah = true;
                                        }

                                        ?>

                                        <tr class="text-center">
                                            <td><?= $ds['kodeRuta']; ?></td>
                                            <td><?= $ds['kodeBs']; ?></td>
                                            <td><?= $ds['nama_pcl']; ?> (<?= $ds['nim_pcl']; ?>)</td>
                                            <td><?= $ds['noSegmen']; ?>/<?= $ds['bf']; ?>/<?= $ds['bs']; ?>/<?= $ds['noUrutRuta']; ?>
                                            </td>
                                            <td><?= $ds['namaKrt']; ?></td>
                                            <td><?= $ds['kodeeligible']; ?></td>
                                            <td><?= $ds['time']; ?></td>
                                            <td>
                                                <?php if ($sudah) : ?>
                                                    <img src="/assets/images/fotoprofil/accept.png" alt="ok" height="30">
                                                    <div class="hidden"> SUDAH TERISI </div>
                                                <?php endif ?>

                                                <?php if (!$sudah) : ?>
                                                    <div class="hidden"> BELUM TERISI </div>
                                                <?php endif ?>
                                            </td>
                                            <td>
                                                <!-- MODAL DETAIL -->
                                                <div class="vertical-modal-ex">
                                                    <button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#modalListingDetail<?= $ds['kodeRuta']; ?>">
                                                        Detail
                                                    </button>


                                                </div>
                                            </td>
                                        </tr>

                                        <!-- Modal -->
                                        <div class="modal fade" id="modalListingDetail<?= $ds['kodeRuta']; ?>" tabindex="-1" aria-labelledby="modalListingDetailTitle<?= $ds['kodeRuta']; ?>" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h3 class="modal-title fw-bolder" id="modalListingDetailTitle<?= $ds['kodeRuta']; ?>">
                                                            Detail Hasil Sampel
                                                        </h3>
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="text-center">
                                                            <h4 class="fw-bold">Rincian</h4>
                                                            <span class="mb-1 fw-bold">Dicacah Oleh
                                                                <?= $ds['nama_pcl']; ?>
                                                                (<?= $ds['nim_pcl']; ?>)
                                                            </span>
                                                        </div>
                                                        <ul class="list-group list-group-flush text-start">
                                                            <li class="list-group-item">Kode Rumah Tangga:
                                                                <b><?= $ds['kodeRuta']; ?></b>
                                                            </li>
                                                            <li class="list-group-item">Kode Blok Sensus:
                                                                <b><?= $ds['kodeBs']; ?></b>
                                                            </li>
                                                            <li class="list-group-item">Kode Eligible:
                                                                <b><?= $ds['kodeeligible']; ?></b>
                                                            </li>
                                                            <li class="list-group-item">Nomor
                                                                Segmen/bf/bs/noUrutRuta:
                                                                <b><?= $ds['noSegmen'];
                                                                    ?>/<?= $ds['bf']; ?>/<?= $ds['bs']; ?>/<?= $ds['noUrutRuta']; ?>
                                                                </b>
                                                            </li>
                                                            <li class="list-group-item">Nama Kepala Rumah
                                                                Tangga:
                                                                <b><?= $ds['namaKrt']; ?></b>
                                                            </li>
                                                            <li class="list-group-item">Alamat:
                                                                <b><?= $ds['alamat']; ?></b>
                                                            </li>
                                                            <li class="list-group-item">Objek Wisata:
                                                                <b><?= $ds['objekWisata']; ?></b>
                                                            </li>
                                                            <li class="list-group-item">Akomodasi Komersial:
                                                                <b><?= $ds['akomodasiKomersial']; ?></b>
                                                            </li>
                                                            <li class="list-group-item">Lainnya:
                                                                <b><?= $ds['lainnya']; ?></b>
                                                            </li>
                                                            <li class="list-group-item">TTWP Dalam:
                                                                <b><?= $ds['TTPWDalam']; ?></b>
                                                            </li>
                                                            <li class="list-group-item">TTWP Luar:
                                                                <b><?= $ds['TTPWLuar']; ?></b>
                                                            </li>
                                                            <li class="list-group-item">Jumlah Anggota Rumah
                                                                Tangga:
                                                                <b><?= $ds['jmlhanggotaruta']; ?></b>
                                                            </li>
                                                            <li class="list-group-item">Nomor Ruta Luar Kota:
                                                                <b><?= ($ds['nortluarkota']) ? $ds['nortluarkota'] : '-' ?></b>
                                                            </li>
                                                            <li class="list-group-item">Nomor Ruta Dalam Kota:
                                                                <b><?= ($ds['nortluarkota']) ? $ds['nortluarkota'] : '-' ?></b>
                                                            </li>
                                                            <li class="list-group-item">Waktu Pencacahan:
                                                                <b><?= $ds['time']; ?></b>
                                                            </li>
                                                            <li class="list-group-item">Keterangan:
                                                                <b><?= $ds['pemberiInformasi']; ?></b>
                                                            </li>
                                                            <li class="list-group-item">Lokasi : <b> Akurasi
                                                                    <?= $ds['akurasi']; ?> Meter</b>
                                                                <br>
                                                                <iframe class="mt-1" width="100%" height="300" src="https://www.google.com/maps?q=<?= $ds['latitude']; ?>,<?= $ds['longitude']; ?>&hl=es;z=14&output=embed" frameborder="0">
                                                                </iframe>
                                                            </li>
                                                        </ul>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Ok</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="mt-2">
                        <a href="/riset-1-2/sampel" class="btn btn-primary">Kembali</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- SCRIPT -->
<?php
$judul = 'Detail Progress Sampel Riset 1 & 2 di ' . $detailTitle;

$data = [
    'rep' => 1,
    'judul' => $judul
];
echo view('layout/datatable', $data); ?>

<?= $this->endSection(); ?>