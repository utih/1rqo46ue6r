<?= $this->extend('layout/template'); ?>
<?= $this->section('content'); ?>

<div class="card">
    <div class="card-body">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-start mb-0">Monitoring Tim</h2>
                <div class="breadcrumb-wrapper">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">Riset 1 & 2 (Integrasi)</li>
                        <li class="breadcrumb-item active">Monitoring Tim</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-body">
        <div class="col-12 col-lg-12">
            <div class="text-center mb-2 mt-1">
                <h1>Monitoring Tim Petugas Riset 1 & 2 Integrasi (Kota Malang)</h1>
            </div>
            <div>
                <!-- PILL COMPONENTS -->
                <ul class="nav nav-pills nav-justified">
                    <li class="nav-item">
                        <a class="nav-link active fw-bolder" id="listing-tab" data-bs-toggle="pill"
                            href="#listing-justified" aria-expanded="true">Listing</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link fw-bolder" id="sampel-tab-justified" data-bs-toggle="pill"
                            href="#sampel-justified" aria-expanded="false">Sampel</a>
                    </li>
                </ul>
                <!-- PILL COMPONENTS CONTENT -->
                <div class="tab-content">
                    <!-- LISTING -->
                    <div role="tabpanel" class="tab-pane active" id="listing-justified" aria-labelledby="listing-tab"
                        aria-expanded="true">
                        <!-- SELECT BY ID TIM -->
                        <label class="form-label mt-1" for="filter_listing_by_id_tim">
                            <h4>
                                Monitoring PPL Riset 1 & 2 Integrasi Berdasarkan Tim
                            </h4>
                        </label>
                        <div class="col-md-4 mb-2">
                            <select class="form-select" name="filter_listing_by_id_tim" id="filter_listing_by_id_tim">
                                <option value="" disabled selected hidden>Pilih Berdasarkan ID Tim</option>
                                <!-- EDIT OPSI DI SINI -->
                                <?php foreach ($list_tim as $tim) : ?>
                                <option value="<?= $tim['id_tim']; ?>">
                                    <?= $tim['nama']; ?> (<?= $tim['nim']; ?> / Tim <?= $tim['id_tim']; ?>)
                                </option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div id="card-listing">
                            <!-- INFORMASI TIM LENGKAP -->
                            <div>
                                Untuk informasi lengkap mengenai tim, klik <a href="#"
                                    class="badge badge-light-primary fw-bolder" data-bs-toggle="modal"
                                    data-bs-target="#modalDetail">
                                    <u>di sini.</u>
                                </a>
                            </div>
                            <!-- MODAL DETAIL UNTUK INFORMASI TIM LEBIH LANJUT -->
                            <div class="modal fade" id="modalDetail" tabindex="-1" aria-labelledby="modalDetailTitle"
                                aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered modal-xl">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h3 class="modal-title fw-bolder" id="modalDetailTitle">
                                                Detail Informasi Tim
                                            </h3>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="text-center my-2">
                                                <h3 class="fw-bold">Tim <?= $pml_by_tim_default[0]['id_tim']; ?></h3>
                                                <h4>Riset 1 & 2 Integrasi (Kota Malang)</h4>
                                                <hr>
                                                <!-- PERSON -->
                                                <div class="row">
                                                    <div class="user-avatar-section col">
                                                        <div class="d-flex align-items-center flex-column">
                                                            <img class="img-fluid rounded mt-3 mb-2"
                                                                src="<?= $url_foto; ?><?= $pml_by_tim_default[0]['foto']; ?>"
                                                                height="110" width="110" alt="User avatar" />
                                                            <div class="user-info text-center">
                                                                <h4><?= $pml_by_tim_default[0]['nama']; ?></h4>
                                                                <span class="badge bg-light-secondary">PML /
                                                                    <?= $pml_by_tim_default[0]['nim']; ?></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr>
                                                <div class="row">
                                                    <!-- FOR EACH DISNI -->
                                                    <?php foreach ($ppl_by_tim_default as $ppl) : ?>
                                                    <div class="user-avatar-section col">
                                                        <div class="d-flex align-items-center flex-column">
                                                            <img class="img-fluid rounded mt-3 mb-2"
                                                                src="<?= $url_foto; ?><?= $ppl['foto']; ?>" height="110"
                                                                width="110" alt="User avatar" />
                                                            <div class="user-info text-center">
                                                                <h4><?= $ppl['nama']; ?></h4>
                                                                <span class="badge bg-light-secondary">PPL /
                                                                    <?= $ppl['nim']; ?></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php endforeach; ?>
                                                    <!-- END OF FOREACH -->
                                                </div>
                                                <!-- END OF PERSON -->
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-primary" data-bs-dismiss="modal">
                                                    Ok
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- CONTENT HERE -->
                            <div id="table-w1">
                                <div class="table-responsive">
                                    <table class="table my-2" id="table-1">
                                        <thead>
                                            <tr class="text-center">
                                                <th>Kode Blok Sensus</th>
                                                <th>Pencacah</th>
                                                <th>Jumlah Terlisting</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($listing_by_tim_default as $l) : ?>
                                            <tr class="text-center">
                                                <td><?= $l['kodeBs']; ?></td>
                                                <td><?= $l['nama']; ?> (<?= $l['nim']; ?>)</td>
                                                <td><?= $l['jumlah_listing']; ?></td>
                                                <td>
                                                    <a href="/riset-1-2/listing/detail/<?= $l['kodeBs']; ?>"
                                                        class="btn btn-success">Detail</a>
                                                </td>
                                            </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END OF LISTING -->

                    <!-- SAMPEL -->
                    <div class="tab-pane" id="sampel-justified" role="tabpanel" aria-labelledby="sampel-tab-justified"
                        aria-expanded="false">
                        <!-- SELECT BY ID TIM -->
                        <label class="form-label mt-1" for="filter_sampel_by_id_tim">
                            <h4>
                                Monitoring PPL Riset 1 & 2 Integrasi Berdasarkan Tim
                            </h4>
                        </label>
                        <div class="col-md-4 mb-2">
                            <select class="form-select" name="filter_sampel_by_id_tim" id="filter_sampel_by_id_tim">
                                <option value="" disabled selected hidden>Pilih Berdasarkan ID Tim</option>
                                <?php foreach ($list_tim as $tim) : ?>
                                <option value="<?= $tim['id_tim']; ?>">
                                    <?= $tim['nama']; ?> (<?= $tim['nim']; ?> / Tim <?= $tim['id_tim']; ?>)
                                </option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div id="card-sampel">
                            <!-- INFORMASI TIM LENGKAP -->
                            <div>
                                Untuk informasi lengkap mengenai tim, klik <a href="#"
                                    class="badge badge-light-primary fw-bolder" data-bs-toggle="modal"
                                    data-bs-target="#modalDetail2">
                                    <u>di sini.</u>
                                </a>
                            </div>
                            <!-- MODAL DETAIL UNTUK INFORMASI TIM LEBIH LANJUT -->
                            <div class="modal fade" id="modalDetail2" tabindex="-1" aria-labelledby="modalDetailTitle2"
                                aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered modal-xl">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h3 class="modal-title fw-bolder" id="modalDetailTitle2">
                                                Detail Informasi Tim
                                            </h3>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="text-center my-2">
                                                <h3 class="fw-bold">Tim 1</h3>
                                                <h4>Riset 1 & 2 (Kota Malang)</h4>
                                                <hr>
                                                <!-- PERSON -->
                                                <div class="row">
                                                    <div class="user-avatar-section col">
                                                        <div class="d-flex align-items-center flex-column">
                                                            <img class="img-fluid rounded mt-3 mb-2"
                                                                src="<?= $url_foto; ?><?= $pml_by_tim_default[0]['foto']; ?>"
                                                                height="110" width="110" alt="User avatar" />
                                                            <div class="user-info text-center">
                                                                <h4><?= $pml_by_tim_default[0]['nama']; ?></yh4>
                                                                    <span class="badge bg-light-secondary">PML /
                                                                        <?= $pml_by_tim_default[0]['nim']; ?></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr>
                                                <div class="row">
                                                    <!-- FOR EACH DISNI -->
                                                    <?php foreach ($ppl_by_tim_default as $ppl) : ?>
                                                    <div class="user-avatar-section col">
                                                        <div class="d-flex align-items-center flex-column">
                                                            <img class="img-fluid rounded mt-3 mb-2"
                                                                src="<?= $url_foto; ?><?= $ppl['foto']; ?>" height="110"
                                                                width="110" alt="User avatar" />
                                                            <div class="user-info text-center">
                                                                <h4><?= $ppl['nama']; ?></h4>
                                                                <span class="badge bg-light-secondary">PPL /
                                                                    <?= $ppl['nim']; ?></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php endforeach; ?>
                                                    <!-- END OF FOREACH -->
                                                </div>
                                                <!-- END OF PERSON -->
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-primary" data-bs-dismiss="modal">
                                                    Ok
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- CONTENT HERE -->
                            <div id="table-w2">
                                <div class="table-responsive">
                                    <table class="table my-2" id="table-2">
                                        <thead>
                                            <tr class="text-center">
                                                <th>Kode Blok Sensus</th>
                                                <th>Pencacah</th>
                                                <th>Jumlah Sampel</th>
                                                <th>Jumlah Terlisting</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($sampel_by_tim_default as $s) : ?>
                                            <tr class="text-center">
                                                <td><?= $s['kodeBs']; ?></td>
                                                <td> <?= $s['nama']; ?> (<?= $s['nim']; ?>)</td>
                                                <td><?= $s['jumlah_sampel']; ?></td>
                                                <td><?= $s['jumlah_listing']; ?></td>
                                                <td>
                                                    <a href="/riset-1-2/listing/detail/<?= $s['kodeBs']; ?>"
                                                        class="btn btn-success">Detail</a>
                                                </td>
                                            </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END OF SAMPEL -->
                </div>
            </div>
        </div>
    </div>
</div>

<!-- SCRIPT -->
<script src="<?= base_url(); ?>/assets/vendors/js/forms/select/select2.full.min.js"></script>
<script src="<?= base_url(); ?>/assets/js/scripts/forms/form-select2.js"></script>

<?php
$data = [
    'rep' => 1,
    'judul' => "Progress Listing Berdasarkan Tim di Riset 1 & 2 Integrasi (Kota Malang)"
];
echo view('layout/datatable', $data); ?>

<?php
$data = [
    'rep' => 2,
    'judul' => "Progress Listing Berdasarkan Tim di Riset 1 & 2 Integrasi (Kota Malang)"
];
echo view('layout/datatable', $data); ?>

<script>
$(document).ready(function() {
    // SELECT 2
    $('#filter_listing_by_id_tim').select2({
        placeholder: 'Pilih Berdasarkan Tim atau Nama Mahasiswa',
        // allowClear: true
    });
    $('#filter_sampel_by_id_tim').select2({
        placeholder: 'Pilih Berdasarkan Tim atau Nama Mahasiswa',
        // allowClear: true
    });
    $('#filter_listing_by_id_tim').change(function() {
        var listingTimVal = $(this).val();
        // TODO CHANGE TABLE
        $.ajax({
            url: "<?php base_url() ?>/riset-1-2/monitoring-tim-listing/" + listingTimVal,
            type: "GET",
            success: function(data) {
                $("#card-listing").html(data);
            }
        });
    });
    $('#filter_sampel_by_id_tim').change(function() {
        var sampelTimVal = $(this).val();
        // TODO CHANGE TABLE
        $.ajax({
            url: "<?php base_url() ?>/riset-1-2/monitoring-tim-sampel/" + sampelTimVal,
            type: "GET",
            success: function(data) {
                // alert(data);
                $("#card-sampel").html(data);
            }
        });
    });
});
</script>

<?= $this->endSection(); ?>