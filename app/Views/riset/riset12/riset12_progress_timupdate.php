<!-- INFORMASI TIM LENGKAP -->
<div>
    Untuk informasi lengkap mengenai tim, klik <a href="#" class="badge badge-light-primary fw-bolder" data-bs-toggle="modal" data-bs-target="#modalDetail2">
        <u>di sini.</u>
    </a>
</div>
<!-- MODAL DETAIL UNTUK INFORMASI TIM LEBIH LANJUT -->
<div class="modal fade" id="modalDetail2" tabindex="-1" aria-labelledby="modalDetailTitle2" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title fw-bolder" id="modalDetailTitle2">
                    Detail Informasi Tim
                </h3>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="text-center my-2">
                    <h3 class="fw-bold">Tim <?= $idtim; ?></h3>
                    <h4>Riset 1 & 2 (Kota Malang)</h4>
                    <hr>
                    <!-- PERSON -->
                    <div class="row">
                        <div class="user-avatar-section col">
                            <div class="d-flex align-items-center flex-column">
                                <img class="img-fluid rounded mt-3 mb-2" src="<?= $url_foto; ?><?= $pml_by_tim_default[0]['foto']; ?>" height="110" width="110" alt="User avatar" />
                                <div class="user-info text-center">
                                    <h4><?= $pml_by_tim_default[0]['nama']; ?></yh4>
                                        <span class="badge bg-light-secondary">PML /
                                            <?= $pml_by_tim_default[0]['nim']; ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <!-- FOR EACH DISNI -->
                        <?php foreach ($ppl_by_tim_default as $ppl) : ?>
                            <div class="user-avatar-section col">
                                <div class="d-flex align-items-center flex-column">
                                    <img class="img-fluid rounded mt-3 mb-2" src="<?= $url_foto; ?><?= $ppl['foto']; ?>" height="110" width="110" alt="User avatar" />
                                    <div class="user-info text-center">
                                        <h4><?= $ppl['nama']; ?></h4>
                                        <span class="badge bg-light-secondary">PPL /
                                            <?= $ppl['nim']; ?></span>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                        <!-- END OF FOREACH -->
                    </div>
                    <!-- END OF PERSON -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-bs-dismiss="modal">
                        Ok
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- PROGRESSNYA, DEFAULT = SEMUANYA  -->
<div id="progress_progress">
    <?php
    // PROGRESS PERTIM
    $prog = $wilayah->progress_pertim($idtim);
    ?>

    <?php if (!empty($prog)) : ?>
        <h5> <?= $prog['nilai']; ?>% (<?= $prog['terisi'];; ?> selesai dari <?= $prog['semua']; ?>
            sampel)</h5>
        <div class="progress progress-bar-primary">
            <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="80" aria-valuemin="80" aria-valuemax="100" style="width: <?= $prog['nilai']; ?>%"></div>
        </div>
    <?php endif ?>

    <?php if (empty($prog)) : ?>
        <h5> TIM BELUM MEMILIKI PROGRESS PENCACAHAN </h5>
        <div class="progress progress-bar-primary">
            <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="80" aria-valuemin="80" aria-valuemax="100" style="width: 0%"></div>
        </div>
    <?php endif ?>
</div>

<!-- CONTENT HERE -->
<div id="table-w2">
    <div class="table-responsive">
        <table class="table my-2" id="table-2">
            <thead>
                <tr class="text-center">
                    <th>Kode BS</th>
                    <th>Pencacah</th>
                    <th>Jumlah Sampel</th>
                    <th>Sampel Selesai</th>
                    <th>Progress</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($sampel_by_tim_default as $s) : ?>
                    <?php
                    $progress = $wilayah->progress_bs($s['kodeBs']);
                    ?>
                    <tr class="text-center">
                        <td><?= $s['kodeBs']; ?></td>
                        <td> <?= $s['nama']; ?> (<?= $s['nim']; ?>)</td>
                        <td><?= $s['jumlah_sampel']; ?></td>

                        <td> <?= $progress['terisi'] ?> </td>
                        <td> <?= $progress['nilai'] ?>%
                            <div class="progress progress-bar-primary">
                                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="80" aria-valuemin="80" aria-valuemax="100" style="width: <?= $progress['nilai'] ?>%"></div>
                            </div>
                        </td>
                        <td>
                            <a href="/riset-1-2/sampel/detail/<?= $s['kodeBs']; ?>" class="btn btn-success">Detail</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>



<?php
$data = [
    'rep' => 2,
    'judul' => "Progress Sampel Berdasarkan Tim di Riset 1 & 2 Integrasi (Kota Malang)"
];
echo view('layout/datatable', $data); ?>