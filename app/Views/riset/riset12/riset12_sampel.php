<?php $this->extend('layout/template'); ?>
<?= $this->section('content'); ?>

<div class="card">
    <div class="card-body">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-start mb-0">Sampel</h2>
                <div class="breadcrumb-wrapper">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">Riset 1 & 2 (Integrasi)</li>
                        <li class="breadcrumb-item active">Sampel</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>

<div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <ul class="nav nav-pills nav-fill">
                        <li class="nav-item">
                            <a class="nav-link active fw-bolder" id="blok-i-ADD-UNIQUE-tab-fill" data-bs-toggle="pill"
                                href="#blok-i-ADD-UNIQUE-fill" aria-expanded="true" onclick="eligible_r1()">Eligible
                                Riset 1
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link fw-bolder" id="blok-ii-ADD-UNIQUE-tab-fill" data-bs-toggle="pill"
                                href="#blok-ii-ADD-UNIQUE-fill" aria-expanded="false" onclick="eligible_r2()">Eligible
                                Riset 2
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link fw-bolder" id="blok-iii-ADD-UNIQUE-tab-fill" data-bs-toggle="pill"
                                href="#blok-iii-ADD-UNIQUE-fill" onclick="eligible_r12()">Eligible Riset 1 dan 2
                            </a>
                        </li>

                    </ul>

                    <div class="tab-content">

                        <!-- ELLIGIBEL RISET 1  -->
                        <div role="tabpanel" class="tab-pane active" id="blok-i-ADD-UNIQUE-fill"
                            aria-labelledby="blok-i-ADD-UNIQUE-tab-fill" aria-expanded="true">

                            <!-- //TODO 1 : UBAH JUDULNYA SESUAI JUDUL LISTING/SAMPEL NYA -->
                            <h1 class="text-center">
                                Progress Sampel Survei Integrasi Riset 1 & Riset 2 (<i>Pilot Survey Wisatawan
                                    Nusantara</i>)
                            </h1>
                            <h2 class="mt-1" id="title-progress-listingw">Progress Sampel Berdasarkan Blok Sensus
                            </h2>

                            <!-- //TODO 2 : (OPSIONAL) PAKAI INI JIKA TABLE YANG INGIN DITAMPILKAN > 1. PERHATIKAN PENAMAANNYA -->
                            <!-- PENAMAANNYA DISINI PAKE table-w1 , table-w2 untuk option value -->
                            <!-- SELECT BERDASARKAN KATEGORI -->
                            <div class="col-lg-2 col-12">
                                <select class="form-select" id="select-progress-w">
                                    <option value="table-w1">Blok Sensus</option>
                                    <option value="table-w2">Kecamatan</option>
                                    <option value="table-w3">Desa / Kelurahan</option>
                                    <option value="table-w4">Semua Sampel</option>
                                </select>
                            </div>

                            <!-- //TODO 3 : BIKIN TABLE NYA DISINI, DIBUNGKUS DENGAN NAMA NILAI OPTION VALUE TADI-->
                            <!-- TABLE-1 : JUMLAH BLOK SENSUS -->
                            <div id="table-w1">
                                <div class="table-responsive">
                                    <table class="table my-2" id="table-1">
                                        <thead>
                                            <tr class="text-center">
                                                <th>Kode BS</th>
                                                <th>Pencacah</th>
                                                <th>Jumlah Sampel</th>
                                                <th>Jumlah Terlisting</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($r1_sampel_by_bs as $s) : ?>
                                            <tr class="text-center">
                                                <td><?= $s['kode_bs']; ?></td>
                                                <td><?= $s['nama_pcl']; ?> (<?= $s['nim']; ?>)</td>
                                                <td><?= $s['jumlah_sampel']; ?></td>
                                                <td><?= $s['jumlah_listing']; ?></td>
                                                <td>
                                                    <a href="/riset-1-2/sampel/1/detail/<?= $s['kode_bs']; ?>"
                                                        class="btn btn-success">Detail
                                                    </a>
                                                </td>
                                            </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <!-- TABLE-2 : JUMLAH KECAMATAN -->
                            <div id="table-w2" style="display: none;">
                                <div class="table-responsive">
                                    <table class="table my-2" id="table-2">
                                        <thead>
                                            <tr class="text-center">
                                                <th>Nama Kecamatan</th>
                                                <th>Jumlah Sampel</th>
                                                <th>Jumlah Terlisting</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($r1_sampel_by_kecamatan as $s) : ?>
                                            <tr class="text-center">
                                                <td><?= $s['nama_kecamatan']; ?></td>
                                                <td><?= $s['jumlah_sampel']; ?></td>
                                                <td><?= $s['jumlah_listing']; ?></td>
                                                <td>
                                                    <a href="/riset-1-2/sampel/1/detail/<?= $s['kode_kecamatan']; ?>"
                                                        class="btn btn-success">Detail
                                                    </a>
                                                </td>
                                            </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <!-- TABLE-3 : JUMLAH DESA KELURAHAN -->
                            <div id="table-w3" style="display: none;">
                                <div class="table-responsive">
                                    <table class="table my-2" id="table-3">
                                        <thead>
                                            <tr class="text-center">
                                                <th>Nama Desa / Kelurahan</th>
                                                <th>Jumlah Sampel</th>
                                                <th>Jumlah Terlisting</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($r1_sampel_by_kelurahandesa as $s) : ?>
                                            <tr class="text-center">
                                                <td><?= $s['nama_desa']; ?></td>
                                                <td><?= $s['jumlah_sampel']; ?></td>
                                                <td><?= $s['jumlah_listing']; ?></td>
                                                <td>
                                                    <a href="/riset-1-2/sampel/1/detail/<?= $s['kode_kecamatan']; ?>/<?= $s['kode_desa']; ?>"
                                                        class="btn btn-success">Detail
                                                    </a>
                                                </td>
                                            </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <!-- TABLE-4 : SELURUH LISTING -->
                            <div id="table-w4" style="display: none;">
                                <div class="table-responsive">
                                    <table class="table my-2" id="table-10">
                                        <thead>
                                            <tr class="text-center">
                                                <th>Kode Ruta</th>
                                                <th>NIM PPL</th>
                                                <th>Nama PPL</th>
                                                <th>Kode Bs</th>
                                                <th>Nomor Segmen</th>
                                                <th>Nomor bf</th>
                                                <th>Nomor bs</th>
                                                <th>Nomor Urut Ruta</th>
                                                <th>Nama Kepala Rumah Tangga</th>
                                                <th>Jumlah Anggota Rumah Tangga</th>
                                                <th>Alamat</th>
                                                <th>Kode Kabupaten/Kota</th>
                                                <th>Kabupaten/Kota</th>
                                                <th>Kode Kecamatan</th>
                                                <th>Kecamatan</th>
                                                <th>Kode Desa/Kelurahan</th>
                                                <th>Desa/Kelurahan</th>
                                                <th>Objek Wisata</th>
                                                <th>Akomodasi Komersial</th>
                                                <th>Lainnya</th>
                                                <th>TTPWDalam</th>
                                                <th>TTPWLuar</th>
                                                <th>Nomor Ruta Luar Kota</th>
                                                <th>Nomor Ruta Dalam Kota</th>
                                                <th>Kode Eligible</th>
                                                <th>Keterangan</th>
                                                <th>Latitude</th>
                                                <th>Longitude</th>
                                                <th>Akurasi</th>
                                                <th>Waktu Pencacahan</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($r1_sampel_all as $l) : ?>
                                            <tr class="text-center">
                                                <td><?= $l['kodeRuta']; ?></td>
                                                <td><?= $l['nim_pcl']; ?></td>
                                                <td><?= $l['nama_pcl']; ?></td>
                                                <td><?= $l['kodeBs']; ?></td>
                                                <td><?= $l['noSegmen']; ?></td>
                                                <td><?= $l['bf']; ?></td>
                                                <td><?= $l['bs']; ?></td>
                                                <td><?= $l['noUrutRuta']; ?></td>
                                                <td><?= $l['namaKrt']; ?></td>
                                                <td><?= $l['jmlhanggotaruta']; ?></td>
                                                <td><?= $l['alamat']; ?></td>
                                                <td>3573</td>
                                                <td>Kota Malang</td>
                                                <td><?= $l['kode_kecamatan']; ?></td>
                                                <td><?= $l['nama_kecamatan']; ?></td>
                                                <td><?= $l['kode_desa']; ?></td>
                                                <td><?= $l['nama_desa']; ?></td>
                                                <td><?= $l['objekWisata']; ?></td>
                                                <td><?= $l['akomodasiKomersial']; ?></td>
                                                <td><?= $l['lainnya']; ?></td>
                                                <td><?= $l['TTPWDalam']; ?></td>
                                                <td><?= $l['TTPWLuar']; ?></td>
                                                <td><?= $l['nortluarkota']; ?></td>
                                                <td><?= $l['nortdalamkota']; ?></td>
                                                <td><?= $l['kodeeligible']; ?></td>
                                                <td><?= $l['pemberiInformasi']; ?></td>
                                                <td><?= $l['latitude']; ?></td>
                                                <td><?= $l['longitude']; ?></td>
                                                <td><?= $l['akurasi']; ?></td>
                                                <td><?= $l['time']; ?></td>
                                            </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>

                        <!-- ELLIGIBEL RISET 2  -->
                        <div class="tab-pane" id="blok-ii-ADD-UNIQUE-fill" role="tabpanel"
                            aria-labelledby="blok-ii-ADD-UNIQUE-tab-fill" aria-expanded="false">

                            <!-- //TODO 1 : UBAH JUDULNYA SESUAI JUDUL LISTING/SAMPEL NYA -->
                            <h1 class="text-center">
                                Progress Sampel Survei Integrasi Riset 1 & Riset 2 (<i>Pilot Survey Wisatawan
                                    Nusantara</i>)
                            </h1>
                            <h2 class="mt-1" id="title-progress-listingww">Progress Sampel Berdasarkan Blok Sensus
                            </h2>

                            <!-- //TODO 2 : (OPSIONAL) PAKAI INI JIKA TABLE YANG INGIN DITAMPILKAN > 1. PERHATIKAN PENAMAANNYA -->
                            <!-- PENAMAANNYA DISINI PAKE table-w1 , table-w2 untuk option value -->
                            <!-- SELECT BERDASARKAN KATEGORI -->
                            <div class="col-lg-2 col-12">
                                <select class="form-select" id="select-progress-ww">
                                    <option value="table-ww1">Blok Sensus</option>
                                    <option value="table-ww2">Kecamatan</option>
                                    <option value="table-ww3">Desa / Kelurahan</option>
                                    <option value="table-ww4">Semua Sampel</option>
                                </select>
                            </div>

                            <!-- //TODO 3 : BIKIN TABLE NYA DISINI, DIBUNGKUS DENGAN NAMA NILAI OPTION VALUE TADI-->
                            <!-- TABLE-1 : JUMLAH BLOK SENSUS -->
                            <div id="table-ww1">
                                <div class="table-responsive">
                                    <table class="table my-2" id="table-4">
                                        <thead>
                                            <tr class="text-center">
                                                <th>Kode BS</th>
                                                <th>Pencacah</th>
                                                <th>Jumlah Sampel</th>
                                                <th>Jumlah Terlisting</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($r2_sampel_by_bs as $s) : ?>
                                            <tr class="text-center">
                                                <td><?= $s['kode_bs']; ?></td>
                                                <td><?= $s['nama_pcl']; ?> (<?= $s['nim']; ?>)</td>
                                                <td><?= $s['jumlah_sampel']; ?></td>
                                                <td><?= $s['jumlah_listing']; ?></td>
                                                <td>
                                                    <a href="/riset-1-2/sampel/2/detail/<?= $s['kode_bs']; ?>"
                                                        class="btn btn-success">Detail
                                                    </a>
                                                </td>
                                            </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <!-- TABLE-2 : JUMLAH KECAMATAN -->
                            <div id="table-ww2" style="display: none;">
                                <div class="table-responsive">
                                    <table class="table my-2" id="table-5">
                                        <thead>
                                            <tr class="text-center">
                                                <th>Nama Kecamatan</th>
                                                <th>Jumlah Sampel</th>
                                                <th>Jumlah Terlisting</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($r2_sampel_by_kecamatan as $s) : ?>
                                            <tr class="text-center">
                                                <td><?= $s['nama_kecamatan']; ?></td>
                                                <td><?= $s['jumlah_sampel']; ?></td>
                                                <td><?= $s['jumlah_listing']; ?></td>
                                                <td>
                                                    <a href="/riset-1-2/sampel/2/detail/<?= $s['kode_kecamatan']; ?>"
                                                        class="btn btn-success">Detail
                                                    </a>
                                                </td>
                                            </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <!-- TABLE-3 : JUMLAH DESA KELURAHAN -->
                            <div id="table-ww3" style="display: none;">
                                <div class="table-responsive">
                                    <table class="table my-2" id="table-6">
                                        <thead>
                                            <tr class="text-center">
                                                <th>Nama Desa / Kelurahan</th>
                                                <th>Jumlah Sampel</th>
                                                <th>Jumlah Terlisting</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($r2_sampel_by_kelurahandesa as $s) : ?>
                                            <tr class="text-center">
                                                <td><?= $s['nama_desa']; ?></td>
                                                <td><?= $s['jumlah_sampel']; ?></td>
                                                <td><?= $s['jumlah_listing']; ?></td>
                                                <td>
                                                    <a href="/riset-1-2/sampel/2/detail/<?= $s['kode_kecamatan']; ?>/<?= $s['kode_desa']; ?>"
                                                        class="btn btn-success">Detail
                                                    </a>
                                                </td>
                                            </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <!-- TABLE-4 : SELURUH LISTING -->
                            <div id="table-ww4" style="display: none;">
                                <div class="table-responsive">
                                    <table class="table my-2" id="table-11">
                                        <thead>
                                            <tr class="text-center">
                                                <th>Kode Ruta</th>
                                                <th>NIM PPL</th>
                                                <th>Nama PPL</th>
                                                <th>Kode Bs</th>
                                                <th>Nomor Segmen</th>
                                                <th>Nomor bf</th>
                                                <th>Nomor bs</th>
                                                <th>Nomor Urut Ruta</th>
                                                <th>Nama Kepala Rumah Tangga</th>
                                                <th>Jumlah Anggota Rumah Tangga</th>
                                                <th>Alamat</th>
                                                <th>Kode Kabupaten/Kota</th>
                                                <th>Kabupaten/Kota</th>
                                                <th>Kode Kecamatan</th>
                                                <th>Kecamatan</th>
                                                <th>Kode Desa/Kelurahan</th>
                                                <th>Desa/Kelurahan</th>
                                                <th>Objek Wisata</th>
                                                <th>Akomodasi Komersial</th>
                                                <th>Lainnya</th>
                                                <th>TTPWDalam</th>
                                                <th>TTPWLuar</th>
                                                <th>Nomor Ruta Luar Kota</th>
                                                <th>Nomor Ruta Dalam Kota</th>
                                                <th>Kode Eligible</th>
                                                <th>Keterangan</th>
                                                <th>Latitude</th>
                                                <th>Longitude</th>
                                                <th>Akurasi</th>
                                                <th>Waktu Pencacahan</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($r2_sampel_all as $l) : ?>
                                            <tr class="text-center">
                                                <td><?= $l['kodeRuta']; ?></td>
                                                <td><?= $l['nim_pcl']; ?></td>
                                                <td><?= $l['nama_pcl']; ?></td>
                                                <td><?= $l['kodeBs']; ?></td>
                                                <td><?= $l['noSegmen']; ?></td>
                                                <td><?= $l['bf']; ?></td>
                                                <td><?= $l['bs']; ?></td>
                                                <td><?= $l['noUrutRuta']; ?></td>
                                                <td><?= $l['namaKrt']; ?></td>
                                                <td><?= $l['jmlhanggotaruta']; ?></td>
                                                <td><?= $l['alamat']; ?></td>
                                                <td>3573</td>
                                                <td>Kota Malang</td>
                                                <td><?= $l['kode_kecamatan']; ?></td>
                                                <td><?= $l['nama_kecamatan']; ?></td>
                                                <td><?= $l['kode_desa']; ?></td>
                                                <td><?= $l['nama_desa']; ?></td>
                                                <td><?= $l['objekWisata']; ?></td>
                                                <td><?= $l['akomodasiKomersial']; ?></td>
                                                <td><?= $l['lainnya']; ?></td>
                                                <td><?= $l['TTPWDalam']; ?></td>
                                                <td><?= $l['TTPWLuar']; ?></td>
                                                <td><?= $l['nortluarkota']; ?></td>
                                                <td><?= $l['nortdalamkota']; ?></td>
                                                <td><?= $l['kodeeligible']; ?></td>
                                                <td><?= $l['pemberiInformasi']; ?></td>
                                                <td><?= $l['latitude']; ?></td>
                                                <td><?= $l['longitude']; ?></td>
                                                <td><?= $l['akurasi']; ?></td>
                                                <td><?= $l['time']; ?></td>
                                            </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>

                        <!-- ELLIGIBEL RISET 1 DAN RISET 2 -->
                        <div class="tab-pane" id="blok-iii-ADD-UNIQUE-fill" role="tabpanel"
                            aria-labelledby="blok-iii-ADD-UNIQUE-tab-fill" aria-expanded="false">

                            <!-- //TODO 1 : UBAH JUDULNYA SESUAI JUDUL LISTING/SAMPEL NYA -->
                            <h1 class="text-center">
                                Progress Sampel Survei Integrasi Riset 1 & Riset 2 (<i>Pilot Survey Wisatawan
                                    Nusantara</i>)
                            </h1>
                            <h2 class="mt-1" id="title-progress-listingwww">Progress Sampel Berdasarkan Blok Sensus
                            </h2>

                            <!-- //TODO 2 : (OPSIONAL) PAKAI INI JIKA TABLE YANG INGIN DITAMPILKAN > 1. PERHATIKAN PENAMAANNYA -->
                            <!-- PENAMAANNYA DISINI PAKE table-w1 , table-w2 untuk option value -->
                            <!-- SELECT BERDASARKAN KATEGORI -->
                            <div class="col-lg-2 col-12">
                                <select class="form-select" id="select-progress-www">
                                    <option value="table-www1">Blok Sensus</option>
                                    <option value="table-www2">Kecamatan</option>
                                    <option value="table-www3">Desa / Kelurahan</option>
                                    <option value="table-www4">Semua Sampel</option>
                                </select>
                            </div>

                            <!-- //TODO 3 : BIKIN TABLE NYA DISINI, DIBUNGKUS DENGAN NAMA NILAI OPTION VALUE TADI-->
                            <!-- TABLE-1 : JUMLAH BLOK SENSUS -->
                            <div id="table-www1">
                                <div class="table-responsive">
                                    <table class="table my-2" id="table-7">
                                        <thead>
                                            <tr class="text-center">
                                                <th>Kode BS</th>
                                                <th>Pencacah</th>
                                                <th>Jumlah Sampel</th>
                                                <th>Jumlah Terlisting</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($r3_sampel_by_bs as $s) : ?>
                                            <tr class="text-center">
                                                <td><?= $s['kode_bs']; ?></td>
                                                <td><?= $s['nama_pcl']; ?> (<?= $s['nim']; ?>)</td>
                                                <td><?= $s['jumlah_sampel']; ?></td>
                                                <td><?= $s['jumlah_listing']; ?></td>
                                                <td>
                                                    <a href="/riset-1-2/sampel/3/detail/<?= $s['kode_bs']; ?>"
                                                        class="btn btn-success">Detail
                                                    </a>
                                                </td>
                                            </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <!-- TABLE-2 : JUMLAH KECAMATAN -->
                            <div id="table-www2" style="display: none;">
                                <div class="table-responsive">
                                    <table class="table my-2" id="table-8">
                                        <thead>
                                            <tr class="text-center">
                                                <th>Nama Kecamatan</th>
                                                <th>Jumlah Sampel</th>
                                                <th>Jumlah Terlisting</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($r3_sampel_by_kecamatan as $s) : ?>
                                            <tr class="text-center">
                                                <td><?= $s['nama_kecamatan']; ?></td>
                                                <td><?= $s['jumlah_sampel']; ?></td>
                                                <td><?= $s['jumlah_listing']; ?></td>
                                                <td>
                                                    <a href="/riset-1-2/sampel/3/detail/<?= $s['kode_kecamatan']; ?>"
                                                        class="btn btn-success">Detail
                                                    </a>
                                                </td>
                                            </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <!-- TABLE-3 : JUMLAH DESA KELURAHAN -->
                            <div id="table-www3" style="display: none;">
                                <div class="table-responsive">
                                    <table class="table my-2" id="table-9">
                                        <thead>
                                            <tr class="text-center">
                                                <th>Nama Desa / Kelurahan</th>
                                                <th>Jumlah Sampel</th>
                                                <th>Jumlah Terlisting</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($r3_sampel_by_kelurahandesa as $s) : ?>
                                            <tr class="text-center">
                                                <td><?= $s['nama_desa']; ?></td>
                                                <td><?= $s['jumlah_sampel']; ?></td>
                                                <td><?= $s['jumlah_listing']; ?></td>
                                                <td>
                                                    <a href="/riset-1-2/sampel/3/detail/<?= $s['kode_kecamatan']; ?>/<?= $s['kode_desa']; ?>"
                                                        class="btn btn-success">Detail
                                                    </a>
                                                </td>
                                            </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <!-- TABLE-4 : SELURUH LISTING -->
                            <div id="table-www4" style="display: none;">
                                <div class="table-responsive">
                                    <table class="table my-2" id="table-12">
                                        <thead>
                                            <tr class="text-center">
                                                <th>Kode Ruta</th>
                                                <th>NIM PPL</th>
                                                <th>Nama PPL</th>
                                                <th>Kode Bs</th>
                                                <th>Nomor Segmen</th>
                                                <th>Nomor bf</th>
                                                <th>Nomor bs</th>
                                                <th>Nomor Urut Ruta</th>
                                                <th>Nama Kepala Rumah Tangga</th>
                                                <th>Jumlah Anggota Rumah Tangga</th>
                                                <th>Alamat</th>
                                                <th>Kode Kabupaten/Kota</th>
                                                <th>Kabupaten/Kota</th>
                                                <th>Kode Kecamatan</th>
                                                <th>Kecamatan</th>
                                                <th>Kode Desa/Kelurahan</th>
                                                <th>Desa/Kelurahan</th>
                                                <th>Objek Wisata</th>
                                                <th>Akomodasi Komersial</th>
                                                <th>Lainnya</th>
                                                <th>TTPWDalam</th>
                                                <th>TTPWLuar</th>
                                                <th>Nomor Ruta Luar Kota</th>
                                                <th>Nomor Ruta Dalam Kota</th>
                                                <th>Kode Eligible</th>
                                                <th>Keterangan</th>
                                                <th>Latitude</th>
                                                <th>Longitude</th>
                                                <th>Akurasi</th>
                                                <th>Waktu Pencacahan</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($r3_sampel_all as $l) : ?>
                                            <tr class="text-center">
                                                <td><?= $l['kodeRuta']; ?></td>
                                                <td><?= $l['nim_pcl']; ?></td>
                                                <td><?= $l['nama_pcl']; ?></td>
                                                <td><?= $l['kodeBs']; ?></td>
                                                <td><?= $l['noSegmen']; ?></td>
                                                <td><?= $l['bf']; ?></td>
                                                <td><?= $l['bs']; ?></td>
                                                <td><?= $l['noUrutRuta']; ?></td>
                                                <td><?= $l['namaKrt']; ?></td>
                                                <td><?= $l['jmlhanggotaruta']; ?></td>
                                                <td><?= $l['alamat']; ?></td>
                                                <td>3573</td>
                                                <td>Kota Malang</td>
                                                <td><?= $l['kode_kecamatan']; ?></td>
                                                <td><?= $l['nama_kecamatan']; ?></td>
                                                <td><?= $l['kode_desa']; ?></td>
                                                <td><?= $l['nama_desa']; ?></td>
                                                <td><?= $l['objekWisata']; ?></td>
                                                <td><?= $l['akomodasiKomersial']; ?></td>
                                                <td><?= $l['lainnya']; ?></td>
                                                <td><?= $l['TTPWDalam']; ?></td>
                                                <td><?= $l['TTPWLuar']; ?></td>
                                                <td><?= $l['nortluarkota']; ?></td>
                                                <td><?= $l['nortdalamkota']; ?></td>
                                                <td><?= $l['kodeeligible']; ?></td>
                                                <td><?= $l['pemberiInformasi']; ?></td>
                                                <td><?= $l['latitude']; ?></td>
                                                <td><?= $l['longitude']; ?></td>
                                                <td><?= $l['akurasi']; ?></td>
                                                <td><?= $l['time']; ?></td>
                                            </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>

</div>

<!-- //TODO 4 : TAMBAHIN JQUERY BIAR DATATABLE NYA BISA JALAN -->
<!-- INI DITAMBAHIN SESUAI DENGAN JUMLAH TABLE -->
<?php
$data = [
    'rep' => 1,
    'judul' => 'Progress Sampel Berdasarkan Blok Sensus Survei MPD Riset 1 (Kota Surabaya)'
];
echo view('layout/datatable', $data); ?>

<?php
$data = [
    'rep' => 2,
    'judul' => 'Progress Sampel Berdasarkan Blok Sensus Survei MPD Riset 1 (Kota Surabaya)'
];
echo view('layout/datatable', $data); ?> ?>

<?php
$data = [
    'rep' => 3,
    'judul' => 'Progress Sampel Berdasarkan Blok Sensus Survei MPD Riset 1 (Kota Surabaya)'
];
echo view('layout/datatable', $data); ?>

<?php
$data = [
    'rep' => 4,
    'judul' => 'Progress Sampel Berdasarkan Blok Sensus Survei MPD Riset 1 (Kota Surabaya)'
];
echo view('layout/datatable', $data); ?>

<?php
$data = [
    'rep' => 5,
    'judul' => 'Progress Sampel Berdasarkan Blok Sensus Survei MPD Riset 1 (Kota Surabaya)'
];
echo view('layout/datatable', $data); ?> ?>

<?php
$data = [
    'rep' => 6,
    'judul' => 'Progress Sampel Berdasarkan Blok Sensus Survei MPD Riset 1 (Kota Surabaya)'
];
echo view('layout/datatable', $data); ?>

<?php
$data = [
    'rep' => 7,
    'judul' => 'Progress Sampel Berdasarkan Blok Sensus Survei MPD Riset 1 (Kota Surabaya)'
];
echo view('layout/datatable', $data); ?>

<?php
$data = [
    'rep' => 8,
    'judul' => 'Progress Sampel Berdasarkan Blok Sensus Survei MPD Riset 1 (Kota Surabaya)'
];
echo view('layout/datatable', $data); ?> ?>

<?php
$data = [
    'rep' => 9,
    'judul' => 'Progress Sampel Berdasarkan Blok Sensus Survei MPD Riset 1 (Kota Surabaya)'
];
echo view('layout/datatable', $data); ?>

<?php
$data = [
    'rep' => 10,
    'judul' => 'Progress Sampel Keseluruhan Berdasarkan Blok Sensus Survei MPD Riset 1 (Kota Surabaya)'
];
echo view('layout/datatable', $data); ?>

<?php
$data = [
    'rep' => 11,
    'judul' => 'Progress Sampel Keseluruhan Berdasarkan Blok Sensus Survei MPD Riset 1 (Kota Surabaya)'
];
echo view('layout/datatable', $data); ?>

<?php
$data = [
    'rep' => 12,
    'judul' => 'Progress Sampel Keseluruhan Berdasarkan Blok Sensus Survei MPD Riset 1 (Kota Surabaya)'
];
echo view('layout/datatable', $data); ?>



<script>
$('#select-progress-w').change(function() {
    var val = $('#select-progress-w').val();
    if (val == 'table-w1') {
        document.getElementById('title-progress-listingw').innerHTML =
            'Progress Sampel Berdasarkan Blok Sensus';
        document.getElementById('table-w1').style.display = '';
        document.getElementById('table-w2').style.display = 'none';
        document.getElementById('table-w3').style.display = 'none';
        document.getElementById('table-ww1').style.display = 'none';
        document.getElementById('table-ww2').style.display = 'none';
        document.getElementById('table-ww3').style.display = 'none';
        document.getElementById('table-www1').style.display = 'none';
        document.getElementById('table-www2').style.display = 'none';
        document.getElementById('table-www3').style.display = 'none';

        document.getElementById('table-w4').style.display = 'none';
        document.getElementById('table-ww4').style.display = 'none';
        document.getElementById('table-www4').style.display = 'none';

    } else if (val == 'table-w2') {
        document.getElementById('title-progress-listingw').innerHTML =
            'Progress Sampel Berdasarkan Kecamatan';
        document.getElementById('table-w1').style.display = 'none';
        document.getElementById('table-w2').style.display = '';
        document.getElementById('table-w3').style.display = 'none';
        document.getElementById('table-ww1').style.display = 'none';
        document.getElementById('table-ww2').style.display = 'none';
        document.getElementById('table-ww3').style.display = 'none';
        document.getElementById('table-www1').style.display = 'none';
        document.getElementById('table-www2').style.display = 'none';
        document.getElementById('table-www3').style.display = 'none';

        document.getElementById('table-w4').style.display = 'none';
        document.getElementById('table-ww4').style.display = 'none';
        document.getElementById('table-www4').style.display = 'none';
    } else if (val == 'table-w3') {
        document.getElementById('title-progress-listingw').innerHTML =
            'Progress Sampel Berdasarkan Kelurahan/Desa';
        document.getElementById('table-w1').style.display = 'none';
        document.getElementById('table-w2').style.display = 'none';
        document.getElementById('table-w3').style.display = '';
        document.getElementById('table-ww1').style.display = 'none';
        document.getElementById('table-ww2').style.display = 'none';
        document.getElementById('table-ww3').style.display = 'none';
        document.getElementById('table-www1').style.display = 'none';
        document.getElementById('table-www2').style.display = 'none';
        document.getElementById('table-www3').style.display = 'none';

        document.getElementById('table-w4').style.display = 'none';
        document.getElementById('table-ww4').style.display = 'none';
        document.getElementById('table-www4').style.display = 'none';
    } else if (val == 'table-w4') {
        document.getElementById('title-progress-listingw').innerHTML =
            'Progress Sampel Berdasarkan Kelurahan/Desa';
        document.getElementById('table-w1').style.display = 'none';
        document.getElementById('table-w2').style.display = 'none';
        document.getElementById('table-w3').style.display = 'none';
        document.getElementById('table-ww1').style.display = 'none';
        document.getElementById('table-ww2').style.display = 'none';
        document.getElementById('table-ww3').style.display = 'none';
        document.getElementById('table-www1').style.display = 'none';
        document.getElementById('table-www2').style.display = 'none';
        document.getElementById('table-www3').style.display = 'none';

        document.getElementById('table-w4').style.display = '';
        document.getElementById('table-ww4').style.display = 'none';
        document.getElementById('table-www4').style.display = 'none';
    }
});
</script>

<script>
$('#select-progress-ww').change(function() {
    var val = $('#select-progress-ww').val();
    if (val == 'table-ww1') {
        document.getElementById('title-progress-listingww').innerHTML =
            'Progress Sampel Berdasarkan Blok Sensus';
        document.getElementById('table-w1').style.display = 'none';
        document.getElementById('table-w2').style.display = 'none';
        document.getElementById('table-w3').style.display = 'none';
        document.getElementById('table-ww1').style.display = '';
        document.getElementById('table-ww2').style.display = 'none';
        document.getElementById('table-ww3').style.display = 'none';
        document.getElementById('table-www1').style.display = 'none';
        document.getElementById('table-www2').style.display = 'none';
        document.getElementById('table-www3').style.display = 'none';

    } else if (val == 'table-ww2') {
        document.getElementById('title-progress-listingww').innerHTML =
            'Progress Sampel Berdasarkan Kecamatan)';
        document.getElementById('table-w1').style.display = 'none';
        document.getElementById('table-w2').style.display = 'none';
        document.getElementById('table-w3').style.display = 'none';
        document.getElementById('table-ww1').style.display = 'none';
        document.getElementById('table-ww2').style.display = '';
        document.getElementById('table-ww3').style.display = 'none';
        document.getElementById('table-www1').style.display = 'none';
        document.getElementById('table-www2').style.display = 'none';
        document.getElementById('table-www3').style.display = 'none';

        document.getElementById('table-w4').style.display = 'none';
        document.getElementById('table-ww4').style.display = 'none';
        document.getElementById('table-www4').style.display = 'none';

    } else if (val == 'table-ww3') {
        document.getElementById('title-progress-listingww').innerHTML =
            'Progress Sampel Berdasarkan Kelurahan/Desa';
        document.getElementById('table-w1').style.display = 'none';
        document.getElementById('table-w2').style.display = 'none';
        document.getElementById('table-w3').style.display = 'none';
        document.getElementById('table-ww1').style.display = 'none';
        document.getElementById('table-ww2').style.display = 'none';
        document.getElementById('table-ww3').style.display = '';
        document.getElementById('table-www1').style.display = 'none';
        document.getElementById('table-www2').style.display = 'none';
        document.getElementById('table-www3').style.display = 'none';

        document.getElementById('table-w4').style.display = 'none';
        document.getElementById('table-ww4').style.display = 'none';
        document.getElementById('table-www4').style.display = 'none';
    } else if (val == 'table-ww4') {
        document.getElementById('title-progress-listingw').innerHTML =
            'Progress Sampel Berdasarkan Kelurahan/Desa';
        document.getElementById('table-w1').style.display = 'none';
        document.getElementById('table-w2').style.display = 'none';
        document.getElementById('table-w3').style.display = 'none';
        document.getElementById('table-ww1').style.display = 'none';
        document.getElementById('table-ww2').style.display = 'none';
        document.getElementById('table-ww3').style.display = 'none';
        document.getElementById('table-www1').style.display = 'none';
        document.getElementById('table-www2').style.display = 'none';
        document.getElementById('table-www3').style.display = 'none';

        document.getElementById('table-w4').style.display = 'none';
        document.getElementById('table-ww4').style.display = '';
        document.getElementById('table-www4').style.display = 'none';
    }
});
</script>

<script>
$('#select-progress-www').change(function() {
    var val = $('#select-progress-www').val();
    if (val == 'table-www1') {
        document.getElementById('title-progress-listingwww').innerHTML =
            'Progress Sampel Berdasarkan Blok Sensus';
        document.getElementById('table-w1').style.display = 'none';
        document.getElementById('table-w2').style.display = 'none';
        document.getElementById('table-w3').style.display = 'none';
        document.getElementById('table-ww1').style.display = 'none';
        document.getElementById('table-ww2').style.display = 'none';
        document.getElementById('table-ww3').style.display = 'none';
        document.getElementById('table-www1').style.display = '';
        document.getElementById('table-www2').style.display = 'none';
        document.getElementById('table-www3').style.display = 'none';

    } else if (val == 'table-www2') {
        document.getElementById('title-progress-listingwww').innerHTML =
            'Progress Sampel Berdasarkan Kecamatan';
        document.getElementById('table-w1').style.display = 'none';
        document.getElementById('table-w2').style.display = 'none';
        document.getElementById('table-w3').style.display = 'none';
        document.getElementById('table-ww1').style.display = 'none';
        document.getElementById('table-ww2').style.display = 'none';
        document.getElementById('table-ww3').style.display = 'none';
        document.getElementById('table-www1').style.display = 'none';
        document.getElementById('table-www2').style.display = '';
        document.getElementById('table-www3').style.display = 'none';

        document.getElementById('table-w4').style.display = 'none';
        document.getElementById('table-ww4').style.display = 'none';
        document.getElementById('table-www4').style.display = 'none';

    } else if (val == 'table-www3') {
        document.getElementById('title-progress-listingwww').innerHTML =
            'Progress Sampel Berdasarkan Kelurahan/Desa';
        document.getElementById('table-w1').style.display = 'none';
        document.getElementById('table-w2').style.display = 'none';
        document.getElementById('table-w3').style.display = 'none';
        document.getElementById('table-ww1').style.display = 'none';
        document.getElementById('table-ww2').style.display = 'none';
        document.getElementById('table-ww3').style.display = 'none';
        document.getElementById('table-www1').style.display = 'none';
        document.getElementById('table-www2').style.display = 'none';
        document.getElementById('table-www3').style.display = '';

        document.getElementById('table-w4').style.display = 'none';
        document.getElementById('table-ww4').style.display = 'none';
        document.getElementById('table-www4').style.display = 'none';
    } else if (val == 'table-www4') {
        document.getElementById('title-progress-listingw').innerHTML =
            'Progress Sampel Berdasarkan Kelurahan/Desa';
        document.getElementById('table-w1').style.display = 'none';
        document.getElementById('table-w2').style.display = 'none';
        document.getElementById('table-w3').style.display = 'none';
        document.getElementById('table-ww1').style.display = 'none';
        document.getElementById('table-ww2').style.display = 'none';
        document.getElementById('table-ww3').style.display = 'none';
        document.getElementById('table-www1').style.display = 'none';
        document.getElementById('table-www2').style.display = 'none';
        document.getElementById('table-www3').style.display = 'none';

        document.getElementById('table-w4').style.display = 'none';
        document.getElementById('table-ww4').style.display = 'none';
        document.getElementById('table-www4').style.display = '';
    }
});


function eligible_r1() {
    $('#select-progress-w').val('table-w1');
    document.getElementById('title-progress-listingw').innerHTML =
        'Progress Sampel Berdasarkan Blok Sensus';
    document.getElementById('table-w1').style.display = '';
    document.getElementById('table-w2').style.display = 'none';
    document.getElementById('table-w3').style.display = 'none';
    document.getElementById('table-ww1').style.display = 'none';
    document.getElementById('table-ww2').style.display = 'none';
    document.getElementById('table-ww3').style.display = 'none';
    document.getElementById('table-www1').style.display = 'none';
    document.getElementById('table-www2').style.display = 'none';
    document.getElementById('table-www3').style.display = 'none';

    document.getElementById('table-w4').style.display = 'none';
    document.getElementById('table-ww4').style.display = 'none';
    document.getElementById('table-www4').style.display = 'none';

}

function eligible_r2() {
    $('#select-progress-ww').val('table-ww1');
    document.getElementById('title-progress-listingww').innerHTML =
        'Progress Sampel Berdasarkan Blok Sensus';
    document.getElementById('table-w1').style.display = 'none';
    document.getElementById('table-w2').style.display = 'none';
    document.getElementById('table-w3').style.display = 'none';
    document.getElementById('table-ww1').style.display = '';
    document.getElementById('table-ww2').style.display = 'none';
    document.getElementById('table-ww3').style.display = 'none';
    document.getElementById('table-www1').style.display = 'none';
    document.getElementById('table-www2').style.display = 'none';
    document.getElementById('table-www3').style.display = 'none';

    document.getElementById('table-w4').style.display = 'none';
    document.getElementById('table-ww4').style.display = 'none';
    document.getElementById('table-www4').style.display = 'none';

}

function eligible_r12() {
    $('#select-progress-www').val('table-www1');
    document.getElementById('title-progress-listingwww').innerHTML =
        'Progress Sampel Berdasarkan Blok Sensus';
    document.getElementById('table-w1').style.display = 'none';
    document.getElementById('table-w2').style.display = 'none';
    document.getElementById('table-w3').style.display = 'none';
    document.getElementById('table-ww1').style.display = 'none';
    document.getElementById('table-ww2').style.display = 'none';
    document.getElementById('table-ww3').style.display = 'none';
    document.getElementById('table-www1').style.display = '';
    document.getElementById('table-www2').style.display = 'none';
    document.getElementById('table-www3').style.display = 'none';

    document.getElementById('table-w4').style.display = 'none';
    document.getElementById('table-ww4').style.display = 'none';
    document.getElementById('table-www4').style.display = 'none';

}
</script>

<?= $this->endSection(); ?>