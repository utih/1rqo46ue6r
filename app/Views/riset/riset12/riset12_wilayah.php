<?php $this->extend('layout/template');
?>
<?= $this->section('content'); ?>

<div class="card">
    <div class="card-body">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-start mb-0">Progress Pencacahan Wilayah</h2>
                <div class="breadcrumb-wrapper">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">Riset 1 & 2 (Integrasi)</li>
                        <li class="breadcrumb-item active">Progress Wilayah</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>

<div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="mb-2"> Pilih Wilayah Untuk Melihat Progress Pencacahan Riset 1 & 2 (Integrasi)</h4>

                    <!-- SELECT BERDASARKAN KATEGORI -->
                    <div class="col-lg-3 col-12">
                        <h6> Pilih Kecamatan </h6>
                        <select class="form-select" id="select-progress-kecamatan">
                            <option value="all_kecamatan">Semua Kecamatan</option>
                            <?php
                            $daerah = $wilayah->getAllKecamatan();
                            foreach ($daerah as $d) :
                            ?>
                                <option value="<?= $d['kode_kecamatan']; ?>"> <?= $d['nama_kecamatan']; ?> </option>
                            <?php endforeach ?>
                        </select>
                        <div class="mt-2" id="my_keldes"> </div>
                    </div>

                    <!-- AMBIL JUDUL YANG BENAR -->
                    <?php
                    $lokasi = "Kota Malang";
                    $daerah1 = $wilayah->getAllKecamatan();
                    ?>

                    <!-- PROGRESSNYA, DEFAULT = SEMUANYA  -->
                    <div id="progress_progress">
                        <?php
                        $prog = $wilayah->progress_all();
                        ?>
                        <h5 class="mt-2 w-100"> Progress Pencacahan : Keseluruhan (Riset 1 & 2 Integrasi) </h5>

                        <?php if ($prog == null) : ?>
                            <h5> WILAYAH BELUM MEMILIKI PROGRESS PENCACAHAN </h5>
                            <div class="progress progress-bar-primary">
                                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="80" aria-valuemin="80" aria-valuemax="100" style="width: 0%"></div>
                            </div>
                        <?php endif ?>

                        <?php if ($prog != null) : ?>
                            <h5> <?= $prog['nilai']; ?>% (<?= $prog['terisi']; ?> selesai dari <?= $prog['semua']; ?>
                                sampel)</h5>
                            <div class="progress progress-bar-primary">
                                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="80" aria-valuemin="80" aria-valuemax="100" style="width: <?= $prog['nilai']; ?>%"></div>
                            </div>
                        <?php endif ?>

                    </div>

                    <!-- TABELNYA, DEFAULT = KECAMATAN -->
                    <div id="tabel_tabel">
                        <div id="table-w1">
                            <div class="table-responsive">
                                <table class="table my-2" id="table-1">
                                    <thead>
                                        <tr class="text-center">
                                            <th>Nama Kecamatan</th>
                                            <th>Jumlah Sampel</th>
                                            <th>Sampel Selesai</th>
                                            <th>Progress</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $popo = $wilayah->sampel('kecamatan') ?>
                                        <?php foreach ($popo as $s) : ?>

                                            <?php
                                            $progress = $wilayah->progress_kecamatan($s['kode_kecamatan']);
                                            ?>

                                            <tr class="text-center">
                                                <td><?= $s['nama_kecamatan']; ?></td>
                                                <td><?= $s['jumlah_sampel']; ?></td>
                                                <td> <?= $progress['terisi'] ?> </td>
                                                <td> <?= $progress['nilai'] ?>%
                                                    <div class="progress progress-bar-primary">
                                                        <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="80" aria-valuemin="80" aria-valuemax="100" style="width: <?= $progress['nilai'] ?>%">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <a href="<?= $url_detail . $s['kode_kecamatan']; ?>" class="btn btn-success">Detail
                                                    </a>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- JUDUL DEFAULT RISET 4 -->
<?php
$data = [
    'rep' => 1,
    'judul' =>  "",
];
echo view('layout/datatable_new', $data); ?>

<!-- SCRIPT UNTUK PERUBAHAN KECAMATAN -->
<script>
    $('#select-progress-kecamatan').change(function() {
        var val = $('#select-progress-kecamatan').val();
        // IF DEFAULT, rubah juga yang lain jadi default
        if (val == 'all_kecamatan') {
            $("#my_keldes").html("");
        } else {
            $.ajax({
                url: "<?php echo base_url() . $url_ajax ?>/debug-send",
                type: "POST",
                data: {
                    data1: val,
                    data2: '',
                    url_ajax: '<?= $url_ajax ?>'
                },
                success: function(data) {
                    $("#my_keldes").html(data);
                }
            });
        }

        // DONE CHANGE TABEL
        $.ajax({
            url: "<?php echo base_url() . $url_ajax ?>/debug-tabel",
            type: "POST",
            data: {
                data1: val,
                data2: 'all_keldes',
                data3: '',
                myselect: 'kecamatan',
                url_ajax: '<?= $url_ajax ?>'
            },
            success: function(data) {
                $("#tabel_tabel").html(data);
            }
        });

        // DONE CHANGE PROGRESS
        $.ajax({
            url: "<?php echo base_url() . $url_ajax ?>/debug-progress",
            type: "POST",
            data: {
                kec: val,
                des: 'all_keldes',
                bs: '',
                myselect: 'kecamatan',
                url_ajax: '<?= $url_ajax ?>'
            },
            success: function(data) {
                $("#progress_progress").html(data);
            }
        });
    });
</script>

<?= $this->endSection(); ?>