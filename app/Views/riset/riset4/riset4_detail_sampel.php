<?php

use App\Models\Riset\Riset4\Riset4ProgressModel;

$this->extend('layout/template'); ?>
<?= $this->section('content'); ?>
<div class="card">
    <div class="card-body">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-start mb-0">Detail Sampel</h2>
                <div class="breadcrumb-wrapper">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">Riset 4
                        </li>
                        <li class="breadcrumb-item"><a href="/riset-4/sampel" style="color: #6e6b7b">Sampel</a>
                        </li>
                        <li class="breadcrumb-item active">Detail Sampel
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>

<section>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <?php
                    $detailTitle = '';
                    if ($detail_length == 3) {
                        $detailTitle = 'Kelurahan/Desa ' . $detail;
                    } else if ($detail_length == 7) {
                        $detailTitle = 'Kecamatan ' . $detail;
                    } else {
                        $detailTitle = 'Blok Sensus ' . $detail;
                    }
                    ?>
                    <h1 class="mb-2">Riset 4 - Detail Sampel <?= $detailTitle; ?></h1>
                    <?php if ($detail_length > 7) : ?>
                        <span>Informasi lebih lanjut mengenai <?= $detailTitle; ?> dapat dilihat
                            <a href="#" class="badge badge-light-primary fw-bolder" data-bs-toggle="modal" data-bs-target="#modalDetail">
                                di sini
                            </a>
                        </span>
                    <?php endif; ?>
                    <!-- MODAL DETAIL UNTUK INFORMASI LEBIH LANJUT -->
                    <div class="modal fade" id="modalDetail" tabindex="-1" aria-labelledby="modalDetailTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h3 class="modal-title fw-bolder" id="modalDetailTitle">
                                        Detail <?= $detailTitle; ?>
                                    </h3>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <div class="text-center">
                                        <h4 class="fw-bold">Rincian</h4>
                                        <ul class="list-group list-group-flush text-start">
                                            <?php if ($detail_length > 7) : ?>
                                                <li class="list-group-item">Kode Blok Sensus :
                                                    <b><?= $detail_sampel[0]['kodeBs']; ?></b>
                                                </li>
                                                <li class="list-group-item">Nama Blok Sensus :
                                                    <b><?= $detail_sampel[0]['nama']; ?></b>
                                                </li>
                                                <li class="list-group-item">Kabupaten/Kota :
                                                    <b><?= $detail_sampel[0]['nama_kabupaten']; ?></b>
                                                </li>
                                                <li class="list-group-item">Kecamatan :
                                                    <b><?= $detail_sampel[0]['nama_kecamatan']; ?></b>
                                                </li>
                                                <li class="list-group-item">Kelurahan/Desa :
                                                    <b><?= $detail_sampel[0]['nama_desa']; ?></b>
                                                </li>
                                                <li class="list-group-item">Satuan Lingkungan Setempat :
                                                    <b><?= $detail_sampel[0]['sls']; ?></b>
                                                </li>
                                                <li class="list-group-item">Dicacah Oleh :
                                                    <b><?= $detail_sampel[0]['nama_pcl']; ?>
                                                        (<?= $detail_sampel[0]['nim_pcl']; ?>)</b>
                                                </li>
                                            <?php endif; ?>
                                        </ul>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">
                                            Ok
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h2 class="text-center my-2">Daftar Sampel Usaha Pariwisata</h2>
                    <div id="table-w1">
                        <div class="table-responsive">
                            <table class="table my-2" id="table-1">
                                <thead>
                                    <tr class="text-center">
                                        <th>Kode Usaha Pariwisata</th>
                                        <th>Kode Blok Sensus</th>
                                        <th>Pencacah</th>
                                        <th>Nomor Segmen/bf/bs/Urutan Ruta</th>
                                        <th>Nama Kepala Rumah Tangga</th>
                                        <th>Jumlah Usaha Pariwisata</th>
                                        <th>Waktu</th>
                                        <th>Terisi</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($detail_sampel as $dl) : ?>

                                        <?php
                                        $model = new Riset4ProgressModel;


                                        $d1 = $dl['kodeBs'];
                                        $d2 = $dl['noUrutUUP'];
                                        $d3 = $dl['namaPemilikUUP'];

                                        $sudah = false;
                                        if ($model->terisi($d1, $d2, $d3)) {
                                            $sudah = true;
                                        }
                                        ?>
                                        <tr class="text-center">
                                            <td><?= $dl['kodeUUP']; ?></td>
                                            <td><?= $dl['kodeBs']; ?></td>
                                            <td><?= $dl['nama_pcl']; ?> (<?= $dl['nim_pcl']; ?>)</td>
                                            <td><?= $dl['noSegmen']; ?>/<?= $dl['bf']; ?>/<?= $dl['bs']; ?>/<?= $dl['noUrutRuta']; ?>
                                            </td>
                                            <td><?= $dl['namaKRT']; ?></td>
                                            <td><?= $dl['jumlahisUUP']; ?></td>
                                            <td><?= $dl['time']; ?></td>

                                            <td>
                                                <?php if ($sudah) : ?>
                                                    <img src="/assets/images/fotoprofil/accept.png" alt="ok" height="30">
                                                    <div class="hidden"> SUDAH TERISI </div>
                                                <?php endif ?>

                                                <?php if (!$sudah) : ?>
                                                    <div class="hidden"> BELUM TERISI </div>
                                                <?php endif ?>
                                            </td>

                                            <td>
                                                <!-- MODAL DETAIL -->
                                                <div class="vertical-modal-ex">
                                                    <button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#modalListingDetail<?= $dl['kodeUUP']; ?>">
                                                        Detail
                                                    </button>

                                            </td>
                                        </tr>

                                        <!-- Modal -->
                                        <div class="modal fade" id="modalListingDetail<?= $dl['kodeUUP']; ?>" tabindex="-1" aria-labelledby="modalListingDetailTitle<?= $dl['kodeUUP']; ?>" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h3 class="modal-title fw-bolder" id="modalListingDetailTitle<?= $dl['kodeUUP']; ?>">
                                                            Detail Hasil Listing
                                                        </h3>
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="text-center">
                                                            <h4 class="fw-bold">Rincian</h4>
                                                            <span class="mb-1 fw-bold">Dicacah Oleh
                                                                <?= $dl['nama_pcl']; ?>
                                                                (<?= $dl['nim_pcl']; ?>)
                                                            </span>
                                                        </div>
                                                        <ul class="list-group list-group-flush text-start">
                                                            <li class="list-group-item">Kode Usaha Pariwisata:
                                                                <b><?= $dl['kodeUUP']; ?></b>
                                                            </li>
                                                            <li class="list-group-item">Kode Blok Sensus:
                                                                <b><?= $dl['kodeBs']; ?></b>
                                                            </li>
                                                            <li class="list-group-item">Jumlah Usaha Pariwisata:
                                                                <b><?= $dl['jumlahisUUP']; ?></b>
                                                            </li>
                                                            <li class="list-group-item">Nomor
                                                                Segmen/bf/bs/noUrutRuta:
                                                                <b><?= $dl['noSegmen'];
                                                                    ?>/<?= $dl['bf']; ?>/<?= $dl['bs']; ?>/<?= $dl['noUrutRuta']; ?>
                                                                </b>
                                                            </li>
                                                            <li class="list-group-item">Nama Kepala Rumah
                                                                Tangga:
                                                                <b><?= $dl['namaKRT']; ?></b>
                                                            </li>
                                                            <li class="list-group-item">Nomor Urut Usaha
                                                                Pariwisata:
                                                                <b><?= ($dl['noUrutUUP']) ? $dl['noUrutUUP'] : '-' ?></b>
                                                            </li>
                                                            <li class="list-group-item">Nomor Urut Pemilik Usaha
                                                                Pariwisata:
                                                                <b><?= ($dl['noUrutPemilikUUP']) ? $dl['noUrutPemilikUUP'] : '' ?></b>
                                                            </li>
                                                            <li class="list-group-item">Nama Pemilik Usaha
                                                                Pariwisata:
                                                                <b><?= ($dl['namaPemilikUUP']) ? $dl['namaPemilikUUP'] : '' ?></b>
                                                            </li>
                                                            <li class="list-group-item">Kedudukan Usaha
                                                                Pariwisata:
                                                                <b>
                                                                    <?php
                                                                    switch ($dl['kedudukanUP']) {
                                                                        case 1:
                                                                            echo ("Pemilik");
                                                                            break;
                                                                        case 2:
                                                                            echo ("Pengelola");
                                                                            break;
                                                                        default:
                                                                            echo "-";
                                                                    }
                                                                    ?>

                                                                </b>
                                                            </li>
                                                            <li class="list-group-item">Status Kelola:
                                                                <b>
                                                                    <?php
                                                                    switch ($dl['statusKelola']) {
                                                                        case 1:
                                                                            echo ("Dikelola orang lain");
                                                                            break;
                                                                        case 2:
                                                                            echo ("Dikelola sendiri");
                                                                            break;
                                                                        default:
                                                                            echo "-";
                                                                    }
                                                                    ?>
                                                                </b>

                                                            </li>
                                                            <li class="list-group-item">Tanggung Jawab
                                                                Pengelola:
                                                                <b>
                                                                    <?php
                                                                    switch ($dl['tanggungJawab']) {
                                                                        case 1:
                                                                            echo ("Tidak bertanggung jawab atas risiko unit usaha");
                                                                            break;
                                                                        case 2:
                                                                            echo ("Ya, menanggung risiko");
                                                                            break;
                                                                        default:
                                                                            echo "-";
                                                                    }
                                                                    ?>
                                                                </b>

                                                            </li>
                                                            <li class="list-group-item">Lokasi Usaha Pariwisata:
                                                                <b>
                                                                    <?php
                                                                    switch ($dl['lokasiUP']) {
                                                                        case 0:
                                                                            echo ("Luar Kota Baru");
                                                                            break;
                                                                        case 1:
                                                                            echo ("Dalam Kota Batu");
                                                                            break;
                                                                        default:
                                                                            echo "-";
                                                                    }
                                                                    ?>
                                                                </b>

                                                            </li>
                                                            <li class="list-group-item">Skala Usaha:
                                                                <b>
                                                                    <?php
                                                                    switch ($dl['skalaUsaha']) {
                                                                        case 0:
                                                                            echo ("Lebih dari 19 orang");
                                                                            break;
                                                                        case 1:
                                                                            echo ("Mikro (1-4 orang)");
                                                                            break;
                                                                        case 2:
                                                                            echo ("Kecil (5-19 orang)");
                                                                            break;
                                                                        default:
                                                                            echo "-";
                                                                    }
                                                                    ?>
                                                                </b>

                                                            </li>
                                                            <li class="list-group-item">Jenis Usaha Pariwisata:
                                                                <b>
                                                                    <?php
                                                                    switch ($dl['jenisUUP']) {
                                                                        case 1:
                                                                            echo ("Jasa transportasi wisata");
                                                                            break;
                                                                        case 2:
                                                                            echo ("Jasa penyediaan makanan dan minuman");
                                                                            break;
                                                                        case 3:
                                                                            echo ("Penyediaan akomodasi");
                                                                            break;
                                                                        default:
                                                                            echo "-";
                                                                    }
                                                                    ?>
                                                                </b>

                                                            </li>
                                                            <li class="list-group-item">Waktu Pencacahan:
                                                                <b><?= $dl['time']; ?></b>
                                                            </li>
                                                            <li class="list-group-item">Alamat:
                                                                <b><?= $dl['alamat']; ?></b>
                                                            </li>
                                                            <li class="list-group-item">Catatan:
                                                                <b><?= ($dl['catatan']) ? $dl['catatan'] : '-' ?></b>
                                                            </li>
                                                            <li class="list-group-item">Status:
                                                                <b><?= ($dl['status']) ? $dl['status'] : '-' ?></b>
                                                            </li>
                                                            <li class="list-group-item">Lokasi : <b> Akurasi
                                                                    <?= $dl['akurasi']; ?> Meter</b>
                                                                <br>
                                                                <iframe class="mt-1" width="100%" height="300" src="https://www.google.com/maps?q=<?= $dl['latitude']; ?>,<?= $dl['longitude']; ?>&hl=es;z=14&output=embed" frameborder="0">
                                                                </iframe>
                                                            </li>
                                                        </ul>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Ok</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="mt-2">
                        <a href="/riset-4/sampel" class="btn btn-primary">Kembali</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- SCRIPT -->
<?php
$data = [
    'rep' => 1,
    'judul' => "Detail Progress Sampel Riset 4 di " . $detailTitle
];
echo view('layout/datatable', $data); ?>

<?= $this->endSection(); ?>