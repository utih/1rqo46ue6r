<?php $this->extend('layout/template'); ?>
<?= $this->section('content'); ?>

<div class="card">
    <div class="card-body">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-start mb-0">Sampel</h2>
                <div class="breadcrumb-wrapper">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">Riset 4</li>
                        <li class="breadcrumb-item active">Sampel</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>

<div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <!-- //TODO 1 : UBAH JUDULNYA SESUAI JUDUL LISTING/SAMPEL NYA -->
                    <h1 class="text-center">
                        Progress Sampel Survei Riset 4 (<b>Pemanfaatan TIK</b>)
                    </h1>
                    <h2 class="mt-1" id="title-progress-listing">Progress Sampel Berdasarkan Blok Sensus di Riset 4
                        (Kota Batu)
                    </h2>

                    <!-- //TODO 2 : (OPSIONAL) PAKAI INI JIKA TABLE YANG INGIN DITAMPILKAN > 1. PERHATIKAN PENAMAANNYA -->
                    <!-- PENAMAANNYA DISINI PAKE table-w1 , table-w2 untuk option value -->
                    <!-- SELECT BERDASARKAN KATEGORI -->
                    <div class="col-lg-2 col-12">
                        <select class="form-select" id="select-progress-w">
                            <option value="table-w1">Blok Sensus</option>
                            <option value="table-w2">Kecamatan</option>
                            <option value="table-w3">Desa / Kelurahan</option>
                            <option value="table-w4">Keseluruhan</option>
                        </select>
                    </div>

                    <!-- //TODO 3 : BIKIN TABLE NYA DISINI, DIBUNGKUS DENGAN NAMA NILAI OPTION VALUE TADI-->
                    <!-- TABLE-1 : JUMLAH BLOK SENSUS -->
                    <div id="table-w1">
                        <div class="table-responsive">
                            <table class="table my-2" id="table-1">
                                <thead>
                                    <tr class="text-center">
                                        <th>Kode BS</th>
                                        <th>Pencacah</th>
                                        <th>Jumlah Sampel</th>
                                        <th>Jumlah Terlisting</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($sampel_by_bs as $s) : ?>
                                    <tr class="text-center">
                                        <td><?= $s['kode_bs']; ?></td>
                                        <td><?= $s['nama_pcl']; ?> (<?= $s['nim']; ?>)</td>
                                        <td><?= $s['jumlah_sampel']; ?></td>
                                        <td><?= $s['jumlah_listing']; ?></td>
                                        <td>
                                            <a href="/riset-4/sampel/detail/<?= $s['kode_bs']; ?>"
                                                class="btn btn-success">Detail
                                            </a>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <!-- TABLE-2 : JUMLAH KECAMATAN -->
                    <div id="table-w2" style="display: none;">
                        <div class="table-responsive">
                            <table class="table my-2" id="table-2">
                                <thead>
                                    <tr class="text-center">
                                        <th>Nama Kecamatan</th>
                                        <th>Jumlah Sampel</th>
                                        <th>Jumlah Terlisting</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($sampel_by_kecamatan as $s) : ?>
                                    <tr class="text-center">
                                        <td><?= $s['nama_kecamatan']; ?></td>
                                        <td><?= $s['jumlah_sampel']; ?></td>
                                        <td><?= $s['jumlah_listing']; ?></td>
                                        <td>
                                            <a href="/riset-4/sampel/detail/<?= $s['kode_kecamatan']; ?>"
                                                class="btn btn-success">Detail
                                            </a>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <!-- TABLE-3 : JUMLAH DESA KELURAHAN -->
                    <div id="table-w3" style="display: none;">
                        <div class="table-responsive">
                            <table class="table my-2" id="table-3">
                                <thead>
                                    <tr class="text-center">
                                        <th>Nama Desa / Kelurahan</th>
                                        <th>Jumlah Sampel</th>
                                        <th>Jumlah Terlisting</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($sampel_by_kelurahandesa as $s) : ?>
                                    <tr class="text-center">
                                        <td><?= $s['nama_desa']; ?></td>
                                        <td><?= $s['jumlah_sampel']; ?></td>
                                        <td><?= $s['jumlah_listing']; ?></td>
                                        <td>
                                            <a href="/riset-4/sampel/detail/<?= $s['kode_kecamatan']; ?>/<?= $s['kode_desa']; ?>"
                                                class="btn btn-success">Detail
                                            </a>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <!-- TABLE-4 : JUMLAH LISTING KESELURUHAN -->
                    <div id="table-w4" style="display: none;">
                        <div class="table-responsive">
                            <table class="table my-2" id="table-4">
                                <thead>
                                    <tr class="text-center">
                                        <th>Kode UUP</th>
                                        <th>NIM PPL</th>
                                        <th>Nama PPL</th>
                                        <th>Kode BS</th>
                                        <th>Bf</th>
                                        <th>Bs</th>
                                        <th>No Segmen</th>
                                        <th>No Urut Ruta</th>
                                        <th>Kode Kota</th>
                                        <th>Nama Kota</th>
                                        <th>Kode Kecamatan</th>
                                        <th>Nama Kecamatan</th>
                                        <th>Kode Desa</th>
                                        <th>Nama Desa</th>
                                        <th>Nama KRT</th>
                                        <th>Alamat</th>
                                        <th>Jumlah is UUP</th>
                                        <th>No Urut Pemilik UUP</th>
                                        <th>Nama Pemilik UUP</th>
                                        <th>Kedudukan UP</th>
                                        <th>Status Kelola</th>
                                        <th>Lokasi UP</th>
                                        <th>Jenis UP</th>
                                        <th>NO Urut UUP</th>
                                        <th>Latitude</th>
                                        <th>Longitude</th>
                                        <th>Akurasi</th>
                                        <th>Status</th>
                                        <th>Time</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($sampel_all as $s) : ?>
                                    <tr class="text-center">
                                        <td><?= $s['kodeUUP']; ?></td>
                                        <td><?= $s['nim_pcl']; ?></td>
                                        <td><?= $s['nama_pcl']; ?></td>
                                        <td><?= $s['kodeBs']; ?></td>
                                        <td><?= $s['bf']; ?></td>
                                        <td><?= $s['bs']; ?></td>
                                        <td><?= $s['noSegmen']; ?></td>
                                        <td><?= $s['noUrutRuta']; ?></td>
                                        <td>3579</td>
                                        <td>Batu</td>
                                        <td><?= $s['kode_kecamatan']; ?></td>
                                        <td><?= $s['nama_kecamatan']; ?></td>
                                        <td><?= $s['kode_desa']; ?></td>
                                        <td><?= $s['nama_desa']; ?></td>
                                        <td><?= $s['namaKRT']; ?></td>
                                        <td><?= $s['alamat']; ?></td>
                                        <td><?= $s['jumlahisUUP']; ?></td>
                                        <td><?= $s['noUrutPemilikUUP']; ?></td>
                                        <td><?= $s['namaPemilikUUP']; ?></td>
                                        <td><?= $s['kedudukanUP']; ?></td>
                                        <td><?= $s['statusKelola']; ?></td>
                                        <td><?= $s['lokasiUP']; ?></td>
                                        <td><?= $s['jenisUUP']; ?></td>
                                        <td><?= $s['noUrutUUP']; ?></td>
                                        <td><?= $s['latitude']; ?></td>
                                        <td><?= $s['longitude']; ?></td>
                                        <td><?= $s['akurasi']; ?></td>
                                        <td><?= $s['status']; ?></td>
                                        <td><?= $s['time']; ?></td>

                                    </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<!-- //TODO 4 : TAMBAHIN JQUERY BIAR DATATABLE NYA BISA JALAN -->
<!-- INI DITAMBAHIN SESUAI DENGAN JUMLAH TABLE -->
<?php
$data = [
    'rep' => 1,
    'judul' => 'Progress Sampel Berdasarkan Blok Sensus Survei Pemanfaatan TIK Riset 4 (Kota Batu)'
];
echo view('layout/datatable', $data); ?>

<?php
$data = [
    'rep' => 2,
    'judul' => 'Progress Sampel Berdasarkan Kecamatan Survei Pemanfaatan TIK Riset 4 (Kota Batu)'
];
echo view('layout/datatable', $data); ?>

<?php
$data = [
    'rep' => 3,
    'judul' => 'Progress Sampel Berdasarkan Kelurahan/Desa Survei Pemanfaatan TIK Riset 4 (Kota Batu)'
];
echo view('layout/datatable', $data); ?>

<?php
$data = [
    'rep' => 4,
    'judul' => 'Progress Sampel Keseluruhan Survei Pemanfaatan TIK Riset 4 (Kota Batu)'
];
echo view('layout/datatable', $data); ?>

<script>
$('#select-progress-w').change(function() {
    var val = $('#select-progress-w').val();
    if (val == 'table-w1') {
        document.getElementById('title-progress-listing').innerHTML =
            'Progress Sampel Berdasarkan Blok Sensus di Riset 4 (Kota Batu)';
        document.getElementById('table-w1').style.display = '';
        document.getElementById('table-w2').style.display = 'none';
        document.getElementById('table-w3').style.display = 'none';
        document.getElementById('table-w4').style.display = 'none';

    } else if (val == 'table-w2') {
        document.getElementById('title-progress-listing').innerHTML =
            'Progress Sampel Berdasarkan Kecamatan di Riset 4 (Kota Batu)';
        document.getElementById('table-w1').style.display = 'none';
        document.getElementById('table-w2').style.display = '';
        document.getElementById('table-w3').style.display = 'none';
        document.getElementById('table-w4').style.display = 'none';
    } else if (val == 'table-w3') {
        document.getElementById('title-progress-listing').innerHTML =
            'Progress Sampel Berdasarkan Kelurahan/Desa di Riset 4 (Kota Batu)';
        document.getElementById('table-w1').style.display = 'none';
        document.getElementById('table-w2').style.display = 'none';
        document.getElementById('table-w3').style.display = '';
        document.getElementById('table-w4').style.display = 'none';
    } else if (val == 'table-w4') {
        document.getElementById('title-progress-listing').innerHTML =
            'Progress Sampel Keseluruhan di Riset 4 (Kota Batu)';
        document.getElementById('table-w1').style.display = 'none';
        document.getElementById('table-w2').style.display = 'none';
        document.getElementById('table-w3').style.display = 'none';
        document.getElementById('table-w4').style.display = '';
    }
});
</script>

<?= $this->endSection(); ?>