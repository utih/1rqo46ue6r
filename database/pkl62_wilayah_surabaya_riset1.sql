-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 15, 2023 at 04:07 AM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 8.1.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pkl62_wilayah_surabaya_riset1`
--
CREATE DATABASE IF NOT EXISTS `pkl62_wilayah_surabaya_riset1` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
USE `pkl62_wilayah_surabaya_riset1`;

-- --------------------------------------------------------

--
-- Table structure for table `bloksensus`
--

CREATE TABLE `bloksensus` (
  `id` varchar(14) NOT NULL,
  `kabupaten` char(4) NOT NULL,
  `kecamatan` char(7) NOT NULL,
  `kelurahandesa` varchar(5) NOT NULL,
  `nama` varchar(200) NOT NULL,
  `stratifikasi` enum('1','2') NOT NULL,
  `jumlah_rt` varchar(4) NOT NULL,
  `jumlah_rt_update` varchar(4) NOT NULL,
  `sls` varchar(256) NOT NULL,
  `nim` varchar(9) NOT NULL,
  `beban_cacah` int(11) NOT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'listing',
  `jumlah_rt_internet` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bloksensus`
--

INSERT INTO `bloksensus` (`id`, `kabupaten`, `kecamatan`, `kelurahandesa`, `nama`, `stratifikasi`, `jumlah_rt`, `jumlah_rt_update`, `sls`, `nim`, `beban_cacah`, `status`, `jumlah_rt_internet`) VALUES
('3578030003001B', '3578', '3578030', '003', '001B', '1', '0', '0', 'Jl. Ahmad Yani No.152E, Gayungan, Kec. Gayungan, Kota Surabaya, Jawa Timur', '222011282', 2, 'uploaded', 0),
('3578030003002B', '3578', '3578030', '003', '002B', '1', '0', '0', 'Jl. Ahmad Yani No.152E, Gayungan, Kec. Gayungan, Kota Surabaya, Jawa Timur', '212011263', 2, 'uploaded', 0),
('3578030003003B', '3578', '3578030', '003', '003B', '1', '0', '0', 'Jl. Ahmad Yani No.152E, Gayungan, Kec. Gayungan, Kota Surabaya, Jawa Timur', '212011661', 2, 'uploaded', 0),
('3578030003004B', '3578', '3578030', '003', '004B', '1', '0', '0', 'Jl. Ahmad Yani No.152E, Gayungan, Kec. Gayungan, Kota Surabaya, Jawa Timur', '222011365', 2, 'uploaded', 0),
('3578030003005B', '3578', '3578030', '003', '005B', '1', '0', '0', 'Jl. Ahmad Yani No.152E, Gayungan, Kec. Gayungan, Kota Surabaya, Jawa Timur', '212011506', 2, 'uploaded', 0),
('3578030003006B', '3578', '3578030', '003', '006B', '1', '0', '0', 'Jl. Ahmad Yani No.152E, Gayungan, Kec. Gayungan, Kota Surabaya, Jawa Timur', '222011636', 2, 'uploaded', 0),
('3578030003007B', '3578', '3578030', '003', '007B', '1', '0', '0', 'Jl. Ahmad Yani No.152E, Gayungan, Kec. Gayungan, Kota Surabaya, Jawa Timur', '222011592', 2, 'uploaded', 0),
('3578030003008B', '3578', '3578030', '003', '008B', '1', '0', '0', 'Jl. Ahmad Yani No.152E, Gayungan, Kec. Gayungan, Kota Surabaya, Jawa Timur', '212011474', 2, 'uploaded', 0),
('3578030003009B', '3578', '3578030', '003', '009B', '1', '0', '0', 'Jl. Ahmad Yani No.152E, Gayungan, Kec. Gayungan, Kota Surabaya, Jawa Timur', '222011317', 2, 'uploaded', 0),
('3578030003010B', '3578', '3578030', '003', '010B', '1', '0', '0', 'Jl. Ahmad Yani No.152E, Gayungan, Kec. Gayungan, Kota Surabaya, Jawa Timur', '222011453', 2, 'uploaded', 0),
('3578030003013B', '3578', '3578030', '003', '013B', '1', '0', '0', 'Jl. Ahmad Yani No.152E, Gayungan, Kec. Gayungan, Kota Surabaya, Jawa Timur', '222011538', 2, 'uploaded', 0),
('3578030003551B', '3578', '3578030', '003', '551B', '1', '0', '0', 'Jl. Ahmad Yani No.152E, Gayungan, Kec. Gayungan, Kota Surabaya, Jawa Timur', '555555551', 2, 'uploaded', 0),
('3578030003555B', '3578', '3578030', '003', '555B', '1', '0', '0', 'Jl. Ahmad Yani No.152E, Gayungan, Kec. Gayungan, Kota Surabaya, Jawa Timur', '555555555', 2, 'uploaded', 0),
('3578050002001A', '3578', '3578050', '002', '001A', '1', '0', '0', 'Jl. Raya Kendangsari Industri No. 43-44, Kendangsari, Kec. Tenggilis mejoyo, Kota Surabaya, Jawa Timur', '222011282', 8, 'uploaded', 0),
('3578050002002A', '3578', '3578050', '002', '002A', '1', '0', '0', 'Jl. Raya Kendangsari Industri No. 43-44, Kendangsari, Kec. Tenggilis mejoyo, Kota Surabaya, Jawa Timur', '212011263', 8, 'uploaded', 0),
('3578050002003A', '3578', '3578050', '002', '003A', '1', '0', '0', 'Jl. Raya Kendangsari Industri No. 43-44, Kendangsari, Kec. Tenggilis mejoyo, Kota Surabaya, Jawa Timur', '212011661', 8, 'uploaded', 0),
('3578050002004A', '3578', '3578050', '002', '004A', '1', '0', '0', 'Jl. Raya Kendangsari Industri No. 43-44, Kendangsari, Kec. Tenggilis mejoyo, Kota Surabaya, Jawa Timur', '222011365', 8, 'uploaded', 0),
('3578050002005A', '3578', '3578050', '002', '005A', '1', '0', '0', 'Jl. Raya Kendangsari Industri No. 43-44, Kendangsari, Kec. Tenggilis mejoyo, Kota Surabaya, Jawa Timur', '212011506', 8, 'uploaded', 0),
('3578050002006A', '3578', '3578050', '002', '006A', '1', '0', '0', 'Jl. Raya Kendangsari Industri No. 43-44, Kendangsari, Kec. Tenggilis mejoyo, Kota Surabaya, Jawa Timur', '222011636', 8, 'uploaded', 0),
('3578050002007A', '3578', '3578050', '002', '007A', '1', '0', '0', 'Jl. Raya Kendangsari Industri No. 43-44, Kendangsari, Kec. Tenggilis mejoyo, Kota Surabaya, Jawa Timur', '222011592', 8, 'uploaded', 0),
('3578050002008A', '3578', '3578050', '002', '008A', '1', '0', '0', 'Jl. Raya Kendangsari Industri No. 43-44, Kendangsari, Kec. Tenggilis mejoyo, Kota Surabaya, Jawa Timur', '212011474', 8, 'uploaded', 0),
('3578050002009A', '3578', '3578050', '002', '009A', '1', '0', '0', 'Jl. Raya Kendangsari Industri No. 43-44, Kendangsari, Kec. Tenggilis mejoyo, Kota Surabaya, Jawa Timur', '222011317', 8, 'uploaded', 0),
('3578050002010A', '3578', '3578050', '002', '010A', '1', '0', '0', 'Jl. Raya Kendangsari Industri No. 43-44, Kendangsari, Kec. Tenggilis mejoyo, Kota Surabaya, Jawa Timur', '222011453', 8, 'uploaded', 0),
('3578050002011A', '3578', '3578050', '002', '011A', '1', '0', '0', 'Jl. Raya Kendangsari Industri No. 43-44, Kendangsari, Kec. Tenggilis mejoyo, Kota Surabaya, Jawa Timur', '222011330', 8, 'uploaded', 0),
('3578050002012A', '3578', '3578050', '002', '012A', '1', '0', '0', 'Jl. Raya Kendangsari Industri No. 43-44, Kendangsari, Kec. Tenggilis mejoyo, Kota Surabaya, Jawa Timur', '222011345', 8, 'uploaded', 0),
('3578050002013A', '3578', '3578050', '002', '013A', '1', '0', '0', 'Jl. Raya Kendangsari Industri No. 43-44, Kendangsari, Kec. Tenggilis mejoyo, Kota Surabaya, Jawa Timur', '222011538', 8, 'uploaded', 0),
('3578050002022A', '3578', '3578050', '002', '012A', '1', '0', '0', 'Jl. Raya Kendangsari Industri No. 43-44, Kendangsari, Kec. Tenggilis mejoyo, Kota Surabaya, Jawa Timur', '222011815', 8, 'uploaded', 0);

-- --------------------------------------------------------

--
-- Table structure for table `datast`
--

CREATE TABLE `datast` (
  `kodeBs` varchar(14) NOT NULL,
  `kodeRuta` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `datast`
--

INSERT INTO `datast` (`kodeBs`, `kodeRuta`) VALUES
('3578050002008A', '021a0104-443f-1925-b'),
('3578030003009B', '032a0481-727f-3677-b'),
('3578050002006A', '034a0830-120f-5403-b'),
('3578050002002A', '045a0252-345f-4448-b'),
('3578030003005B', '050a6977-320f-0291-b'),
('3578030003009B', '057a9630-193f-9799-b'),
('3578050002001A', '0ed3c972-50cb-4d94-8'),
('3578050002002A', '103a0684-485f-3661-a'),
('3578050002003A', '116a0557-721f-9274-a'),
('3578050002009A', '124a0213-930f-5566-d'),
('3578050002001A', '12d14257-a9fd-4185-b'),
('3578050002010A', '133a0673-254f-4741-d'),
('3578050002002A', '13f98eba-f802-4e3b-b'),
('3578030003005B', '140a0720-176f-7453-b'),
('3578050002007A', '140a0914-384f-3661-c'),
('3578050002004A', '162a0179-884f-8453-d'),
('3578050002001A', '166a0196-552f-5382-c'),
('3578050002009A', '169a0832-881f-3547-c'),
('3578050002001A', '174a0580-916f-1417-d'),
('3578050002010A', '190a0587-141f-4539-d'),
('3578030003001B', '192a0595-295f-8786-b'),
('3578030003005B', '196a0736-562f-2250-b'),
('3578050002003A', '1b2551c9-a448-4f64-8'),
('3578050002003A', '1de5e73d-a6a2-4af9-b'),
('3578030003005B', '211a0501-991f-1529-b'),
('3578050002008A', '230a0455-368f-1803-d'),
('3578050002002A', '233a0731-229f-8414-d'),
('3578030003005B', '233a0896-204f-1373-b'),
('3578050002003A', '235adc07-b789-454f-b'),
('3578050002004A', '250a0391-153f-3739-b'),
('3578030003010B', '258a0193-239f-9169-d'),
('3578050002001A', '259a0922-598f-3942-b'),
('3578050002004A', '263a0446-768f-8667-c'),
('3578030003007B', '270a0767-818f-4420-a'),
('3578050002005A', '272a0299-103f-7456-a'),
('3578030003009B', '288a0611-268f-1207-c'),
('3578050002006A', '302a0945-859f-7801-b'),
('3578030003003B', '3212bc0f-6509-4b85-8'),
('3578030003009B', '337a0753-811f-1775-b'),
('3578050002009A', '352a0652-400f-5218-d'),
('3578050002008A', '378a0369-514f-6631-d'),
('3578030003003B', '386ebb56-bc95-4024-8'),
('3578050002002A', '391a0819-246f-2871-a'),
('3578030003003B', '3b78f5dc-8eb1-4b12-9'),
('3578030003003B', '3c65b671-e1bf-4fc2-9'),
('3578050002013A', '40959e02-63d5-43f6-b'),
('3578050002004A', '422a0949-973f-1953-b'),
('3578050002003A', '429a0305-480f-3550-a'),
('3578050002009A', '432a0211-117f-3497-d'),
('3578050002003A', '440a0574-421f-4229-d'),
('3578050002002A', '440a0729-933f-9522-a'),
('3578050002009A', '449a0738-939f-6621-c'),
('3578050002013A', '44ee8e24-e7c6-404d-8'),
('3578030003004B', '459a0511-552f-7720-c'),
('3578050002009A', '461a0744-303f-3907-b'),
('3578030003007B', '473a0172-841f-8088-d'),
('3578050002001A', '475a0478-257f-3027-a'),
('3578050002003A', '478a0729-554f-6079-d'),
('3578050002001A', '479a0961-492f-6875-b'),
('3578050002003A', '486a0851-781f-2764-c'),
('3578050002013A', '4a46cf65-ac94-42d6-9'),
('3578030003013B', '4c984a90-f2a4-41fa-8'),
('3578030003013B', '4da9824c-31df-4c2d-b'),
('3578050002004A', '4de93e82-3b13-48ea-9'),
('3578050002007A', '508a0847-348f-3856-b'),
('3578050002004A', '50e100df-e889-49dd-9'),
('3578050002006A', '511a0824-942f-7049-a'),
('3578050002002A', '526a0231-920f-2383-c'),
('3578030003005B', '532a0604-246f-7398-b'),
('3578050002005A', '540a0474-295f-4959-d'),
('3578050002009A', '543a0844-448f-7040-d'),
('3578050002002A', '562a0653-123f-5542-d'),
('3578050002008A', '568a0694-517f-8524-c'),
('3578030003003B', '581a0232-632f-5692-d'),
('3578050002002A', '583a0732-539f-1783-a'),
('3578050002006A', '595a0545-143f-6913-a'),
('3578050002010A', '598a0834-470f-4082-c'),
('3578050002004A', '603ab350-349d-4139-9'),
('3578050002009A', '606a0230-169f-8901-c'),
('3578050002004A', '60965d3d-0cf9-4cf8-a'),
('3578050002002A', '609a0875-787f-8363-a'),
('3578050002004A', '61575125-ab94-419e-9'),
('3578050002010A', '623a0823-298f-9286-c'),
('3578050002007A', '636ae913-a226-4533-b'),
('3578050002006A', '667a0862-528f-8408-a'),
('3578050002007A', '688a5fa5-0632-46fc-b'),
('3578050002007A', '6f62bcff-df21-49fb-b'),
('3578050002011A', '7025800b-b081-47fa-8'),
('3578050002011A', '7147251f-2305-4e33-a'),
('3578050002007A', '715a0793-672f-3321-d'),
('3578050002001A', '741a0677-889f-1373-c'),
('3578030003009B', '752a0266-566f-8727-d'),
('3578050002001A', '758a0229-241f-5406-c'),
('3578030003010B', '759a0152-790f-6374-c'),
('3578050002002A', '762a0204-546f-4201-b'),
('3578050002002A', '767a0955-621f-8432-a'),
('3578050002006A', '771a0481-421f-8584-b'),
('3578050002007A', '781a0963-775f-7947-a'),
('3578050002009A', '788a0523-927f-6372-c'),
('3578050002003A', '792a0796-625f-2028-d'),
('3578030003008B', '796a0385-919f-7831-c'),
('3578050002011A', '7e3b4c13-8665-4f31-9'),
('3578030003010B', '815a0494-918f-1506-b'),
('3578050002002A', '816a0860-823f-2118-b'),
('3578050002011A', '817414fb-a2df-4418-9'),
('3578030003009B', '827a0255-791f-3492-c'),
('3578030003008B', '834a0600-762f-4495-d'),
('3578050002008A', '849a0616-346f-8991-b'),
('3578050002011A', '84fadd82-ac0a-48f6-9'),
('3578050002008A', '850a0732-318f-4559-b'),
('3578050002002A', '853a0211-117f-1052-b'),
('3578030003008B', '873a0184-423f-6176-d'),
('3578050002011A', '87da81df-7e86-415b-b'),
('3578050002005A', '880a0663-413f-5202-a'),
('3578050002008A', '8855e8da-e3ab-404b-9'),
('3578030003005B', '907a0932-730f-2538-d'),
('3578050002004A', '914a0318-725f-6406-b'),
('3578030003002B', '917a0183-868f-9196-a'),
('3578050002008A', '920ec453-7ddb-43e7-8'),
('3578050002010A', '922a0573-798f-8793-b'),
('3578050002006A', '935a0103-661f-5149-b'),
('3578050002008A', '935bebc0-3408-4b10-a'),
('3578050002006A', '946a0989-951f-5973-a'),
('3578030003009B', '951a0899-424f-9936-a'),
('3578050002010A', '966a0614-974f-3725-d'),
('3578030003001B', '968a0837-157f-1572-c'),
('3578030003007B', '981a0917-539f-5476-a'),
('3578050002002A', '987a0566-619f-2365-c'),
('3578050002007A', '998a0913-550f-8093-a'),
('3578050002012A', '9b39cc70-1b9b-4b23-9'),
('3578050002012A', 'a1694524-0f09-4f21-8'),
('3578050002012A', 'aea44466-23ac-4793-a'),
('3578050002012A', 'aef17422-5fc7-4a9f-b'),
('3578050002012A', 'bbdb8bcf-cc43-44ec-b'),
('3578050002010A', 'beac9f2e-c9ca-4cf5-9'),
('3578050002010A', 'c41c002c-4359-4f6b-a'),
('3578050002010A', 'c5d7319a-a7ff-4752-a'),
('3578050002010A', 'c9767110-285b-400c-8'),
('3578050002010A', 'ca20863e-48c2-4ef7-b'),
('3578050002010A', 'cb922d3e-fb48-41d4-a'),
('3578050002010A', 'cbb99b5b-653c-4db9-9'),
('3578050002010A', 'df1539fb-39c9-4f62-9');

-- --------------------------------------------------------

--
-- Table structure for table `data_konven`
--

CREATE TABLE `data_konven` (
  `kodebs` varchar(20) NOT NULL,
  `kab` varchar(50) NOT NULL,
  `desa` varchar(50) NOT NULL,
  `kodes` varchar(20) NOT NULL,
  `bs` varchar(20) NOT NULL,
  `jumlah` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `desa`
--

CREATE TABLE `desa` (
  `id_desa` char(10) NOT NULL,
  `kabno` char(4) NOT NULL,
  `kecno` char(7) NOT NULL,
  `desano` varchar(10) NOT NULL,
  `nama` varchar(20) NOT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'listing'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `desa`
--

INSERT INTO `desa` (`id_desa`, `kabno`, `kecno`, `desano`, `nama`, `status`) VALUES
('3201052001', '3201', '3201052', '001', 'Cijayanti', 'listing'),
('3202020003', '3202', '3202020', '003', 'Sayang', 'listing'),
('3203200003', '3203', '3203200', '003', 'DUMMY DESA', 'listing'),
('3203200200', '3202', '3202020', '002', 'Sukamaju', 'listing'),
('3573010001', '3573', '3573010', '001', 'Arjowinangun', 'listing'),
('3573010002', '3573', '3573010', '002', 'Tlogowaru', 'listing'),
('3573010003', '3573', '3573010', '003', 'Wonokoyo', 'listing'),
('3573010004', '3573', '3573010', '004', 'Bumiayu', 'listing'),
('3573010005', '3573', '3573010', '005', 'Buring', 'listing'),
('3573010006', '3573', '3573010', '006', 'Mergosono', 'listing'),
('3573010007', '3573', '3573010', '007', 'Kotalama', 'listing'),
('3573010008', '3573', '3573010', '008', 'Kedungkandang', 'listing'),
('3573010009', '3573', '3573010', '009', 'Sawojajar', 'listing'),
('3573010010', '3573', '3573010', '010', 'Madyopuro', 'listing'),
('3573010011', '3573', '3573010', '011', 'Lesanpuro', 'listing'),
('3573010012', '3573', '3573010', '012', 'Cemorokandang', 'listing'),
('3573020001', '3573', '3573020', '001', 'Kebonsari', 'listing'),
('3573020002', '3573', '3573020', '002', 'Gadang', 'listing'),
('3573020003', '3573', '3573020', '003', 'Ciptomulyo', 'listing'),
('3573020004', '3573', '3573020', '004', 'Sukun', 'listing'),
('3573020005', '3573', '3573020', '005', 'Bandungrejosari', 'listing'),
('3573020006', '3573', '3573020', '006', 'Bakalan krajan', 'listing'),
('3573020007', '3573', '3573020', '007', 'Mulyorejo', 'listing'),
('3573020008', '3573', '3573020', '008', 'Bandulan', 'listing'),
('3573020009', '3573', '3573020', '009', 'Tanjungrejo', 'listing'),
('3573020010', '3573', '3573020', '010', 'Pisang Candi', 'listing'),
('3573020011', '3573', '3573020', '011', 'Karang Besuki', 'listing'),
('3573030001', '3573', '3573030', '001', 'Kasin', 'listing'),
('3573030002', '3573', '3573030', '002', 'Sukoharjo', 'listing'),
('3573030003', '3573', '3573030', '003', 'Kidul Dalem', 'listing'),
('3573030004', '3573', '3573030', '004', 'Kauman', 'listing'),
('3573030005', '3573', '3573030', '005', 'Bareng', 'listing'),
('3573030006', '3573', '3573030', '006', 'Gadingkasri', 'listing'),
('3573030007', '3573', '3573030', '007', 'Oro Oro Dowo', 'listing'),
('3573030008', '3573', '3573030', '008', 'Klojen', 'listing'),
('3573030009', '3573', '3573030', '009', 'Rampal Celaket', 'listing'),
('3573030010', '3573', '3573030', '010', 'Samaan', 'listing'),
('3573030011', '3573', '3573030', '011', 'Penanggungan', 'listing'),
('3573040001', '3573', '3573040', '001', 'Jodipan', 'listing'),
('3573040002', '3573', '3573040', '002', 'Polehan', 'listing'),
('3573040003', '3573', '3573040', '003', 'Kesatrian', 'listing'),
('3573040004', '3573', '3573040', '004', 'Bunulrejo', 'listing'),
('3573040005', '3573', '3573040', '005', 'Purwantoro', 'listing'),
('3573040006', '3573', '3573040', '006', 'Pandanwangi', 'listing'),
('3573040007', '3573', '3573040', '007', 'Blimbing', 'listing'),
('3573040008', '3573', '3573040', '008', 'Purwodadi', 'listing'),
('3573040009', '3573', '3573040', '009', 'Polowijen', 'listing'),
('3573040010', '3573', '3573040', '010', 'Arjosari', 'listing'),
('3573040011', '3573', '3573040', '011', 'Balearjosari', 'listing'),
('3573050001', '3573', '3573050', '001', 'Merjosari', 'listing'),
('3573050002', '3573', '3573050', '002', 'Dinoyo', 'listing'),
('3573050003', '3573', '3573050', '003', 'Sumbersari', 'listing'),
('3573050004', '3573', '3573050', '004', 'Ketawanggede', 'listing'),
('3573050005', '3573', '3573050', '005', 'Jatimulyo', 'listing'),
('3573050006', '3573', '3573050', '006', 'Lowokwaru', 'listing'),
('3573050007', '3573', '3573050', '007', 'Tulusrejo', 'listing'),
('3573050008', '3573', '3573050', '008', 'Mojolangu', 'listing'),
('3573050009', '3573', '3573050', '009', 'Tunjungsekar', 'listing'),
('3573050010', '3573', '3573050', '010', 'Tasikmadu', 'listing'),
('3573050011', '3573', '3573050', '011', 'Tunggulwulung', 'listing'),
('3573050012', '3573', '3573050', '012', 'Tlogomas', 'listing'),
('3578010001', '3578', '3578010', '001', 'Warugunung', 'listing'),
('3578010002', '3578', '3578010', '002', 'Karang Pilang', 'listing'),
('3578010003', '3578', '3578010', '003', 'Kebraon', 'listing'),
('3578010004', '3578', '3578010', '004', 'Kedurus', 'listing'),
('3578020001', '3578', '3578020', '001', 'Pagesangan', 'listing'),
('3578020002', '3578', '3578020', '002', 'Kebonsari', 'listing'),
('3578020003', '3578', '3578020', '003', 'Jambangan', 'listing'),
('3578020004', '3578', '3578020', '004', 'Karah', 'listing'),
('3578030001', '3578', '3578030', '001', 'Dukuh Menanggal', 'listing'),
('3578030002', '3578', '3578030', '002', 'Menanggal', 'listing'),
('3578030003', '3578', '3578030', '003', 'Gayungan', 'listing'),
('3578030004', '3578', '3578030', '004', 'Ketintang', 'listing'),
('3578040001', '3578', '3578040', '001', 'Siwalankerto', 'listing'),
('3578040002', '3578', '3578040', '002', 'Jemur Wonosari', 'listing'),
('3578040003', '3578', '3578040', '003', 'Margorejo', 'listing'),
('3578040004', '3578', '3578040', '004', 'Bendul Merisi', 'listing'),
('3578040005', '3578', '3578040', '005', 'Sidosermo', 'listing'),
('3578050001', '3578', '3578050', '001', 'Kutisari', 'listing'),
('3578050002', '3578', '3578050', '002', 'Kendangsari', 'listing'),
('3578050003', '3578', '3578050', '003', 'Tenggilis Mejoyo', 'listing'),
('3578050005', '3578', '3578050', '005', 'Panjang Jiwo', 'listing'),
('3578060001', '3578', '3578060', '001', 'Rungkut Menanggal', 'listing'),
('3578060002', '3578', '3578060', '002', 'Rungkut Tengah', 'listing'),
('3578060003', '3578', '3578060', '003', 'Gunung Anyar', 'listing'),
('3578060004', '3578', '3578060', '004', 'Gunung Anyar Tambak', 'listing'),
('3578070001', '3578', '3578070', '001', 'Rungkut Kidul', 'listing'),
('3578070002', '3578', '3578070', '002', 'Medokan Ayu', 'listing'),
('3578070003', '3578', '3578070', '003', 'Wonorejo', 'listing'),
('3578070004', '3578', '3578070', '004', 'Penjaringan Sari', 'listing'),
('3578070005', '3578', '3578070', '005', 'Kedung Baruk', 'listing'),
('3578070006', '3578', '3578070', '006', 'Kali Rungkut', 'listing'),
('3578080001', '3578', '3578080', '001', 'Ngiden Jangkungan', 'listing'),
('3578080002', '3578', '3578080', '002', 'Semolowaru', 'listing'),
('3578080003', '3578', '3578080', '003', 'Medokan Semampir', 'listing'),
('3578080004', '3578', '3578080', '004', 'Keputih', 'listing'),
('3578080005', '3578', '3578080', '005', 'Gebang Putih', 'listing'),
('3578080006', '3578', '3578080', '006', 'Klampis Ngasem', 'listing'),
('3578080007', '3578', '3578080', '007', 'Menur Pumpungan', 'listing'),
('3578090001', '3578', '3578090', '001', 'Manyar sabrangan', 'listing'),
('3578090002', '3578', '3578090', '002', 'Mulyorejo', 'listing'),
('3578090003', '3578', '3578090', '003', 'Kejawen Putih Tambak', 'listing'),
('3578090004', '3578', '3578090', '004', 'Kalisari', 'listing'),
('3578090005', '3578', '3578090', '005', 'Dukuh Sutorejo', 'listing'),
('3578090006', '3578', '3578090', '006', 'Kalijudan', 'listing'),
('3578100001', '3578', '3578100', '001', 'Baratajaya', 'listing'),
('3578100002', '3578', '3578100', '002', 'Pucang Sewu', 'listing'),
('3578100003', '3578', '3578100', '003', 'Kertajaya', 'listing'),
('3578100004', '3578', '3578100', '004', 'Gubeng', 'listing'),
('3578100005', '3578', '3578100', '005', 'Airlangga', 'listing'),
('3578100006', '3578', '3578100', '006', 'Mojo', 'listing'),
('3578110001', '3578', '3578110', '001', 'Sawunggaling', 'listing'),
('3578110002', '3578', '3578110', '002', 'Wonokromo', 'listing'),
('3578110003', '3578', '3578110', '003', 'Jagir', 'listing'),
('3578110004', '3578', '3578110', '004', 'Ngagelrejo', 'listing'),
('3578110005', '3578', '3578110', '005', 'Ngagel', 'listing'),
('3578110006', '3578', '3578110', '006', 'Darmo', 'listing'),
('3578120001', '3578', '3578120', '001', 'Gunungsari', 'listing'),
('3578120002', '3578', '3578120', '002', 'Dukuh Pakis', 'listing'),
('3578120003', '3578', '3578120', '003', 'Pradahkali Kendal', 'listing'),
('3578120004', '3578', '3578120', '004', 'Dukuh kupang', 'listing'),
('3578130001', '3578', '3578130', '001', 'Balas Klumprik', 'listing'),
('3578130002', '3578', '3578130', '002', 'Babatan', 'listing'),
('3578130003', '3578', '3578130', '003', 'Wiyung', 'listing'),
('3578130004', '3578', '3578130', '004', 'Jajar Tunggal', 'listing'),
('3578140001', '3578', '3578140', '001', 'Bangkingan', 'listing'),
('3578140002', '3578', '3578140', '002', 'Sumur Welut', 'listing'),
('3578140003', '3578', '3578140', '003', 'Lidah Wetan', 'listing'),
('3578140004', '3578', '3578140', '004', 'Lidah Kulon', 'listing'),
('3578140005', '3578', '3578140', '005', 'Jeruk', 'listing'),
('3578140006', '3578', '3578140', '006', 'Lakarsantri', 'listing'),
('3578141001', '3578', '3578141', '001', 'Made', 'listing'),
('3578141002', '3578', '3578141', '002', 'Bringin', 'listing'),
('3578141003', '3578', '3578141', '003', 'Sambikerep', 'listing'),
('3578141004', '3578', '3578141', '004', 'Lontar', 'listing'),
('3578150006', '3578', '3578150', '006', 'Karangpoh', 'listing'),
('3578150007', '3578', '3578150', '007', 'Balongsari', 'listing'),
('3578150009', '3578', '3578150', '009', 'Manukan Wetan', 'listing'),
('3578150010', '3578', '3578150', '010', 'Manukan Kulon', 'listing'),
('3578150011', '3578', '3578150', '011', 'Banjar Sugihan', 'listing'),
('3578150013', '3578', '3578150', '013', 'Tandes', 'listing'),
('3578160001', '3578', '3578160', '001', 'Putat Gede', 'listing'),
('3578160002', '3578', '3578160', '002', 'Sono Kwijenan', 'listing'),
('3578160003', '3578', '3578160', '003', 'Simomulyo', 'listing'),
('3578160004', '3578', '3578160', '004', 'Suko Manunggal', 'listing'),
('3578160005', '3578', '3578160', '005', 'Tanjungsari', 'listing'),
('3578160006', '3578', '3578160', '006', 'Simomulyo Baru', 'listing'),
('3578170001', '3578', '3578170', '001', 'Pakis', 'listing'),
('3578170002', '3578', '3578170', '002', 'Putat Jaya', 'listing'),
('3578170003', '3578', '3578170', '003', 'Banyu Urip', 'listing'),
('3578170004', '3578', '3578170', '004', 'Kupang Krajan', 'listing'),
('3578170005', '3578', '3578170', '005', 'Petemon', 'listing'),
('3578170006', '3578', '3578170', '006', 'Sawahan', 'listing'),
('3578180001', '3578', '3578180', '001', 'Keputran', 'listing'),
('3578180002', '3578', '3578180', '002', 'Dr. Sutomo', 'listing'),
('3578180003', '3578', '3578180', '003', 'Tegalsari', 'listing'),
('3578180004', '3578', '3578180', '004', 'Wonorejo', 'listing'),
('3578180005', '3578', '3578180', '005', 'Kedungdoro', 'listing'),
('3578190001', '3578', '3578190', '001', 'Embong Kaliasin', 'listing'),
('3578190002', '3578', '3578190', '002', 'Ketabang', 'listing'),
('3578190003', '3578', '3578190', '003', 'Genteng', 'listing'),
('3578190004', '3578', '3578190', '004', 'Peneleh', 'listing'),
('3578190005', '3578', '3578190', '005', 'Kapasari', 'listing'),
('3578200001', '3578', '3578200', '001', 'Pacar Keling', 'listing'),
('3578200002', '3578', '3578200', '002', 'Pacar Kembang', 'listing'),
('3578200003', '3578', '3578200', '003', 'Ploso', 'listing'),
('3578200004', '3578', '3578200', '004', 'Tambaksari', 'listing'),
('3578200005', '3578', '3578200', '005', 'Rangkah', 'listing'),
('3578200006', '3578', '3578200', '006', 'Gading', 'listing'),
('3578200007', '3578', '3578200', '007', 'Kapasmadya Baru', 'listing'),
('3578200008', '3578', '3578200', '008', 'Dukuh Setro', 'listing'),
('3578210005', '3578', '3578210', '005', 'Tanah Kali Kedinding', 'listing'),
('3578210006', '3578', '3578210', '006', 'Sidotopo Wetan', 'listing'),
('3578210007', '3578', '3578210', '007', 'Bulak Banteng', 'listing'),
('3578210008', '3578', '3578210', '008', 'Tambak Wedi', 'listing'),
('3578211003', '3578', '3578211', '003', 'Kenjeran', 'listing'),
('3578211004', '3578', '3578211', '004', 'Bulak', 'listing'),
('3578211005', '3578', '3578211', '005', 'Kedung Cowek', 'listing'),
('3578211006', '3578', '3578211', '006', 'Sukolilo Baru', 'listing'),
('3578220001', '3578', '3578220', '001', 'Kapasan', 'listing'),
('3578220002', '3578', '3578220', '002', 'Tambakrejo', 'listing'),
('3578220003', '3578', '3578220', '003', 'Simokerto', 'listing'),
('3578220004', '3578', '3578220', '004', 'Sidodadi', 'listing'),
('3578220005', '3578', '3578220', '005', 'Simolawang', 'listing'),
('3578230001', '3578', '3578230', '001', 'Ampel', 'listing'),
('3578230002', '3578', '3578230', '002', 'Sidotopo', 'listing'),
('3578230003', '3578', '3578230', '003', 'Pegirian', 'listing'),
('3578230004', '3578', '3578230', '004', 'Wonokusumo', 'listing'),
('3578230005', '3578', '3578230', '005', 'Ujung', 'listing'),
('3578240001', '3578', '3578240', '001', 'Bongkaran', 'listing'),
('3578240002', '3578', '3578240', '002', 'Nyamplungan', 'listing'),
('3578240003', '3578', '3578240', '003', 'Krembangan Utara', 'listing'),
('3578240004', '3578', '3578240', '004', 'Perak Timur', 'listing'),
('3578240005', '3578', '3578240', '005', 'Perak Utara', 'listing'),
('3578250001', '3578', '3578250', '001', 'Tembok Dukuh', 'listing'),
('3578250002', '3578', '3578250', '002', 'Bubutan', 'listing'),
('3578250003', '3578', '3578250', '003', 'Alon-alon Contong', 'listing'),
('3578250004', '3578', '3578250', '004', 'Gundih', 'listing'),
('3578250005', '3578', '3578250', '005', 'Jepara', 'listing'),
('3578260001', '3578', '3578260', '001', 'Dupak', 'listing'),
('3578260002', '3578', '3578260', '002', 'Morokrembangan', 'listing'),
('3578260003', '3578', '3578260', '003', 'Perak Barat', 'listing'),
('3578260004', '3578', '3578260', '004', 'Kemayoran', 'listing'),
('3578260005', '3578', '3578260', '005', 'Krembangan Selatan', 'listing'),
('3578270003', '3578', '3578270', '003', 'Asemrowo', 'listing'),
('3578270006', '3578', '3578270', '006', 'Genting Kalianak', 'listing'),
('3578270007', '3578', '3578270', '007', 'Tambak Sarioso', 'listing'),
('3578280004', '3578', '3578280', '004', 'Sememi', 'listing'),
('3578280006', '3578', '3578280', '006', 'Kandangan', 'listing'),
('3578280007', '3578', '3578280', '007', 'Tambak Oso Wilangon', 'listing'),
('3578280008', '3578', '3578280', '008', 'Romokalisari', 'listing'),
('3578281001', '3578', '3578281', '001', 'Babat Jerawat', 'listing'),
('3578281002', '3578', '3578281', '002', 'Pakal', 'listing'),
('3578281003', '3578', '3578281', '003', 'Benowo', 'listing'),
('3578281004', '3578', '3578281', '004', 'Sumberrejo', 'listing');

-- --------------------------------------------------------

--
-- Table structure for table `kabupaten`
--

CREATE TABLE `kabupaten` (
  `kabno` char(4) NOT NULL,
  `nama` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kabupaten`
--

INSERT INTO `kabupaten` (`kabno`, `nama`) VALUES
('3201', 'Bogor'),
('3202', 'Sukabumi'),
('3203', 'Cianjur'),
('3515', 'Sidoarjo'),
('3573', 'Malang'),
('3578', 'Surabaya');

-- --------------------------------------------------------

--
-- Table structure for table `kecamatan`
--

CREATE TABLE `kecamatan` (
  `kabno` char(4) NOT NULL,
  `kecno` char(7) NOT NULL,
  `nama` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kecamatan`
--

INSERT INTO `kecamatan` (`kabno`, `kecno`, `nama`) VALUES
('3515', '3515100', 'Sukodono'),
('3515', '3515120', 'Buduran'),
('3515', '3515130', 'Sedati'),
('3515', '3515140', 'Waru'),
('3515', '3515150', 'Gedangan'),
('3515', '3515160', 'Taman'),
('3573', '3573010', 'Kedungkandang'),
('3573', '3573020', 'Sukun'),
('3573', '3573030', 'Klojen'),
('3573', '3573040', 'Blimbing'),
('3573', '3573050', 'Lowokwaru'),
('3578', '3578010', 'Karang Pilang'),
('3578', '3578020', 'Jambangan'),
('3578', '3578030', 'Gayungan'),
('3578', '3578040', 'Wonocolo'),
('3578', '3578050', 'Tenggilis Mejoyo'),
('3578', '3578060', 'Gunung Anyar'),
('3578', '3578070', 'Rungkut'),
('3578', '3578080', 'Sukolilo'),
('3578', '3578090', 'Mulyorejo'),
('3578', '3578100', 'Gubeng'),
('3578', '3578110', 'Wonokromo'),
('3578', '3578120', 'Dukuh Pakis'),
('3578', '3578130', 'Wiyung'),
('3578', '3578140', 'Lakarsantri'),
('3578', '3578141', 'Sambikerep'),
('3578', '3578150', 'Tandes'),
('3578', '3578160', 'Suko Manunggal'),
('3578', '3578170', 'Sawahan'),
('3578', '3578180', 'Tegalsari'),
('3578', '3578190', 'Genteng'),
('3578', '3578200', 'Tambaksari'),
('3578', '3578210', 'Kenjeran'),
('3578', '3578211', 'Bulak'),
('3578', '3578220', 'Simokerto'),
('3578', '3578230', 'Semampir'),
('3578', '3578240', 'Pabean Cantian'),
('3578', '3578250', 'Bubutan'),
('3578', '3578260', 'Krembangan'),
('3578', '3578270', 'Asemrowo'),
('3578', '3578280', 'Benowo'),
('3578', '3578281', 'Pakal');

-- --------------------------------------------------------

--
-- Table structure for table `notif`
--

CREATE TABLE `notif` (
  `unique_id_instance` varchar(50) NOT NULL,
  `nim` varchar(9) NOT NULL,
  `kortim` varchar(9) NOT NULL,
  `status_isian` varchar(10) NOT NULL,
  `status` varchar(20) NOT NULL,
  `form_id` varchar(100) NOT NULL,
  `_id` int(11) NOT NULL,
  `UploadName` varchar(150) NOT NULL,
  `time` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `rumahtangga`
--

CREATE TABLE `rumahtangga` (
  `kodeRuta` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kodeBs` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nomorUrut` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `noSegmen` varchar(4) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bf` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bs` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `namaPemberiInformasi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `noHp` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provider` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamatDomisili` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provinsi` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kodeProvinsi` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kabkot` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kodeKabkot` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kecamatan` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kodeKecamatan` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kelurahan` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kodeKelurahan` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  `akurasi` int(11) NOT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `rumahtangga`
--

INSERT INTO `rumahtangga` (`kodeRuta`, `kodeBs`, `nomorUrut`, `noSegmen`, `bf`, `bs`, `namaPemberiInformasi`, `noHp`, `provider`, `alamatDomisili`, `provinsi`, `kodeProvinsi`, `kabkot`, `kodeKabkot`, `kecamatan`, `kodeKecamatan`, `kelurahan`, `kodeKelurahan`, `latitude`, `longitude`, `akurasi`, `status`, `time`) VALUES
('021a0104-443f-1925-b', '3578050002008A', '001', 'S020', 'https://goo.gl/maps/RxMNUeZiWpHabTWy7', '001', 'Dr. Dadang Hardiwan, S.Si, M.Si', '081355965454', 'Telkomsel', 'Jln. Raya Ngagel Jaya No 51 Gubeng, Surabaya', 'Jawa Timur', '35', 'Surabaya', '78', 'Gubeng', '100', 'Pucang Sewu', '002', 0, 0, 21, '', '0000-00-00 00:00:00'),
('032a0481-727f-3677-b', '3578030003009B', '001', 'S009', 'https://goo.gl/maps/iDjTjqiwb291rpYh7', '001', 'Ichwan Fu\'adi', '082233327272', 'Telkomsel', 'Ngagelrejo utara 7/20', 'Jawa Timur', '35', 'Surabaya', '78', 'Wonokromo', '110', 'Ngagelrejo', '004', 0, 0, 21, '', '0000-00-00 00:00:00'),
('034a0830-120f-5403-b', '3578050002006A', '001', 'S007', 'https://goo.gl/maps/Y9847ANHxtm6tSD77', '001', 'Elita Susilawati, SE', '081357698579', 'Telkomsel', 'Perumahan Kartika Chandra No 28 Desa Tambakrejo Kec. Waru Kab. Sidoarjo', 'Jawa Timur', '35', 'Sidoarjo', '15', 'Waru', '140', 'Tambakrejo', '008', 0, 0, 21, '', '0000-00-00 00:00:00'),
('045a0252-345f-4448-b', '3578050002002A', '001', 'S011', 'https://goo.gl/maps/E1jDXi2vd2T4GvQG6', '001', 'Thomas Wunang Tjahjo, M.Sc., M.Eng.', '081234286688', 'Telkomsel', 'Jl. Kutisari Selatan II No. 53', 'Jawa Timur', '35', 'Surabaya', '78', 'Tenggilis Mejoyo', '050', 'Kutisari', '001', 0, 0, 21, '', '0000-00-00 00:00:00'),
('050a6977-320f-0291-b', '3578030003005B', '001', 'S007', 'https://goo.gl/maps/kF4isbK2XtzvUfka9', '001', 'Paramitha dian L P', '081333072005', 'Telkomsel', 'Griya bhayangkara masangan kulon', 'Jawa Timur', '35', 'Sidoarjo', '15', 'Sukodono', '100', 'Masangan Kulon', '017', 0, 0, 21, '', '0000-00-00 00:00:00'),
('057a9630-193f-9799-b', '3578030003009B', '001', 'S012', 'https://goo.gl/maps/Axn6oSRXu4jKRCcKA', '001', 'Fahrudin sukarno', '082233020273', 'Telkomsel', 'Karangrejo sawah 4 no. 15B', 'Jawa Timur', '35', 'Surabaya', '78', 'Wonokromo', '110', 'Wonokromo', '002', 0, 0, 21, '', '0000-00-00 00:00:00'),
('0ed3c972-50cb-4d94-8', '3578050002001A', '001', 'S007', 'https://goo.gl/maps/mHszbmj8ZcYgW5Rk7', '001', 'Dr. Agus Budhi Santosa, S.Si, M.T', '081332795840', 'Telkomsel', 'Perum. Graha Menganti E3/12 Menganti-Gresik', 'Jawa Timur', '35', 'Gresik', '25', 'Menganti', '040', 'Bringkang', '002', 0, 0, 21, '', '0000-00-00 00:00:00'),
('103a0684-485f-3661-a', '3578050002002A', '001', 'S005', 'https://goo.gl/maps/jt27ZftrtDLhTxsJ6', '001', 'Eko Hardiyanto, SST., M.T.', '085780237483/081332050083', 'Indosat/Telkomsel', 'Jl kutisari utara, gg masjid Kota Surabaya', 'Jawa Timur', '35', 'Surabaya', '78', 'Tenggilis Mejoyo', '050', 'Kutisari', '001', 0, 0, 21, '', '0000-00-00 00:00:00'),
('116a0557-721f-9274-a', '3578050002003A', '001', 'S005', 'https://goo.gl/maps/hdo3Jg3MGjEWq1By6', '001', 'Rofikotul Arfati, S.Si, M.A', '081234568800', 'Telkomsel', 'Perumahan Gunung Anyar Emas Blok A nomor 15 Surabaya', 'Jawa Timur', '35', 'Surabaya', '78', 'Gunung Anyar', '060', 'Gunung Anyar Tambak', '004', 0, 0, 21, '', '0000-00-00 00:00:00'),
('124a0213-930f-5566-d', '3578050002009A', '001', 'S003', 'https://goo.gl/maps/SbqiiMfzbwnWXFzCA', '001', 'Widia Puspitasari, SST, M.Stat', '081278750001', 'Telkomsel', 'Jambangan VII C No.10 Kota Surabaya', 'Jawa Timur', '35', 'Surabaya', '78', 'Jambangan', '020', 'Jambangan', '003', 0, 0, 21, '', '0000-00-00 00:00:00'),
('12d14257-a9fd-4185-b', '3578050002001A', '001', 'S011', 'https://goo.gl/maps/tBNFuE8duQKwoSy67', '001', 'Umar Sjaifudin, M.Si', '08155041359', 'Indosat', 'Jl.cluster tigris I No.19, Perumahan Tirtasari Residence Kab. Malang', 'Jawa Timur', '35', 'Malang', '07', 'Wagir', '210', 'Sitirejo', '003', 0, 0, 21, '', '0000-00-00 00:00:00'),
('133a0673-254f-4741-d', '3578050002010A', '001', 'S011', 'https://goo.gl/maps/ghPvynLmmdZ7tn7w5', '001', 'Desy Widya Indahyani Hartono, S.A.', '085733943700', 'Indosat', 'Griya Citra Asri RM 22/25 Kota Surabaya', 'Jawa Timur', '35', 'Surabaya', '78', 'Benowo', '280', 'Sememi', '004', 0, 0, 21, '', '0000-00-00 00:00:00'),
('13f98eba-f802-4e3b-b', '3578050002002A', '001', 'S008', 'https://goo.gl/maps/eAx8oKYvFFYcGuiUA', '001', 'Wahyu Pujiati, S.AP', '081317733156', 'Telkomsel', 'Jalan Setiabudi V Gang II No. 12 Setiabudi Jakarta Selatan', 'DKI Jakarta', '31', 'Jakarta Selatan', '71', 'Setiabudi', '100', 'Setia Budi', '008', 0, 0, 21, '', '0000-00-00 00:00:00'),
('140a0720-176f-7453-b', '3578030003005B', '001', 'S004', 'https://goo.gl/maps/UZbWGVrMXrRqg5nN7', '001', 'FADJAR SUHAERI', '082228407070', 'Telkomsel', 'Sapphire Residence blok 6i no 32', 'Jawa Timur', '35', 'Sidoarjo', '15', 'Buduran', '120', 'Prasung', '007', 0, 0, 21, '', '0000-00-00 00:00:00'),
('140a0914-384f-3661-c', '3578050002007A', '001', 'S008', 'https://goo.gl/maps/kKpZMTHeeuhHFxqE9', '001', 'Adis Nalia, S.M.', '085329003491', 'Telkomsel', 'Perum Alam Juanda Blok. A5. No.4 Kab.Sidoarjo', 'Jawa Timur', '35', 'Sidoarjo', '15', 'Sedati', '130', 'Pepe', '002', 0, 0, 21, '', '0000-00-00 00:00:00'),
('162a0179-884f-8453-d', '3578050002004A', '001', 'S009', 'https://goo.gl/maps/PDyddXtkdYXsurhp6', '001', 'NIZAR IRSYAD', '082317529050/082264022304', 'Telkomsel', 'Taman Permata Inda B-16, Kalijaten, Taman, Sidoarjo', 'Jawa Timur', '35', 'Sidoarjo', '15', 'Taman', '160', 'Kalijaten', '019', 0, 0, 21, '', '0000-00-00 00:00:00'),
('166a0196-552f-5382-c', '3578050002001A', '001', 'S003', 'https://goo.gl/maps/N7GrEup2SntBWNgT6', '001', 'Abdullah Hakim, SE', '081553490162', 'Indosat', 'Rungkut Kidul 5 Kalimir No. 9 Surabaya', 'Jawa Timur', '35', 'Surabaya', '78', 'Rungkut', '070', 'Rungkut Kidul', '001', 0, 0, 21, '', '0000-00-00 00:00:00'),
('169a0832-881f-3547-c', '3578050002009A', '001', 'S011', 'https://goo.gl/maps/NMWmvbwtZqZT4QWs5', '001', 'Rendy Karuniawan, S.H.', '085732439798', 'Indosat', 'Perumahan Karah Agung VI Kav. 70, Surabaya', 'Jawa Timur', '35', 'Surabaya', '78', 'Jambangan', '020', 'Karah', '004', 0, 0, 21, '', '0000-00-00 00:00:00'),
('174a0580-916f-1417-d', '3578050002001A', '001', 'S001', 'https://goo.gl/maps/r7kCRQYyqMA7Umxr7', '001', 'Dyah Pembayun Indrijatmiko, SE, M.Ec.Dev.', '081330907091', 'Telkomsel', 'Rungkut Asri Tengah Gg XX no 22 Kota Surabaya', 'Jawa Timur', '35', 'Surabaya', '78', 'Rungkut', '070', 'Rungkut Kidul', '001', 0, 0, 21, '', '0000-00-00 00:00:00'),
('190a0587-141f-4539-d', '3578050002010A', '001', 'S005', 'https://goo.gl/maps/8pjxju3XSrnjTY2JA', '001', 'Sulistyorini, SE', '081216378875', 'Telkomsel', 'Pakis Tirtosari No. 45 Kota Surabaya', 'Jawa Timur', '35', 'Surabaya', '78', 'Sawahan', '170', 'Pakis', '001', 0, 0, 21, '', '0000-00-00 00:00:00'),
('192a0595-295f-8786-b', '3578030003001B', '001', 'S007', 'https://goo.gl/maps/brzQjRTH8T9nfT526', '001', 'WULAN NURYULIANINGDYAH', '085645745325', 'Indosat', 'KEDUNG ASEM GG 4 NO 2', 'Jawa Timur', '35', 'Surabaya', '78', 'Rungkut', '070', 'Kedung Baruk', '005', 0, 0, 21, '', '0000-00-00 00:00:00'),
('196a0736-562f-2250-b', '3578030003005B', '001', 'S010', 'https://goo.gl/maps/dBUBJY29WSkVGYzn8', '001', 'Heri Purnomo', '081553608220', 'Indosat', 'Griya Bhayangkara blok i3 no 5, Masangan kulon,Sukodono,Sidoarjo', 'Jawa Timur', '35', 'Sidoarjo', '15', 'Sukodono', '100', 'Masangan Kulon', '017', 0, 0, 21, '', '0000-00-00 00:00:00'),
('1b2551c9-a448-4f64-8', '3578050002003A', '001', 'S012', 'https://goo.gl/maps/yfLaY27dVYVYyL2H9', '001', 'Agus Wachyu Purwono', '081234972417', 'Telkomsel', 'Khayangan Residence F2 No. 18 Kab.Bangkalan', 'Jawa Timur', '35', 'Bangkalan', '26', 'Burneh', '120', 'Burneh', '003', 0, 0, 21, '', '0000-00-00 00:00:00'),
('1de5e73d-a6a2-4af9-b', '3578050002003A', '001', 'S002', 'https://goo.gl/maps/RLuu7nYGWkqKnWQC9', '001', 'Nurhayati, SE', '081330187019', 'Telkomsel', 'Jl. Panca Warna 5 No. 41 Perumnas Kota Baru Driyorejo Kab.Gresik', 'Jawa Timur', '35', 'Gresik', '25', 'Driyorejo', '020', 'Mulung', '005', 0, 0, 21, '', '0000-00-00 00:00:00'),
('211a0501-991f-1529-b', '3578030003005B', '001', 'S005', 'https://goo.gl/maps/9h19Qrp3Lbndpt9V6', '001', 'Ary Hindratmo, S.Mn, M.M', '081231552375', 'Telkomsel', 'Perum Taman Suko Asri Blok CC-42', 'Jawa Timur', '35', 'Sidoarjo', '15', 'Sukodono', '100', 'Suko', '016', 0, 0, 21, '', '0000-00-00 00:00:00'),
('230a0455-368f-1803-d', '3578050002008A', '001', 'S006', 'https://goo.gl/maps/QTW2zuE6vFtjdsqk8', '001', 'Tatis Yuliarisaningtyas, S.Si', '081330559986', 'Telkomsel', 'Ikan Duyung No. 36 Kota Surabaya', 'Jawa Timur', '35', 'Surabaya', '78', 'Krembangan', '260', 'Perak Barat', '003', 0, 0, 21, '', '0000-00-00 00:00:00'),
('233a0731-229f-8414-d', '3578050002002A', '001', 'S004', 'https://goo.gl/maps/tfHx1LpqMZm8mVyv7', '001', 'Agus Sutikno', '081235628282/085101419696', 'Telkomsel', 'Tenggilis Mejoyo Selatan V/28 Kota Surabaya', 'Jawa Timur', '35', 'Surabaya', '78', 'Tenggilis Mejoyo', '050', 'Tenggilis Mejoyo', '003', 0, 0, 21, '', '0000-00-00 00:00:00'),
('233a0896-204f-1373-b', '3578030003005B', '001', 'S005', 'https://goo.gl/maps/Mvh3Zqz2YncqmiYV6', '001', 'aris kuswantoro', '08165430340', 'Indosat', 'Griya Bhayangkara Masangan Kulon Blok i3 No.16 Sidoarjo', 'Jawa Timur', '35', 'Sidoarjo', '15', 'Sukodono', '100', 'Masangan Kulon', '017', 0, 0, 21, '', '0000-00-00 00:00:00'),
('235adc07-b789-454f-b', '3578050002003A', '001', 'S007', 'https://goo.gl/maps/UMrgKf6G6Pb6WmqT9', '001', 'Amin Sani Kertiyasa, SST, MT', '081331890887', 'Telkomsel', 'Jalan Wisnu Wardhana IV No. 65 A Kab.Jombang', 'Jawa Timur', '35', 'Jombang', '17', 'Jombang', '130', 'Kaliwungu', '006', 0, 0, 21, '', '0000-00-00 00:00:00'),
('250a0391-153f-3739-b', '3578050002004A', '001', 'S002', 'https://goo.gl/maps/tGkpAVfHpYt2KQGv7', '001', 'Eko Susanto, SST', '082148639165/085697051527', 'Telkomsel/Indosat', 'Perumahan Nizar Mansion E2/10, Desa Bohar, Kec. Taman, Kab. Sidoarjo', 'Jawa Timur', '35', 'Sidoarjo', '15', 'Taman', '160', 'Bohar', '015', 0, 0, 21, '', '0000-00-00 00:00:00'),
('258a0193-239f-9169-d', '3578030003010B', '001', 'S004', 'https://goo.gl/maps/vh3c2rvUuvc9yrRE9', '001', 'Agung Prasetiyo', '081335218040', 'Telkomsel', 'Jl. Manukan Tengah No 42 Tandes Surabaya ', 'Jawa Timur', '35', 'Surabaya', '78', 'Tandes', '150', 'Manukan Kulon', '010', 0, 0, 21, '', '2023-01-29 22:49:18'),
('259a0922-598f-3942-b', '3578050002001A', '001', 'S008', 'https://goo.gl/maps/S3YyGqgdmdDuRuap9', '001', 'Daizy Pangeswari, SE, M.M', '082140739916', 'Telkomsel', 'Perum Yekape Penjaringan Sari 2 Blok K Nomor 17 Kota Surabaya', 'Jawa Timur', '35', 'Surabaya', '78', 'Rungkut', '070', 'Penjaringan Sari', '004', 0, 0, 21, '', '0000-00-00 00:00:00'),
('263a0446-768f-8667-c', '3578050002004A', '001', 'S001', 'https://goo.gl/maps/ervxhAH6WUnTGDct7', '001', 'Nanik Hidayati, SE, M.A.', '085707504618', 'Indosat', 'Ketegan Barat RT 2 RW 1 Kab.Sidoarjo', 'Jawa Timur', '35', 'Sidoarjo', '15', 'Taman', '160', 'Ketegan', '022', 0, 0, 21, '', '0000-00-00 00:00:00'),
('270a0767-818f-4420-a', '3578030003007B', '001', 'S004', 'https://goo.gl/maps/YDYcp3jx5me5ccmS9', '001', 'Jamaludin', '082334263339', 'Telkomsel', 'Permata Kwangsan Residen Blok C2/14', 'Jawa Timur', '35', 'Sidoarjo', '15', 'Sedati', '130', 'Kwangsan', '001', 0, 0, 21, '', '0000-00-00 00:00:00'),
('272a0299-103f-7456-a', '3578050002005A', '001', 'S003', 'https://goo.gl/maps/JKDK7v4HvZvspseAA', '001', 'Aldizah Dajustia Hutami', '082174070066', 'Telkomsel', 'Jl. Kesatrian, Perumahan Park Royal Regency, Kelurahan Sidokerto, Kecamatan Buduran, Kabupaten Sidoarjo', 'Jawa Timur', '35', 'Sidoarjo', '15', 'Buduran', '120', 'Sidokerto', '003', 0, 0, 21, '', '0000-00-00 00:00:00'),
('288a0611-268f-1207-c', '3578030003009B', '001', 'S002', 'https://goo.gl/maps/14qHecpthekP4dVR8', '001', 'Aries Kurniawan', '08113509953', 'Telkomsel', 'Ngagel dadi 3a no 12a', 'Jawa Timur', '35', 'Surabaya', '78', 'Wonokromo', '110', 'Ngagelrejo', '004', 0, 0, 21, '', '2023-01-29 23:00:26'),
('302a0945-859f-7801-b', '3578050002006A', '001', 'S001', 'https://goo.gl/maps/tKcYo6YEQ78uqNjP6', '001', 'Akhmad Yuliadi', '082336037066', 'Telkomsel', 'Jl. Ngeni Dalam I, RT.01 RW. 02 Kab.Sidoarjo', 'Jawa Timur', '35', 'Sidoarjo', '15', 'Waru', '140', 'Kepuh Kiriman', '013', 0, 0, 21, '', '0000-00-00 00:00:00'),
('3212bc0f-6509-4b85-8', '3578030003003B', '001', 'S003', 'https://goo.gl/maps/zW7uT1m8Ex1fhDaV6', '001', 'Anilur Rohman', '082140668150', 'Telkomsel', 'Perum Griya Kamulan Blok A No 3 Talun Kab. Blitar', 'Jawa Timur', '35', 'Blitar', '05', 'Talun', '090', 'Kamulan', '010', 0, 0, 21, '', '0000-00-00 00:00:00'),
('326a0230-918f-3895-d', '3578050002008A', '001', 'S011', 'https://goo.gl/maps/7aY7usgWio8RGohP9', '001', 'Arga Parama Yufinanda, SST', '081340853520', 'Telkomsel', 'Jl Gubeng Klingsingan No 2 RT1 RW3, Kelurahan Gubeng, Kecamatan Gubeng, Surabaya, 60281', 'Jawa Timur', '35', 'Surabaya', '78', 'Gubeng', '100', 'Gubeng', '004', 0, 0, 21, '', '0000-00-00 00:00:00'),
('337a0753-811f-1775-b', '3578030003009B', '001', 'S005', 'https://goo.gl/maps/rmapQkvZbD8woi8c6', '001', 'Titik suryani', '081217686611', 'Telkomsel', 'Karangrejo sawah 4/7 surabaya', 'Jawa Timur', '35', 'Surabaya', '78', 'Wonokromo', '110', 'Wonokromo', '002', 0, 0, 21, '', '0000-00-00 00:00:00'),
('352a0652-400f-5218-d', '3578050002009A', '001', 'S009', 'https://goo.gl/maps/tAPiicpGuGBrwmUR7', '001', 'Natria Nur Wulan, S.Si', '081235457102', 'Telkomsel', 'Jl. Kebonsari LVK VI/ No 4 Kota Surabaya', 'Jawa Timur', '35', 'Surabaya', '78', 'Jambangan', '020', 'Kebonsari', '002', 0, 0, 21, '', '0000-00-00 00:00:00'),
('378a0369-514f-6631-d', '3578050002008A', '001', 'S004', 'https://goo.gl/maps/tzWFkVbNK5UabrTDA', '001', 'Juli Purwanto, S.T, M.M.', '081235128875', 'Telkomsel', 'Kedung Tarukan Baru 4/56 Kota Surabaya', 'Jawa Timur', '35', 'Surabaya', '78', 'Gubeng', '100', 'Mojo', '006', 0, 0, 21, '', '0000-00-00 00:00:00'),
('386ebb56-bc95-4024-8', '3578030003003B', '001', 'S007', 'https://goo.gl/maps/QKqUt2trSKn8xLvr7', '001', 'Wahyuningsih', '082189597345', 'Telkomsel', 'Perum ABR Blok E6/17 Gresik', 'Jawa Timur', '35', 'Gresik', '25', 'Kebomas', '090', 'Randuagung', '020', 0, 0, 21, '', '0000-00-00 00:00:00'),
('391a0819-246f-2871-a', '3578050002002A', '001', 'S005', 'https://goo.gl/maps/jUyCFncRUr9wcdX78', '001', 'Sumaidi', '082331218954', 'Telkomsel', 'Kutisari Utara RT06 RW02 Kota Surabaya', 'Jawa Timur', '35', 'Surabaya', '78', 'Tenggilis Mejoyo', '050', 'Kutisari', '001', 0, 0, 21, '', '0000-00-00 00:00:00'),
('3b78f5dc-8eb1-4b12-9', '3578030003003B', '001', 'S007', 'https://goo.gl/maps/skKTvR1mz9fksu6T9', '001', 'Agus Budi Santoso', '081330312408', 'Telkomsel', 'Jln Banten A6-17 Mojokerto', 'Jawa Timur', '35', 'Mojokerto', '16', 'Sooko', '130', 'Japan', '009', 0, 0, 21, '', '0000-00-00 00:00:00'),
('3c65b671-e1bf-4fc2-9', '3578030003003B', '001', 'S005', 'https://goo.gl/maps/DeHQbEfG86PFJcKy6', '001', 'Suwarno', '081216847232', 'Telkomsel', 'Perum Darus Sakinah Residence No. B.03 Japan-sooko Kab. Mojokerto', 'Jawa Timur', '35', 'Mojokerto', '16', 'Sooko', '130', 'Japan', '009', 0, 0, 21, '', '0000-00-00 00:00:00'),
('40959e02-63d5-43f6-b', '3578050002013A', '001', 'S009', 'https://goo.gl/maps/SpbSuBvNDCZFEubL6', '001', 'Amin Fathullah, SST', '082331149141', 'Telkomsel', 'Jalan Otto Iskandardinata, Gang Mulia No. 9 Jakarta Timur', 'DKI Jakarta', '31', 'Jakarta Timur', '72', 'Jatinegara', '060', 'Bidara Cina', '001', 0, 0, 21, '', '0000-00-00 00:00:00'),
('422a0949-973f-1953-b', '3578050002004A', '001', 'S001', '-', '001', 'Bastomi', '085655492511', 'Indosat', 'Dusun Sambibulu Rt 08/01 Kab.Sidoarjo', 'Jawa Timur', '35', 'Sidoarjo', '15', 'Taman', '160', 'Sambi Bulu', '007', 0, 0, 21, '', '0000-00-00 00:00:00'),
('429a0305-480f-3550-a', '3578050002003A', '001', 'S001', 'https://goo.gl/maps/XXb4BCca3HqdvzNe8', '001', 'Novi Rosiana, SST', '085726436540', 'Indosat', 'Jalan Rungkut Barata III no 4.b Kota Surabaya', 'Jawa Timur', '35', 'Surabaya', '78', 'Gunung Anyar', '060', 'Rungkut Menanggal', '001', 0, 0, 21, '', '0000-00-00 00:00:00'),
('432a0211-117f-3497-d', '3578050002009A', '001', 'S011', 'https://goo.gl/maps/QN5z19buyA4czUVaA', '001', 'Damas Iskandar Wahidayat, SST', '085217477101', 'Telkomsel', 'Jl. Tales II No. 5A Kelurahan Jagir Kecamatan Wonokromo Kota Surabaya', 'Jawa Timur', '35', 'Surabaya', '78', 'Wonokromo', '110', 'Jagir', '003', 0, 0, 21, '', '0000-00-00 00:00:00'),
('440a0574-421f-4229-d', '3578050002003A', '001', 'S008', 'https://goo.gl/maps/LLeRLQTgmddQSm8P6', '001', 'Luxy Lutfiana Rachmawati, SST', '085655743163', 'Indosat', 'Perumahan Taman Gunung Anyar Blok G no. 51 Kota Surabaya', 'Jawa Timur', '35', 'Surabaya', '78', 'Gunung Anyar', '060', 'Gunung Anyar', '003', 0, 0, 21, '', '0000-00-00 00:00:00'),
('440a0729-933f-9522-a', '3578050002002A', '001', 'S011', 'https://goo.gl/maps/ZTafJMgmhgryj7XR6', '001', 'Herlina Sri Martanti, S.Si', '081233719287', 'Telkomsel', 'Kutisari Indah Barat III No.33 Kota Surabaya', 'Jawa Timur', '35', 'Surabaya', '78', 'Tenggilis Mejoyo', '050', 'Kutisari', '001', 0, 0, 21, '', '0000-00-00 00:00:00'),
('449a0738-939f-6621-c', '3578050002009A', '001', 'S003', 'https://goo.gl/maps/gPrdKoeNufhQBmhXA', '001', 'R. Arief Budi Setyono, A.Md', '081332354298', 'Telkomsel', 'Gunung Sari I No. 7 Kota Surabaya', 'Jawa Timur', '35', 'Surabaya', '78', 'Wonokromo', '110', 'Sawunggaling', '001', 0, 0, 21, '', '0000-00-00 00:00:00'),
('44ee8e24-e7c6-404d-8', '3578050002013A', '001', 'S011', 'https://goo.gl/maps/pXs8s6HwiJJFSPsG6', '001', 'Arih Thoyyibatul Izdihar, S.Tr.Stat.', '082333567032', 'Telkomsel', 'Jalan Otista No.16 RT 2 RW 9 Jakarta Timur', 'DKI Jakarta', '31', 'Jakarta Timur', '72', 'Jatinegara', '060', 'Bidara Cina', '001', 0, 0, 21, '', '0000-00-00 00:00:00'),
('459a0511-552f-7720-c', '3578030003004B', '001', 'S004', 'https://goo.gl/maps/VhbKc9hLhnukKuHB6', '001', 'Iir Lismawati S.Si.', '085256067898', 'Telkomsel', 'Tamna wage regency no 12', 'Jawa Timur', '35', 'Sidoarjo', '15', 'Taman', '160', 'Wage', '016', 0, 0, 21, '', '0000-00-00 00:00:00'),
('461a0744-303f-3907-b', '3578050002009A', '001', 'S007', 'https://goo.gl/maps/uhL8psWGhNGLMp6K6', '001', 'Tamami Ikhwan, SST', '085551141042', 'Indosat', 'Jl. Bendul Merisi Besar Timur No.34a Kota Surabaya', 'Jawa Timur', '35', 'Surabaya', '78', 'Wonocolo', '040', 'Bendul Merisi', '004', 0, 0, 21, '', '0000-00-00 00:00:00'),
('473a0172-841f-8088-d', '3578030003007B', '001', 'S002', 'https://goo.gl/maps/FpBGHGMLsAP59FSw5', '001', 'Bagus Wahyu Purnomo', '081250294888', 'Telkomsel', 'Perum Alam Juanda A5/04', 'Jawa Timur', '35', 'Sidoarjo', '15', 'Sedati', '130', 'Pepe', '002', 0, 0, 21, '', '0000-00-00 00:00:00'),
('475a0478-257f-3027-a', '3578050002001A', '001', 'S011', 'https://goo.gl/maps/bbB9hSMMpPkh2Tsv6', '001', 'Ika Widiati Nugraheny, A.Md', '081334987685', 'Telkomsel', 'Jl. Medayu Pesona IV / C 52 Kota Surabaya', 'Jawa Timur', '35', 'Surabaya', '78', 'Rungkut', '070', 'Medokan Ayu', '002', 0, 0, 21, '', '0000-00-00 00:00:00'),
('478a0729-554f-6079-d', '3578050002003A', '001', 'S001', 'https://goo.gl/maps/dwckHxA52CjNPo6S8', '001', 'Rahma Nuryanti, S.Si., MA.', '081216848433', 'Telkomsel', 'Perum Gunung Anyar Emas Blok c2/14 Kota Surabaya', 'Jawa Timur', '35', 'Surabaya', '78', 'Gunung Anyar', '060', 'Gunung Anyar Tambak', '004', 0, 0, 21, '', '0000-00-00 00:00:00'),
('479a0961-492f-6875-b', '3578050002001A', '001', 'S002', 'https://goo.gl/maps/PErdcanuQwyFfEmG9', '001', 'Debora Sulistya Rini, M.Si', '08123274660', 'Telkomsel', 'Rungkut Harapan H-15 Kota Surabaya', 'Jawa Timur', '35', 'Surabaya', '78', 'Rungkut', '070', 'Kali Rungkut', '006', 0, 0, 21, '', '0000-00-00 00:00:00'),
('486a0851-781f-2764-c', '3578050002003A', '001', 'S010', 'https://goo.gl/maps/7FftEMCW2dzen5hg8', '001', 'Fitriana Kusmarini, S.Si', '081351102536', 'Telkomsel', 'PERUM GAE, Jl Raya Modern, No 47 BB Kota Surabaya', 'Jawa Timur', '35', 'Surabaya', '78', 'Gunung Anyar', '060', 'Gunung Anyar Tambak', '004', 0, 0, 21, '', '0000-00-00 00:00:00'),
('4a46cf65-ac94-42d6-9', '3578050002013A', '001', 'S011', 'https://goo.gl/maps/a6AHG9JpWJbZQyW76', '001', 'Achmad Tadjeri, S.M.', '081382478414', 'Telkomsel', 'Perum Graha Menganti Jl. Belimbing Blok A/II, Kec. Menganti, Kab. Gresik', 'Jawa Timur', '35', 'Gresik', '25', 'Menganti', '040', 'Bringkang', '002', 0, 0, 21, '', '0000-00-00 00:00:00'),
('4c984a90-f2a4-41fa-8', '3578030003013B', '001', 'S011', 'https://goo.gl/maps/NthGH8yfezrB4xzW7', '001', 'Rizka', '081344379123', 'Telkomsel', 'Kotabaru', 'Jawa Timur', '35', 'Gresik', '25', 'Driyorejo', '020', 'Mulung', '005', 0, 0, 21, '', '2023-01-26 08:50:46'),
('4da9824c-31df-4c2d-b', '3578030003013B', '001', 'S009', 'https://goo.gl/maps/VYM8mxFYNgiVc1kT6', '001', 'Nur Arsywati Huda', '081553856575', 'Indosat', 'Sidoarjo', 'Jawa Timur', '35', 'Sidoarjo', '15', '', '', '', '', 0, 0, 21, '', '0000-00-00 00:00:00'),
('4de93e82-3b13-48ea-9', '3578050002004A', '001', 'S002', 'https://goo.gl/maps/EZ6dU83S2sV68Cnu7', '001', 'Nik Imatun, S.E', '081332885848', 'Telkomsel', 'Perumahan Permata Taman Delta Blok AA No. 4 Kab.Sidoarjo', 'Jawa Timur', '35', 'Sidoarjo', '15', 'Candi', '070', 'Kendalpecabean', '006', 0, 0, 21, '', '0000-00-00 00:00:00'),
('508a0847-348f-3856-b', '3578050002007A', '001', 'S010', 'https://goo.gl/maps/RkjGa4F4tBQKkY7S7', '001', 'Ir. Kusriyawanto', '081331168163', 'Telkomsel', 'Permata Alam Permai Jl. Safire V Blok F1 No. 6', 'Jawa Timur', '35', 'Sidoarjo', '15', 'Gedangan', '150', 'Gemurung', '005', 0, 0, 21, '', '0000-00-00 00:00:00'),
('50e100df-e889-49dd-9', '3578050002004A', '001', 'S010', 'https://goo.gl/maps/DcJP4opzXEURpULh7', '001', 'Dwi Agustin Rahayu Susanti, S.Si', '081230357044', 'Telkomsel', 'Simogirang, RT 03 RW 03 Prambon Sidoarjo', 'Jawa Timur', '35', 'Sidoarjo', '15', 'Prambon', '020', 'Simogirang', '018', 0, 0, 21, '', '0000-00-00 00:00:00'),
('511a0824-942f-7049-a', '3578050002006A', '001', 'S005', 'https://goo.gl/maps/m5svEim9GpQF5oTx6', '001', 'Dyah Reni Irmawati', '08568860747/082244240747', 'Indosat/Telkomsel', 'Perum Makarya Binangun Jl. Simpang Dewi Sartika XA-17 Waru Sidoarjo', 'Jawa Timur', '35', 'Sidoarjo', '15', 'Waru', '140', 'Janti', '015', 0, 0, 21, '', '0000-00-00 00:00:00'),
('526a0231-920f-2383-c', '3578050002002A', '001', 'S011', 'https://goo.gl/maps/949xt2ko5KGJ78my6', '001', 'Lulu Lutfiasari, S.Si.,M.M.', '08123469874', 'Telkomsel', 'My Tower Apartement Continent Lt. 12 Unit 1202 Tower A Kota Surabaya', 'Jawa Timur', '35', 'Surabaya', '78', 'Tenggilis Mejoyo', '050', 'Kutisari', '001', 0, 0, 21, '', '0000-00-00 00:00:00'),
('532a0604-246f-7398-b', '3578030003005B', '001', 'S002', 'https://goo.gl/maps/gvHkaszSAZhn9gFYA', '001', 'Setyorini indah purwanti', '082131674983', 'Telkomsel', 'Griya palem putri 2 no 50, masangan wetan, sidoarjo', 'Jawa Timur', '35', 'Sidoarjo', '15', 'Sukodono', '100', 'Masangan Wetan', '015', 0, 0, 21, '', '0000-00-00 00:00:00'),
('540a0474-295f-4959-d', '3578050002005A', '001', 'S011', 'https://goo.gl/maps/Z2iVSYAiR4d3oWPDA', '001', 'Faridah, S.Si., M.M.', '082231432455', 'Telkomsel', 'Perumahan Bumi Koperasi Suko B-75 Kab.Sidoarjo', 'Jawa Timur', '35', 'Sidoarjo', '15', 'Sukodono', '100', 'Suko', '016', 0, 0, 21, '', '0000-00-00 00:00:00'),
('543a0844-448f-7040-d', '3578050002009A', '001', 'S010', 'https://goo.gl/maps/yKqY4seoSn8XmKJr7', '001', 'Andy Asyarisupriyadi, SE', '081216552223', 'Telkomsel', 'Siwalan Kerto Selatan No. 88-A 60236 Kota Surabaya', 'Jawa Timur', '35', 'Surabaya', '78', 'Wonocolo', '040', 'Siwalankerto', '001', 0, 0, 21, '', '0000-00-00 00:00:00'),
('562a0653-123f-5542-d', '3578050002002A', '001', 'S009', 'https://goo.gl/maps/9bC4i6CADEvk4Fjx5', '001', 'Dian Parwitasari SE., M. Si', '08113337193/082140631943', 'Telkomsel', 'Kendangsari 3/20 Ivory A1 Surabaya', 'Jawa Timur', '35', 'Surabaya', '78', 'Tenggilis Mejoyo', '050', 'Kendangsari', '010', 0, 0, 21, '', '0000-00-00 00:00:00'),
('568a0694-517f-8524-c', '3578050002008A', '001', 'S009', 'https://goo.gl/maps/EA2VAiRW4dgDF9Yy7', '001', 'Heru Priambodo', '081279666677', 'Telkomsel', 'Jl. Burhanudin Saman No. 10 Kompleks AL Kenjeran Kota Surabaya', 'Jawa Timur', '35', 'Surabaya', '78', 'Bulak', '211', 'Kenjeran', '003', 0, 0, 21, '', '0000-00-00 00:00:00'),
('581a0232-632f-5692-d', '3578030003003B', '001', 'S001', 'https://goo.gl/maps/qBqpcWSpvNJLWgVJ9', '001', 'Falah Hardiman', '085691435491', 'Indosat', 'Gunung Anyar Tengah VII.A/2 Surabaya', 'Jawa Timur', '35', 'Surabaya', '78', 'Gunung Anyar', '060', 'Gunung Anyar', '003', 0, 0, 21, '', '0000-00-00 00:00:00'),
('583a0732-539f-1783-a', '3578050002002A', '001', 'S005', 'https://goo.gl/maps/JgxRv4dosnZ16Zjw6', '001', 'Joko Ade Nursiyono, SST', '081244019483', 'Telkomsel', 'Jalan Kendangsari Gang II, Surabaya', 'Jawa Timur', '35', 'Surabaya', '78', 'Tenggilis Mejoyo', '050', 'Kendangsari', '010', 0, 0, 21, '', '0000-00-00 00:00:00'),
('595a0545-143f-6913-a', '3578050002006A', '001', 'S002', 'https://goo.gl/maps/A6r1h814hsKoQWWb9', '001', 'Akhmad Dardiri, SST, M.M.', '081280086369', 'Telkomsel', 'Delta Sari Indah Blok AS No. 9 Kab.Sidoarjo', 'Jawa Timur', '35', 'Sidoarjo', '15', 'Waru', '140', 'Kureksari', '004', 0, 0, 21, '', '0000-00-00 00:00:00'),
('598a0834-470f-4082-c', '3578050002010A', '001', 'S007', 'https://goo.gl/maps/4yGpkL2Eo9TpdDV57', '001', 'Riena Widianingtyas, S.Si,M.T', '085649119122', 'Indosat', 'Dukuh Kupang Barat XXVI No. 30 Kota Surabaya', 'Jawa Timur', '35', 'Surabaya', '78', 'Dukuh Pakis', '120', 'Dukuh Kupang', '004', 0, 0, 21, '', '0000-00-00 00:00:00'),
('603ab350-349d-4139-9', '3578050002004A', '001', 'S005', 'https://goo.gl/maps/jmGcGDqvU3c5pDR8A', '001', 'Yeni Rahmawati, S.Si', '085645627979', 'Indosat', 'Gg. Pelabuhan No 17 Karanggayam Kab.Sidoarjo', 'Jawa Timur', '35', 'Sidoarjo', '15', 'Sidoarjo', '110', 'Pucanganom', '011', 0, 0, 21, '', '0000-00-00 00:00:00'),
('606a0230-169f-8901-c', '3578050002009A', '001', 'S004', 'https://goo.gl/maps/ARKBhqM4H2yktWoE7', '001', 'Ir. Anggoro Sosiantiwi', '085204852569', 'Telkomsel', 'ngagel kebonsari II / 1 Surabaya', 'Jawa Timur', '35', 'Surabaya', '78', 'Wonokromo', '110', 'Ngagelrejo', '004', 0, 0, 21, '', '0000-00-00 00:00:00'),
('60965d3d-0cf9-4cf8-a', '3578050002004A', '001', 'S002', 'https://goo.gl/maps/8pJGano8ooqXtDP28', '001', 'Muhammad Basorudin, S.Tr.Stat.', '08990088553', 'Indosat', 'Dusun Rame RT023 RW 011 Kab. Sidoarjo', 'Jawa Timur', '35', 'Sidoarjo', '15', 'Wonoayu', '090', 'Pilang', '005', 0, 0, 21, '', '0000-00-00 00:00:00'),
('609a0875-787f-8363-a', '3578050002002A', '001', 'S002', 'https://goo.gl/maps/WhQ2JVbAs9Y5VLS38', '001', 'Megasari Mamita, S.T, M.M.', '081335861982', 'Telkomsel', 'Kutisari Indah Utara V/ 59 Surabaya', 'Jawa Timur', '35', 'Surabaya', '78', 'Tenggilis Mejoyo', '050', 'Kutisari', '001', 0, 0, 21, '', '0000-00-00 00:00:00'),
('61575125-ab94-419e-9', '3578050002004A', '001', 'S005', 'https://goo.gl/maps/aoM15oEs3Yb2ocUg8', '001', 'R. Rino Yunarno, S.E', '082198109535', 'Telkomsel', 'Perumahan Puri Indah X-4 Sidoarjo', 'Jawa Timur', '35', 'Sidoarjo', '15', 'Sidoarjo', '110', 'Suko', '002', 0, 0, 21, '', '0000-00-00 00:00:00'),
('623a0823-298f-9286-c', '3578050002010A', '001', 'S002', 'https://goo.gl/maps/areavYfvo2LLV9n79', '001', 'Icha Merisa Anie Prananita, A.Md., S.M.', '082187535627', 'Telkomsel', 'Jl. Banyu Urip Kidul 2 Molin 1 No 8 Kota Surabaya', 'Jawa Timur', '35', 'Surabaya', '78', 'Sawahan', '170', 'Banyu Urip', '003', 0, 0, 21, '', '0000-00-00 00:00:00'),
('636ae913-a226-4533-b', '3578050002007A', '001', 'S008', 'https://goo.gl/maps/AVcWHeWAn3eiteY28', '001', 'Ir. Nurul Andriana', '081330215878', 'Telkomsel', 'Jl Gajah Mada gang Elang no 32 Bandar Lampung', 'Lampung', '18', 'Bandar Lampung', '71', 'Tanjungkarang Timur', '040', 'Tanjung Agung', '003', 0, 0, 21, '', '0000-00-00 00:00:00'),
('667a0862-528f-8408-a', '3578050002006A', '001', 'S006', 'https://goo.gl/maps/PEFaxU1WBjVHEfFM8', '001', 'Sevtie Marthalena, SST', '081278250843', 'Telkomsel', 'Jalan Durian III/E-457 Kab Sidoarjo', 'Jawa Timur', '35', 'Sidoarjo', '15', 'Waru', '140', 'Tambak Sumur', '010', 0, 0, 21, '', '0000-00-00 00:00:00'),
('688a5fa5-0632-46fc-b', '3578050002007A', '001', 'S004', 'https://goo.gl/maps/xEB1X9RDATY6vmyP6', '001', 'Kiki Ferdiana, SST, M.Si', '081349106997', 'Telkomsel', 'Jl. Raya Ponorogo No. 564 Kab Madiun', 'Jawa Timur', '35', 'Madiun', '19', 'Geger', '020', 'Sumberejo', '007', 0, 0, 21, '', '0000-00-00 00:00:00'),
('6f62bcff-df21-49fb-b', '3578050002007A', '001', 'S002', 'https://goo.gl/maps/paSX78zokU3h2GMM8', '001', 'Ahmad Junaedi, S.Si, M.M.', '081333154610', 'Telkomsel', 'Jl Wachid Hasyim RT 04 RW 03, Desa Belung Kecamatan Poncokusumo Kabupaten Malang', 'Jawa Timur', '35', 'Malang', '07', 'Poncokusumo', '100', 'Belung', '012', 0, 0, 21, '', '0000-00-00 00:00:00'),
('7025800b-b081-47fa-8', '3578050002011A', '001', 'S008', 'https://goo.gl/maps/sSG1RRH5TAQWKKYs8', '001', 'Boby Eko Heru Mulyadi, SST, M.Si', '081234009780', 'Telkomsel', 'Perumahan Gria Banjar Asri Blok F No.8 Jl. Banjarwaru IV Kota Madiun', 'Jawa Timur', '35', 'Madiun', '77', 'Taman', '020', 'Banjarejo', '004', 0, 0, 21, '', '0000-00-00 00:00:00'),
('7147251f-2305-4e33-a', '3578050002011A', '001', 'S001', 'https://goo.gl/maps/FKvEH5NQuNze1uF57', '001', 'Rika Muji Astuti, SST', '082190796337', 'Telkomsel', 'Jl Raya Klampok Krajan Gg Mundu No 2 Kab. Malang', 'Jawa Timur', '35', 'Malang', '07', 'Singosari', '280', 'Klampok', '012', 0, 0, 21, '', '0000-00-00 00:00:00'),
('715a0793-672f-3321-d', '3578050002007A', '001', 'S009', 'https://goo.gl/maps/pbWbsjghhUdQRZfC9', '001', 'Irien Kamaratih Arsiani', '085755424322/082245924282', 'Indosat/Telkomsel', 'Griyo Pabean I Jl Melati Kav No 9, Pabean, Sedati, Sidoarjo', 'Jawa Timur', '35', 'Sidoarjo', '15', 'Sedati', '130', 'Pabean', '012', 0, 0, 21, '', '0000-00-00 00:00:00'),
('741a0677-889f-1373-c', '3578050002001A', '001', 'S007', 'https://goo.gl/maps/8qxR1NM4kxZHeKxE7', '001', 'Herlina Prasetyowati Sambodo, SST.,M.Si', '08158850231', 'Indosat', 'Komplek Citra Medayu Residence B2 Jalan Medokan Sawah Baru Kota Surabaya', 'Jawa Timur', '35', 'Surabaya', '78', 'Rungkut', '070', 'Medokan Ayu', '002', 0, 0, 21, '', '0000-00-00 00:00:00'),
('752a0266-566f-8727-d', '3578030003009B', '001', 'S008', 'https://goo.gl/maps/FaibNCVbfi3bWmH77', '001', 'Achmad Riyadi', '08999962004', 'Indosat', 'Jl Ketintang Baru III/42 Surabaya.', 'Jawa Timur', '35', 'Surabaya', '78', 'Gayungan', '030', 'Ketintang', '004', 0, 0, 21, '', '0000-00-00 00:00:00'),
('758a0229-241f-5406-c', '3578050002001A', '001', 'S007', 'https://goo.gl/maps/36yo6quRHSjhvXpc8', '001', 'Dyah Sujiati, S.Si', '081216394165', 'Telkomsel', 'Griya Pesona Asri Blok G No-14 Kota Surabaya', 'Jawa Timur', '35', 'Surabaya', '78', 'Rungkut', '070', 'Medokan Ayu', '002', 0, 0, 21, '', '0000-00-00 00:00:00'),
('759a0152-790f-6374-c', '3578030003010B', '001', 'S002', 'https://goo.gl/maps/TqJgWiUoVJuKbnZJA', '001', 'Risti Sagita Rahmayanti', '082335422461', 'Telkomsel', 'Jln Simomulyo Baru 4A/09 Surabaya', 'Jawa Timur', '35', 'Surabaya', '78', 'Sukomanunggal', '160', 'Simomulyo Baru', '006', 0, 0, 21, '', '2023-01-29 22:52:37'),
('762a0204-546f-4201-b', '3578050002002A', '001', 'S010', 'https://goo.gl/maps/zUuX4U17Za7kcpgDA', '001', 'Fajar Choirul Anwar, SST', '082312315466', 'Telkomsel', 'Jl Kutisari Selatan II nomor 23 Kota Surabaya', 'Jawa Timur', '35', 'Surabaya', '78', 'Tenggilis Mejoyo', '050', 'Kutisari', '001', 0, 0, 21, '', '0000-00-00 00:00:00'),
('767a0955-621f-8432-a', '3578050002002A', '001', 'S002', 'https://goo.gl/maps/CaRpNoWb6mJcK7fj7', '001', 'Arum Widyastuti, A.P.Kb.N.', '085749254196', 'Indosat', 'jalan Kutisari 4/5 Kota Surabaya', 'Jawa Timur', '35', 'Surabaya', '78', 'Tenggilis Mejoyo', '050', 'Kutisari', '001', 0, 0, 21, '', '0000-00-00 00:00:00'),
('771a0481-421f-8584-b', '3578050002006A', '001', 'S010', 'https://goo.gl/maps/Rz7sgrTqV9MWc2uh8', '001', 'Achmad Aziz Effendy, SST', '082242945166', 'Telkomsel', 'Jl. Yos Sudarso BX.28 Wisma Tropodo Kab.Sidoarjo', 'Jawa Timur', '35', 'Sidoarjo', '15', 'Waru', '140', 'Tropodo', '006', 0, 0, 21, '', '0000-00-00 00:00:00'),
('781a0963-775f-7947-a', '3578050002007A', '001', 'S002', 'https://goo.gl/maps/fHpXRK2zcDqs7XBB7', '001', 'Citra Kusumaningtyas, S.Si', '081357266293', 'Telkomsel', 'Gate Garden Juanda 1 No 9 Kab.Sidoarjo', 'Jawa Timur', '35', 'Sidoarjo', '15', 'Sedati', '130', 'Semampir', '013', 0, 0, 21, '', '0000-00-00 00:00:00'),
('788a0523-927f-6372-c', '3578050002009A', '001', 'S009', 'https://goo.gl/maps/sGVrWzUxEUA345KN6', '001', 'Muslikin', '085852991008', 'Indosat', 'Wonocolo Gang Buntu 11B Kota Surabaya', 'Jawa Timur', '35', 'Surabaya', '78', 'Wonocolo', '040', 'Jemur Wonosari', '002', 0, 0, 21, '', '0000-00-00 00:00:00'),
('792a0796-625f-2028-d', '3578050002003A', '001', 'S012', 'https://goo.gl/maps/sJBYoL3wd9dgTbDp7', '001', 'Ai Nuraeni, SST. M.Si', '082329720055/081703818792', 'Telkomsel/XL', 'RUNGKUT PERMAI 10 / G - 02 Kota Surabaya', 'Jawa Timur', '35', 'Surabaya', '78', 'Gunung Anyar', '060', 'Rungkut Tengah', '002', 0, 0, 21, '', '0000-00-00 00:00:00'),
('796a0385-919f-7831-c', '3578030003008B', '001', 'S012', 'https://goo.gl/maps/6rRPs3NP4dSXMquAA', '001', 'Iswanto', '081230198198', 'Telkomsel', 'Margorukun XII/4b, surabaya', 'Jawa Timur', '35', 'Surabaya', '78', 'Bubutan', '250', 'Gundih', '004', 0, 0, 21, '', '0000-00-00 00:00:00'),
('7e3b4c13-8665-4f31-9', '3578050002011A', '001', 'S005', 'https://goo.gl/maps/UHNnD76X2E7PLXMX9', '001', 'Tri Dewi Wahyuningsih, A.Md', '081280281170', 'Telkomsel', 'Komplek Dit Bekang TNI AD Jl. Sena IV No.6 Kab.Bogor', 'Jawa Timur', '35', 'Surabaya', '78', 'Cibinong', '210', 'Cirimekar', '011', 0, 0, 21, '', '2023-01-26 20:36:12'),
('815a0494-918f-1506-b', '3578030003010B', '001', 'S012', 'https://goo.gl/maps/YSu3v6DUfump6FNq6', '001', 'Fitri Kusumowardhani', '08113327979', 'Telkomsel', 'Jl. Pakis Sidorejo I no 11A, Surabaya', 'Jawa Timur', '35', 'Surabaya', '78', 'Sawahan', '170', 'Pakis', '001', 0, 0, 21, '', '0000-00-00 00:00:00'),
('816a0860-823f-2118-b', '3578050002002A', '001', 'S008', 'https://goo.gl/maps/XDfj98Um9YZRYrc66', '001', 'Argo Setianto, A.Md, S.M.', '085205707677', 'Telkomsel', 'Panjang Jiwo, Gang Masjid 32A Kota Surabaya', 'Jawa Timur', '35', 'Surabaya', '78', 'Tenggilis Mejoyo', '050', 'Panjang Jiwo', '005', 0, 0, 21, '', '0000-00-00 00:00:00'),
('817414fb-a2df-4418-9', '3578050002011A', '001', 'S010', 'https://goo.gl/maps/CCmzQyT76BdQVBWo9', '001', 'Bahrul Ulum, S.Si.', '085755537728', 'Indosat', 'Tawanganom Permai Blok Q No 13 Kab.Magetan', 'Jawa Timur', '35', 'Magetan', '20', 'Magetan', '060', 'Tawanganom', '022', 0, 0, 21, '', '0000-00-00 00:00:00'),
('827a0255-791f-3492-c', '3578030003009B', '001', 'S012', 'https://goo.gl/maps/65hSYnCQfq5wefj26', '001', 'Yusril Yuma Alfana', '081335077391', 'Telkomsel', 'Elveka V no 36', 'Jawa Timur', '35', 'Surabaya', '78', 'Jambangan', '020', 'Kebonsari', '002', 0, 0, 21, '', '0000-00-00 00:00:00'),
('834a0600-762f-4495-d', '3578030003008B', '001', 'S003', 'https://goo.gl/maps/pai5FfUgyMfoipWTA', '001', 'Alfian Rosyadi', '08113831896', 'Telkomsel', 'Jl. Kemayoran Baru No. 65A, Surabaya', 'Jawa Timur', '35', 'Surabaya', '78', 'Krembangan', '260', 'Kemayoran', '004', 0, 0, 21, '', '0000-00-00 00:00:00'),
('849a0616-346f-8991-b', '3578050002008A', '001', 'S007', 'https://goo.gl/maps/zUyUQPS7W8qoEDos6', '001', 'Putri Rachma Sari, A.Md.', '081330777209', 'Telkomsel', 'KARANG EMPAT II NO. 15 Kota Surabaya', 'Jawa Timur', '35', 'Surabaya', '78', 'Tambaksari', '200', 'Ploso', '003', 0, 0, 21, '', '0000-00-00 00:00:00'),
('84fadd82-ac0a-48f6-9', '3578050002011A', '001', 'S001', 'https://goo.gl/maps/FFbRvjGj8uQT39e9A', '001', 'Ajiwasesa Harumeka, SST, M.Stat', '082189221863', 'Telkomsel', 'Perumahan Greenleaf Residence 1 No C2, Sukun, Kota Malang', 'Jawa Timur', '35', 'Malang', '73', 'Sukun', '020', 'Bandungrejosari', '004', 0, 0, 21, '', '0000-00-00 00:00:00'),
('850a0732-318f-4559-b', '3578050002008A', '001', 'S002', 'https://goo.gl/maps/WZXGWnYAcvdU1b8k9', '001', 'Merina Andriati, A.Md', '081357243061', 'Telkomsel', 'Jl Wonorejo III/76 B Kota Surabaya', 'Jawa Timur', '35', 'Surabaya', '78', 'Tegalsari', '180', 'Wonorejo', '004', 0, 0, 21, '', '0000-00-00 00:00:00'),
('853a0211-117f-1052-b', '3578050002002A', '001', 'S009', 'https://goo.gl/maps/QMujbkLNDEKzrbxk8', '001', 'Nurcholis', '081357136200', 'Telkomsel', 'Kendangsari Gang VI-31 Kota Surabaya', 'Jawa Timur', '35', 'Surabaya', '78', 'Tenggilis Mejoyo', '050', 'Kendangsari', '010', 0, 0, 21, '', '0000-00-00 00:00:00'),
('873a0184-423f-6176-d', '3578030003008B', '001', 'S012', 'https://goo.gl/maps/zzqyQhGie2jMNj117', '001', 'Retno Larasati', '082141261037', 'Telkomsel', 'Jl Sutorejo No 61 Kec Mulyorejo Surabaya', 'Jawa Timur', '35', 'Surabaya', '78', 'Mulyorejo', '090', 'Dukuh Sutorejo', '005', 0, 0, 21, '', '0000-00-00 00:00:00'),
('87da81df-7e86-415b-b', '3578050002011A', '001', 'S012', 'https://goo.gl/maps/WrGpkuL3Mue1AnQT9', '001', 'Bambang Yulianto, A.Md', '081334354000', 'Telkomsel', 'Perum Dirgantara Permai Blok B2 Nomor 26 Kota Malang', 'Jawa Timur', '35', 'Malang', '73', 'Kedungkandang', '010', 'Sawojajar', '009', 0, 0, 21, '', '0000-00-00 00:00:00'),
('880a0663-413f-5202-a', '3578050002005A', '001', 'S007', 'https://goo.gl/maps/ZatzLQNdRGbuo76JA', '001', 'Arga Parama Yufinanda', '081340853520/085786957804', 'Telkomsel/Indosat', 'Perum Lotus Residence 1 Blok A10, Sukodono, Sidoarjo', 'Jawa Timur', '35', 'Sidoarjo', '15', 'Sukodono', '100', 'Suruh', '005', 0, 0, 21, '', '0000-00-00 00:00:00'),
('8855e8da-e3ab-404b-9', '3578050002008A', '001', 'S001', 'https://goo.gl/maps/2Ua33VbncyryhAYLA', '001', 'Drs. Hadi Santoso, MM', '081230087316', 'Telkomsel', 'Perum Tectona A8, Jln Ikan Gurame Kota Malang', 'Jawa Timur', '35', 'Malang', '73', 'Lowokwaru', '050', 'Tasikmadu', '010', 0, 0, 21, '', '0000-00-00 00:00:00'),
('907a0932-730f-2538-d', '3578030003005B', '001', 'S003', 'https://goo.gl/maps/QyJ1HdabHLyVcDtE9', '001', 'Soleh Afandi', '085104495051', 'Indosat', 'Perum Taman Sukoasri CC.66 Sidoarjo', 'Jawa Timur', '35', 'Sidoarjo', '15', 'Sukodono', '100', 'Suko', '016', 0, 0, 21, '', '0000-00-00 00:00:00'),
('914a0318-725f-6406-b', '3578050002004A', '001', 'S002', 'https://goo.gl/maps/vnrj5LYvoJ68q31EA', '001', 'Fitriana Zahroh, SST, M.E', '085213373393', 'Telkomsel', ' Grand Aloha Regency D7-17 Kab.Sidoarjo', 'Jawa Timur', '35', 'Sidoarjo', '15', 'Taman', '160', 'Wage', '016', 0, 0, 21, '', '0000-00-00 00:00:00'),
('917a0183-868f-9196-a', '3578030003002B', '001', 'S002', 'https://goo.gl/maps/huFuRA3W2gwDbARd6', '001', 'Bibitha Maulana Zanky Dausat', '082140805577', 'Telkomsel', 'Jalan Kutisari Utara I 55A Tenggilis Mejoyo', 'Jawa Timur', '35', 'Surabaya', '78', 'Tenggilis Mejoyo', '050', 'Kutisari', '001', 0, 0, 21, '', '0000-00-00 00:00:00'),
('920ec453-7ddb-43e7-8', '3578050002008A', '001', 'S002', 'https://goo.gl/maps/zwVXF1aCoNhFpnP89', '001', 'Drs. Sunaryo, M.Si', '081230248717', 'Telkomsel', 'Jl. Janti Barat Blok C Dalam IV/ No.12 Kota Malang', 'Jawa Timur', '35', 'Malang', '73', 'Sukun', '020', 'Bandungrejosari', '004', 0, 0, 21, '', '0000-00-00 00:00:00'),
('922a0573-798f-8793-b', '3578050002010A', '001', 'S005', 'https://goo.gl/maps/ftise5EKJuzXMqSA8', '001', 'Muhammad Dwi Prasetiyo, S.Si, M.SE', '082187087015', 'Telkomsel', 'DK. KRAMAT Kota Surabaya', 'Jawa Timur', '35', 'Surabaya', '78', 'Wiyung', '130', 'Jajar Tunggal', '004', 0, 0, 21, '', '0000-00-00 00:00:00'),
('935a0103-661f-5149-b', '3578050002006A', '001', 'S009', 'https://goo.gl/maps/uwN1cxF5eng4QgN5A', '001', 'Ummatin Yulinda Sangaji, S.Si.', '081328197940', 'Telkomsel', 'Delta Puspa no.26 Perumahan Delta Sari Baru Sidoarjo Jawa Timur', 'Jawa Timur', '35', 'Sidoarjo', '15', 'Waru', '140', 'Ngingas', '005', 0, 0, 21, '', '0000-00-00 00:00:00'),
('935bebc0-3408-4b10-a', '3578050002008A', '001', 'S007', 'https://goo.gl/maps/uhL6nfF8E2aB2n1P7', '001', 'Drs. Adenan, MM', '082139900345', 'Telkomsel', 'Jl. Made Mulyo V No.18, Perumnas Made-lamongan 62218', 'Jawa Timur', '35', 'Lamongan', '24', 'Lamongan', '130', 'Made', '014', 0, 0, 21, '', '0000-00-00 00:00:00'),
('946a0989-951f-5973-a', '3578050002006A', '001', 'S012', 'https://goo.gl/maps/y7ju7rZJw8zhg4zZA', '001', 'Peni Meivita', '081331206006/082144356201', 'Telkomsel', 'wisma tropodo jl Flamboyan Blok AH-18 waru sidoarjo', 'Jawa Timur', '35', 'Sidoarjo', '15', 'Waru', '140', 'Tropodo', '006', 0, 0, 21, '', '0000-00-00 00:00:00'),
('951a0899-424f-9936-a', '3578030003009B', '001', 'S012', 'https://goo.gl/maps/ep32FGAiiDDKqG7JA', '001', 'Ali Subagiyo', '08123213167', 'Telkomsel', 'Jl. Jambangan Kebon Agung Asri No 10 Surabaya', 'Jawa Timur', '35', 'Surabaya', '78', 'Jambangan', '020', 'Jambangan', '020', 0, 0, 21, '', '0000-00-00 00:00:00'),
('966a0614-974f-3725-d', '3578050002010A', '001', 'S003', 'https://goo.gl/maps/WhoGz3RLpyhMYeaW8', '001', 'Maryono, SE', '081347001124', 'Telkomsel', 'Perum pondok maritim indah blok e. No.28 Kota Surabaya', 'Jawa Timur', '35', 'Surabaya', '78', 'Wiyung', '130', 'Balas Klumprik', '001', 0, 0, 21, '', '0000-00-00 00:00:00'),
('968a0837-157f-1572-c', '3578030003001B', '001', 'S007', 'https://goo.gl/maps/sQAp5XjReKhviStw9', '001', 'Diah Asri N', '085645115494', 'Indosat', 'Medokan sawah 131A', 'Jawa Timur', '35', 'Surabaya', '78', 'Rungkut', '070', 'Medokan Ayu', '002', 0, 0, 21, '', '0000-00-00 00:00:00'),
('981a0917-539f-5476-a', '3578030003007B', '001', 'S008', 'https://goo.gl/maps/Da2KJivErd6sL7WHA', '001', 'Lusy Rochmayanti', '085648362226', 'Indosat', 'GPG M3/11, Sidoarjo', 'Jawa Timur', '35', 'Sidoarjo', '15', 'Gedangan', '150', 'Keboananom', '008', 0, 0, 21, '', '0000-00-00 00:00:00'),
('987a0566-619f-2365-c', '3578050002002A', '001', 'S005', 'https://goo.gl/maps/2ZjACYj8E3zwm23a7', '001', 'Soleh', '081235195860', 'Telkomsel', 'Jl. Kutisari Selatan II/30 Kota Surabaya', 'Jawa Timur', '35', 'Surabaya', '78', 'Tenggilis Mejoyo', '050', 'Kutisari', '001', 0, 0, 21, '', '0000-00-00 00:00:00'),
('998a0913-550f-8093-a', '3578050002007A', '001', 'S009', 'https://goo.gl/maps/t8SbGTfTYKMf3fjD8', '001', 'Rahayu Rachmawati, S.ST, M.Si', '082233209502', 'Telkomsel', 'Perum Gate Garden Juanda I no 21 Kab. Sidoarjo', 'Jawa Timur', '35', 'Sidoarjo', '15', 'Sedati', '130', 'Semampir', '013', 0, 0, 21, '', '0000-00-00 00:00:00'),
('9b39cc70-1b9b-4b23-9', '3578050002012A', '001', 'S009', 'https://goo.gl/maps/a4jBCrnCjwkkSb8j8', '001', 'Pramana Yhoga Chandra Kusuma, SST, M.T', '085298766565', 'Telkomsel', 'Perum Jaya Abadi Blok i No. 6, Jl. Gub. Suryo, Jombang', 'Jawa Timur', '35', 'Jombang', '17', 'Jombang', '130', 'Jombatan', '004', 0, 0, 21, '', '0000-00-00 00:00:00'),
('9b39cc70-1b9b-4b23-a', '3578050002022A', '001', 'S009', 'https://goo.gl/maps/a4jBCrnCjwkkSb8j8', '001', 'Pramana Yhoga Chandra Kusuma, SST, M.T', '085298766565', 'Telkomsel', 'Perum Jaya Abadi Blok i No. 6, Jl. Gub. Suryo, Jombang', 'Jawa Timur', '35', 'Jombang', '17', 'Jombang', '130', 'Jombatan', '004', 0, 0, 21, '', '0000-00-00 00:00:00'),
('a1694524-0f09-4f21-8', '3578050002012A', '001', 'S007', 'https://goo.gl/maps/tYVZ2ihewq79dUfFA', '001', 'Prayudho Bagus Jatmiko, SST, M.Si', '085236013276', 'Telkomsel', 'Perumahan Panji Permai Blok KK-08 Situbondo', 'Jawa Timur', '35', 'Situbondo', '12', 'Panji', '120', 'Mimbaan', '008', 0, 0, 21, '', '0000-00-00 00:00:00'),
('aea44466-23ac-4793-a', '3578050002012A', '001', 'S001', 'https://goo.gl/maps/GW6tTTjzCYzSssqo6', '001', 'Evy Trisusanti, S.Si.,M.T., M.Sc', '081334400910', 'Telkomsel', 'Jalan Narotama Barat K161 RT004 RW09 Kelurahan Kesatrian Kecamatan Blimbing Kota Malang', 'Jawa Timur', '35', 'Malang', '73', 'Blimbing', '040', 'Kesatrian', '003', 0, 0, 21, '', '0000-00-00 00:00:00'),
('aef17422-5fc7-4a9f-b', '3578050002012A', '001', 'S002', 'https://goo.gl/maps/gbtfvGBspYhNJAMK7', '001', 'Abdus Salam, S.ST, M.E', '081332781961', 'Telkomsel', 'Toko Murah Jaya Jl. Sayyid Sulaiman Dusun Rejoslamet Kab.Jombang', 'Jawa Timur', '35', 'Jombang', '17', 'Mojoagung', '090', 'Mancilan', '013', 0, 0, 21, '', '0000-00-00 00:00:00'),
('bbdb8bcf-cc43-44ec-b', '3578050002012A', '001', 'S004', 'https://goo.gl/maps/RuLWxX9oZCFkPCpy6', '001', 'Warisna Endah Fitrianti, SST', '082137186613', 'Telkomsel', 'Jl. Borobudur Agung Barat II/16 Kota Malang', 'Jawa Timur', '35', 'Malang', '73', 'Lowokwaru', '050', 'Mojolangu', '008', 0, 0, 21, '', '0000-00-00 00:00:00'),
('beac9f2e-c9ca-4cf5-9', '3578050002010A', '001', 'S011', 'https://goo.gl/maps/Hxy4z8ydRfaLz3N87', '001', 'DIAN TRI KUSUMA HARDIYANTO', '085607229444/082231133622', 'Indosat/Telkomsel', 'Mojokerto', 'Jawa Timur', '35', 'Mojokerto', '16', '', '', '', '', 0, 0, 21, '', '0000-00-00 00:00:00'),
('c41c002c-4359-4f6b-a', '3578050002010A', '001', 'S007', 'https://goo.gl/maps/xis6zrgSbAkHiiF48', '001', 'LA ODE AHMAD ARAFAT', '081252842502/082247763270', 'Telkomsel', 'Jl.Raya Sambiroto, Ds.Mlaten, Kec.Puri, Kab.Mojokerto', 'Jawa Timur', '35', 'Mojokerto', '16', 'Puri', '110', 'Mlaten', '010', 0, 0, 21, '', '0000-00-00 00:00:00'),
('c5d7319a-a7ff-4752-a', '3578050002010A', '001', 'S005', 'https://goo.gl/maps/kcCpUdYxcuRXH2zE6', '001', 'Heri Soesanto, SE', '081330512456', 'Telkomsel', 'Tropodo I Barat RT 21 RW 02, Waru Sidoarjo dan Dusun Pesanan RT 01 RW 01 Desa Bicak, Kec. Trowulan Mojokerto', 'Jawa Timur', '35', 'Mojokerto', '16', 'Trowulan', '120', 'Bicak', '016', 0, 0, 21, '', '0000-00-00 00:00:00'),
('c9767110-285b-400c-8', '3578050002010A', '001', 'S007', 'https://goo.gl/maps/U9uDQvqnurkAP8se9', '001', 'Yogi Chandra Sasmita, S.Si.', '085645459289', 'Indosat', 'Sinoman 1 / 25 Kota Mojokerto', 'Jawa Timur', '35', 'Mojokerto', '76', 'Kranggan', '021', 'Miji', '004', 0, 0, 21, '', '0000-00-00 00:00:00'),
('ca20863e-48c2-4ef7-b', '3578050002010A', '001', 'S010', 'https://goo.gl/maps/FzQQAcJePkiF47Ne7', '001', 'Endro Cahyono, SST, MM', '085815151570', 'Indosat', 'Griya Permata Tembok Blok A No. 4 Pasuruan', 'Jawa Timur', '35', 'Pasuruan', '75', 'Purworejo', '020', 'Tembokrejo', '003', 0, 0, 21, '', '0000-00-00 00:00:00'),
('cb922d3e-fb48-41d4-a', '3578050002010A', '001', 'S002', 'https://goo.gl/maps/x9R8Luhn7UzL9rbA8', '001', 'Yuli Nugraheni, SE.M.Si', '081330010049', 'Telkomsel', 'Perumahan IStana Bestari Blok AB 10 Kota Pasuruan', 'Jawa Timur', '35', 'Pasuruan', '75', 'Panggungrejo', '031', 'Pekuncen', '002', 0, 0, 21, '', '0000-00-00 00:00:00'),
('cbb99b5b-653c-4db9-9', '3578050002010A', '001', 'S003', 'https://goo.gl/maps/fodKiCaCzja2Vqxp6', '001', 'Vidya Nurina Paramita, SST, M.Si', '08115013801', 'Telkomsel', 'Perum Leces Permai B04Rt 02 Rw 06 67273 Kab.Probolinggo', 'Jawa Timur', '35', 'Probolinggo', '13', 'Leces', '050', 'Leces', '004', 0, 0, 21, '', '0000-00-00 00:00:00'),
('df1539fb-39c9-4f62-9', '3578050002010A', '001', 'S005', 'https://goo.gl/maps/PkvYL8U5yC8tTfE6A', '001', 'Marfuah Apriyani, SST', '082137370658', 'Telkomsel', 'Jl. Elang Gg. Satria No. 61 Samarinda', 'Kalimantan Timur', '64', 'Samarinda', '72', 'Sungai Pinang', '061', 'Sungai Pinang Dalam', '003', 0, 0, 21, '', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bloksensus`
--
ALTER TABLE `bloksensus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `datast`
--
ALTER TABLE `datast`
  ADD PRIMARY KEY (`kodeRuta`);

--
-- Indexes for table `data_konven`
--
ALTER TABLE `data_konven`
  ADD PRIMARY KEY (`kodebs`);

--
-- Indexes for table `desa`
--
ALTER TABLE `desa`
  ADD PRIMARY KEY (`id_desa`),
  ADD KEY `kecno` (`kecno`);

--
-- Indexes for table `kabupaten`
--
ALTER TABLE `kabupaten`
  ADD PRIMARY KEY (`kabno`);

--
-- Indexes for table `kecamatan`
--
ALTER TABLE `kecamatan`
  ADD PRIMARY KEY (`kabno`,`kecno`) USING BTREE;

--
-- Indexes for table `notif`
--
ALTER TABLE `notif`
  ADD PRIMARY KEY (`_id`);

--
-- Indexes for table `rumahtangga`
--
ALTER TABLE `rumahtangga`
  ADD PRIMARY KEY (`kodeRuta`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `notif`
--
ALTER TABLE `notif`
  MODIFY `_id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
